package main

import (
	"alaric_cms/app"
	"flag"
	"runtime"
)

func main() {
	// maximize CPU usage for maximum performance
	runtime.GOMAXPROCS(runtime.NumCPU())

	var config *string = flag.String("config", "app", "Config file app")
	var port *string = flag.String("port", "", "Port app")
	var env *string = flag.String("env", "development", "Environment app")
	var seed *bool = flag.Bool("seed", false, "Init data")
	var verbose *bool = flag.Bool("verbose", false, "Verbose mode log")
	flag.Parse()

	app := &app.App{}
	app.ConfigFile = config
	app.Port = port
	app.Env = env
	app.Seed = seed
	app.Verbose = verbose

	app.Initialize()
	if !*seed {
		app.Run()
	}
}
