package services

import (
	"crypto/tls"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gopkg.in/gomail.v2"
)

func CreateJWT(id string, issuer string, secret string, expire int) (string, error) {
	// Create the token
	token := jwt.New(jwt.GetSigningMethod("HS256"))
	// Set some claims
	token.Claims = jwt.MapClaims{
		"jti": id,
		"iss": issuer,
		"exp": time.Now().Add(time.Minute * time.Duration(expire)).Unix(),
	}
	// Sign and get the complete encoded token as a string
	tokenString, err := token.SignedString([]byte(secret))

	return tokenString, err
}

func SendMail(params map[string]interface{}) (bool, error) {
	smtp, ok := params["smtp"].(string)
	if !ok {
		smtp = ""
	}
	port, ok := params["port"].(int)
	if !ok {
		port = 0
	}
	from, ok := params["from"].(string)
	if !ok {
		from = ""
	}
	to, ok := params["to"].([]string)
	if !ok {
		to = []string{}
	}
	cc, ok := params["cc"].([]string)
	if !ok {
		cc = []string{}
	}
	subject, ok := params["subject"].(string)
	if !ok {
		subject = ""
	}
	message, ok := params["message"].(string)
	if !ok {
		message = ""
	}
	// "text/plain", "text/html"
	format, ok := params["format"].(string)
	if !ok {
		format = "text/plain"
	}

	m := gomail.NewMessage()
	m.SetHeader("From", from)
	m.SetHeader("To", to...)
	m.SetHeader("Cc", cc...)
	m.SetHeader("Subject", subject)
	m.SetBody(format, message)

	d := gomail.Dialer{Host: smtp, Port: port}
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	if err := d.DialAndSend(m); err != nil {
		return false, err
	}

	return true, nil
}
