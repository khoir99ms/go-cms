package services

import (
	"context"
	"fmt"

	"github.com/mongodb/mongo-go-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func OpenMongoSession(host string, port string, user string, password string, db string) (*mongo.Client, *mongo.Database, error) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// mongodb://<dbuser>:<dbpassword>@ds145895.mlab.com:45895/alaric_cms
	var auth string
	if user != "" {
		auth = fmt.Sprintf("%s:%s@", user, password)
	}

	uri := fmt.Sprintf(`mongodb://%s%s:%s/%s`, auth, host, port, db)
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		return nil, nil, fmt.Errorf("couldn't connect to mongo: %v", err)
	}
	err = client.Connect(ctx)
	if err != nil {
		return nil, nil, fmt.Errorf("mongo client couldn't connect with background context: %v", err)
	}
	DB := client.Database(db)

	return client, DB, nil
}
