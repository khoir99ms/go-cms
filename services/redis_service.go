package services

import (
	"fmt"

	"github.com/go-redis/redis"
)

func OpenRedisSession(host string, port string, password string, db int) (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", host, port),
		Password: password, // no password set
		DB:       db,       // use default DB
	})

	_, err := client.Ping().Result()
	if err != nil {
		return nil, fmt.Errorf("couldn't connect to redis: %v", err)
	}

	return client, nil
}
