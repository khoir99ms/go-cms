$(function () {
    "use strict";

    var corporateID = "";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    $.fn.getCorporateID = function () {
        if (pageSession.code == "COR") {
            var $self = $(this);
            
            var $request = $.ajax({
                url: '/setup/corporate/data' // wherever your data is actually coming from
            });
            
            $request.then(function (json) {
                if (json.success) {
                    corporateID = json.data.item.property_id;
                    $self.trigger({
                        type: "loaded.corporate-id",
                        id: corporateID
                    });
                }
            });
        }
    }
    
    $(document).getCorporateID();

    $('.select2-no-search').select2({
        minimumResultsForSearch: Infinity,
        placeholder: 'Choose'
    });

    $('.az-toggle.toggle-user-status').on('click', function () {
        $(this).toggleClass('on');
        
        var $status = $(this).closest("form").find("input[name=status]");
        if($(this).hasClass('on')) {
            $status.val("active");
        } else {
            $status.val("inactive");
        }
    });

    var $oTableUser = $("#datatable-users").DataTable({
        responsive: true,
        language: {
            searchPlaceholder: "Search ...",
            sSearch: "",
            lengthMenu: "_MENU_ items/page",
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        processing: true,
        serverSide: true, 
        deferRender: true,
        ajax: {
            url: "/users/load",
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);

                json.recordsTotal = json.data.total;
                json.recordsFiltered = json.data.total;
                // json.recordsFiltered = json.data.item ? json.data.item.length : 0;
                json.data = json.data.item ? json.data.item : [];
                
                return JSON.stringify(json); // return JSON string
            }
        },
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: "user_firstname user_lastname",
                render: function (data, type, row, meta) {
                    return row.user_firstname + ' ' + row.user_lastname;
                }
            },
            { data: "user_login" },
            { data: "user_email" },
            { 
                data: "brand.brand_name",
                render: function (data, type, row, meta) {
                    var brand = "";
                    if (row.brand) {
                        brand = row.brand.brand_name ? row.brand.brand_name : "";
                    }
                    return brand;
                }
            },
            { 
                data: "property.property_name",
                render: function (data, type, row, meta) {
                    var property = "";
                    if (row.property) {
                        property = row.property.property_name ? row.property.property_name : "";
                    }
                    return property;
                }
            },
            { data: "user_role" },
            { 
                data: "user_status",
                render: function (data, type, row, meta) {
                    var badge = row.user_status == "active" ? "badge-success" : "badge-danger";
                    return '<span class="badge ' + badge + '">' + row.user_status.ucwords()+'</span>';
                }
            },
            { data: "user_registered" },
            { 
                data: "actions", 
                className: "",
                searchable: false, 
                orderable: false,
                render: function (data, type, row, meta) {
                    var levels = ["brand", "hotel"];
                    var action = '<a href="/users/update/' + row.user_id + '" class="badge badge-secondary" data-trigger="update">Edit</a>';
                    if (!levels.includes(pageSession.level)) {
                        action += ' <a href="/users/delete/' + row.user_id + '" class="badge badge-danger" data-trigger="delete">Delete</a>';
                    }
                    return action;
                }
            }
        ],
        order: [[1, "asc"]]
    });

    $("div.dataTables_filter input").unbind().keyup(function (e) {
        if (e.keyCode == 13) $oTableUser.search(this.value).draw();
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    $(document).on("click", ".btn-refresh-table", function (e) {
        $oTableUser.draw();
    });

    $(document).on("submit", ".form-user", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type='submit']");

        var url = $form.attr('action');
        var data = $form.serializeObject();
        var btnText = $btnSave.html();

        if (pageSession.code == "COR" && (data.property_id === undefined || data.property_id == "")) {
            data["property_id"] = corporateID;
        }

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: "post",
            dataType: "json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");

                $oTableUser.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
            $form.closest(".modal").modal("hide");
        });
    });

    $(document).on("click", "[data-trigger=update]", function(e) {
        e.preventDefault();

        var url = $(this).attr('href');
        var $form = $(".form-user.update");
        var $modal = $("#modal-form-user-update");
        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                var formData = jqXHR.data.item;

                $form.find('input[name=user_id]').val(formData.user_id);
                $form.find('input[name=firstname]').val(formData.user_firstname);
                $form.find('input[name=lastname]').val(formData.user_lastname);
                $form.find('input[name=username]').val(formData.user_login);
                $form.find('input[name=email]').val(formData.user_email);
                $form.find('select[name=role]').val(formData.user_role).change();

                var $brand = $form.find("select[name=brand_id]")
                // Set the value, creating a new option if necessary
                if ($brand.find("option[value='" + formData.brand.brand_id + "']").length) {
                    $brand.val(formData.brand.brand_id).trigger('change');
                } else {
                    // Create a DOM Option and pre-select by default
                    var newOption = new Option(formData.brand.brand_name, formData.brand.brand_id, true, true);
                    // Append it to the select
                    $brand.append(newOption).trigger('change');
                }

                var $property = $form.find("select[name=property_id]")
                // Set the value, creating a new option if necessary
                if ($property.find("option[value='" + formData.property.property_id + "']").length) {
                    $property.val(formData.property.property_id).trigger('change');
                } else {
                    // Create a DOM Option and pre-select by default
                    var newOption = new Option(formData.property.property_name, formData.property.property_id, true, true);
                    // Append it to the select
                    $property.append(newOption).trigger('change');
                } 

                $form.find('input[name=status]').val(formData.user_status);
                if (formData.user_status == "active") {
                    $form.find('.az-toggle.toggle-user-status').addClass("on");
                } else {
                    $form.find('.az-toggle.toggle-user-status').removeClass("on");
                }
                
                $modal.modal("show");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete]", function (e) {
        e.preventDefault();
        
        var user = $(this).closest("tr").find("td:eq(1)").text();
        var accept = confirm("Are you sure to delete " + user + " ?");
        if (!accept) return;
        
        var $btn = $(this);
        var url = $(this).attr('href');
        var btnText = $btn.html();
        
        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");
                $oTableUser.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
                
                $btn.html(btnText);
            }
        });
    });
    
    $(document).on("click", ".change-password", function (e) {
        var $modal = $("#modal-form-password");
        var userId = $(this).closest("form").find("input[name=user_id]").val();
        
        $modal.find("input[name=user_id]").val(userId);
        $modal.modal("show");
    });

    $(document).on("change", "select[name=role]", function (e) {
        var role = $(this).val();
        var $brand = $(this).closest("form").find("select[name=brand_id]");
        var $property = $(this).closest("form").find("select[name=property_id]");
        
        if (role == 1) {
            $brand.prop('required', false);
            $brand.prop('disabled', true);
            $('.select2.brand').val("").trigger("change");
            
            $property.prop('required', false);
            $property.prop('disabled', true);
            $('.select2.property').val("").trigger("change");
        } else if (role == 2) {
            $brand.prop('required', true);
            $brand.prop('disabled', false);

            $property.prop('required', false);
            $property.prop('disabled', true);
            $('.select2.property').val("").trigger("change");
        } else {
            $brand.prop('required', true);
            $brand.prop('disabled', false);
            
            $property.prop('required', true);
            $property.prop('disabled', false);
        }
    });

    $('.select2.brand').select2({
        placeholder: 'Choose',
        searchInputPlaceholder: 'Search',
        allowClear: true,
        ajax: {
            url: "/utils/brands",
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                var options = [];
                $.each(data.data.item, function (i, v) {
                    options.push({ id: v.brand_id, text: v.brand_name })
                });

                return {
                    results: options
                };
            }
        },
    });

    $('.select2.property').select2({
        placeholder: 'Choose'
    });

    $(document).on("select2:select", ".select2.brand", function (e) {
        var brandId = $(this).val();
        
        $('.select2.property').select2({
            placeholder: 'Choose',
            searchInputPlaceholder: 'Search',
            allowClear: true,
            ajax: {
                url: "/utils/property?brandId=" + brandId,
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    var options = [];
                    $.each(data.data.item, function (i, v) {
                        options.push({ id: v.property_id, text: v.property_name })
                    });
                    
                    return {
                        results: options
                    };
                }
            },
        });
    });

    $(document).on("shown.bs.modal", "#modal-form-user-update", function (e) {
        var $modal = $(this);

        var levels = ["brand", "hotel"];
        if (levels.includes(pageSession.level)) {
            $modal.find('.row.role').addClass("d-none");
        } else {
            $modal.find('.row.role').removeClass("d-none");
        }
    });
    
});