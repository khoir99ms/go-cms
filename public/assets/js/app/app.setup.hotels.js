$(function () {
    "use strict";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    var allLang = [];
    var primaryLang = "";
    var primaryLangJson = {};
    var allBrand = [];
    var allCity = [];
    var facilitiesGroupedLang = {};
    
    var arrangeTab = function (tagline, description, customField, customLink) {
        var tabData = { tagline: {}, description: {}, custom_field: {}, custom_link: {} };

        if (typeof tagline == "string") {
            tabData["tagline"][primaryLang] = tagline;
        } else {
            if ($.isEmptyObject(tagline)) {
                tabData["tagline"][primaryLang] = "";
            } else {
                tabData["tagline"] = tagline;
            }
        }
        if (typeof description == "string") {
            tabData["description"][primaryLang] = description;
        } else {
            if ($.isEmptyObject(description)) {
                tabData["description"][primaryLang] = "";
            } else {
                tabData["description"] = description;
            }
        }
        if (typeof customField == "string") {
            tabData["custom_field"][primaryLang] = customField;
        } else {
            if ($.isEmptyObject(customField)) {
                tabData["custom_field"][primaryLang] = "";
            } else {
                tabData["custom_field"] = customField;
            }
        }
        if (typeof customLink == "string") {
            tabData["custom_link"][primaryLang] = customLink;
        } else {
            if ($.isEmptyObject(customLink)) {
                tabData["custom_link"][primaryLang] = "";
            } else {
                tabData["custom_link"] = customLink;
            }
        }

        var primary = {}, others = {};
        $.each(tabData, function (key, items) {
            $.each(items, function (lang, item) {
                if (lang == primaryLang) {
                    if (primary["lang"] === undefined) {
                        primary["lang"] = lang;
                    }
                    primary[key] = item;
                } else {
                    if (others[lang] === undefined) {
                        others[lang] = {};
                    }
                    if (others[lang]["lang"] === undefined) {
                        others[lang]["lang"] = lang;
                    }
                    others[lang][key] = item;
                }
            });
        });

        var results = [primary];
        $.each(others, function (i, other) {
            results.push(other);
        });

        return results;
    }

    var serializeFacilitiesTree = function (json) {
        var tree = [];

        $.each(json, function (i, item) {
            var children = item.children;

            var text = '<a href="#" class="tx-gray-800" data-id="' + item.id + '" ' + (item.parent_id ? 'data-parent-id="' + item.parent_id + '"' : '') + '>';

            var $icon = $.parseHTML(item.icon);
            var isImg = $icon.length > 0 && $icon[0].nodeName.toLowerCase() == "img";
            if (isImg) {
                item.icon = $("<div>").append($(item.icon).attr({ "width": 20, "height": 20 })).html();
            }

            if (item.icon) {
                text += '<span class="mg-l-5 mg-r-10">' + item.icon + '</span>';
            }

            text += item.name;
            text += '</a>';

            var node = {
                text: text,
            }
            if (children && children.length > 0) {
                node["nodes"] = serializeFacilitiesTree(children);
            }
            tree.push(node);
        });

        return tree;
    }

    var getFacilitiesGroupedLang = function () {
        var $request = $.ajax({
            url: "/facility/tree/grouped-lang", // wherever your data is actually coming from
        });

        $request.then(function (json) {
            facilitiesGroupedLang = json.data ? json.data : [];
            $(document).trigger("loaded.facility");
        });

        $request.always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    var getBrand = function () {
        var $request = $.ajax({
            url: "/utils/brands",
        });

        $request.then(function (json) {
            $.each(json.data.item, function (i, v) {
                allBrand.push({ id: v.brand_id, code: v.brand_code, name: v.brand_name });
            });
            $(document).trigger("loaded.brand");
        });

        $request.always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    var getCity = function () {
        var $request = $.ajax({
            url: "/utils/cities",
        });

        $request.then(function (json) {
            $.each(json.data, function (i, v) {
                allCity.push({ id: v.id, code: v.code, name: v.name });
            });
            $(document).trigger("loaded.city");
        });

        $request.always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    $.fn.getAllLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data.item) {
                toastr["error"]("All lang isn't set.", "Failed");
            } else {
                allLang = json.data.item;
                $self.trigger("loaded.all-lang");
            }
        });
    }

    $.fn.getPrimaryLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang/primary' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data) {
                toastr["error"]("Primary lang isn't set.", "Failed");
            } else {
                primaryLang = json.data.code;
                primaryLangJson = json.data;
                $self.trigger({
                    type: "loaded.primary-lang",
                    lang: json.data.code
                });
            }
        });
    }

    $.fn.generateFacilitiesTreeView = function () {
        var $self = $(this);
        
        $self.each(function (i) {
            var $tree = $(this);
            var $container = $tree.closest(".form-group");
            var tree = serializeFacilitiesTree(facilitiesGroupedLang[primaryLang]);
            
            if (tree.length > 0) {
                $tree.treeview({
                    expandIcon: 'fas fa-caret-right',
                    collapseIcon: 'fas fa-caret-down',
                    checkedIcon: 'far fa-check-square',
                    uncheckedIcon: 'far fa-square',
                    showCheckbox: true,
                    highlightSelected: false,
                    data: tree,
                });
                $tree.checkFacilitiesTreeView();
                $container.removeClass("d-none");
            } else {
                $container.addClass("d-none");
            }
            
            $tree.trigger("loaded.treeview");
        });
    }

    $.fn.checkFacilitiesTreeView = function () {
        var $self = $(this);
        var items = $self.data("facilities");
        
        items = items ? items : [];
        $.each(items, function (i, facility) {
            var $item = $self.find(".list-group-item").find("a[data-id=" + facility + "]");
            var $node = $item.closest(".list-group-item");
            var $formGroup = $node.closest(".form-group");
            var nID = $node.data("nodeid");
            var notExistFacility = $formGroup.find('input[name=facilities][value=' + facility + ']').length == 0;
            
            if (notExistFacility) {
                $formGroup.append('<input type="hidden" name="facilities" value="' + facility + '">');
            }

            if ($item.length > 0) {
                $self.treeview('checkNode', [nID, { silent: true }]);
            }
        });
    }

    $.fn.generateBrandDropdown = function () {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            // Select2 has been initialized
            var options = [];
            $.each(allBrand, function (i, v) {
                options.push({ id: v.id, text: v.name })
            });

            $self.select2({
                placeholder: 'Brand',
                allowClear: true,
                minimumResultsForSearch: Infinity,
                data: options
            });

            var value = $self.data("value");
            if (value) {
                $self.val(value);
            }
            $self.trigger('change');
        }
    }

    $.fn.generateCityDropdown = function () {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            // Select2 has been initialized
            var options = [];
            $.each(allCity, function (i, v) {
                options.push({ id: v.code, text: v.name })
            });

            $self.select2({
                placeholder: 'City',
                allowClear: true,
                minimumResultsForSearch: Infinity,
                data: options
            });

            var value = $self.data("value");
            if (value) {
                $self.val(value);
            }
            $self.trigger('change');
        }
    }

    $.fn.generateTranslateToDropdown = function (exceptIDs) {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            exceptIDs = exceptIDs ? exceptIDs : [];

            // Select2 has been initialized
            var options = [];
            $.each(allLang, function (i, v) {
                if ($.inArray(v.code, exceptIDs) === -1) {
                    options.push({ id: v.code, text: v.name })
                }
            });

            if (options.length > 0) {
                $self.select2({
                    placeholder: 'Transalte To',
                    allowClear: true,
                    minimumResultsForSearch: Infinity,
                    data: options
                });
                $self.closest(".form-group").removeClass("d-none");
            } else {
                $self.closest(".form-group").addClass("d-none");
            }
        }
    }

    $.fn.destroySelectDropdown = function () {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("select2-hidden-accessible")) {
                $(this).select2('destroy');
                $(this).find("option").not(':first').remove();
                $(this).removeAttr("data-select2-id tabindex aria-hidden");
            }
        });
    }

    $.fn.destroyTranslateToDropdown = function () {
        var $self = $(this);

        $self.destroySelectDropdown();
        $self.closest(".form-group").addClass("d-none");
    }

    $.fn.generateTinyMce = function(options) {
        var $self = $(this);

        var defaults = {
            max_height: 800,
            placeholder: 'Type here ...',
            branding: false,
            // menubar: false,
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons autoresize',
            menubar: 'file edit view insert format tools table help',
            toolbar: 'preview code | undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | superscript subscript | outdent indent | table | numlist bullist | forecolor backcolor removeformat | hr pagebreak | charmap emoticons | fullscreen save print | insertfile image media template link anchor codesample | ltr rtl',
            image_caption: true,
            image_advtab: true,
            toolbar_sticky: true,
            toolbar_mode: 'sliding',
            importcss_append: true,
            allow_script_urls: true,
            relative_urls : false,
            remove_script_host : false,
            imagetools_toolbar: "imageoptions",
            content_css: ['/static/lib/tinymce/skins/content/default/content.min.css', '/static/lib/fontawesome-free/css/all.min.css'],
            quickbars_selection_toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | h1 h2 h3 blockquote | quicklink quicktable',
            quickbars_insert_toolbar: false,
            contextmenu: "link image table",
        };

        // Merge defaults and options, without modifying defaults
        var settings = $.extend({}, defaults, options ? options : {});

        $self.each(function () {
            $(this).tinymce(settings);
            $(this).addClass("has-tinymce");
        });
    }

    $.fn.destroyTinyMce = function() {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("has-tinymce")) {
                $(this).tinymce().destroy();
            }
        });
    }

    getBrand();
    getCity();

    $(document).getAllLang();
    $(document).getPrimaryLang();

    $(document).on("loaded.primary-lang", function (e) {
        getFacilitiesGroupedLang();
    });

    var $tableHotel = $("#datatable-hotels").DataTable({
        responsive: true,
        language: {
            searchPlaceholder: "Search ...",
            sSearch: "",
            lengthMenu: "_MENU_ items/page",
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        processing: true,
        serverSide: true,
        deferRender: true,
        ajax: {
            url: "/hotels",
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);

                json.recordsTotal = json.data.total;
                json.recordsFiltered = json.data.total;
                // json.recordsFiltered = json.data.item ? json.data.item.length : 0;
                json.data = json.data.item ? json.data.item : [];

                return JSON.stringify(json); // return JSON string
            }
        },
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                className: "align-middle",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: "property_logo",
                className: "text-center",
                width: "150px",
                render: function (data, type, row, meta) {
                    var date = new Date();
                    return '<img src="/static/uploads/media/' + row.property_logo + '?' + date.getTime() + '" onerror="this.src=\'/static/images/default-img.png\';" class="bd rounded-circle" width="75" height="75">';
                }
            },
            { data: "property_code", className: "align-middle" },
            { data: "property_name", className: "align-middle" },
            { data: "property_order", className: "align-middle" },
            { data: "brand.brand_name", className: "align-middle" },
            {
                data: "city.name",
                className: "align-middle",
                render: function (data, type, row, meta) {
                    var city = "";
                    if (row.city) {
                        city = row.city.name ? row.city.name : "";
                    }
                    return city;
                }
            },
            { data: "property_registered", className: "align-middle" },
            {
                data: "actions",
                className: "align-middle",
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a href="/hotels/update/' + row.property_id + '" class="badge badge-secondary" data-trigger="update">Edit</a> \
                            <a href="/hotels/' + row.property_id + '" class="badge badge-danger" data-trigger="delete">Delete</a>';
                }
            }
        ],
        order: [[2, "asc"]]
    });
    
    $("div.dataTables_filter input").unbind().keyup(function (e) {
        if (e.keyCode == 13) $tableHotel.search(this.value).draw();
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    $(document).on("click", ".btn-refresh-table", function (e) {
        $tableHotel.draw();
    });

    $(document).on("submit", ".form-setup.hotel", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");
        
        var isUpdate = $form.hasClass("update");
        var btnText = $btnSave.html();
        var url = $form.attr('action')
        var type = $form.attr('type');
        
        // clean editor
        $form.find("textarea[name]").each(function () {
            var content = cleanString($(this).val());
            $(this).val(content);
        });
        
        var data = $form.serializeObject();
        var tagline = {}, desc = {}, customField = {}, customLink = {};
        
        if (isUpdate) {
            $form.find(".tab-pane").each(function () {
                var tab = $(this).find("input[name], select[name], textarea[name]").serializeObject();

                tagline[tab.lang] = tab.tagline;
                desc[tab.lang] = tab.description;
                customField[tab.lang] = tab.custom_field;
                customLink[tab.lang] = tab.custom_link;
            })
        } else {
            tagline[primaryLang] = data.tagline;
            desc[primaryLang] = data.description;
            customField[primaryLang] = data.custom_field;
            customLink[primaryLang] = data.custom_link;
        }
        data["tagline"] = tagline;
        data["description"] = desc;
        data["custom_field"] = customField;
        data["custom_link"] = customLink;

        if (data.facilities) {
            data["facilities[]"] = data.facilities;
            delete data["facilities"];
        }
        data.attachment_image = $form.find("input[name=attachment_image]").data("values");

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            dataType: "json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
                $tableHotel.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);       
            $form.closest(".modal").modal("hide");
        });
    });

    $(document).on("click", "[data-trigger=update]", function (e) {
        e.preventDefault();

        var url = $(this).attr('href');
        var $form = $(".form-setup.hotel.update");
        var $defaultForm = $form.html();
        var $modal = $form.closest(".modal");
        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                var formData = jqXHR.data.item;
                var date = new Date();
                
                $form.find("input[name=hotel_id]").val(formData.property_id);
                $form.find("input[name=hotel_code]").val(formData.property_code);
                $form.find("input[name=booking_url]").val(formData.property_booking_url);
                $form.find("input[name=corporate_booking_code]").val(formData.property_corporate_booking_code);
                $form.find("input[name=brand_booking_code]").val(formData.property_brand_booking_code);
                $form.find("input[name=hotel_booking_code]").val(formData.property_hotel_booking_code);
                $form.find("input[name=hotel_name]").val(formData.property_name);
                $form.find("input[name=hotel_display_name]").val(formData.property_display_name);
                $form.find("select[name=brand_id]").data("value", formData.brand.brand_id);
                
                $form.find("input[name=phone]").val(formData.property_phone);
                $form.find("input[name=fax]").val(formData.property_fax);
                $form.find("input[name=email]").val(formData.property_email);
                $form.find("textarea[name=address]").val(formData.property_address);
                $form.find("input[name=latitude]").val(formData.property_latitude);
                $form.find("input[name=longitude]").val(formData.property_longitude);
                $form.find("input[name=status][value='" + (formData.property_status ? formData.property_status : '') + "']").prop("checked", true);
                $form.find("input[name=order]").val(formData.property_order);
                $form.find("select[name=city]").data("value", formData.city.code);

                var tabData = arrangeTab(formData.property_tagline, formData.property_description, formData.property_custom_field, formData.property_custom_link);
                var $tabs = $modal.find(".nav.nav-tabs");
                var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

                var $tabContent = $modal.find(".tab-content");
                var tabPaneContent = $('<div>').append($tabContent.find(".tab-pane").first().clone(true)).html();

                $tabs.html("");
                $tabContent.html("");
                $.each(tabData, function (i, item) {
                    var $thisTab = $(tab);
                    var $thisTabPane = $(tabPaneContent);
                    
                    var sourceLang = $.grep(allLang, function (v) {
                        return v.code == item.lang;
                    });
                    var itemLang = sourceLang.length > 0 ? sourceLang[0] : sourceLang;

                    var href = $thisTab.attr("href").replace("default", itemLang.code);
                    var tabID = href.replace("#", "");

                    $thisTab.attr("href", href);
                    $thisTab.html(itemLang.name);

                    $tabs.append($thisTab);

                    $thisTabPane.attr("id", tabID);
                    $thisTabPane.find("input[name=lang]").val(itemLang.code);
                    $thisTabPane.find("input[name=tagline]").val(item.tagline);
                    $thisTabPane.find("textarea[name=description]").val(item.description);
                    $thisTabPane.find("input[name=custom_field]").val(item.custom_field);
                    $thisTabPane.find("input[name=custom_link]").val(item.custom_link);

                    $tabContent.append($thisTabPane);
                });
                $tabs.find('a.nav-link:first-child').tab('show');
                
                var logo = formData.property_logo;
                if (logo) {
                    $form.find("[data-trigger=delete-logo]").removeClass("d-none");
                }
                $form.find("input[name=logo]").val(logo);
                logo = logo.match(/^http/) ? logo : "/static/uploads/media/" + logo;
                $form.find("img.logo").attr("src", logo);

                var secondaryLogo = formData.property_secondary_logo ? formData.property_secondary_logo : "";
                if (secondaryLogo) {
                    $form.find("[data-trigger=delete-secondary-logo]").removeClass("d-none");
                }
                $form.find("input[name=secondary_logo]").val(secondaryLogo);
                secondaryLogo = secondaryLogo.match(/^http/) ? secondaryLogo : "/static/uploads/media/" + secondaryLogo;
                $form.find("img.secondary-logo").attr("src", secondaryLogo);

                var favicon = formData.property_favicon ? formData.property_favicon : "";
                if (favicon) {
                    $form.find("[data-trigger=delete-favicon]").removeClass("d-none");
                }
                $form.find("input[name=favicon]").val(favicon);
                favicon = favicon.match(/^http/) ? favicon : "/static/uploads/media/" + favicon;
                $form.find("img.favicon").attr("src", favicon);

                var featuredImage = formData.property_featured_image;
                if (featuredImage) {
                    $form.find("[data-trigger=delete-fi]").removeClass("d-none");
                }
                $form.find("input[name=featured_image]").val(featuredImage);
                featuredImage = featuredImage.match(/^http/) ? featuredImage : "/static/uploads/media/" + featuredImage;
                $form.find("img.featured-image").attr("src", featuredImage);

                var imageAttachment = "";
                if (formData.property_image_attachment) {
                    $.each(formData.property_image_attachment, function (i, ia) {
                        var iaUrl = ia.match(/^http/) ? ia : "/static/uploads/media/" + ia;
                        imageAttachment += ' \
                        <div class="col-lg-3 col-md-4 col-6 ai-item"> \
                        <div class="d-block mb-4 h-100 pos-relative"> \
                            <img data-image="'+ ia + '" class="img-fluid img-thumbnail" src="' + iaUrl + '" onerror="this.src=\'/static/images/default-img.png\';" alt="" style="width: 200px; height: 100px"> \
                            <div class="ai-toolbar pos-absolute" style="top: 3px; right: 8px;"> \
                                <a href="#" data-trigger="delete-ai" class="badge badge-danger tx-bold">X</a> \
                            </div> \
                        </div> \
                        </div >';
                    });
                }
                $form.find("input[name=attachment_image]").val(formData.property_image_attachment).data({
                    values: formData.property_image_attachment,
                    current_values: formData.property_image_attachment
                });
                $form.find(".image-attachment").html(imageAttachment);

                $modal.find(".hotelFacilitiesTree").data("facilities", formData.property_facilities);

                $modal.modal({
                    show: true,
                    keyboard: false,
                    backdrop: "static",
                }).data({
                    defaultForm: $defaultForm,
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });
    
    $(document).on("click", "[data-trigger=delete]", function (e) {
        e.preventDefault();
        
        var brand = $(this).closest("tr").find("td:eq(3)").text();
        var accept = confirm("Are you sure to delete " + brand + " ?");
        if (!accept) return;
        
        var $btn = $(this);
        var url = $(this).attr('href');
        var btnText = $btn.html();
        
        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");
                
                $tableHotel.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
                
                $btn.html(btnText);
            }
        });
    });

    $(document).on("click", "[data-trigger=delete-logo]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $form = $(this).closest("form");
        $form.find("input[name=logo]").val("");
        $form.find("img.logo").attr("src", "#");
        $form.find("[data-trigger=delete-logo]").addClass("d-none");
    });

    $(document).on("click", "[data-trigger=delete-secondary-logo]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $form = $(this).closest("form");
        $form.find("input[name=secondary_logo]").val("");
        $form.find("img.secondary-logo").attr("src", "#");
        $form.find("[data-trigger=delete-secondary-logo]").addClass("d-none");
    });

    $(document).on("click", "[data-trigger=delete-favicon]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $form = $(this).closest("form");
        $form.find("input[name=favicon]").val("");
        $form.find("img.favicon").attr("src", "#");
        $form.find("[data-trigger=delete-favicon]").addClass("d-none");
    });

    $(document).on("click", "[data-trigger=delete-fi]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $form = $(this).closest("form");
        $form.find("input[name=featured_image]").val("");
        $form.find(".featured-image").attr("src", "#");
        $form.find("[data-trigger=delete-fi]").addClass("d-none");
    });

    $(document).on("click", "[data-trigger=delete-ai]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $self = $(this);
        var $form = $self.closest("form");

        var images = $form.find("input[name=attachment_image]").data("values");
        var itemToRemove = $self.closest(".ai-item").find("img").data("image");

        images.splice($.inArray(itemToRemove, images), 1);
        $form.find("input[name=attachment_image]").data({
            values: images,
            current_values: images,
        }).val(images);

        $self.closest(".ai-item").remove();
    });

    $(document).on("click", "[data-trigger=delete-tab]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $card = $self.closest(".card");

        var $translateToDropdown = $card.find('select.translate-to');

        var $tabs = $card.find(".nav.nav-tabs");
        var $navTabLinks = $tabs.find("a.nav-link");
        var $activeTab = $tabs.find("a.nav-link.active");

        var $tabContent = $card.find(".tab-content"); primaryLangJson
        var $activeTabPane = $tabContent.find(".tab-pane.active");

        var activeLang = $activeTab.text();
        var accept = confirm("Delete this translation " + activeLang + " ?");
        if (!accept) return;

        if ($navTabLinks.length > 1) {
            $activeTab.remove();
            $activeTabPane.remove();
            $tabs.find('a.nav-link:last-child').tab("show");
        }

        var langIDs = $card.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.generateTranslateToDropdown(langIDs);
    });

    $(document).on("select2:select", "select.translate-to", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $parent = $self.closest(".card");
        var defaultForm = $modal.data("defaultForm");

        var lang = $self.select2("data")[0];

        var $tabs = $parent.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $parent.find(".tab-content");

        var $tab = $(tab);
        var $tabPane = $(defaultForm).find(".tab-pane");

        var href = $tab.attr("href").replace(/\w+$/, lang.id);
        var tabID = href.replace("#", "");

        $tab.removeClass("active show");
        $tabPane.removeClass("active show");

        $tab.attr("href", href);
        $tab.html(lang.text);
        $tab.addClass("new");
        $tabs.append($tab);

        $tabPane.attr("id", tabID);
        $tabPane.find("input[name=lang]").val(lang.id);
        $tabPane.find("input[name=tagline]").val("");
        $tabPane.find("textarea[name=description]").val("");
        $tabPane.find("input[name=custom_field]").val("");
        $tabPane.find("input[name=custom_link]").val("");

        $tabContent.append($tabPane);
        $tabs.find('a.nav-link:last-child').tab("show");


        $tabPane.find('textarea[name=description]').generateTinyMce({menubar: false});

        var langIDs = $parent.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $self.destroyTranslateToDropdown();
        $self.generateTranslateToDropdown(langIDs);
    });

    $(document).on("selected.media.library", "input[name=logo]", function (e) {
        var $modal = $(this).closest(".modal");
        var image = e.value.match(/^http/) ? e.value : "/static/uploads/media/" + e.value;

        $modal.find("[data-trigger=delete-logo]").removeClass("d-none");
        $modal.find("img.logo").attr("src", image);
    });

    $(document).on("selected.media.library", "input[name=secondary_logo]", function (e) {
        var $modal = $(this).closest(".modal");
        var image = e.value.match(/^http/) ? e.value : "/static/uploads/media/" + e.value;

        $modal.find("[data-trigger=delete-secondary-logo]").removeClass("d-none");
        $modal.find("img.secondary-logo").attr("src", image);
    });

    $(document).on("selected.media.library", "input[name=favicon]", function (e) {
        var $modal = $(this).closest(".modal");
        var image = e.value.match(/^http/) ? e.value : "/static/uploads/media/" + e.value;

        $modal.find("[data-trigger=delete-favicon]").removeClass("d-none");
        $modal.find("img.favicon").attr("src", image);
    });

    $(document).on("selected.media.library", "input[name=featured_image]", function (e) {
        var $modal = $(this).closest(".modal");
        var image = e.value.match(/^http/) ? e.value : "/static/uploads/media/" + e.value;

        $modal.find("[data-trigger=delete-fi]").removeClass("d-none");
        $modal.find("img.featured-image").attr("src", image);
    });

    $(document).on("selected.multiple-media.library", "input[name=attachment_image]", function (e) {
        var $parent = $(this).closest(".modal").find(".image-attachment");
        var currentValues = $(this).data("current_values");
        var images = e.value ? e.value : [];
        images = $.merge((currentValues ? currentValues : []), images);
        $(this).closest(".modal").find("input[name=attachment_image]").val(images).data({ values: images, current_values: images });

        var imageAttachment = "";
        $.each(images, function (i, ia) {
            var iaUrl = ia.match(/^http/) ? ia : "/static/uploads/media/" + ia;
            imageAttachment += ' \
            <div class="col-lg-3 col-md-4 col-6 ai-item"> \
                <div class="d-block mb-4 h-100 pos-relative"> \
                    <img data-image="'+ ia + '" class="img-fluid img-thumbnail" src="' + iaUrl + '" onerror="this.src=\'/static/images/default-img.png\';" alt="" style="width: 200px; height: 100px"> \
                    <div class="ai-toolbar pos-absolute" style="top: 3px; right: 8px;"> \
                        <a href="#" data-trigger="delete-ai" class="badge badge-danger tx-bold">X</a> \
                    </div> \
                </div> \
            </div>';
        });
        $parent.html(imageAttachment);
    });

    $(document).on("show.bs.tab", "[data-toggle=tab]", function (e) {
        var $self = $(this);
        var tabID = $self.attr("href");
        var $btnDelete = $(tabID).find("[data-trigger=delete-tab]");

        if ($self.index() > 0) {
            $btnDelete.removeClass("d-none");
        } else {
            $btnDelete.addClass("d-none");
        }
    });

    $(document).on("shown.bs.modal", "#modal-form-hotel, #modal-form-hotel-update", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form.form-setup.hotel");
        var isModalUpdate = $form.hasClass("update");

        if (isModalUpdate) {
            var $card = $modal.find(".card");
            var langIDs = $card.find("input[name=lang]").map(function () {
                return this.value;
            }).get();
            $card.find('select.translate-to').generateTranslateToDropdown(langIDs);
        } else {
            var $defaultForm = $form.html();
            $modal.data("defaultForm", $defaultForm);
        }

        $modal.find('select[name=brand_id]').generateBrandDropdown();
        $modal.find('select[name=city]').generateCityDropdown();
        $modal.find('.hotelFacilitiesTree').generateFacilitiesTreeView();
        $modal.find('textarea[name=description]').generateTinyMce({menubar: false});
    });

    $(document).on("hidden.bs.modal", "#modal-form-hotel, #modal-form-hotel-update", function (e) {
        var $modal = $(this);
        var defaultForm = $modal.data('defaultForm');
        var isModalUpdate = $modal.find("form").hasClass("update");

        if (isModalUpdate) {
            $modal.find('select.translate-to').destroyTranslateToDropdown();
        }

        $modal.find('select[name=brand_id]').destroySelectDropdown();
        $modal.find('select[name=city]').destroySelectDropdown();
        $modal.find('textarea[name=description]').destroyTinyMce();
        $modal.find('form.form-setup.hotel').html(defaultForm);
        $modal.find(".hotelFacilitiesTree").removeData();
        $modal.removeData();
    });

    $(document).on("nodeChecked", ".treeview", function (e, data) {
        var $self = $(this);
        var $formGroup = $self.closest(".form-group");
        
        var ID = $(data.text).data("id");
        var parentID = $(data.text).data("parentId");
        
        $self.find(".list-group-item").each(function () {
            var nId = $(this).data("nodeid");
            var fID = $(this).find("a").data("id");
            var pID = $(this).find("a").data("parentId");

            if (ID == fID) {
                if ($formGroup.find('input[name=facilities][value=' + fID + ']').length == 0) {
                    $formGroup.append('<input type="hidden" name="facilities" value="' + fID + '">');
                }
            }

            // check parent
            if (parentID == fID) {
                $self.treeview('checkNode', [nId, { silent: true }]);
                // add input field facilities parent
                if ($formGroup.find('input[name=facilities][value=' + fID + ']').length == 0) {
                    $formGroup.append('<input type="hidden" name="facilities" value="' + fID + '">');
                }
            }

            // check child
            if (ID == pID) {
                $self.treeview('checkNode', [nId, { silent: true }]);
                // add input field facilities child
                if ($formGroup.find('input[name=facilities][value=' + fID + ']').length == 0) {
                    $formGroup.append('<input type="hidden" name="facilities" value="' + fID + '">');
                }
            }
        });
    });

    $(document).on("nodeUnchecked ", ".treeview", function (e, data) {
        var $self = $(this);
        var $formGroup = $self.closest(".form-group");
        
        var ID = $(data.text).data("id");
        var parentID = $(data.text).data("parentId");
        
        $self.find(".list-group-item").each(function () {
            var nID = $(this).data("nodeid");
            var fID = $(this).find("a").data("id");
            var pID = $(this).find("a").data("parentId");

            if (ID == fID) {
                $formGroup.find('input[name=facilities][value=' + fID + ']').remove();
            }

            // uncheck parent
            if (parentID == fID) {
                var childs = $self.find(".list-group-item.node-checked").find("a[data-parent-id=" + parentID + "]").length;
                if (childs == 1) {
                    $self.treeview('uncheckNode', [nID, { silent: true }]);
                    $formGroup.find('input[name=facilities][value=' + fID + ']').remove();
                }
            }
            
            // uncheck child
            if (ID == pID) {
                $self.treeview('uncheckNode', [nID, { silent: true }]);
                $formGroup.find('input[name=facilities][value=' + fID + ']').remove();
            }
        });
    });

    $(document).on("keyup", "input[name=hotel_code]", function (e) {
        e.preventDefault();
        this.value = this.value.toUpperCase();
        $.utils.no_spaces(this, '');
    });

    $(document).on("keyup", "input[name=corporate_booking_code], input[name=brand_booking_code], input[name=hotel_booking_code]", function (e) {
        e.preventDefault();
        
        $.utils.no_spaces(this, '');
    });
});