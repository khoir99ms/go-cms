/* 
usage:
    data-toggle="media-library" 
    data-multiple="yes|no" 
    self-parent="yes|no"
    data-parent-target="#myparenttarget" (optional)
    data-input-target="#mytargetinput" (for set input value)
    data-target="#mytargetdiv" (optional, target area for append input)
    data-input-name="#appendinputname"  (for append input)
    data-filter="image,icon,video,attachment" 
*/
$(function () {
    "use strict";

    var modal = "#modal-media-library";
    var perPage = 12;

    $.fn.mediaLibrary = function() {
        var $modal = $(modal);
        var filter = {
            image: "Image",
            icon: "Icon",
            video: "Video",
            attachment: "Attachment",
        };

        var isMultiple = this.data("multiple") && this.data("multiple").toLowerCase() == "yes" ? true : false;
        var selfParent = this.data("self-parent") && this.data("self-parent").toLowerCase() == "yes" ? this : this;
        var parentTarget = this.data("parent-target");
        var inputTarget = this.data("input-target");
        var target = this.data("target");
        var inputName = this.data("input-name");
        var activeFilter = this.data("filter");
        activeFilter = activeFilter ? activeFilter.split(",") : [];

        var options = '';
        var existAnyOneFilter = false;
        if (activeFilter.length > 0) {
            $.each(activeFilter, function (i, fk) {
                fk = fk.trim();
                if (typeof filter[fk] !== 'undefined') {
                    options += '<option value="' + fk + '">' + filter[fk] + '</option>';
                    existAnyOneFilter = true;
                }
            });
        } 
        
        if (activeFilter.length == 0 || !existAnyOneFilter) {
            options += '<option value="">All Media</option>';
            $.each(filter, function (fk, name) {
                options += '<option value="' + fk + '">' + name + '</option>';
            });
        }
        $modal.find("select[name=type]").html(options);

        $modal.data({
            selfParent: selfParent,
            parentTarget: parentTarget,
            target: target,
            inputTarget: inputTarget,
            inputName: inputName,
            isMultiple: isMultiple,
        }).modal({
            show: true,
            keyboard: false,
            backdrop: "static",
        });
    }

    $(document).on("submit", modal + " form.media-library", function (e) {
        e.preventDefault();
        var $modal = $(this).closest(".modal");
        var isMultiple = $modal.data("isMultiple");

        var oEvent = (e.originalEvent && e.originalEvent.type) || e.totalView;
        var $btnMore = $modal.find(".btn-more-media");
        var btnMoreTxt = $btnMore.html();

        var total = 0;
        var totalView = 0;
        var page = $btnMore.data("page");
        var url = $(this).attr('action');
        var params = { start: page, length: perPage };
        if (oEvent) {
            params.start = 0;
            params.length = e.totalView && e.totalView > perPage ? e.totalView : perPage;
        }
        var data = $(this).serialize() + "&" + $.param(params);

        $.ajaxq("aq", {
            url: url,
            data: data,
            beforeSend: function () {
                $btnMore.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                var medias = ""
                var items = jqXHR.data.item;
                total = jqXHR.data.total;
                if (items) {
                    $.each(items, function (i, item) {
                        var mediaValue = item.url ? item.url : item.file.name;

                        var thumbnail = "";
                        if (item.type == "video" && item.url) {
                            var isYoutubeUrl = item.url.trim().match(/(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=|\/sandalsResorts#\w\/\w\/.*\/))([^\/&]{10,12})/);
                            thumbnail = isYoutubeUrl ? "https://img.youtube.com/vi/" + isYoutubeUrl[1] + "/mqdefault.jpg" : item.url;
                        } else if ((item.type == "image" || item.type == "icon" || item.type == "attachment") && item.url) {
                            thumbnail = item.url;
                        } else {
                            thumbnail = item.file.name ? "/static/uploads/media/" + item.file.name : "";
                        }
                        if (item.type == "attachment") {
                            thumbnail = "/static/images/attachment.png";
                        }

                        var selectMode = "";
                        var setData = JSON.stringify({
                            "id": item.id,
                            "title": item.title,
                            "caption": item.caption,
                            "file": item.file,
                            "url": item.url,
                            "type": item.type
                        });
                        if (isMultiple) {
                            $modal.find("[data-toggle=select-multiple-image]").removeClass("d-none");
                            selectMode = '<div class="form-check pos-absolute" style="top: 5px; right: 5px;"> \
                                <input id="select-image-'+ item.id + '" class="form-check-input" type="checkbox" value="' + mediaValue + '" style="cursor: pointer;"> \
                                <label class="form-check-label" for="select-image-'+ item.id +'" style="cursor: pointer;"> \
                                    <span class="badge badge-info tx-bold">Select</span> \
                                </label> \
                            </div>';
                        } else {
                            $modal.find("[data-toggle=select-multiple-image]").addClass("d-none");
                            selectMode = '<a href="#" data-toggle="select-image" class="badge badge-info pos-absolute tx-bold" style="top: 5px; right: 5px;">Select</a>';
                        }
                        medias += '\
                        <div class="col-md-3 media-item" data-json=\'' + setData + '\'>\
                            <div class="card bd-0 pos-relative mg-b-20" data-media="' + mediaValue + '"> \
                                <img class="img-fluid bd ht-150 wd-100p" src="'+ thumbnail + '" alt="' + item.title + '" onerror="this.src=\'https://via.placeholder.com/500x334?text=No Picture\';"> \
                                '+ selectMode +' \
                                <div class="card-body pd-10 bd bd-t-0" style="cursor: pointer;"> \
                                    <p class="card-text">' + item.caption + '</p> \
                                </div> \
                            </div> \
                        </div>';
                    });
                }

                if (items.length == 0) {
                    medias += '<div class="col-md"><div class="alert alert-outline-warning mg-b-0 tx-center" role="alert">No media.</div></div>';
                }

                if (params.start == 0) { 
                    $modal.find("#medias").html(medias);
                } else {
                    $modal.find("#medias").append(medias);
                }
                totalView = $modal.find("#medias > .media-item").length;
            }

            var page = Math.floor(totalView / perPage);
            if (totalView < total) {
                $btnMore.removeClass("d-none").data("page", page);
            } else {
                $btnMore.addClass("d-none").data("page", 0);
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnMore.prop('disabled', false).html(btnMoreTxt);
        });
    });
    
    $(document).on("click", modal + " .btn-more-media", function (e) {
        e.preventDefault();

        $(modal + " form.media-library").trigger("submit");
    }); 
    
    $(document).on("submit", modal + " .form-media.add", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $modal = $form.closest(".modal");
        var $formFilterMedia = $modal.find(".modal-header > form.media-library");
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr('action');
        var method = $form.attr('type');
        var btnText = $btnSave.html();
        var fileLength = $form.find("input[name=file]").prop("files").length;
        
        $form.find('input[name=rows]').val(fileLength);
        var data = new FormData(this);

        var fileType = $formFilterMedia.find("select[name=type]").val();
        for (var i = 0; i < fileLength; i++) {
            data.append("type", fileType);
        }
        
        $.ajaxq("aq", {
            url: url,
            data: data,
            type: method,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
                var totalView = $modal.find("#medias > .media-item").length;
                totalView = Math.ceil(totalView / perPage) * perPage;
                
                $modal.find("form.media-library").trigger({
                    type: "submit",
                    totalView: totalView
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
            $form.find("label.custom-file-label").html("Choose file");
        });
    });
    
    $(document).on("change", modal + " input[name=file]", function (e) {
        var files = e.target.files;
        var $parent = $(this).closest(".custom-file");
        if ($(this).is('[multiple]')) {
            if (files) {
                var label = files.length > 1 ? "Selected " + files.length + " file(s)." : files[0].name;
                $parent.find("label.custom-file-label").html(label);
            }
        } else {
            $parent.find("label.custom-file-label").html(files[0].name);
        }
    });

    $(document).on("click", "[data-toggle=media-library]", function (e) {
        e.preventDefault();

        $(this).mediaLibrary();
    });

    $(document).on("click", "[data-toggle=select-image]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $modal = $self.closest(".modal");
        var selfParent = $modal.data("self-parent");
        var parentTarget = $modal.data("parent-target");
        var inputTarget = $modal.data("input-target");
        var target = $modal.data("target");
        var inputName = $modal.data("input-name");

        var json = $self.closest(".media-item").data("json");
        var value = $self.closest(".card").data("media");

        var $target;
        if (parentTarget && selfParent) {
            $target = $(selfParent).closest(parentTarget);
        } else if (parentTarget) {
            $target = $(parentTarget)
        } else {
            $target = $(inputTarget);
        }
        
        if (target) $target = $target.find(target);
        if (inputName) {
            $target.find('input[name=' + inputName + ']').remove();
            $target.append('<input type="hidden" name="' + inputName + '" value="' + value + '">');
            $target.trigger({
                type: "selected.media.library",
                value: value,
                json: json
            });
        } else if (inputTarget) {
            $target = $target.find(inputTarget);
            $target.val(value).trigger({
                type: "selected.media.library",
                value: value,
                json: json
            });
        } else {
            var isHasValue = $target.is("input") || $target.is("select") || $target.is("textarea");
            
            if (isHasValue) $target.val(value);
            $target.trigger({
                type: "selected.media.library",
                value: value,
                json: json
            });
        }
        $modal.modal("hide");
    });

    $(document).on("click", "[data-toggle=select-multiple-image]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $modal = $self.closest(".modal");
        var selfParent = $modal.data("self-parent");
        var parentTarget = $modal.data("parent-target");
        var inputTarget = $modal.data("input-target");
        var target = $modal.data("target");
        var inputName = $modal.data("input-name");
        
        var json = [];
        var values = [];
        $modal.find("#medias .form-check input:checked").each(function () {
            var $self = $(this);

            var js = $self.closest(".media-item").data("json");
            var value = $self.val();

            json.push(js);
            values.push(value);
        });

        var $target;
        if (parentTarget && selfParent) {
            $target = $(selfParent).closest(parentTarget);
        } else if (parentTarget) {
            $target = $(parentTarget)
        } else {
            $target = $(inputTarget);
        }

        if (target) $target = $target.find(target);
        if (inputName) {
            $.each(values, function (i, value) {
                $target.append('<input type="hidden" name="' + inputName + '" value="' + value + '">');
            });
            $target.trigger({
                type: "selected.multiple-media.library",
                value: values,
                json: json
            });
        } else if (inputTarget) {
            $target = $target.find(inputTarget);
            $target.data({ values: values }).val(values).trigger({
                type: "selected.multiple-media.library",
                value: values,
                json: json
            });
        } else {
            var isHasValue = $target.is("input") || $target.is("select") || $target.is("textarea");

            if (isHasValue) $target.val(values);
            $target.data({ values: values }).trigger({
                type: "selected.multiple-media.library",
                value: values,
                json: json
            });
        }
        $modal.modal("hide");
    });

    $(document).on("shown.bs.modal", modal, function(){
        $(this).find("form.media-library").trigger("submit");
    });

    $(document).on("hidden.bs.modal", modal, function(){
        $(this).removeData();
        $(this).find("label.custom-file-label").html("Choose file");
        $(this).find("#medias").html("");
        $(this).find(".btn-more-media").data("page", 0);
    });
});