$(function () {
    "use strict";
    
    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    var perPage = 18;

    $('.select2.fltType').select2({
        placeholder: 'All Media',
        allowClear: true,
        minimumResultsForSearch: Infinity,
    });

    $('.select2.input-row').select2({
        minimumResultsForSearch: Infinity,
    });

    $(document).on("change", "input[name=file]", function (e) {
        var fileName = e.target.files[0].name;
        var $parent = $(this).closest(".custom-file");

        $parent.find("label.custom-file-label").html(fileName);
    });

    $(document).on("click", ".form-insert-row button.go-btn", function(e){
        e.preventDefault();

        var $modal = $("#modal-form-media");
        var $cloneRow = $(".media-group").first().clone(true);

        $cloneRow.find("input[name=title]").val("");
        $cloneRow.find("select[name=type]").val("");
        $cloneRow.find("input[name=url]").val("");
        $cloneRow.find("input[name=file]").val("");
        $cloneRow.find("label.custom-file-label").html("Choose file");

        var cloneRowHtml = $('<div>').append($cloneRow).html();
        
        var rawHtml = "";
        var rows = parseInt($(".form-insert-row select[name=row]").val());
        for (var i = 0; i < rows; i++) {
            rawHtml += cloneRowHtml;
        } 
        $modal.find(".modal-body").append(rawHtml);
    });

    $(document).on("shown.bs.modal", "#modal-form-media", function (e) {
        $(this).find("input[name=rows]").val(1);
        $("select[name=row]").val("1").trigger("change");
    });

    $(document).on("hidden.bs.modal", "#modal-form-media", function (e) {
        e.preventDefault();
        var $mediaGroupInput = $(".media-group");

        $mediaGroupInput.not(':first').remove();
        $mediaGroupInput.find("input[name=rows]").val(1);
        $mediaGroupInput.find("input[name=title]").val("");
        $mediaGroupInput.find("select[name=type]").get(0).selectedIndex = 0;
        $mediaGroupInput.find("input[name=url]").val("");
        $mediaGroupInput.find("input[name=file]").val("");
        $mediaGroupInput.find("label.custom-file-label").html("Choose file");
    });

    $(document).on("shown.bs.modal", "#modal-form-media-update", function (e) {
        var imgHeight = $(this).find(".media-attribute").outerHeight();
        $(this).find(".media-preview img").height(imgHeight);
    });

    $(document).on("click", "#btn-more-media", function () {
        $(".form-media.filter").trigger("submit");
    });
    
    $(document).on("submit", ".form-media.filter", function(e) {
        e.preventDefault();
        
        var oEvent = (e.originalEvent && e.originalEvent.type) || e.totalView;
        var $btnMore = $("#btn-more-media");
        var btnMoreTxt = $btnMore.html();

        var total = 0;
        var totalView = 0;
        var page = $btnMore.data("page");
        var url = $(this).attr('action');
        var params = { start: page, length: perPage };
        if (oEvent) {
            params.start = 0;
            params.length = e.totalView && e.totalView > perPage ? e.totalView : perPage;
        }
        var data = $(this).serialize() + "&" + $.param(params);
        
        $.ajaxq("aq", {
            url: url,
            data: data,
            beforeSend: function(){
                $btnMore.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                var medias = ""
                var items = jqXHR.data.item;
                total = jqXHR.data.total;
                if (items) {
                    $.each(items, function (i, item) {
                        var thumbnail = "";
                        if (item.type == "video" && item.url) {
                            var isYoutubeUrl = item.url.trim().match(/(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=|\/sandalsResorts#\w\/\w\/.*\/))([^\/&]{10,12})/);
                            thumbnail = isYoutubeUrl ? "https://img.youtube.com/vi/" + isYoutubeUrl[1] + "/mqdefault.jpg" : item.url;
                        } else if ((item.type == "image" || item.type == "icon" || item.type == "attachment") && item.url) {
                            thumbnail = item.url;
                        } else {
                            thumbnail = item.file.name ? "/static/uploads/media/" + item.file.name : "";
                        }
                        if (item.type == "attachment") {
                            thumbnail = "/static/images/attachment.png";
                        }

                        medias += '\
                        <div class="col-md-2 media-item">\
                            <div class="card bd-0 pos-relative mg-b-20"> \
                                <img class="img-fluid bd ht-150 wd-100p" src="'+ thumbnail + '" alt="' + item.title + '" onerror="this.src=\'https://via.placeholder.com/500x334?text=No Picture\';"> \
                                <a href="/media/update/' + item.id + '" data-trigger="update" class="badge badge-info pos-absolute tx-bold" style="top: 5px; right: 60px;">View</a> \
                                <a href="/media/' + item.id + '" data-trigger="delete" class="badge badge-danger pos-absolute tx-bold" style="top: 5px; right: 5px;">X Delete</a> \
                                <div class="card-body pd-10 bd bd-t-0" data-url="/media/update/' + item.id + '" data-trigger="update" style="cursor: pointer;"> \
                                    <p class="card-text">' + item.caption + '</p> \
                                </div> \
                            </div> \
                        </div>';
                    });
                } 
                
                if (items.length == 0 ) {
                    medias += '<div class="col-md"><div class="alert alert-outline-warning mg-b-0 tx-center" role="alert">No media.</div></div>';
                }
                
                if (params.start == 0) { 
                    $("#medias").html(medias);
                } else {
                    $("#medias").append(medias);
                }
                totalView = $("#medias > .media-item").length;
            }

            var page = Math.floor(totalView / perPage);
            if (totalView < total) {
                $btnMore.removeClass("d-none").data("page", page);
            } else {
                $btnMore.addClass("d-none").data("page", 0);
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnMore.prop('disabled', false).html(btnMoreTxt);
        });        
    });
    
    $(document).on("submit", ".form-media.add", function (e) {
        e.preventDefault();
        var $form = $(this);
        var $modal = $form.closest(".modal");
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr('action');
        var method = $form.attr('type');
        var btnText = $btnSave.html();
        $form.find('input[name=rows]').val($(".media-group").length);
        
        var data = new FormData(this);
        
        $.ajaxq("aq", {
            url: url,
            data: data,
            type: method, 
            contentType: false,
            processData: false,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
                var totalView = $("#medias > .media-item").length;
                totalView = Math.ceil(totalView / perPage) * perPage;

                $(".form-media.filter").trigger({
                    type: "submit",
                    totalView: totalView
                });
                
                $modal.modal("hide");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("submit", ".form-media.update", function (e) {
        e.preventDefault();
        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr('action');
        var method = $form.attr('type');
        var btnText = $btnSave.html();        
        var data = new FormData(this);
        
        $.ajaxq("aq", {
            url: url,
            data: data,
            type: method, 
            contentType: false,
            processData: false,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                var data = jqXHR.data.media
                toastr["success"](jqXHR.message, "success");
                
                var mediaUrl = "";
                if (data.url) {
                    var isYoutubeUrl = data.url.trim().match(/(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=|\/sandalsResorts#\w\/\w\/.*\/))([^\/&]{10,12})/);
                    mediaUrl = isYoutubeUrl ? "https://www.youtube.com/embed/" + isYoutubeUrl[1] + "?rel=0&showinfo=1&autoplay=0" : data.url.trim();
                } else {
                    mediaUrl = data.file ? "/static/uploads/media/" + data.file : "";
                }

                var mediaPreview = "";
                if (data.type == "video") {
                    mediaPreview += '<iframe class="wd-100p ht-100p" src="' + mediaUrl + '" frameborder="0" allowfullscreen></iframe>';
                } else if (data.type == "attachment") {
                    mediaPreview += '<a href="' + mediaUrl + '" target="_blank"> \
                        <img src="/static/images/attachment.png" class="wd-100p" style="height: 570px" onerror="this.src=\'https://via.placeholder.com/500x334?text=No Picture\'">\
                    </a>';
                } else {
                    mediaPreview += '<a href="' + mediaUrl + '" target="_blank"> \
                        <img src="' + mediaUrl + '" class="wd-100p" style="height: 570px" onerror="this.src=\'https://via.placeholder.com/500x334?text=No Picture\'">\
                    </a>';
                }
                $form.find(".media-preview").html(mediaPreview);

                var totalView = $("#medias > .media-item").length;
                $(".form-media.filter").trigger({
                    type: "submit",
                    totalView: totalView
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
            $form.find("input[name=file]").val("");
            $form.find("label.custom-file-label").html("Choose file");
        });
    });

    $(document).on("click", "[data-trigger=update]", function (e) {
        e.preventDefault();

        var url = $(this).attr('href');
        url = url ? url : $(this).data('url');

        var $form = $(".form-media.update");
        var $modal = $form.closest(".modal");

        $.ajaxq("aq", {
            url: url,
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                var formData = jqXHR.data.item;

                $form.find("input[name=media_id]").val(formData.id);
                $form.find("input[name=title]").val(formData.title);
                $form.find("input[name=caption]").val(formData.caption);
                $form.find("select[name=type]").val(formData.type);
                $form.find("input[name=url]").val(formData.url);
                $form.find("input[name=as_gallery]").prop("checked", formData.gallery);
                $form.find("textarea[name=description]").html(formData.description);
                $form.find("[data-trigger=delete]").data("url", "/media/" + formData.id );
                
                var mediaUrl = "";
                if (formData.url) {
                    var isYoutubeUrl = formData.url.trim().match(/(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=|\/sandalsResorts#\w\/\w\/.*\/))([^\/&]{10,12})/);
                    mediaUrl = isYoutubeUrl ? "https://www.youtube.com/embed/" + isYoutubeUrl[1] + "?rel=0&showinfo=1&autoplay=0" : formData.url.trim();
                } else {
                    mediaUrl = formData.file ? "/static/uploads/media/" + formData.file.name : "";
                }

                var mediaPreview = "";
                if (formData.type == "video") {
                    mediaPreview += '<iframe class="wd-100p ht-100p" src="' + mediaUrl + '" frameborder="0" allowfullscreen></iframe>';
                } else if (formData.type == "attachment") {
                    mediaPreview += '<a href="' + mediaUrl + '" target="_blank"> \
                        <img src="/static/images/attachment.png" class="wd-100p" style="height: 570px" onerror="this.src=\'https://via.placeholder.com/500x334?text=No Picture\'">\
                    </a>';
                } else {
                    mediaPreview += '<a href="' + mediaUrl + '" target="_blank"> \
                        <img src="' + mediaUrl + '" class="wd-100p" style="height: 570px" onerror="this.src=\'https://via.placeholder.com/500x334?text=No Picture\'">\
                    </a>';
                }
                $form.find(".media-preview").html(mediaPreview);

                $modal.modal("show");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    });

    $(document).on("click", "[data-trigger=delete]", function (e) {
        e.preventDefault();
        var $this = $(this);
        var sourceEvt = $this.data("source-event");

        var mediaTitle = sourceEvt == "modal" ? $this.closest("form").find("input[name=title]").val() : $this.closest(".card").find(".card-text").text();
        var accept = confirm("Are you sure to delete " + mediaTitle + " ?");
        if (!accept) return;

        var $btn = $this;
        var url = sourceEvt == "modal" ? $this.data('url') : $this.attr('href');
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");
                if (sourceEvt == "modal") {
                    $this.closest(".modal").modal("hide");
                }

                var totalView = $("#medias > .media-item").length;
                $(".form-media.filter").trigger({
                    type: "submit",
                    totalView: totalView
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");

            }
            $btn.html(btnText);
        });
        return;
    });

    $(".form-media.filter").trigger("submit");
});