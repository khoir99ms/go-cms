$.requests = (function(){
	var fn = {}, id = 0, Q = {};
	$(document).ajaxSend(function(e, jqx){
		jqx._id = ++id;
		Q[jqx._id] = jqx;
	});

	$(document).ajaxComplete(function(e, jqx){
		delete Q[jqx._id];
	});
	
	fn.abortAll = function(){
		var r = [];
		$.each(Q, function(i, jqx){
			r.push(jqx._id);
			jqx.abort();
		});

		return r;
	}
	
	fn.getJson = function (url){
		request = $.ajaxq('ajaxQ', {
			url: url,
			cache: false,
			dataType: 'json'
		});
		
		/* request.done(function(data){
			return data;
		}); */
		
		request.pipe(function(data) {
			return data.responseCode != 200 ? $.Deferred().reject( data ) : data;
		});
		
		request.fail(function (jqXHR, exception) {
			// Our error logic here
			var msg = '';
			if (jqXHR.status === 0 ) {
				if (jqXHR.statusText !== 'abort') {
					// Offline mode
					msg = 'Not connect.\n Verify Network.';
				}
			} else if (jqXHR.status == 404) {
				msg = 'Requested page not found. [404]';
			} else if (jqXHR.status == 500) {
				msg = 'Internal Server Error [500].';
			} else if (exception === 'parsererror') {
				msg = 'Requested JSON parse failed.';
			} else if (exception === 'timeout') {
				msg = 'Time out error.';
			// } else if (exception === 'abort') {
				// msg = 'Ajax request aborted.';
			} else {
				msg = 'Uncaught Error.\n' + jqXHR.responseText;
			}
			
			if (msg) alert(msg);
		});
		
		return request;
	}
	
	return fn;
})();

$.utils = (function(){
	var fn = {};

	fn.no_spaces = function (val, replacer){
		replacer = (typeof replacer === undefined ? '':replacer);
		if(val.value.match(/\s/g)){
			val.value = val.value.replace(/^\s/g, '');
			val.value = val.value.replace(/\s/g, replacer);
		}
	}

	fn.parse_url = function(url) {
		// regExp = /^(?:https?:\/\/)?(?:www\.)?([^\/]+)/;
		var regExp = /^(https?:\/\/)?(www\.)?([^\/]+)(.*?)$/,
		part_of_uris = {};

		if (regExp.test(url)){
			uri_scheme = url.match(regExp);
			part_of_uris = {
				scheme:uri_scheme[1], 
				web:uri_scheme[2] !== undefined ? uri_scheme[2]: '', 
				domain:uri_scheme[3], 
				path_query:uri_scheme[4] !== undefined ? uri_scheme[4]:'/'
			}
		}

		return part_of_uris;
	}
	
	fn.convert_filesize = function(bytes){
		if (bytes == 0) return '0.00 B';

		var e = Math.floor(Math.log(bytes) / Math.log(1024));
		return (bytes/Math.pow(1024, e)).toFixed(2)+' '+' KMGTP'.charAt(e)+'B';
	}
	
	fn.array_sum = function(arr) {
		var r = 0;
		$.each(arr, function(i, v) {
			r += v;
		});
		return r;
	}

	fn.calcTime = function(city, offset){
		// create Date object for current location
		d = new Date();

		// convert to msec
		// add local time zone offset
		// get UTC time in msec
		utc = d.getTime() + (d.getTimezoneOffset() * 60000);

		// create new Date object for different city
		// using supplied offset
		date = new Date(utc + (3600000 * offset));

		// return time as a string
		return date
	}
	
	fn.time_ago = function(time){
		switch (typeof time) {
			case 'number': break;
			case 'string': time = +new Date(time.replace(/-/g, '/').replace(/[TZ]/g, ' ')); break;
			case 'object': if (time.constructor === Date) time = time.getTime(); break;
			default: time = +new Date();
		}

		// set to datetime object
		time = new Date(time);
		// get kuala lumpur offset with diff 60 min(1 hour local-ID)
		offset = time.getTimezoneOffset() - 60;

		// set to UTC time
		time = (time.getTime() + offset*60*1000);

		// set offset to indonesia offset
		offset = (offset + 60)
		// set to local-ID time
		time = new Date(time - (offset * 60 * 1000));

		var time_formats = [
			[60, 'seconds', 1], // 60
			[120, '1 minute ago', '1 minute from now'], // 60*2
			[3600, 'minutes', 60], // 60*60, 60
			[7200, '1 hour ago', '1 hour from now'], // 60*60*2
			[86400, 'hours', 3600], // 60*60*24, 60*60
			[172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
			[604800, 'days', 86400], // 60*60*24*7, 60*60*24
			[1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
			[2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
			[4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
			[29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
			[58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
			[2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
			[5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
			[58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
		];
		var seconds = (+new Date() - time) / 1000,
			token = 'ago', list_choice = 1;

		// if (seconds == 0) {
		if (seconds >= 0 && seconds < 1) {
			return 'Just now'
		}
		if (seconds < 0) {
			seconds = Math.abs(seconds);
			token = 'from now';
			list_choice = 2;
		}
		var i = 0, format;
		while (format = time_formats[i++])
			if (seconds < format[0]) {
				if (typeof format[2] == 'string')
					return format[list_choice];
				else
					return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
			}
		return time;
	}

	fn.number_format = function(number){
		var n = number.toString(), p = n.indexOf('.');
		return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function($0, i){
			return p<0 || i<p ? ($0+'.') : $0;
		});
	}
	
	return fn;

})();

$.fn.serializeObject = function () {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function () {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

$.fn.serializeObjectifyForm = function(){

	var self = this,
		json = {},
		push_counters = {},
		patterns = {
			"validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
			"key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
			"push":     /^$/,
			"fixed":    /^\d+$/,
			"named":    /^[a-zA-Z0-9_]+$/
		};


	this.build = function(base, key, value){
		base[key] = value;
		return base;
	};

	this.push_counter = function(key){
		if(push_counters[key] === undefined){
			push_counters[key] = 0;
		}
		return push_counters[key]++;
	};

	$.each($(this).serializeArray(), function(){

		// Skip invalid keys
		if(!patterns.validate.test(this.name)){
			return;
		}

		var k,
			keys = this.name.match(patterns.key),
			merge = this.value,
			reverse_key = this.name;

		while((k = keys.pop()) !== undefined){

			// Adjust reverse_key
			reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

			// Push
			if(k.match(patterns.push)){
				merge = self.build([], self.push_counter(reverse_key), merge);
			}

			// Fixed
			else if(k.match(patterns.fixed)){
				merge = self.build([], k, merge);
			}

			// Named
			else if(k.match(patterns.named)){
				merge = self.build({}, k, merge);
			}
		}

		json = $.extend(true, json, merge);
	});

	return json;
};

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
}

String.prototype.ucwords = function() {
    str = this.toLowerCase();
    return str.replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g, function($1){
        return $1.toUpperCase();
    });
}

var cleanString = function(content) {
	content = content ? content : "";
	
	return content.replace(/<(\w+) [^>]+?data-f-id[^>]*>.*?<\/\1>/gi, "");
}

$(document).ready(function() {
	// Remove Boostrap Modal Data If Modal On Event Hidden 
	$(document).on('hidden.bs.modal', '.modal', function(e) {
		var that = this;
		
		$.each($(that).find('form'), function(i, fr){
			fr.reset();
			if ($.fn.parsley) {
				$(fr).parsley().reset();
			}
			$(fr).find("select").val(null).trigger("change");
		});
		
		$(this).removeData('bs.modal');

		if ($('.modal.show').length > 0) {
			$('body').addClass('modal-open');
		}
	});

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true, 
		"newestOnTop": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}
});

$(document).on('click', function (e) {
	$('[data-toggle="popover"],[data-toggle="popover-icon"],[data-original-title]').each(function () {
		// the 'is' for buttons that trigger popups
		// the 'has' for icons within a button that triggers a popup
		if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
			(($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false  // fix for BS 3.3.6
		}

	});
});