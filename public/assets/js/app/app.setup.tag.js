$(function () {
    "use strict";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    var primaryLang = '';

    $.fn.getPrimaryLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang/primary' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data) {
                toastr["error"]("Primary lang isn't set.", "Failed");
            } else {
                primaryLang = json.data.code;
                $self.val(json.data.code).trigger({ 
                    type: "loaded.primary-lang", 
                    lang: json.data.code 
                });
            }
        });
    }

    $.fn.getTranslateToDropdown = function (exceptIDs) {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            exceptIDs = exceptIDs ? exceptIDs : [];

            // Select2 has been initialized
            var $request = $.ajax({
                url: '/lang' // wherever your data is actually coming from
            });

            $request.then(function (json) {
                var options = [];
                $.each(json.data.item, function (i, v) {
                    if ($.inArray(v.code, exceptIDs) === -1) {
                        options.push({ id: v.code, text: v.name })
                    }
                });

                if (options.length > 0) {
                    $self.select2({
                        placeholder: 'Transalte To',
                        allowClear: true,
                        minimumResultsForSearch: Infinity,
                        data: options
                    });
                    $self.closest(".form-group").removeClass("d-none");
                    $self.closest(".modal-header").addClass("pd-t-10 pd-b-10");
                } else {
                    $self.closest(".form-group").addClass("d-none");
                    $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
                }
            });
        }
    }

    $.fn.destroySelectDropdown = function () {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("select2-hidden-accessible")) {
                $(this).select2('destroy');
                $(this).find("option").not(':first').remove();
                $(this).removeAttr("data-select2-id tabindex aria-hidden");
            }
        });
    }

    $.fn.destroyTranslateToDropdown = function () {
        var $self = $(this);

        $self.destroySelectDropdown();
        $self.closest(".form-group").addClass("d-none");
        $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
    }

    var $tableTag = $("#datatable-tag").DataTable({
        responsive: false,
        language: {
            searchPlaceholder: "Search ...",
            sSearch: "",
            lengthMenu: "_MENU_ items/page",
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        processing: true,
        serverSide: true,
        deferRender: true,
        ajax: {
            url: "/tag",
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);

                json.recordsTotal = json.data ? json.data.total : 0;
                json.recordsFiltered = json.data ? json.data.total : 0;
                // json.recordsFiltered = json.data.item ? json.data.item.length : 0;
                json.data = json.data && json.data.item ? json.data.item : [];

                return JSON.stringify(json); // return JSON string
            }
        },
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                className: "align-middle",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: "name" },
            { data: "title" },
            {
                data: "lang.code",
                render: function (data, type, row, meta) {
                    return row.lang.code.toUpperCase();
                }
            },
            { data: "created_at" },
            {
                data: "actions",
                className: "align-middle",
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a href="/tag/update/' + row.id + '" class="badge badge-secondary" data-trigger="update">Edit</a> \
                            <a href="/tag/' + row.id + '" class="badge badge-danger" data-trigger="delete">Delete</a>';
                }
            }
        ],
        order: [[1, "asc"]]
    });

    $("input.primary-lang").getPrimaryLang();

    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    $(document).on("click", ".btn-refresh-table", function (e) {
        $tableTag.draw();
    });

    $(document).on("click", "[data-trigger=update]", function (e) {
        e.preventDefault();

        var url = $(this).attr('href');
        var $form = $(".form-tag.update");
        var $defaultForm = $form.html();
        var $modal = $form.closest(".modal");

        var $tabs = $modal.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $modal.find(".tab-content");
        var tabPaneContent = $('<div>').append($tabContent.find(".tab-pane").first().clone(true)).html();

        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (json) {
            if (json.success) {
                var tag = json.data;

                var primaryTag = [];
                var otherTag = [];
                $.each(tag.items, function (i, item) {
                    if (item.lang.code == primaryLang) {
                        primaryTag.push(item);
                    } else {
                        otherTag.push(item);
                    }
                });
                var tags = $.merge(primaryTag, otherTag);
                
                $form.find("input[name=id]").val(tag.id);

                $tabs.html("");
                $tabContent.html("");
                $.each(tags, function (i, item) {
                    var $thisTab = $(tab);
                    var $thisTabPane = $(tabPaneContent);

                    var href = $thisTab.attr("href").replace("default", item.lang.code);
                    var tabID = href.replace("#", "");

                    $thisTab.attr("href", href);
                    $thisTab.html(item.lang.name);

                    $tabs.append($thisTab);

                    $thisTabPane.attr("id", tabID);
                    $thisTabPane.find("input[name=lang]").val(item.lang.code);
                    $thisTabPane.find("input[name=name]").val(item.name);
                    $thisTabPane.find("input[name=title]").val(item.title);

                    $tabContent.append($thisTabPane);
                });
                $tabs.find('a.nav-link:first-child').tab('show');

                $modal.modal("show").data({
                    defaultForm: $defaultForm,
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete]", function (e) {
        e.preventDefault();

        var tag = $(this).closest("tr").find("td:eq(1)").text();
        var accept = confirm("Are you sure to delete " + tag + " ?");
        if (!accept) return;

        var $btn = $(this);
        var url = $(this).attr('href');
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");

                $tableTag.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete-tab]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $modal.find("form");

        var $translateToDropdown = $modal.find('.select2.translate-to');

        var $tabs = $form.find(".nav.nav-tabs");
        var $navTabLinks = $tabs.find("a.nav-link");
        var $activeTab = $tabs.find("a.nav-link.active");

        var $tabContent = $form.find(".tab-content");
        var $activeTabPane = $tabContent.find(".tab-pane.active");

        var activeLang = $activeTab.text();
        var accept = confirm("Delete this tag in " + activeLang + " language ?");
        if (!accept) return;

        if ($activeTab.hasClass("new")) {
            $activeTab.remove();
            $activeTabPane.remove();
            $tabs.find('a.nav-link:last-child').tab("show");

            var langIDs = $modal.find("input[name=lang]").map(function () {
                return this.value;
            }).get();
            $translateToDropdown.destroyTranslateToDropdown();
            $translateToDropdown.getTranslateToDropdown(langIDs);
        } else {
            var id = $form.find("input[name=id]").val();
            var lang = $activeTabPane.find("input[name=lang]").val();
            var url = $self.attr("href");
            var btnText = $self.html();

            $.ajaxq("aq", {
                url: url + '/' + id + '/' + lang,
                type: "delete",
                beforeSend: function () {
                    $self.html(waitText);
                }
            }).done(function (jqXHR) {
                if (jqXHR.success) {
                    toastr["success"](jqXHR.message, "Success");

                    if ($navTabLinks.length > 1) {
                        $activeTab.remove();
                        $activeTabPane.remove();
                        $tabs.find('a.nav-link:last-child').tab("show");
                    }

                    var langIDs = $modal.find("input[name=lang]").map(function () {
                        return this.value;
                    }).get();
                    $translateToDropdown.destroyTranslateToDropdown();
                    $translateToDropdown.getTranslateToDropdown(langIDs);

                    $tableTag.draw();
                }
            }).always(function (jqXHR) {
                if (!jqXHR.success) {
                    var r = jqXHR.responseJSON;
                    var message = r ? r.error : jqXHR.statusText;
                    toastr["error"](message, "Failed");
                }
                $self.html(btnText);
            });
        }
    });

    $(document).on("submit", ".form-tag", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $modal = $form.closest(".modal");
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr("action");
        var type = $form.attr("type");
        var btnText = $btnSave.html();
        var isUpdateMode = $form.hasClass("update");

        var data;
        if (isUpdateMode) {
            var id = $form.find("input[name=id]").val();
            var params = [];
            $form.find(".tab-content .tab-pane").each(function (index) {
                var param = $(this).find("input[name], select[name], textarea[name]").serializeObject();
                params.push(param);
            });

            url = url + "/" + id;
            data = JSON.stringify(params);
        } else {
            var param = $form.serializeObject();
            if (param.amenities && typeof param.amenities === 'string') {
                param.amenities = [param.amenities]
            }
            data = JSON.stringify(param);
        }

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            contentType: "application/json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
                $tableTag.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }

            $btnSave.prop('disabled', false).html(btnText);
            $modal.modal("hide");
        });
    });

    $(document).on("select2:select", "select.translate-to", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $modal.find("form.form-tag");
        var defaultForm = $modal.data("defaultForm");
        var $translateToDropdown = $modal.find('.select2.translate-to');

        var lang = $(this).select2("data")[0];

        var $tabs = $modal.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $modal.find(".tab-content");
        
        var $tab = $(tab);
        var $tabPane = $(defaultForm).find(".tab-pane");

        var href = $tab.attr("href").replace(/\w+$/, lang.id);
        var tabID = href.replace("#", "");

        $tab.removeClass("active show");
        $tabPane.removeClass("active show");
        $tabPane.find(".fr-box").remove();

        $tab.attr("href", href);
        $tab.html(lang.text);
        $tab.addClass("new");
        $tabs.append($tab);

        $tabPane.attr("id", tabID);
        $tabPane.find("input[name=lang]").val(lang.id);
        $tabPane.find("input[name=name]").val("");
        $tabPane.find("input[name=title]").val("");
        
        $tabContent.append($tabPane);
        $tabs.find('a.nav-link:last-child').tab("show");
        
        $tabPane.find("input[name=show_date]").each(function (i) {
            $(this).datepicker({
                dateFormat: "yy-mm-dd",
            });
        });

        $tabPane.find("input[name=end_date]").each(function (i) {
            $(this).datepicker({
                dateFormat: "yy-mm-dd",
            });
        });

        var langIDs = $form.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.getTranslateToDropdown(langIDs);
    });

    $(document).on("show.bs.tab", "[data-toggle=tab]", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $btnDelete = $modal.find("[data-trigger=delete-tab]");

        if ($self.index() > 0) {
            $btnDelete.removeClass("d-none");
        } else {
            $btnDelete.addClass("d-none");
        }
    });

    $(document).on("shown.bs.modal", "#modal-form-tag, #modal-form-tag-update", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form");
        var isModalUpdate = $form.hasClass("update");

        if (isModalUpdate) {
            var langIDs = $modal.find("input[name=lang]").map(function () {
                return this.value;
            }).get();

            $('.select2.translate-to').getTranslateToDropdown(langIDs);
        } else {
            var $defaultForm = $form.html();
            $modal.data("defaultForm", $defaultForm);
        }
    });

    $(document).on("hidden.bs.modal", "#modal-form-tag, #modal-form-tag-update", function (e) {
        var $modal = $(this);
        var defaultForm = $modal.data('defaultForm');
        var isModalUpdate = $modal.find("form").hasClass("update");

        if (isModalUpdate) {
            $modal.find('.select2.translate-to').destroyTranslateToDropdown();
        }

        $modal.find('form.form-tag').html(defaultForm);
        $modal.removeData();
    });
});