$(function () {
    "use strict";

    var languages = {};

    $.fn.loadLanguange = function() {
        var $self = $(this);

        $.ajaxq("aq", {
            url: '/lang',
            dataType: 'json',
        }).done(function (json) {
            var html = '';
            var items = json.data.item ? json.data.item : [];

            $.each(items, function (i, item) {
                html += '<tr> \
                    <td><strong>'+ item.code.toUpperCase() +'</strong></td > \
                    <td>'+ item.name +'</td> \
                    <td>'+ item.native +'</td> \
                    <td> \
                        <a href="/lang/change_status/'+ item.id +'/'+ item.status +'" data-trigger="change-status"> \
                            <span class="badge badge-'+ item.status + '">' + item.status +'</span> \
                        </a> \
                    </td> \
                    <td><a href="/lang/'+ item.id +'" data-trigger="delete-lang" class="badge badge-danger tx-bold" style="color: #fff;">X</a></td> \
                </tr>';
            });

            if (items.length == 0) {
                $(".alert.langInfo").removeClass("d-none");
                $self.closest(".table-responsive").addClass("d-none");
            } else {
                $(".alert.langInfo").addClass("d-none");
                $self.closest(".table-responsive").removeClass("d-none");
                $self.html(html);
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    $('.select2.lang').select2({
        placeholder: 'Select Language',
        allowClear: true,
        minimumResultsForSearch: Infinity,
        ajax: {
            url: "/lang/list",
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (json, params) {
                params.page = params.page || 1;

                var options = [];

                languages = json.data;
                $.each(languages, function (i, v) {
                    options.push({ id: i, text: i.toUpperCase() + ' - ' + v.name })
                });

                return {
                    results: options
                };
            }
        },
    }).on("select2:select", function (e) {
        var $form = $(this).closest("form");

        var index = $(this).val();
        var language = languages[index];

        $form.find("input[name=name]").val(language.name);
        $form.find("input[name=native]").val(language.native);
    }).on("select2:clear", function (e) {
        var $form = $(this).closest("form");

        $form.find("input[name=name]").val("");
        $form.find("input[name=native]").val("");
    });

    $(document).on("submit", ".form-setup.lang", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr('action');
        var data = $form.serialize();
        var type = $form.attr('type');
        var btnText = $btnSave.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");

                $("table.actived-language > tbody").loadLanguange();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);

            $form.find("select[name=code]").val(null).trigger("change");
            $form.find("input[name=name]").val("");
            $form.find("input[name=native]").val("");
            $form.parsley().reset();
        });
    });

    $(document).on("click", "[data-trigger=delete-lang]", function (e) {
        e.preventDefault();
        var $self = $(this);

        var name = $self.closest("tr").find("td").eq(1).text();
        var accept = confirm("Are you sure to delete language " + name + " ?");
        if (!accept) return;

        var url = $self.attr("href");
        var btnText = $self.html();

        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $self.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");

                $self.closest("tbody").loadLanguange();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $self.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=change-status]", function (e) {
        e.preventDefault();
        var $self = $(this);

        var url = $self.attr("href");
        var btnText = $self.html();

        $.ajaxq("aq", {
            url: url,
            type: "put",
            beforeSend: function () {
                $self.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");

                $self.closest("tbody").loadLanguange();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $self.html(btnText);
        });
    });

    $("table.actived-language > tbody").loadLanguange();
});