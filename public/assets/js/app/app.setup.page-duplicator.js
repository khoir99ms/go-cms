$(function () {
    "use strict";
    
    $.fn.getPropertyDropdown = function () {
        var $dropdown = $(this);

        // Select2 has been initialized
        var $request = $.ajax({
            url: '/utils/brands/property' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            var options = [];
            $.each(json.data.item, function (i, brand) {
                options.push({ id: brand.id, text: "<div data-is-brand='true' class='tx-semibold'>" + brand.name + "</div>" });

                $.each(brand.properties, function (i, property) {
                    options.push({ id: property.id, text: "<div class='pd-l-15'>" + property.name + "</div>"});
                });
            });

            $dropdown.each(function () {
                var $self = $(this);

                $self.select2({
                    placeholder: $self.attr("placeholder"),
                    allowClear: true,
                    searchInputPlaceholder: 'Search',
                    data: options,
                    escapeMarkup: function (m) { return m; },
                }).on("select2:select", function (e) {
                    var $parent  = $(this).closest(".form-group");
                    var isBrand  = $(e.params.data.text).data("is-brand");
                    
                    $parent.find("input[type=hidden]").val(isBrand ? "brand" : "property");
                }).on("select2:clear", function (e) {
                    var $parent  = $(this).closest(".form-group");
            
                    $parent.find("input[type=hidden]").val("");
                });
            });
            
        });
    }

    $("select[name='source[id]'], select[name='destination[id]']").getPropertyDropdown();

    $("select[name='source[category_id]']").select2({
        placeholder: 'Select Category',
        allowClear: true,
        searchInputPlaceholder: 'Search',
        ajax: {
            url: "/category/primary",
            dataType: "json",
            data: function (params) {
                return {
                    search: {value: params.term}, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                var options = [];
                $.each(data.data.item, function (i, v) {
                    options.push({ id: v.id, text: v.name });
                });

                return {
                    results: options
                };
            }
        },
    });

    $(document).on("submit", ".form-setup.duplicator", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $alert = $form.find(".alert.info");
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr("action");
        var data = $form.serialize();
        var type = $form.attr("type");
        var btnText = $btnSave.html();
        
        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + waitText);
            }
        }).done(function (json) {
            if (json.success) {
                toastr["success"](json.message, "success");

                $alert.removeClass("d-none");
                $alert.html("Duplicated <strong>"+ json.data.duplicated +" pages</strong>, Failed <strong>"+ json.data.failed +" pages.</strong>");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });
});