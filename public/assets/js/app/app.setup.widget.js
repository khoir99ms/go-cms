$(function () {
    "use strict";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    $.fn.generateAceEditor = function () {
        var $self = $(this);

        $self.each(function (i) {
            var $textarea = $(this);
            var mode = $textarea.data('editor');

            var $container = $('<div>', {
                'class': 'ace-editor ace-editor-' + i
            }).insertBefore($textarea);
            $textarea.addClass("d-none");

            var editor = ace.edit($container[0]);
            editor.setOptions({
                maxLines: 50
            });
            
            editor.getSession().setValue($textarea.val());
            editor.getSession().setMode("ace/mode/" + mode);

            editor.getSession().on("change", function (e) {
                var value = editor.getSession().getValue();
                $textarea.val(value);
            });
        });
    }

    $.fn.destroyAceEditor = function () {
        var $self = $(this);

        $self.each(function () {
            var $editor = $(this);
            if ($editor.length > 0) {
                $editor[0].env.editor.destroy();
                $editor.remove();
            }
        });
    }

    var $tableWidget = $("#datatable-widget").DataTable({
        responsive: false,
        language: {
            searchPlaceholder: "Search ...",
            sSearch: "",
            lengthMenu: "_MENU_ items/page",
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        processing: true,
        serverSide: true,
        deferRender: true,
        ajax: {
            url: "/widget",
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);

                json.recordsTotal = json.data.total;
                json.recordsFiltered = json.data.total;
                // json.recordsFiltered = json.data.item ? json.data.item.length : 0;
                json.data = json.data.item ? json.data.item : [];

                return JSON.stringify(json); // return JSON string
            }
        },
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                className: "align-middle",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: "name" },
            { data: "type" },
            { data: "created_at" },
            {
                data: "actions",
                className: "align-middle",
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a href="/widget/' + row.id + '" class="badge badge-secondary" data-trigger="update">Edit</a> \
                            <a href="/widget/' + row.id + '" class="badge badge-danger" data-trigger="delete">Delete</a>';
                }
            }
        ],
        order: [[1, "asc"]]
    });

    $(document).on("click", ".btn-refresh-table", function (e) {
        $tableWidget.draw();
    });

    // Select2
    $(".dataTables_length select").select2({ minimumResultsForSearch: Infinity });

    $("select[name=widget]").select2({
        placeholder: "Widget Type",
        minimumResultsForSearch: Infinity
    });

    $(document).on("submit", ".form-widget", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr("action");
        var type = $form.attr("type");
        var btnText = $btnSave.html();
        var data = $form.serialize();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
                $tableWidget.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
            $form.closest(".modal").modal("hide");
        });
    });

    $(document).on("click", "[data-trigger=update]", function (e) {
        e.preventDefault();

        var url = $(this).attr('href');
        var $form = $(".form-widget.update");
        var $modal = $form.closest(".modal");

        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (json) {
            if (json.success) {
                var data = json.data;

                $form.find("input[name=id]").val(data.id);
                $form.find("input[name=name]").val(data.name);
                $form.find("input[name=code]").val(data.code);
                $form.find("select[name=widget]").val(data.widget).trigger("change");
                $form.find("textarea[name=content]").val(data.content);

                $modal.modal("show");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete]", function (e) {
        e.preventDefault();

        var category = $(this).closest("tr").find("td:eq(1)").text();
        var accept = confirm("Are you sure to delete " + category + " ?");
        if (!accept) return;

        var $btn = $(this);
        var url = $(this).attr('href');
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");

                $tableWidget.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("show.bs.modal", "#modal-form-widget, #modal-form-widget-update", function (e) {
        var $modal = $(this);

        $modal.find("textarea[data-editor]").generateAceEditor();
    });

    $(document).on("hidden.bs.modal", "#modal-form-widget, #modal-form-widget-update", function (e) {
        var $modal = $(this);

        $modal.find(".ace_editor").destroyAceEditor();
        $modal.find("select[name=widget]").get(0).selectedIndex = 0;
        $modal.find("select[name=widget]").trigger("change");
    });
});