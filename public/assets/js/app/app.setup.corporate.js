$(function () {
    "use strict";
    
    var allLang = [];
    var primaryLang = "";
    var primaryLangJson = {};

    var arrangeTab = function (tagline, description) {
        var tabData = { tagline: {}, description: {} };

        if (typeof tagline == "string") {
            tabData["tagline"][primaryLang] = tagline;
        } else {
            if ($.isEmptyObject(tagline)) {
                tabData["tagline"][primaryLang] = "";
            } else {
                tabData["tagline"] = tagline;
            }
        }
        if (typeof description == "string") {
            tabData["description"][primaryLang] = description;
        } else {
            if ($.isEmptyObject(description)) {
                tabData["description"][primaryLang] = "";
            } else {
                tabData["description"] = description;
            }
        }

        var primary = {}, others = {};
        $.each(tabData, function (key, items) {
            $.each(items, function (lang, item) {
                if (lang == primaryLang) {
                    if (primary["lang"] === undefined) {
                        primary["lang"] = lang;
                    }
                    primary[key] = item;
                } else {
                    if (others[lang] === undefined) {
                        others[lang] = {};
                    }
                    if (others[lang]["lang"] === undefined) {
                        others[lang]["lang"] = lang;
                    }
                    others[lang][key] = item;
                }
            });
        });

        var results = [primary];
        $.each(others, function (i, other) {
            results.push(other);
        });

        return results;
    }

    $.fn.getAllLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data.item) {
                toastr["error"]("All lang isn't set.", "Failed");
            } else {
                allLang = json.data.item;
                $self.trigger("loaded.all-lang");
            }
        });
    }

    $.fn.getPrimaryLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang/primary' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data) {
                toastr["error"]("Primary lang isn't set.", "Failed");
            } else {
                primaryLang = json.data.code;
                primaryLangJson = json.data;
                $self.trigger({
                    type: "loaded.primary-lang",
                    lang: json.data.code
                });
            }
        });
    }

    $.fn.generateTranslateToDropdown = function (exceptIDs) {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            exceptIDs = exceptIDs ? exceptIDs : [];

            // Select2 has been initialized
            var options = [];
            $.each(allLang, function (i, v) {
                if ($.inArray(v.code, exceptIDs) === -1) {
                    options.push({ id: v.code, text: v.name })
                }
            });

            if (options.length > 0) {
                $self.select2({
                    placeholder: 'Transalte To',
                    allowClear: true,
                    minimumResultsForSearch: Infinity,
                    data: options
                });
                $self.closest(".form-group").removeClass("d-none");
            } else {
                $self.closest(".form-group").addClass("d-none");
            }
        }
    }

    $.fn.destroySelectDropdown = function () {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("select2-hidden-accessible")) {
                $(this).select2('destroy');
                $(this).find("option").not(':first').remove();
                $(this).removeAttr("data-select2-id tabindex aria-hidden");
            }
        });
    }

    $.fn.destroyTranslateToDropdown = function () {
        var $self = $(this);

        $self.destroySelectDropdown();
        $self.closest(".form-group").addClass("d-none");
    }

    $.fn.generateTinyMce = function(options) {
        var $self = $(this);

        var defaults = {
            max_height: 800,
            placeholder: 'Type here ...',
            branding: false,
            // menubar: false,
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons autoresize',
            menubar: 'file edit view insert format tools table help',
            toolbar: 'preview code | undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | superscript subscript | outdent indent | table | numlist bullist | forecolor backcolor removeformat | hr pagebreak | charmap emoticons | fullscreen save print | insertfile image media template link anchor codesample | ltr rtl',
            image_caption: true,
            image_advtab: true,
            toolbar_sticky: true,
            toolbar_mode: 'sliding',
            importcss_append: true,
            allow_script_urls: true,
            relative_urls : false,
            remove_script_host : false,
            imagetools_toolbar: "imageoptions",
            content_css: ['/static/lib/tinymce/skins/content/default/content.min.css', '/static/lib/fontawesome-free/css/all.min.css'],
            quickbars_selection_toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | h1 h2 h3 blockquote | quicklink quicktable',
            quickbars_insert_toolbar: false,
            contextmenu: "link image table",
        };

        // Merge defaults and options, without modifying defaults
        var settings = $.extend({}, defaults, options ? options : {});

        $self.each(function () {
            $(this).tinymce(settings);
            $(this).addClass("has-tinymce");
        });
    }

    $.fn.destroyTinyMce = function() {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("has-tinymce")) {
                $(this).tinymce().destroy();
            }
        });
    }

    $.fn.loadCorporateData = function() {
        var $form = $(this);
        var $defaultForm = $form.html();

        $.ajaxq("aq", {
            url: "/setup/corporate/data",
        }).done(function (jqXHR) {
            if (!jqXHR.success) {
                toastr["error"](jqXHR.message, "success");
            } else {
                var corporate = jqXHR.data.item;

                $form.find("input[name=corporate_id]").val(corporate.property_id);
                $form.find("input[name=corporate_name]").val(corporate.property_name);
                $form.find("input[name=corporate_display_name]").val(corporate.property_display_name);
                $form.find("input[name=phone]").val(corporate.property_phone);
                $form.find("input[name=fax]").val(corporate.property_fax);
                $form.find("input[name=email]").val(corporate.property_email);
                $form.find("textarea[name=address]").val(corporate.property_address);
                $form.find("input[name=latitude]").val(corporate.property_latitude);
                $form.find("input[name=longitude]").val(corporate.property_longitude);
                $form.find("input[name=booking_url]").val(corporate.property_booking_url);

                // Set the value, creating a new option if necessary
                if ($form.find("select[name=city]").find("option[value='" + corporate.city.code + "']").length) {
                    $form.find("select[name=city]").val(corporate.city.code).trigger('change');
                } else {
                    // Create a DOM Option and pre-select by default
                    var newOption = new Option(corporate.city.name, corporate.city.code, true, true);
                    // Append it to the select
                    $form.find("select[name=city]").append(newOption).trigger('change');
                }

                var tabData = arrangeTab(corporate.property_tagline, corporate.property_description);
                var $tabs = $form.find(".nav.nav-tabs");
                var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

                var $tabContent = $form.find(".tab-content");
                var tabPaneContent = $('<div>').append($tabContent.find(".tab-pane").first().clone(true)).html();

                $tabs.html("");
                $tabContent.html("");
                $.each(tabData, function (i, item) {
                    var $thisTab = $(tab);
                    var $thisTabPane = $(tabPaneContent);

                    var sourceLang = $.grep(allLang, function (v) {
                        return v.code == item.lang;
                    });
                    var itemLang = sourceLang.length > 0 ? sourceLang[0] : sourceLang;

                    var href = $thisTab.attr("href").replace("default", itemLang.code);
                    var tabID = href.replace("#", "");

                    $thisTab.attr("href", href);
                    $thisTab.html(itemLang.name);

                    $tabs.append($thisTab);

                    $thisTabPane.attr("id", tabID);
                    $thisTabPane.find("input[name=lang]").val(itemLang.code);
                    $thisTabPane.find("input[name=tagline]").val(item.tagline);
                    $thisTabPane.find("textarea[name=description]").val(item.description);

                    $tabContent.append($thisTabPane);
                });
                $tabs.find('a.nav-link:first-child').tab('show');

                var logo = corporate.property_logo;
                if (logo) {
                    $form.find("[data-trigger=delete-logo]").removeClass("d-none");
                }
                $form.find("input[name=logo]").val(logo);
                logo = logo.match(/^http/) ? logo : "/static/uploads/media/" + logo;
                $form.find("img.logo").attr("src", logo);

                var secondaryLogo = corporate.property_secondary_logo ? corporate.property_secondary_logo : "";
                if (secondaryLogo) {
                    $form.find("[data-trigger=delete-secondary-logo]").removeClass("d-none");
                }
                $form.find("input[name=secondary_logo]").val(secondaryLogo);
                secondaryLogo = secondaryLogo.match(/^http/) ? secondaryLogo : "/static/uploads/media/" + secondaryLogo;
                $form.find("img.secondary-logo").attr("src", secondaryLogo);

                var favicon = corporate.property_favicon ? corporate.property_favicon : "";
                if (favicon) {
                    $form.find("[data-trigger=delete-favicon]").removeClass("d-none");
                }
                $form.find("input[name=favicon]").val(favicon);
                favicon = favicon.match(/^http/) ? favicon : "/static/uploads/media/" + favicon;
                $form.find("img.favicon").attr("src", favicon);

                var featuredImage = corporate.property_featured_image;
                if (featuredImage) {
                    $form.find("[data-trigger=delete-fi]").removeClass("d-none");
                }
                $form.find("input[name=featured_image]").val(featuredImage);
                featuredImage = featuredImage.match(/^http/) ? featuredImage : "/static/uploads/media/" + featuredImage;
                $form.find("img.featured-image").attr("src", featuredImage);

                var imageAttachment = "";
                if (corporate.property_image_attachment) {
                    $.each(corporate.property_image_attachment, function (i, ia) {
                        var iaUrl = ia.match(/^http/) ? ia : "/static/uploads/media/" + ia;
                        imageAttachment += ' \
                        <div class="col-lg-3 col-md-4 col-6 ai-item"> \
                        <div class="d-block mb-4 h-100 pos-relative"> \
                            <img data-image="'+ ia + '" class="img-fluid img-thumbnail" src="' + iaUrl + '" onerror="this.src=\'/static/images/default-img.png\';" alt="" style="width: 200px; height: 100px"> \
                            <div class="ai-toolbar pos-absolute" style="top: 3px; right: 8px;"> \
                                <a href="#" data-trigger="delete-ai" class="badge badge-danger tx-bold">X</a> \
                            </div> \
                        </div> \
                        </div >';
                    });
                }
                $form.find("input[name=attachment_image]").val(corporate.property_image_attachment).data({
                    values: corporate.property_image_attachment,
                    current_values: corporate.property_image_attachment
                });
                $form.find(".image-attachment").html(imageAttachment);
                
                $form.trigger("loaded.corporate-data").data({
                    defaultForm: $defaultForm,
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    $(document).getAllLang();
    $(document).getPrimaryLang();
    
    $(document).on("loaded.primary-lang", function(){
        var $form = $(".form-setup.corporate");

        $form.loadCorporateData();
    });

    $(document).on("loaded.corporate-data", ".form-setup.corporate", function () { 
        var $form = $(this);
        var $card = $form.find(".card");

        var langIDs = $card.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $card.find('select.translate-to').generateTranslateToDropdown(langIDs);
        $card.find('textarea[name=description]').generateTinyMce({menubar: false});
    });

    $(document).on("submit", ".form-setup.corporate", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");

        var btnText = $btnSave.html();
        var url = $form.attr('action')
        var type = $form.attr('type');
        
        // clean editor
        $form.find("textarea[name]").each(function () {
            var content = cleanString($(this).val());
            $(this).val(content);
        });
        
        var data = $form.serializeObject();
        var tagline = {}, desc = {};
        
        $form.find(".tab-pane").each(function () {
            var tab = $(this).find("input[name], select[name], textarea[name]").serializeObject();
            
            tagline[tab.lang] = tab.tagline;
            desc[tab.lang] = tab.description;
        });
        data.tagline = tagline;
        data.description = desc;
        data.attachment_image = $form.find("input[name=attachment_image]").data("values");

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            dataType: "json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete-logo]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $form = $(this).closest("form");
        $form.find("input[name=logo]").val("");
        $form.find("img.logo").attr("src", "#");
        $form.find("[data-trigger=delete-logo]").addClass("d-none");
    });

    $(document).on("click", "[data-trigger=delete-secondary-logo]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $form = $(this).closest("form");
        $form.find("input[name=secondary_logo]").val("");
        $form.find("img.secondary-logo").attr("src", "#");
        $form.find("[data-trigger=delete-secondary-logo]").addClass("d-none");
    });

    $(document).on("click", "[data-trigger=delete-favicon]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $form = $(this).closest("form");
        $form.find("input[name=favicon]").val("");
        $form.find("img.favicon").attr("src", "#");
        $form.find("[data-trigger=delete-favicon]").addClass("d-none");
    });

    $(document).on("click", "[data-trigger=delete-fi]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $form = $(this).closest("form");
        $form.find("input[name=featured_image]").val("");
        $form.find(".featured-image").attr("src", "#");
        $form.find("[data-trigger=delete-fi]").addClass("d-none");
    });

    $(document).on("click", "[data-trigger=delete-ai]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $self = $(this);
        var $form = $self.closest("form");

        var images = $form.find("input[name=attachment_image]").data("values");
        var itemToRemove = $self.closest(".ai-item").find("img").data("image");

        images.splice($.inArray(itemToRemove, images), 1);
        $form.find("input[name=attachment_image]").data({
            values: images,
            current_values: images,
        }).val(images);

        $self.closest(".ai-item").remove();
    });

    $(document).on("click", "[data-trigger=delete-tab]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $card = $self.closest(".card");

        var $translateToDropdown = $card.find('select.translate-to');

        var $tabs = $card.find(".nav.nav-tabs");
        var $navTabLinks = $tabs.find("a.nav-link");
        var $activeTab = $tabs.find("a.nav-link.active");

        var $tabContent = $card.find(".tab-content"); primaryLangJson
        var $activeTabPane = $tabContent.find(".tab-pane.active");

        var activeLang = $activeTab.text();
        var accept = confirm("Delete this translation " + activeLang + " ?");
        if (!accept) return;

        if ($navTabLinks.length > 1) {
            $activeTab.remove();
            $activeTabPane.remove();
            $tabs.find('a.nav-link:last-child').tab("show");
        }

        var langIDs = $card.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.generateTranslateToDropdown(langIDs);
    });

    $(document).on("select2:select", "select.translate-to", function (e) {
        var $self = $(this);
        var $form = $self.closest("form");
        var $parent = $self.closest(".card");

        var defaultForm = $form.data("defaultForm");
        var lang = $self.select2("data")[0];

        var $tabs = $parent.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $parent.find(".tab-content");

        var $tab = $(tab);
        var $tabPane = $(defaultForm).find(".tab-pane");

        var href = $tab.attr("href").replace(/\w+$/, lang.id);
        var tabID = href.replace("#", "");

        $tab.removeClass("active show");
        $tabPane.removeClass("active show");

        $tab.attr("href", href);
        $tab.html(lang.text);
        $tab.addClass("new");
        $tabs.append($tab);

        $tabPane.attr("id", tabID);
        $tabPane.find("input[name=lang]").val(lang.id);
        $tabPane.find("input[name=tagline]").val("");
        $tabPane.find("textarea[name=description]").val("");
        $tabPane.find("textarea[name=content]").val("");

        $tabContent.append($tabPane);
        $tabs.find('a.nav-link:last-child').tab("show");

        $tabPane.find('textarea[name=description]').generateTinyMce({menubar: false});

        var langIDs = $parent.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $self.destroyTranslateToDropdown();
        $self.generateTranslateToDropdown(langIDs);
    });

    $(document).on("selected.media.library", "input[name=logo]", function (e) {
        var $form = $(this).closest("form");
        var image = e.value.match(/^http/) ? e.value : "/static/uploads/media/" + e.value;

        $form.find("[data-trigger=delete-logo]").removeClass("d-none");
        $form.find("img.logo").attr("src", image);
    });

    $(document).on("selected.media.library", "input[name=secondary_logo]", function (e) {
        var $form = $(this).closest("form");
        var image = e.value.match(/^http/) ? e.value : "/static/uploads/media/" + e.value;

        $form.find("[data-trigger=delete-secondary-logo]").removeClass("d-none");
        $form.find("img.secondary-logo").attr("src", image);
    });

    $(document).on("selected.media.library", "input[name=favicon]", function (e) {
        var $form = $(this).closest("form");
        var image = e.value.match(/^http/) ? e.value : "/static/uploads/media/" + e.value;

        $form.find("[data-trigger=delete-favicon]").removeClass("d-none");
        $form.find("img.favicon").attr("src", image);
    });

    $(document).on("selected.media.library", "input[name=featured_image]", function (e) {
        var $form = $(this).closest("form");
        var image = e.value.match(/^http/) ? e.value : "/static/uploads/media/" + e.value;

        $form.find("[data-trigger=delete-fi]").removeClass("d-none");
        $form.find("img.featured-image").attr("src", image);
    });

    $(document).on("selected.multiple-media.library", "input[name=attachment_image]", function (e) {
        var $parent = $(this).closest("form").find(".image-attachment");
        var currentValues = $(this).data("current_values");
        var images = e.value ? e.value : [];
        images = $.merge((currentValues ? currentValues : []), images);
        $(this).closest("form").find("input[name=attachment_image]").val(images).data({ values: images, current_values: images });

        var imageAttachment = "";
        $.each(images, function (i, ia) {
            var iaUrl = ia.match(/^http/) ? ia : "/static/uploads/media/" + ia;
            imageAttachment += ' \
            <div class="col-lg-3 col-md-4 col-6 ai-item"> \
                <div class="d-block mb-4 h-100 pos-relative"> \
                    <img data-image="'+ ia + '" class="img-fluid img-thumbnail" src="' + iaUrl + '" onerror="this.src=\'/static/images/default-img.png\';" alt="" style="width: 200px; height: 100px"> \
                    <div class="ai-toolbar pos-absolute" style="top: 3px; right: 8px;"> \
                        <a href="#" data-trigger="delete-ai" class="badge badge-danger tx-bold">X</a> \
                    </div> \
                </div> \
            </div>';
        });
        $parent.html(imageAttachment);
    });

    $(document).on("show.bs.tab", "[data-toggle=tab]", function (e) {
        var $self = $(this);
        var tabID = $self.attr("href");
        var $btnDelete = $(tabID).find("[data-trigger=delete-tab]");

        if ($self.index() > 0) {
            $btnDelete.removeClass("d-none");
        } else {
            $btnDelete.addClass("d-none");
        }
    });

    $('.select2.city').select2({
        placeholder: 'Choose one',
        searchInputPlaceholder: 'Search',
        ajax: {
            url: "/utils/cities",
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                var options = [];
                $.each(data.data, function (i, v) {
                    options.push({ id: v.code, text: v.name })
                });

                return {
                    results: options
                };
            }
        },
    });
});