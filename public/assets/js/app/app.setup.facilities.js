$(function () {
    "use strict";

    var primaryLang = '';
    var facilities = [];
    var facilityGroupedLang = {};
    var amenities = [];
    var amenitiesGroupedLang = {};
    var iconpickerOpts = {
        arrowClass: 'btn-primary',
        arrowPrevIconClass: 'fas fa-angle-left',
        arrowNextIconClass: 'fas fa-angle-right',
        cols: 6,
        iconset: 'fontawesome5',
        rows: 5,
        search: true,
        searchText: 'Search',
        labelHeader: '{0} - {1} pages',
        labelFooter: '{0} - {1} of {2} icons',
        selectedClass: 'btn-success',
        unselectedClass: ''
    };

    var getFacilityGroupedLang = function (type) {
        type = type ? type : 'facility';
        var url = (type == 'facility' ? '/facility' : '/amenities') + '/grouped-lang';

        var $request = $.ajax({
            url: url, // wherever your data is actually coming from
        });

        $request.then(function (json) {
            var data = json.data ? json.data : [];
            if (type == "facility") {
                facilityGroupedLang = data;
            } else {
                amenitiesGroupedLang = data;
            }
            $(document).trigger("loaded.facility." + type);
        });

        $request.always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    var serializeFacilitiesTree = function (json, type) {        
        var tree = [];
        $.each(json, function (i, item) {
            var text = '';
            var children = item.children;

            var $icon = $.parseHTML(item.icon);
            var isImg = $icon.length > 0 && $icon[0].nodeName.toLowerCase() == "img";
            if (isImg) {
                var origin = window.location.origin;
                var imgSrc = $(item.icon).attr("src");
                var mapImgSrc = $.utils.parse_url(imgSrc);
                
                item.icon = $("<div>").append($(item.icon).attr({ 
                    "scr": origin + mapImgSrc.path_query, 
                    "width": 20, 
                    "height": 20 
                })).html();
            }
            
            if (item.icon) {
                text += item.icon + '<span class="mg-r-10"></span>';
            }
            
            text += '<a href="#" data-id="' + item.id + '" data-trigger="update-' + type + '" class="tx-gray-800">' + item.name + '</a>';
            text += ' \
                <span class="float-right" ' + (item.parent_id ? 'data-parentId="' + item.parent_id + '"' : '') + '> \
                    <a href="#" data-id="' + item.id + '" ' + (item.parent_id ? 'data-parentId="' + item.parent_id + '"' : '') + ' data-trigger="update-' + type + '" class="badge badge-secondary tx-bold mg-l-10" style="color: #fff;">Edit</a> \
                    <a href="#" data-id="' + item.id + '" ' + (item.parent_id ? 'data-parentId="' + item.parent_id + '"' : '') + ' data-trigger="delete-' + type + '" class="badge badge-danger tx-bold mg-l-10" style="color: #fff;">X</a> \
                </span> \
            ';

            var node = {
                text: text,
            }
            if (children && children.length > 0) {
                node["nodes"] = serializeFacilitiesTree(children, type);
            }
            tree.push(node);
        });

        return tree;
    }

    $.fn.getPrimaryLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang/primary' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data) {
                toastr["error"]("Primary lang isn't set.", "Failed");
            } else {
                primaryLang = json.data.code;
                $self.val(json.data.code).trigger({
                    type: "loaded.primary-lang",
                    lang: json.data.code
                });
            }
        });
    }

    $.fn.iconpickerDestroy = function () {
        $(this).unbind().removeData();
    }

    $.fn.generateParentDropdown = function (lang) {
        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");
        var facilities = $form.hasClass("facility") ? facilityGroupedLang : amenitiesGroupedLang;

        $self.each(function (i) {
            var $select = $(this);
            var $parent = isModalUpdate ? $(this).closest(".tab-pane") : $form;

            var indexLang = lang;
            if (indexLang === undefined) {
                indexLang = $parent.find("input[name=lang]").val();
            }
            var exceptId = $parent.find("input[name=id]").val();

            $select.destroySelectDropdown();

            $select.select2({
                placeholder: 'Group Facility',
                searchInputPlaceholder: 'Search',
                allowClear: true,
                escapeMarkup: function (m) { return m; },
            });
            
            $.each(facilities[indexLang], function (i, v) {
                if (exceptId != v.id) {
                    var $icon = $.parseHTML(v.icon);
                    var isImg = $icon.length > 0 && $icon[0].nodeName.toLowerCase() == "img";

                    var icon = v.icon;
                    if (isImg) {
                        icon = $("<div>").append($(icon).attr({ "width": 20, "height": 20 })).html();
                    }
                    var option = new Option(icon + ' <span class="mg-r-10"></span>' + v.name, v.id, false, false);
                    $select.append(option);
                }
            });

            var value = $select.data("value");
            if (value) {
                $select.val(value);
            }
            $select.trigger('change');

            if (lang !== undefined) return;
        });
    }

    $.fn.generateFilterIconDropdown = function () {
        var $self = $(this);

        $self.each(function (i) {
            var $select = $(this);
            var $form = $select.closest("form");
            var $parent = $select.closest(".row");
            
            $select.select2({
                minimumResultsForSearch: Infinity,
            }).on("change", function(){
                var iconType = $self.val();

                if (iconType == "image") {
                    $parent.find(".filter-image-icon").removeClass("d-none");
                    $parent.find(".filter-image-font").addClass("d-none");
                } else if (iconType == "font") {
                    $parent.find(".filter-image-font").removeClass("d-none");
                    $parent.find(".filter-image-icon").addClass("d-none");
                } else {
                    $parent.find(".filter-image-icon").addClass("d-none");
                    $parent.find(".filter-image-font").addClass("d-none");
                }
            });

            var value = $select.data("value");
            if (value) {
                var icon = $form.find("input[name=icon]").val();
                var $icon = $.parseHTML(icon);
                var isImg = $icon.length > 0 && $icon[0].nodeName.toLowerCase() == "img";

                if (isImg) {
                    var imageIcon = $("<div>").append($(icon).attr({ "width": 20, "height": 20 })).html();
                    $parent.find(".filter-image-icon > button").html(imageIcon);
                } else {
                    var pickerIcon = $icon.length > 0 ? $icon[0].className : "";
                    $parent.find(".iconpicker").iconpicker("setIcon", pickerIcon);
                }

                $select.val(value);
            }
            $select.trigger('change');
        });
    }

    $.fn.generateDropdown = function () {
        var $self = $(this);

        $self.each(function (i) {
            var $select = $(this);

            $select.select2({
                minimumResultsForSearch: Infinity,
            });
        });
    }

    $.fn.facilitiesTreeView = function (type) {
        var json = type == "facility" ? facilities : amenities;
        $(this).treeview({
            expandIcon: 'fas fa-caret-right',
            collapseIcon: 'fas fa-caret-down',
            selectedBackColor: '#3366ff',
            data: serializeFacilitiesTree(json, type),
        });
    }

    $.fn.getTranslateToDropdown = function (exceptIDs) {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            exceptIDs = exceptIDs ? exceptIDs : [];

            // Select2 has been initialized
            var $request = $.ajax({
                url: '/lang' // wherever your data is actually coming from
            });

            $request.then(function (json) {
                var options = [];
                $.each(json.data.item, function (i, v) {
                    if ($.inArray(v.code, exceptIDs) === -1) {
                        options.push({ id: v.code, text: v.name })
                    }
                });

                if (options.length > 0) {
                    $self.select2({
                        placeholder: 'Transalte To',
                        allowClear: true,
                        minimumResultsForSearch: Infinity,
                        data: options
                    });
                    $self.closest(".form-group").removeClass("d-none");
                    $self.closest(".modal-header").addClass("pd-t-10 pd-b-10");
                } else {
                    $self.closest(".form-group").addClass("d-none");
                    $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
                }
            });
        }
    }

    $.fn.destroySelectDropdown = function () {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("select2-hidden-accessible")) {
                $(this).select2('destroy');
                $(this).find("option").not(':first').remove();
                $(this).removeAttr("data-select2-id tabindex aria-hidden");
            }
        });
    }

    $.fn.destroyTranslateToDropdown = function () {
        var $self = $(this);

        $self.destroySelectDropdown();
        $self.closest(".form-group").addClass("d-none");
        $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
    }

    getFacilityGroupedLang();
    getFacilityGroupedLang("amenities");
    
    $("input.primary-lang").getPrimaryLang();

    $(document).on("submit", ".form-filter", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");
        var facilityType = $(this).data("type");
        var treeContainer = facilityType == "facility" ? "#hotelFacilitiesTree" : "#roomAmenitiesTree"; 
        var $treeContainer = $(treeContainer); 
        var $alertInfo = $treeContainer.closest(".card-body").find(".alert"); 
        var url = $form.attr('action');
        var data = $form.serialize();
        var btnText = $btnSave.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                if (jqXHR.data.item) {
                    var json = jqXHR.data.item ? jqXHR.data.item : [];
                    if (facilityType == "facility") {
                        facilities = json;
                    } else {
                        amenities = json;
                    }
                    $treeContainer.facilitiesTreeView(facilityType);
                    $treeContainer.removeClass("d-none");
                    $alertInfo.addClass("d-none");
                } else {
                    $treeContainer.addClass("d-none");
                    $alertInfo.removeClass("d-none");
                }
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("submit", ".form-setup", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $modal = $form.closest(".modal");
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr('action');
        var data = $form.serialize();
        var type = $form.attr('type');
        var btnText = $btnSave.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");

                if ($form.hasClass("facility")) {
                    $(".form-filter.facility").trigger("submit");
                } else {
                    $(".form-filter.amenities").trigger("submit");
                }
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);

            $modal.modal("hide");
        });
    });

    $(document).on("click", "[data-trigger=update-facility]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var id = $self.data("id");
        var url = "/facility/update/" + id;
        var $form = $(".form-setup.facility.update");
        var $defaultForm = $form.html();
        var $modal = $form.closest(".modal");

        var $tabs = $modal.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $modal.find(".tab-content");
        var tabPaneContent = $('<div>').append($tabContent.find(".tab-pane").first().clone(true)).html();

        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                var facility = jqXHR.data;

                var primaryItems = [];
                var otherItems = [];
                $.each(facility.items, function (i, item) {
                    if (item.lang.code == primaryLang) {
                        primaryItems.push(item);
                    } else {
                        otherItems.push(item);
                    }
                });

                var items = $.merge(primaryItems, otherItems);
                var icon = facility.icon;
                var $icon = $.parseHTML(icon);
                var isImg = $icon.length > 0 && $icon[0].nodeName.toLowerCase() == "img";
                if (isImg) {
                    var origin = window.location.origin;
                    var imgSrc = $(icon).attr("src");
                    var mapImgSrc = $.utils.parse_url(imgSrc);

                    icon = $('<div>').append($(icon).attr("src", origin + mapImgSrc.path_query)).html();
                }
                var filterIcon = isImg ? "image" : "font";

                $form.find("input[name=id]").val(facility.id);
                $form.find("input[name=icon]").val(icon);

                $tabs.html("");
                $tabContent.html("");
                $.each(items, function (i, item) {
                    var $thisTab = $(tab);
                    var $thisTabPane = $(tabPaneContent);

                    var href = $thisTab.attr("href").replace("default", item.lang.code);
                    var tabID = href.replace("#", "");

                    $thisTab.attr("href", href);
                    $thisTab.html(item.lang.name);

                    $tabs.append($thisTab);

                    $thisTabPane.attr("id", tabID);
                    $thisTabPane.find("input[name=name]").val(item.name);
                    $thisTabPane.find("input[name=title]").val(item.title);
                    $thisTabPane.find("input[name=description]").val(item.description);
                    $thisTabPane.find("input[name=lang]").val(item.lang.code);
                    $thisTabPane.find("select[name=parent_id]").data("value", facility.parent_id);
                    $thisTabPane.find("select[name=filter_icon]").data("value", filterIcon);
                    
                    $tabContent.append($thisTabPane);
                });
                $tabs.find('a.nav-link:first-child').tab('show');

                $modal.modal("show").data({
                    defaultForm: $defaultForm,
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });

    });

    $(document).on("click", "[data-trigger=delete-facility]", function (e) {
        e.preventDefault();
        var $self = $(this);

        var name = $self.closest("li").find("a:first").text();
        var accept = confirm("Are you sure to delete facility " + name + " ?");
        if (!accept) return;

        var id = $self.data("id");
        var url = "/facility/" + id;
        var btnText = $self.html();
        
        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $self.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");

                $(".form-filter.facility").trigger("submit");
                getFacilityGroupedLang("facility");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $self.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=update-amenities]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var id = $self.data("id");
        var url = "/amenities/update/" + id;
        var $form = $(".form-setup.amenities.update");
        var $defaultForm = $form.html();
        var $modal = $form.closest(".modal");

        var $tabs = $modal.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $modal.find(".tab-content");
        var tabPaneContent = $('<div>').append($tabContent.find(".tab-pane").first().clone(true)).html();

        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                var facility = jqXHR.data;

                var primaryItems = [];
                var otherItems = [];
                $.each(facility.items, function (i, item) {
                    if (item.lang.code == primaryLang) {
                        primaryItems.push(item);
                    } else {
                        otherItems.push(item);
                    }
                });
                var items = $.merge(primaryItems, otherItems);
                var icon = facility.icon;
                var $icon = $.parseHTML(facility.icon);
                var isImg = $icon.length > 0 && $icon[0].nodeName.toLowerCase() == "img";
                if (isImg) {
                    var origin = window.location.origin;
                    var imgSrc = $(icon).attr("src");
                    var mapImgSrc = $.utils.parse_url(imgSrc);

                    icon = $('<div>').append($(icon).attr("src", origin + mapImgSrc.path_query)).html();
                }
                var filterIcon = isImg ? "image" : "font";

                $form.find("input[name=id]").val(facility.id);
                $form.find("input[name=icon]").val(icon);

                $tabs.html("");
                $tabContent.html("");
                $.each(items, function (i, item) {
                    var $thisTab = $(tab);
                    var $thisTabPane = $(tabPaneContent);

                    var href = $thisTab.attr("href").replace("default", item.lang.code);
                    var tabID = href.replace("#", "");

                    $thisTab.attr("href", href);
                    $thisTab.html(item.lang.name);

                    $tabs.append($thisTab);

                    $thisTabPane.attr("id", tabID);
                    $thisTabPane.find("input[name=name]").val(item.name);
                    $thisTabPane.find("input[name=title]").val(item.title);
                    $thisTabPane.find("input[name=description]").val(item.description);
                    $thisTabPane.find("input[name=lang]").val(item.lang.code);
                    $thisTabPane.find("select[name=parent_id]").data("value", facility.parent_id);
                    $thisTabPane.find("select[name=filter_icon]").data("value", filterIcon);

                    $tabContent.append($thisTabPane);
                });
                $tabs.find('a.nav-link:first-child').tab('show');

                $modal.modal("show").data({
                    defaultForm: $defaultForm,
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });

    });

    $(document).on("click", "[data-trigger=delete-amenities]", function (e) {
        e.preventDefault();
        var $self = $(this);

        var name = $self.closest("li").find("a:first").text();
        var accept = confirm("Are you sure to delete amenities " + name + " ?");
        if (!accept) return;

        var id = $self.data("id");
        var url = "/amenities/" + id;
        var btnText = $self.html();

        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $self.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");

                $(".form-filter.amenities").trigger("submit");
                getFacilityGroupedLang("amenities");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $self.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete-tab]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $self.closest("form");

        var $translateToDropdown = $modal.find('.select2.translate-to');

        var $tabs = $form.find(".nav.nav-tabs");
        var $navTabLinks = $tabs.find("a.nav-link");
        var $activeTab = $tabs.find("a.nav-link.active");

        var $tabContent = $form.find(".tab-content");
        var $activeTabPane = $tabContent.find(".tab-pane.active");

        var activeLang = $activeTab.text();
        var facilityType = $form.hasClass("facility") ? "facility" : "amenities";
        var accept = confirm("Delete this " + facilityType + " in " + activeLang + " language ?");
        if (!accept) return;

        if ($activeTab.hasClass("new")) {
            $activeTab.remove();
            $activeTabPane.remove();
            $tabs.find('a.nav-link:last-child').tab("show");

            var langIDs = $modal.find("input[name=lang]").map(function () {
                return this.value;
            }).get();
            $translateToDropdown.destroyTranslateToDropdown();
            $translateToDropdown.getTranslateToDropdown(langIDs);
        } else {
            var id = $form.find("input[name=id]").val();
            var lang = $activeTabPane.find("input[name=lang]").val();
            var url = $self.attr("href");
            var btnText = $self.html();

            $.ajaxq("aq", {
                url: url + '/' + id + '/' + lang,
                type: "delete",
                beforeSend: function () {
                    $self.html(waitText);
                }
            }).done(function (jqXHR) {
                if (jqXHR.success) {
                    toastr["success"](jqXHR.message, "Success");

                    if ($navTabLinks.length > 1) {
                        $activeTab.remove();
                        $activeTabPane.remove();
                        $tabs.find('a.nav-link:last-child').tab("show");
                    }

                    var langIDs = $modal.find("input[name=lang]").map(function () {
                        return this.value;
                    }).get();
                    $translateToDropdown.destroyTranslateToDropdown();
                    $translateToDropdown.getTranslateToDropdown(langIDs);

                    getFacilityGroupedLang(facilityType);
                }
            }).always(function (jqXHR) {
                if (!jqXHR.success) {
                    var r = jqXHR.responseJSON;
                    var message = r ? r.error : jqXHR.statusText;
                    toastr["error"](message, "Failed");
                }
                $self.html(btnText);
            });
        }
    });

    $(document).on("click", ".form-insert-row button.go-btn", function (e) {
        e.preventDefault();
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var defaultForm = $modal.data("defaultForm");

        var rows = $modal.find(".row.item-facility").length;
        var newRows = parseInt($modal.find(".form-insert-row select[name=row]").val());

        for (var i = rows; i < (rows + newRows); i++) {
            var $row = $(defaultForm).find(".row.item-facility");

            $modal.find(".modal-body").append($row);
            $modal.find('.iconpicker').eq(i).iconpicker(iconpickerOpts);
            $modal.find("select[name=filter_icon]").eq(i).generateFilterIconDropdown();
            $modal.find("select[name=parent_id]").eq(i).generateParentDropdown();
        }
    });

    $(document).on("change", ".iconpicker", function (e) {
        e.preventDefault();
        if (e.icon === "empty") return;

        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");
        var $parent = isModalUpdate ? $form : $self.closest(".row");

        // don't change tag i icon
        var icon = '<i class="' + e.icon + '"></i>';

        $parent.find("input[name=icon]").val(icon);
        $parent.find(".iconpicker > i").attr('class', e.icon);
        $parent.find(".iconpicker > input").val(e.icon);
    });
    
    $(document).on("select2:select", "select[name=filter_icon]", function (e) {
        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");
        var $parent = isModalUpdate ? $form : $self.closest(".row");
        var iconType = $self.val();

        if (isModalUpdate) {
            $parent.find("select[name=filter_icon]").val(iconType).trigger("change");
        }

        $parent.find(".filter-image-icon > button").html("Choose");
        $parent.find(".iconpicker").iconpicker("setIcon", "");
        $parent.find("input[name=icon]").val("");
    });

    $(document).on("select2:select", "select.translate-to", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $modal.find("form.form-setup");
        var defaultForm = $modal.data("defaultForm");
        var $translateToDropdown = $modal.find('.select2.translate-to');

        var lang = $(this).select2("data")[0];

        var $tabs = $modal.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $modal.find(".tab-content");
        var $tabPaneSource = $tabContent.find(".tab-pane").first();

        var $tab = $(tab);
        var $tabPane = $(defaultForm).find(".tab-pane");

        var href = $tab.attr("href").replace(/\w+$/, lang.id);
        var tabID = href.replace("#", "");
        var source = $tabPaneSource.find("input[name], select[name], textarea[name]").serializeObject();

        $tab.removeClass("active show");
        $tabPane.removeClass("active show");

        $tab.attr("href", href);
        $tab.html(lang.text);
        $tab.addClass("new");
        $tabs.append($tab);

        $tabPane.attr("id", tabID);
        $tabPane.find("input[name=lang]").val(lang.id);
        $tabPane.find("input[name=name]").val("");
        $tabPane.find("input[name=title]").val("");
        $tabPane.find("input[name=description]").val("");
        $tabPane.find("select[name=parent_id]").data("value", source.parent_id);

        var icon = $form.find("input[name=icon]").val();
        var $icon = $.parseHTML(icon);
        var isImg = $icon.length > 0 && $icon[0].nodeName.toLowerCase() == "img";
        var filterIcon = isImg ? "image" : "font";

        $tabPane.find("select[name=filter_icon]").data("value", filterIcon);

        $tabContent.append($tabPane);
        $tabs.find('a.nav-link:last-child').tab("show");

        $tabPane.find('.iconpicker').iconpicker(iconpickerOpts);
        $tabPane.find('select[name=parent_id]').generateParentDropdown();
        $tabPane.find("select[name=filter_icon]").generateFilterIconDropdown();

        var langIDs = $form.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.getTranslateToDropdown(langIDs);
    });

    $(document).on("select2:select select2:clear", "select[name=parent_id]", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $modal.find("form.form-setup");
        var isModalUpdate = $form.hasClass("update");
        var $parent = isModalUpdate ? $self.closest(".tab-pane") : $form;

        var data = $self.select2('data')[0];
        if (isModalUpdate) {
            var selectedLang = $parent.find("input[name=lang]").val();
            $modal.find(".tab-pane").each(function (i) {
                var $tabPane = $(this);
                var $targetParent = $tabPane.find("select[name=parent_id]");

                var lang = $tabPane.find("input[name=lang]").val();
                if (lang != selectedLang) {
                    $targetParent.val(data.id).trigger("change");
                }
            });
        }
    });

    $(document).on("selected.media.library", "input[name=icon_image]", function (e) {
        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");
        var $parent = isModalUpdate ? $form : $self.closest(".row");

        var value = $self.val();
        var origin = window.location.origin;
        var image = value.match(/^http/) ? value : origin + "/static/uploads/media/" + value;
        var $image = '<img src="' + image + '">';
        var $displayImage = '<img src="' + image + '" width="20" height="20">';

        $parent.find("input[name=icon]").val($image);
        $parent.find(".filter-image-icon > button").html($displayImage);
    });

    $(document).on("show.bs.tab", "[data-toggle=tab]", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $btnDelete = $modal.find("[data-trigger=delete-tab]");
        
        if ($self.index() > 0) {
            $btnDelete.removeClass("d-none");
        } else {
            $btnDelete.addClass("d-none");
        }
    });

    $(document).on("show.bs.modal", "#modal-form-facility, #modal-form-facility-update, #modal-form-amenities, #modal-form-amenities-update", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form.form-setup");
        var isModalUpdate = $form.hasClass("update");
        var facilityType = $form.hasClass("facility") ? "facility" : "amenities";

        getFacilityGroupedLang(facilityType);

        if (isModalUpdate) {
            var langIDs = $modal.find("input[name=lang]").map(function () {
                return this.value;
            }).get();

            $modal.find('.select2.translate-to').getTranslateToDropdown(langIDs);
        } else {
            var $defaultForm = $form.html();
            
            $modal.find('.select2.input-row').generateDropdown();
            $modal.data("defaultForm", $defaultForm);
        }

        $modal.find('.iconpicker').iconpicker(iconpickerOpts);
        $modal.find("select[name=parent_id]").generateParentDropdown();
        $modal.find("select[name=filter_icon]").generateFilterIconDropdown();
    });

    $(document).on("hidden.bs.modal", "#modal-form-facility, #modal-form-facility-update, #modal-form-amenities, #modal-form-amenities-update", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form.form-setup");
        var defaultForm = $modal.data('defaultForm');
        var isModalUpdate = $modal.find("form.form-setup").hasClass("update");
        var facilityType = $form.hasClass("facility") ? "facility" : "amenities";

        getFacilityGroupedLang(facilityType);
        
        $modal.find(".iconpicker").iconpicker('setIcon', '');
        $modal.find("select[name=parent_id]").destroySelectDropdown();
        $modal.find(".row.item-facility").not(":first").remove();

        if (isModalUpdate) {
            $modal.find('.select2.translate-to').destroyTranslateToDropdown();
        } else {
            $modal.find('.select2.input-row').destroySelectDropdown();
            $modal.find("select[name=row]").val("1").trigger("change");
        }

        $modal.find('form.form-setup').html(defaultForm);
        $modal.removeData();

    });

    $(".form-filter.facility").trigger("submit");

    $(".form-filter.amenities").trigger("submit");
});