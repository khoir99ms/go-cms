$(function () {
    "use strict";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    var allLang = [];

    function generateMenuStructure(
        targetID,
        navType,
        navTypeLabel,
        refID,
        navUrl,
        navLabel,
        navLabelImage,
        openInNewtab,
        originalTitle,
        children,
    ) {
        navTypeLabel = navTypeLabel == "gc" ? "Group Content" : navTypeLabel;
        var navImage = navLabelImage && navLabelImage.match(/^http/) ? navLabelImage : "/static/uploads/media/" + navLabelImage;
        var showDeleteBtnNavImage = navLabelImage ? "" : " d-none";
        var checked = openInNewtab ? "checked" : "";
        var html = '\
        <li> \
            <div class="card bd mg-b-5 tx-gray-600 wd-50p"> \
                <div class="card-header tx-medium cursor-pointer" data-toggle="collapse" data-target="#' + targetID + '"> \
                    ' + navLabel + ' \
                    <span class="type-link text-capitalize float-right"> \
                        ' + navTypeLabel + ' \
                        <a class="badge badge-danger delete-menu-item tx-bold mg-l-10" style="color: #fff;">X</a> \
                    </span> \
                </div> \
                <div id="' + targetID + '" class="collapse" data-parent="#menu_structure"> \
                    <div class="card-body bd-t"> \
                        <input type="hidden" name="type" value="' + navType + '" required> \
                        <input type="hidden" name="ref_id" value="' + refID + '"> \
                        <div class="form-group"> \
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Url</label> \
                            <input type="text" name="url" class="form-control" value="'+ navUrl + '" placeholder="http://"> \
                        </div><!-- form-group --> \
                        <div class="form-group"> \
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Label</label> \
                            <input type="text" name="label" class="form-control" value="'+ navLabel + '" placeholder="Url Text"> \
                        </div><!-- form-group --> \
                        <div class="form-group"> \
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image</label> \
                            <div class="pos-relative wd-150"> \
                                <img src="'+ navImage + '" onerror="this.src=\'/static/images/default-img.png\';" class="img-fluid img-thumbnail wd-100p ht-100 nav-image"> \
                                <div class="logo-toolbar pos-absolute" style="top: 3px; right: 8px;"> \
                                    <a href="#" data-toggle="media-library" data-parent-target="#' + targetID + '" data-input-target="input[name=image]" class="badge badge-info tx-bold">Change</a> \
                                    <a href="#" data-trigger="delete-nav-image" class="badge badge-danger tx-bold'+ showDeleteBtnNavImage + '">X</a> \
                                </div> \
                            </div> \
                            <input type="hidden" name="image" value="' + navLabelImage + '"> \
                        </div><!-- form-group --> \
                        <label class="ckbox mg-b-15"> \
                            <input type="checkbox" name="new_tab" '+ checked + '><span>Open In New Tab</span> \
                        </label> \
                        <div class="form-group mg-b-5"> \
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Original</label> \
                            <div class="bd pd-t-3 pd-b-3 pd-l-12 pd-r-12 bg-gray-100">'+ originalTitle + '</div> \
                        </div><!-- form-group --> \
                    </div> \
                </div> \
            </div> \
            <ol>'+ children + '</ol> \
        </li>';

        return html;
    }

    function generateNestedMenuStructure(menus) {
        var html = "";
        $.each(menus, function (i, item) {
            var eID = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5))
            var targetID = item.type + "Menu" + eID + item.ref_id,
                navType = item.type,
                navTypeLabel = item.type == "custom" ? "Custom Link" : (item.type == "gc" ? "Group Content" : item.type),
                refID = item.ref_id ? item.ref_id : "",
                navUrl = item.url,
                navLabel = item.label,
                navLabelImage = item.image,
                openInNewtab = item.new_tab,
                originalTitle = item.type == "custom" ? item.label :item.original;

            var children = "";
            if (item.children && item.children.length > 0) {
                children = generateNestedMenuStructure(item.children)
            }
            html += generateMenuStructure(
                targetID,
                navType,
                navTypeLabel,
                refID,
                navUrl,
                navLabel,
                navLabelImage,
                openInNewtab,
                originalTitle,
                children
            );
        });
        return html;
    }

    $.fn.getAllLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data.item) {
                toastr["error"]("All lang isn't set.", "Failed");
            } else {
                allLang = json.data.item;
                $(document).trigger("loaded.all-lang");
            }
        });
    }

    $.fn.generateLangDropdown = function () {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            var options = [];
            $.each(allLang, function (i, v) {
                options.push({ id: v.code, text: v.name });
            });
            
            $self.select2({
                placeholder: 'Select Language',
                minimumResultsForSearch: Infinity,
                data: options
            });
        }
    }

    $.fn.generateMenuLangDropdown = function () {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {

            $self.select2({
                placeholder: "Select Menu",
                allowClear: true,
                searchInputPlaceholder: "Search",
                ajax: {
                    url: "/menu",
                    data: function (params) {},
                    processResults: function (json, params) {
                        var items = json.data.item ? json.data.item : [];

                        var options = [];
                        $.each(items, function (i, v) {
                            var id = v.id + "-" + v.lang;
                            var text = v.lang.toUpperCase() + " - " + v.location.toUpperCase();

                            options.push({ id: id, text: text });
                        });

                        return {
                            results: options
                        };
                    }
                },
            });
        }
    }

    $.fn.generateCategoryDropdown = function (lang) {
        var $self = $(this);
        
        if (!$self.hasClass("select2-hidden-accessible")) {
            lang = lang ? lang : "";

            $self.select2({
                placeholder: "Select Category",
                allowClear: true,
                searchInputPlaceholder: "Search",
                ajax: {
                    url: "/category/get?lang=" + lang,
                    data: function (params) {},
                    processResults: function (json, params) {
                        var items = json.data.item ? json.data.item : [];

                        var options = [];
                        $.each(items, function (i, v) {
                            options.push({ id: v.id, text: v.name })
                        });

                        return {
                            results: options
                        };
                    }
                },
            });
        }
    }

    $.fn.destroySelectDropdown = function () {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("select2-hidden-accessible")) {
                $(this).select2('destroy');
                $(this).find("option").not(':first').remove();
                $(this).removeAttr("data-select2-id tabindex aria-hidden");
            }
        });
    }

    $.fn.serializeMenu = function() {
        var $self = $(this);
        var $parent = $self.children();
        if ($parent.length == 0) return;
        
        var results = [];
        $parent.each(function () {
            var $li = $(this)
            var $sub = $li.children('ol');

            var id = $li.find("input[name=ref_id]").val();
            var type = $li.find("input[name=type]").val();
            var url = $li.find("input[name=url]").val();
            var navLabel = $li.find("input[name=label]").val();
            var navImage = $li.find("input[name=image]").val();
            var openInNewTab = $li.find("input[name=new_tab]").is(':checked');

            var item = {
                ref_id: id ? id : null,
                type: type,
                url: url,
                label: navLabel,
                image: navImage,
                new_tab: openInNewTab,
            }
            if ($sub.length > 0) {
                item["children"] = $sub.serializeMenu();
            }
            results.push(item)
        });

        return results;
    }

    $.fn.getPages = function (params) {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        params = params ? params : {};
        params.lang = params.lang ? params.lang : "";
        params.category_id = params.category_id ? params.category_id : "";

        $self.pagination({
            dataSource: "/page/get",
            ajax: {
                data: {
                    lang: params.lang,
                    category_id: params.category_id,
                }
            },
            locator: "data.item",
            alias: {
                pageNumber: "start",
                pageSize: "length"
            },
            totalNumberLocator: function (response) {
                // you can return totalNumber by analyzing response content
                return response.data.total;
            },
            pageSize: 10,
            showPageNumbers: false,
            showNavigator: true,
            callback: function (data, pagination) {
                var html = "";
                $.each(data, function (i, v) {
                    html += '\
                    <div class="form-check">\
                        <input class="form-check-input" type="checkbox" value="'+ v.id + '" id="page-' + v.id + '">\
                        <label class="form-check-label" for="page-' + v.id + '">\
                            ' + v.title + '\
                        </label>\
                    </div>';
                });
                
                if (data.length == 0) {
                    $alert.html('No items.').removeClass("d-none");
                    $self.addClass("d-none");
                } else {
                    $alert.addClass("d-none");
                    $self.removeClass("d-none");
                    $items.html(html);
                }
            }
        });
    }

    $.fn.resetPages = function () {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $header = $parent.find(".card-header");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        $header.html("Pages");
        $alert.html("No items.").removeClass("d-none");
        $items.html("");
        $self.addClass("d-none");
    }

    $.fn.getCategories = function (params) {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        params = params ? params : {};
        params.lang = params.lang ? params.lang : "";

        $self.pagination({
            dataSource: "/category/get",
            ajax: {
                data: { 
                    lang: params.lang
                }
            },
            locator: "data.item",
            alias: {
                pageNumber: "start",
                pageSize: "length"
            },
            totalNumberLocator: function (response) {
                // you can return totalNumber by analyzing response content
                return response.data.total;
            },
            pageSize: 10,
            showPageNumbers: false,
            showNavigator: true,
            callback: function (data, pagination) {
                var html = "";
                $.each(data, function (i, v) {
                    html += '\
                    <div class="form-check">\
                        <input class="form-check-input" type="checkbox" value="'+ v.id + '" id="category-' + v.id + '">\
                        <label class="form-check-label" for="category-' + v.id + '">\
                            ' + v.name + '\
                        </label>\
                    </div>';
                });

                if (data.length == 0) {
                    $alert.html('No items.').removeClass("d-none");
                    $self.addClass("d-none");
                } else {
                    $alert.addClass("d-none");
                    $self.removeClass("d-none");
                    $items.html(html);
                }
            }
        });
    }

    $.fn.resetCategories = function () {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $header = $parent.find(".card-header");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        $alert.html("No items.").removeClass("d-none");
        $items.html("");
        $self.addClass("d-none");
    }

    $.fn.getGroupContent = function () {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        $self.pagination({
            dataSource: "/group_content/get",
            ajax: {
                data: {}
            },
            locator: 'data.item',
            alias: {
                pageNumber: "start",
                pageSize: "length"
            },
            totalNumberLocator: function (response) {
                // you can return totalNumber by analyzing response content
                return response.data.total;
            },
            pageSize: 10,
            showPageNumbers: false,
            showNavigator: true,
            callback: function (data, pagination) {
                var html = "";
                $.each(data, function (i, v) {
                    var label = v.name.replace(/-/g, ' ');
                    html += '\
                    <div class="form-check">\
                        <input class="form-check-input" type="checkbox" value="'+ v.id + '" id="gc-' + v.id + '">\
                        <label class="form-check-label" for="gc-' + v.id + '">\
                            ' + label.ucwords() + '\
                        </label>\
                    </div>';
                });

                if (data.length == 0) {
                    $alert.html('No items.').removeClass("d-none");
                    $self.addClass("d-none");
                } else {
                    $alert.addClass("d-none");
                    $self.removeClass("d-none");
                    $items.html(html);
                }
            }
        });
    }

    $.fn.resetGroupContent = function () {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $header = $parent.find(".card-header");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        $alert.html("No items.").removeClass("d-none");
        $items.html("");
        $self.addClass("d-none");
    }

    $.fn.viewCustomLink = function () {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $alert = $parent.find(".alert");

        $alert.addClass("d-none");
        $self.removeClass("d-none");
    }

    $.fn.hideCustomLink = function () {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $alert = $parent.find(".alert");

        $alert.removeClass("d-none");
        $self.addClass("d-none");
    }

    $.fn.getFormMenu = function (id) {
        var $form = $(this);

        var $request = $.ajax({
            url: "/menu/" + id,
        });

        $request.then(function (json) {
            if (!json.success) {
                toastr["error"](json.message, "Failed");
            } else {
                var menu = json.data;
                var html = generateNestedMenuStructure(menu.structure);

                $form.find("input[name=id]").val(menu.id);
                $form.find("input[name=name]").removeAttr("disabled").val(menu.name);
                $form.find("select[name=lang]").removeAttr("disabled").val(menu.lang).trigger("change");
                $form.find("select[name=location]").removeAttr("disabled").val(menu.location).trigger("change");
                $form.find(".menu_structure").html(html);
                $form.closest(".card-body").removeClass("d-none");
            }
        });
    }

    $.fn.resetFormMenu = function (id) {
        var $form = $(this);

        $form.find("input[name=id]").val("");
        $form.find("input[name=name]").prop('disabled', true).val("");
        $form.find("select[name=lang]").prop('disabled', true).val("").trigger("change");
        $form.find("select[name=location]").prop('disabled', true).val("").trigger("change");
        $form.find(".menu_structure").html("");
        $form.closest(".card-body").addClass("d-none");
    }
    
    $(document).getAllLang();

    $("select.flt-menu").generateMenuLangDropdown();

    $("select.flt-category").select2({
        placeholder: 'Select Category',
        allowClear: true,
        searchInputPlaceholder: 'Search',
    });

    var oldContainer;
    $("ol.menu_structure").sortable({
        group: 'nested',
        afterMove: function (placeholder, container) {
            if (oldContainer != container) {
                if (oldContainer)
                    oldContainer.el.removeClass("active");
                container.el.addClass("active");

                oldContainer = container;
            }
        },
        onDrop: function ($item, container, _super) {
            container.el.removeClass("active");
            _super($item, container);
        }
    });

    $(document).on("loaded.all-lang", function() {
        $("select.language").generateLangDropdown();

        $("select.location").select2({
            placeholder: 'Select Location',
            minimumResultsForSearch: Infinity,
        });
    });

    $(document).on("submit", ".form-menu.filter", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $formUpdate = $(".form-menu.update");
        var $formFilterItem = $(".form-menu.filter-item");

        var $fltMenu = $form.find("select.flt-menu");
        var $fltCategory = $formFilterItem.find("select[name=category_id]");
        var $collapse = $("#accordion").find(".collapse");
        var values = $fltMenu.val().split("-");

        if (values.length != 2) return;

        var id = values[0];
        var lang = values[1];
        var categoryId = $fltCategory.val();
        var params = { lang: lang, category_id: categoryId };

        $fltCategory.destroySelectDropdown();
        $fltCategory.generateCategoryDropdown(lang);
        $fltCategory.removeAttr("disabled");

        $(".pagesContainer").getPages(params);
        $(".categoryContainer").getCategories(params);
        $(".groupContentContainer").getGroupContent();
        $(".customLinkContainer").viewCustomLink();
        $formUpdate.getFormMenu(id);

        $collapse.removeClass("show");
        $collapse.first().addClass("show");
    });

    $(document).on("submit", ".form-menu.add", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $modal = $form.closest(".modal");
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr("action");
        var type = $form.attr("type");
        var data = $form.serialize();
        var btnText = $btnSave.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");

                $modal.modal("hide");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    }); 

    $(document).on("submit", ".form-menu.update", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");
        var btnText = $btnSave.html();

        var id = $form.find("input[name=id]").val();
        var name = $form.find("input[name=name]").val();
        var lang = $form.find("select[name=lang]").val();
        var location = $form.find("select[name=location]").val();
        var structure = $form.find(".menu_structure").serializeMenu();

        var url = $form.attr("action");
        var type = $form.attr("type");
        var data = {
            name: name,
            lang: lang,
            location: location,
            structure: structure,
        }
        
        $.ajaxq("aq", {
            url: url + "/" + id,
            data: JSON.stringify(data),
            type: type,
            contentType: "application/json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
        
    });

    $(document).on("submit", ".form-menu.custom-link", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $formUpdate = $(".form-menu.update");

        var eID = new Date().getTime();
        var targetID = "customMenu" + eID,
            navType = "custom",
            navTypeLabel = "Custom Link",
            refID = "",
            navUrl = $form.find("input[name=url]").val(),
            navLabel = $form.find("input[name=label]").val(),
            navLabelImage = "",
            openInNewtab = false,
            originalTitle = navLabel,
            children = "";

        var html = generateMenuStructure(
            targetID,
            navType,
            navTypeLabel,
            refID,
            navUrl,
            navLabel,
            navLabelImage,
            openInNewtab,
            originalTitle,
            children
        );
        $formUpdate.find(".menu_structure").append(html);

        $form[0].reset();
        if ($.fn.parsley) {
            $form.parsley().reset();
        }
    });

    $(document).on("click", ".add-to", function (e) {
        e.preventDefault();
        
        var $formUpdate = $(".form-menu.update");
        var $menuItemChecked = $(this).closest(".collapse").find(".items > .form-check input:checked");

        var html = "";
        $menuItemChecked.each(function (i) {
            var eID = new Date().getTime();
            var $parent = $(this).closest(".form-check");
            var $checkbox = $parent.find("input[type=checkbox]");
            var typeId = $checkbox.attr("id").split("-");

            var targetID = typeId[0] + "Menu" + eID + typeId[1],
                navType = typeId[0],
                navTypeLabel = typeId[0],
                refID = typeId[1],
                navUrl = "",
                navLabel = $parent.find("label.form-check-label").html().trim(),
                navLabelImage = "",
                openInNewtab = false,
                originalTitle = navLabel,
                children = "";

            html += generateMenuStructure(
                targetID,
                navType,
                navTypeLabel,
                refID,
                navUrl,
                navLabel,
                navLabelImage,
                openInNewtab,
                originalTitle,
                children
            );
            this.checked = false;
        });
        $formUpdate.find(".menu_structure").append(html);
    });

    $(document).on("click", ".delete-menu-item", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        $(this).closest("li").remove();
    });

    $(document).on("click", ".delete-menu", function (e) {
        e.preventDefault();

        var $form = $(this).closest("form");
        var $formFilter = $(".form-menu.filter");
        var $formFilterItem = $(".form-menu.filter-item");
        
        var id = $form.find("input[name=id]").val();
        var name = $form.find("input[name=name]").val();
        
        if (!id) return;
        
        var accept = confirm("Are you sure to delete menu " + name + " ?");
        if (!accept) return;

        var $btn = $(this);
        var url = $(this).attr('href');
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url + "/" + id,
            type: "delete",
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");

                $form.resetFormMenu();
                
                $('.pagesContainer').resetPages();
                $('.categoryContainer').resetCategories();
                $('.groupContentContainer').resetGroupContent();
                $(".customLinkContainer").hideCustomLink();
                
                $formFilter.find("select.flt-menu").val(null).trigger("change");
                $formFilterItem.find("select[name=category_id]").val(null).trigger("change").attr('disabled', true);
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete-nav-image]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $parent = $(this).closest(".card-body");
        $parent.find("input[name=image]").val("");
        $parent.find("img.nav-image").attr("src", "#");
        $parent.find("[data-trigger=delete-nav-image]").addClass("d-none");
    });

    $(document).on("select2:select select2:clear", "select.flt-category", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $formFilter = $(".form-menu.filter");
        var $collapse = $("#accordion").find(".collapse");
        var values = $formFilter.find("select.flt-menu").val().split("-");
        
        if (values.length != 2) return;
        
        var lang = values[1];
        var categoryId = $self.val();
        var params = { lang: lang, category_id: categoryId };

        $(".pagesContainer").getPages(params);

        $collapse.removeClass("show");
        $collapse.first().addClass("show");
    }); 
    
    $(document).on("selected.media.library", "input[name=image]", function (e) {
        e.preventDefault();
        var $parent = $(this).closest(".card-body");
        var image = e.value.match(/^http/) ? e.value : "/static/uploads/media/" + e.value;

        $parent.find("[data-trigger=delete-nav-image]").removeClass("d-none");
        $parent.find("img.nav-image").attr("src", image);
    });
});