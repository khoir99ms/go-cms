$(function () {
    "use strict";
    
    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    var allLang = [];
    var primaryLang = "";
    var primaryLangJson = {};

    var groupingBanner = function(items){
        var group = {};
        $.each(items, function(i, item) {
            if (group[item.image] === undefined) {
                group[item.image] = {}
            }
            group[item.image][item.lang.code] = item;
        });

        var results = [];
        $.each(group, function (i, item) {
            var primaryBanner = [];
            var otherBanner = [];
            $.each(item, function (k, v) {
                if (k == primaryLang) {
                    primaryBanner.push(v);
                } else {
                    otherBanner.push(v);
                }
            });
            var banner = $.merge(primaryBanner, otherBanner);

            results.push(banner);
        });
        return results;
    }

    $.fn.getAllLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data.item) {
                toastr["error"]("All lang isn't set.", "Failed");
            } else {
                allLang = json.data.item;
                $(document).trigger("loaded.all-lang");
            }
        });
    }

    $.fn.getPrimaryLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang/primary' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data) {
                toastr["error"]("Primary lang isn't set.", "Failed");
            } else {
                primaryLang = json.data.code;
                primaryLangJson = json.data;
                $self.val(json.data.code).trigger({
                    type: "loaded.primary-lang",
                    lang: json.data.code
                });
            }
        });
    }

    $.fn.generatePageDropdown = function () {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            var $request = $.ajax({
                url: '/page/get',
                data: {lang: primaryLang},
            });

            $request.then(function (json) {
                var options = [];
                $.each(json.data.item, function (i, v) {
                    options.push({ id: v.id, text: v.title });
                });

                $self.destroySelectDropdown();

                $self.select2({
                    placeholder: 'Select Page',
                    allowClear: true,
                    data: options
                });
            });
        }
    }

    $.fn.generateCategoryDropdown = function () {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            var $request = $.ajax({
                url: '/category/get',
                data: {lang: primaryLang},
            });

            $request.then(function (json) {
                var options = [];
                $.each(json.data.item, function (i, v) {
                    options.push({ id: v.id, text: v.name });
                });

                $self.destroySelectDropdown();

                $self.select2({
                    placeholder: 'Select Category',
                    allowClear: true,
                    data: options
                });
            });
        }
    }

    $.fn.generateGCDropdown = function () {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            var $request = $.ajax({
                url: '/group_content/get',
                data: {lang: primaryLang},
            });

            $request.then(function (json) {
                var options = [];
                $.each(json.data.item, function (i, v) {
                    options.push({ id: v.id, text: v.name });
                });

                $self.destroySelectDropdown();

                $self.select2({
                    placeholder: 'Select Group Content',
                    allowClear: true,
                    data: options
                });
            });
        }
    }

    $.fn.changeBannerImage = function () {
        var $self = $(this);
        var $parent = $self.closest(".form-group");

        var value = $self.val();
        var image = value.match(/^http/) ? value : "/static/uploads/media/" + value;
        $parent.find("img.banner-image").attr("src", image);
    }

    $.fn.generateTranslateToDropdown = function (exceptIDs) {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            exceptIDs = exceptIDs ? exceptIDs : [];

            // Select2 has been initialized
            var options = [];
            $.each(allLang, function (i, v) {
                if ($.inArray(v.code, exceptIDs) === -1) {
                    options.push({ id: v.code, text: v.name })
                }
            });

            if (options.length > 0) {
                $self.select2({
                    placeholder: 'Transalte To',
                    allowClear: true,
                    minimumResultsForSearch: Infinity,
                    data: options
                });
                $self.closest(".form-group").removeClass("d-none");
                $self.closest(".modal-header").addClass("pd-t-10 pd-b-10");
            } else {
                $self.closest(".form-group").addClass("d-none");
                $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
            }
        }
    }

    $.fn.destroySelectDropdown = function () {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("select2-hidden-accessible")) {
                $(this).select2('destroy');
                $(this).find("option").not(':first').remove();
                $(this).removeAttr("data-select2-id tabindex aria-hidden");
            }
        });
    }

    $.fn.destroyTranslateToDropdown = function () {
        var $self = $(this);

        $self.destroySelectDropdown();
        $self.closest(".form-group").addClass("d-none");
        $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
    }

    $.fn.getAssignedBanner = function () {
        var $self = $(this);
        var $tabPane = $self.closest(".tab-pane");
        var $form = $tabPane.find("form.form-assign");
        var $alert = $self.find("div.alert");
        var $table = $self.find("table");
        var $tbody = $table.find("tbody");

        var id = $form.find("input[name=id]").val();
        var url = $self.data("api");
        var type = $self.data("type");

        var $request = $.ajax({
            url: url,
            data: {id: id},
        });

        $request.then(function (json) {
            if (!json.success) {
                toastr["error"](r.error, "Failed");
            }

            var items = json.data.item ? json.data.item : [];
            
            if (items.length > 0) {
                var tbody = "";
                $.each(items, function (i, item) {
                    tbody += '\
                    <tr>\
                        <td>'+ (i + 1) + '</td>\
                        <td>'+ item.title + '</td>\
                        <td><a href="/banner/assigned/'+ type + '/' + item.banner_id + '/' + item.id + '" data-trigger="delete-assigned" class="badge badge-danger tx-bold">X</a></td>\
                    </tr>\
                    ';
                });
                $tbody.html(tbody);
                $table.closest("div").removeClass("d-none");
                $alert.addClass("d-none");
            } else {
                $table.closest("div").addClass("d-none");
                $alert.removeClass("d-none");
            }
        });
    }

    $(document).getAllLang();
    $("input.primary-lang").getPrimaryLang();

    var $tableBanner = $("#datatable-banner").DataTable({
        responsive: true,
        language: {
            searchPlaceholder: "Search ...",
            sSearch: "",
            lengthMenu: "_MENU_ items/page",
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        processing: true,
        serverSide: true, 
        deferRender: true,
        ajax: {
            url: "/banner/load",
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);

                json.recordsTotal = json.data.total;
                json.recordsFiltered = json.data.total;
                // json.recordsFiltered = json.data.item ? json.data.item.length : 0;
                json.data = json.data.item ? json.data.item : [];
                
                return JSON.stringify(json); // return JSON string
            },
        },
        initComplete: function (settings, json) {
            var api = new $.fn.dataTable.Api(settings);
            if (pageSession.level == "brand") {
                api.columns([4]).visible(false);
            } else {
                api.columns([3]).visible(false);
            }
        },
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: "name" },
            {
                data: null,
                searchable: false,
                orderable: false,
                visible: false,
                render: function (data, type, row, meta) {
                    var count = row.items ? row.items.length : 0;;
                    return count;
                }
            },
            {
                data: "brand.name",
                render: function (data, type, row, meta) {
                    var brand = "";
                    if (row.brand) {
                        brand = row.brand.name ? row.brand.name : "";
                    }
                    return brand;
                }
            },
            {
                data: "property.name",
                render: function (data, type, row, meta) {
                    var property = "";
                    if (row.property) {
                        property = row.property.name ? row.property.name : "";
                    }
                    return property;
                }
            },
            { data: "created_at" },
            { 
                data: "actions", 
                className: "",
                searchable: false, 
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a href="#" data-id="' + row.id + '" class="badge badge-info" data-trigger="assign">Assign</a> \
                            <a href="/banner/update/' + row.id + '" class="badge badge-secondary" data-trigger="update">Edit</a> \
                            <a href="/banner/delete/' + row.id + '" class="badge badge-danger" data-trigger="delete">Delete</a>';
                }
            }
        ],
        order: [[1, "asc"]]
    });

    $("div.dataTables_filter input").unbind().keyup(function (e) {
        if (e.keyCode == 13) $tableBanner.search(this.value).draw();
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    $(document).on("submit", ".form-banner", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type='submit']");

        var url = $form.attr('action');
        var type = $form.attr('type');
        var data = $form.serialize();
        var btnText = $btnSave.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            dataType: "json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");

                $tableBanner.draw();
                $form.closest(".modal").modal("hide");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("submit", ".form-assign", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $parent = $form.closest(".tab-pane");
        var $card = $parent.find(".card");
        var $btnSave = $form.find("button[type='submit']");

        var url = $form.attr('action');
        var type = $form.attr('type');
        var data = $form.serialize();
        var btnText = $btnSave.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            dataType: "json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
                $form.find("select[name]").val(null).trigger("change");
                $card.getAssignedBanner();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("click", ".btn-refresh-table", function (e) {
        $tableBanner.draw();
    });

    $(document).on("click", "[data-trigger=update]", function(e) {
        e.preventDefault();
        
        var url = $(this).attr('href');
        var $form = $(".form-banner.update");
        var $defaultForm = $form.html();
        var $modal = $form.closest(".modal");

        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (json) {
            if (json.success) {
                var data = json.data.item;

                $form.find('input[name=id]').val(data.id);
                $form.find('input[name=name]').val(data.name);
                
                var $banner = $modal.find(".banner-items > .card").first().clone(true);
                $banner.find("input[name=title], textarea[name=description], input[name=image]").val("");
                $banner.find("input[name=order]").val(0);
                var banner = $('<div>').append($banner).html();

                var items = data.items ? data.items : [];
                var banners = groupingBanner(items);

                var $row = $('<div>');
                var total = banners ? banners.length : 0;
                for (var i = 1; i < total; i++) {
                    $row.append(banner);
                }
                $modal.find(".banner-items").append($row.html());

                if (total > 0) {
                    $modal.find(".banner-items > .card").each(function (i) {
                        var index = i;
                        var $item = $(this);
                        var items = banners[i];
                        
                        var $tabs = $item.find(".nav.nav-tabs");
                        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

                        var $tabContent = $item.find(".tab-content");
                        var tabPaneContent = $('<div>').append($tabContent.find(".tab-pane").first().clone(true)).html();

                        $tabs.html("");
                        $tabContent.html("");
                        $.each(items, function (i, item) {
                            var $thisTab = $(tab);
                            var $thisTabPane = $(tabPaneContent);

                            var href = $thisTab.attr("href").replace("default", index + "-" + item.lang.code);
                            var tabID = href.replace("#", "");
                            
                            $thisTab.attr("href", href);
                            $thisTab.html(item.lang.name);

                            $tabs.append($thisTab);

                            var image = item.image ? item.image : "";
                            var src = image.match(/^http/) ? image : "/static/uploads/media/" + image;
                            
                            $thisTabPane.attr("id", tabID);
                            $thisTabPane.find("input[name=title]").val(item.title);
                            $thisTabPane.find("input[name=lang]").val(item.lang.code);
                            $thisTabPane.find("input[name=order]").val(item.order);
                            $thisTabPane.find("textarea[name=description]").val(item.description);
                            $thisTabPane.find("input[name=image]").val(image).changeBannerImage();
                        
                            $tabContent.append($thisTabPane);
                        });
                        $tabs.find('a.nav-link:first-child').tab('show');
                    });
                    $modal.find(".banner-items > .card").removeClass("d-none");
                } else {
                    $modal.find(".banner-items > .card").addClass("d-none");
                }

                $modal.modal("show").data({
                    defaultForm: $defaultForm,
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=assign]", function(e) {
        e.preventDefault();
        
        var $self = $(this);
        var $modal = $("#modal-assign-banner");
        var $form = $modal.find("form.form-assign");
        var $tabs = $modal.find(".nav.nav-tabs");
        
        var id = $self.data("id");
        var name = $self.closest("tr").find("td").eq(1).text();
        var modalTitle = "Assign Banner " + name.ucwords();

        $form.find("input[name=id]").val(id);
        $modal.find(".modal-title").html(modalTitle);

        $tabs.find('a.nav-link:first-child').tab('show');
        $modal.modal("show");
    });

    $(document).on("click", "[data-trigger=delete]", function (e) {
        e.preventDefault();
        
        var team = $(this).closest("tr").find("td:eq(1)").text();
        var accept = confirm("Are you sure to delete " + team + " ?");
        if (!accept) return;
        
        var $btn = $(this);
        var url = $(this).attr('href');
        var btnText = $btn.html();
        
        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");
                $tableBanner.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
                
                $btn.html(btnText);
            }
        });
    });

    $(document).on("click", "[data-trigger=delete-bi]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");
        var $item = isModalUpdate ? $self.closest(".card") : $self.closest(".row");

        $item.remove();
    });

    $(document).on("click", "[data-trigger=delete-tab]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $card = $self.closest(".card");

        var $translateToDropdown = $card.find('select.translate-to');

        var $tabs = $card.find(".nav.nav-tabs");
        var $navTabLinks = $tabs.find("a.nav-link");
        var $activeTab = $tabs.find("a.nav-link.active");

        var $tabContent = $card.find(".tab-content"); primaryLangJson
        var $activeTabPane = $tabContent.find(".tab-pane.active");

        var activeLang = $activeTab.text();
        var accept = confirm("Delete this page in " + activeLang + " language ?");
        if (!accept) return;

        if ($navTabLinks.length > 1) {
            $activeTab.remove();
            $activeTabPane.remove();
            $tabs.find('a.nav-link:last-child').tab("show");
        }

        var langIDs = $card.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.generateTranslateToDropdown(langIDs);
    });

    $(document).on("click", "[data-trigger=delete-assigned]", function (e) {
        e.preventDefault();
        var $self = $(this);
        var $card = $self.closest(".card");

        var item = $self.closest("tr").find("td:eq(1)").text();
        var accept = confirm("Are you sure to delete " + item + " ?");
        if (!accept) return;

        var url = $self.attr('href');
        var btnText = $self.html();

        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $self.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");
                $card.getAssignedBanner();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");

                $self.html(btnText);
            }
        });
    });

    $(document).on("change", "input[name=order]", function (e) {
        var $self = $(this);
        var $parent = $self.closest(".card-body");
        var value = $self.val();

        $parent.find("input[name=order]").val(value);
    });

    $(document).on("select2:select", "select.translate-to", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $modal.find("form.form-banner");
        var $parent = $self.closest(".card");
        var defaultForm = $modal.data("defaultForm");
        var $translateToDropdown = $parent.find('select.translate-to');

        var lang = $self.select2("data")[0];

        var $tabs = $parent.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $parent.find(".tab-content");
        var $tabPaneSource = $tabContent.find(".tab-pane").first();

        var $tab = $(tab);
        var $tabPane = $(defaultForm).find(".tab-pane");

        var href = $tab.attr("href").replace(/\w+$/, lang.id);
        var tabID = href.replace("#", "");
        var source = $tabPaneSource.find("input[name], select[name], textarea[name]").serializeObject();

        $tab.removeClass("active show");
        $tabPane.removeClass("active show");

        $tab.attr("href", href);
        $tab.html(lang.text);
        $tab.addClass("new");
        $tabs.append($tab);

        $tabPane.attr("id", tabID);
        $tabPane.find("input[name=id]").val("");
        $tabPane.find("input[name=lang]").val(lang.id);
        $tabPane.find("input[name=title]").val("");
        $tabPane.find("input[name=order]").val(source.order);
        $tabPane.find("textarea[name=description]").val("");
        $tabPane.find("input[name=image]").val(source.image).changeBannerImage();

        $tabContent.append($tabPane);
        $tabs.find('a.nav-link:last-child').tab("show");

        var langIDs = $parent.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.generateTranslateToDropdown(langIDs);
    });

    $(document).on("selected.media.library", "input[name=image]", function (e) {
        e.preventDefault();

        $(this).changeBannerImage();
    });

    $(document).on("selected.multiple-media.library", ".banner-items", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");
        var defaultForm = $modal.data("defaultForm");
        var bannerSelector = ".banner-items";
        var itemSelector = bannerSelector + " > " + (isModalUpdate ? ".card" : ".row");
        var images = e.value ? e.value : [];
        
        var $items = $('<div>');
        var banner = $(defaultForm).find(bannerSelector).html();
        
        var i = $modal.find(itemSelector).first().is(":hidden") ? 1 : 0;
        for (i; i < e.value.length; i++) {
            $items.append(banner);
        }
        $modal.find(bannerSelector).append($items.html());

        if (images.length > 0) {
            var imageIndex = 0;
            $modal.find(itemSelector).each(function (i) {
                var $item = $(this);
                
                if ($item.is(":hidden")) {
                    var image = images[imageIndex];
                    var src = image.match(/^http/) ? image : "/static/uploads/media/" + image;

                    $item.find("input[name=lang]").val(primaryLang);
                    $item.find("input[name=image]").val(image).changeBannerImage();
                    $item.find("input[name=order]").val(0);

                    if (isModalUpdate) {
                        var $tab = $item.find("a.nav-link");
                        var $tabPane = $item.find(".tab-pane");
                        var $translateToDropdown = $item.find('select.translate-to');
                        
                        var href = $tab.attr("href").replace("default", i + "-" + primaryLangJson.code);
                        var tabID = href.replace("#", "");

                        $tab.attr("href", href);
                        $tab.html(primaryLangJson.name);
                        $tabPane.attr("id", tabID);

                        var langIDs = $item.find("input[name=lang]").map(function () {
                            return this.value;
                        }).get();
                        $translateToDropdown.destroyTranslateToDropdown();
                        $translateToDropdown.generateTranslateToDropdown(langIDs);

                        $tab.tab("show");
                    }

                    imageIndex++;
                }
            });
            $modal.find(itemSelector).removeClass("d-none");
        } else {
            $modal.find(itemSelector).addClass("d-none");
        }
    });

    $(document).on("show.bs.tab", "[data-toggle=tab]", function (e) {
        var $self = $(this);
        var $card = $self.closest(".card");
        var $btnDelete = $card.find("[data-trigger=delete-tab]");

        if ($self.index() > 0) {
            $btnDelete.removeClass("d-none");
        } else {
            $btnDelete.addClass("d-none");
        }
    });

    $(document).on("shown.bs.modal", "#modal-form-banner, #modal-form-banner-update", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form.form-banner");
        var isModalUpdate = $form.hasClass("update");

        if (isModalUpdate) {
            $modal.find(".banner-items > .card").each(function() {
                var $item = $(this);
                
                var langIDs = $item.find("input[name=lang]").map(function () {
                    return this.value;
                }).get();
                $item.find('select.translate-to').generateTranslateToDropdown(langIDs);
            })

        } else {
            var $defaultForm = $form.html();
            $modal.data("defaultForm", $defaultForm);
        }
    });

    $(document).on("shown.bs.modal", "#modal-assign-banner", function (e) {
        var $modal = $(this);
        
        $modal.find("div.modal-title").html("Assign Banner");
        $modal.find("div.page-data").getAssignedBanner();
        $modal.find("div.category-data").getAssignedBanner();
        $modal.find("div.group-content-data").getAssignedBanner();
        $modal.find("select[name=page_id]").generatePageDropdown();
        $modal.find("select[name=category_id]").generateCategoryDropdown();
        $modal.find("select[name=group_id]").generateGCDropdown();
    });
    
    $(document).on("hidden.bs.modal", "#modal-form-banner, #modal-form-banner-update", function (e) {
        var $modal = $(this);
        var defaultForm = $modal.data('defaultForm');
        var isModalUpdate = $modal.find("form").hasClass("update");

        if (isModalUpdate) {
            $modal.find('select.translate-to').destroyTranslateToDropdown();
        }

        $modal.find('form.form-banner').html(defaultForm);
        $modal.removeData();
    });
    
    $(document).on("hidden.bs.modal", "#modal-assign-banner", function (e) {
        var $modal = $(this);
        
        $modal.find("select[name=page_id], select[name=category_id], select[name=group_id]").destroySelectDropdown();
    });
});