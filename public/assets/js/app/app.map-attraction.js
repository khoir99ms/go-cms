$(function () {
    "use strict";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    var headerEditor = ace.edit("header-script");
    headerEditor.session.setMode("ace/mode/html");
    var bodyEditor = ace.edit("body-script");
    bodyEditor.session.setMode("ace/mode/html");

    $('[data-toggle="tooltip"]').tooltip();

    $('.select2').select2({
        placeholder: 'Choose',
        minimumResultsForSearch: Infinity,
    });

    $(document).on("click", "button.remove-filter", function(){
        $(this).closest(".row").remove();

        var length = $("#filter-area").find(".row").length
        if (length < 1) {
            $(".filterInfo").show();
        }
    });

    $(document).on("change", "select[name=icon_color]", function(){
        var currentColor = $(this).data("color");
        var color = $(this).val();
        var re = new RegExp(currentColor, "g");

        var $images = $("img[id^=pin]");
        $images.each(function(i) {
            var src = $(this).attr("src");
            src = src.replace(re, color);

            $(this).attr("src", src);
        });
        $(this).data("color", color);
    });

    $(document).on("change", "select[name=filter-icon-type]", function(){
        var $parent = $(this).closest(".row");
        var iconType = $(this).val();

        if (iconType == "image") {
            $parent.find(".filter-image-icon-div").show();
            $parent.find(".filter-image-font-div").hide();
        } else if (iconType == "font") {
            $parent.find(".filter-image-icon-div").hide();
            $parent.find(".filter-image-font-div").show();
        } else {
            $parent.find(".filter-image-icon-div").hide();
            $parent.find(".filter-image-font-div").hide();
        }
    });

    $(document).on("click", "button.add-filter", function () {
        var html = '<div class="row row-sm filter-item bd pd-l-10 pd-r-10 pd-t-10 mg-1 mg-b-10"> \
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1"> \
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Filter 1</label> \
                <div><button class="remove-filter btn btn-danger pull-right" data-toggle="tooltip" data-placement="top" title="Remove this menu position.">&times;</button></div> \
            </div> \
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2"> \
                <div class="form-group"> \
                    <label class="az-content-label tx-11 tx-medium tx-gray-600" data-toggle="tooltip" data-placement="top" title="Places filter menu name">Filter name</label> \
                    <input type="text" name="filter-name" class="form-control"> \
                </div> \
            </div> \
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1"> \
                <div class="form-group"> \
                    <input type="hidden" name="filter-icon" class="form-control"> \
                    <label class="az-content-label tx-11 tx-medium tx-gray-600" data-toggle="tooltip" data-placement="top" title="Places filter menu icon">Icon</label> \
                    <div><button class="btn btn-secondary iconpicker" data-target="filter-icon"></button></div> \
                </div> \
            </div> \
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2"> \
                <div class="form-group"> \
                    <label class="az-content-label tx-11 tx-medium tx-gray-600" data-toggle="tooltip" data-placement="top" title="Choose places categories for this link (you can choose more than one).">Filter Types</label> \
                    <select name="filter-types" class="form-control select2" placeholder="Start Types" multiple> \
                        <option value="accounting"> accounting</option> \
                        <option value="airport"> airport</option> \
                        <option value="amusement_park"> amusement park</option> \
                        <option value="aquarium"> aquarium</option> \
                        <option value="art_gallery"> art gallery</option> \
                        <option value="atm"> atm</option> \
                        <option value="bakery"> bakery</option> \
                        <option value="bank"> bank</option> \
                        <option value="bar"> bar</option> \
                        <option value="beauty_salon"> beauty salon</option> \
                        <option value="bicycle_store"> bicycle store</option> \
                        <option value="book_store"> book store</option> \
                        <option value="bowling_alley"> bowling alley</option> \
                        <option value="bus_station"> bus station</option> \
                        <option value="cafe"> cafe</option> \
                        <option value="campground"> campground</option> \
                        <option value="car_dealer"> car dealer</option> \
                        <option value="car_rental"> car rental</option> \
                        <option value="car_repair"> car repair</option> \
                        <option value="car_wash"> car wash</option> \
                        <option value="casino"> casino</option> \
                        <option value="cemetery"> cemetery</option> \
                        <option value="church"> church</option> \
                        <option value="city_hall"> city hall</option> \
                        <option value="clothing_store"> clothing store</option> \
                        <option value="convenience_store"> convenience store</option> \
                        <option value="courthouse"> courthouse</option> \
                        <option value="dentist"> dentist</option> \
                        <option value="department_store"> department store</option> \
                        <option value="doctor"> doctor</option> \
                        <option value="electrician"> electrician</option> \
                        <option value="electronics_store"> electronics store</option> \
                        <option value="embassy"> embassy</option> \
                        <option value="fire_station"> fire station</option> \
                        <option value="florist"> florist</option> \
                        <option value="funeral_home"> funeral home</option> \
                        <option value="furniture_store"> furniture store</option> \
                        <option value="gas_station"> gas station</option> \
                        <option value="gym"> gym</option> \
                        <option value="hair_care"> hair care</option> \
                        <option value="hardware_store"> hardware store</option> \
                        <option value="hindu_temple"> hindu temple</option> \
                        <option value="home_goods_store"> home goods store</option> \
                        <option value="hospital"> hospital</option> \
                        <option value="insurance_agency"> insurance agency</option> \
                        <option value="jewelry_store"> jewelry store</option> \
                        <option value="laundry"> laundry</option> \
                        <option value="lawyer"> lawyer</option> \
                        <option value="library"> library</option> \
                        <option value="liquor_store"> liquor store</option> \
                        <option value="local_government_office"> local government office</option> \
                        <option value="locksmith"> locksmith</option> \
                        <option value="lodging"> lodging</option> \
                        <option value="meal_delivery"> meal delivery</option> \
                        <option value="meal_takeaway"> meal takeaway</option> \
                        <option value="mosque"> mosque</option> \
                        <option value="movie_rental"> movie rental</option> \
                        <option value="movie_theater"> movie theater</option> \
                        <option value="moving_company"> moving company</option> \
                        <option value="museum"> museum</option> \
                        <option value="night_club"> night club</option> \
                        <option value="painter"> painter</option> \
                        <option value="park"> park</option> \
                        <option value="parking"> parking</option> \
                        <option value="pet_store"> pet store</option> \
                        <option value="pharmacy"> pharmacy</option> \
                        <option value="physiotherapist"> physiotherapist</option> \
                        <option value="plumber"> plumber</option> \
                        <option value="police"> police</option> \
                        <option value="post_office"> post office</option> \
                        <option value="real_estate_agency"> real estate agency</option> \
                        <option value="restaurant"> restaurant</option> \
                        <option value="roofing_contractor"> roofing contractor</option> \
                        <option value="rv_park"> rv park</option> \
                        <option value="school"> school</option> \
                        <option value="shoe_store"> shoe store</option> \
                        <option value="shopping_mall"> shopping mall</option> \
                        <option value="spa"> spa</option> \
                        <option value="stadium"> stadium</option> \
                        <option value="storage"> storage</option> \
                        <option value="store"> store</option> \
                        <option value="subway_station"> subway station</option> \
                        <option value="synagogue"> synagogue</option> \
                        <option value="taxi_stand"> taxi stand</option> \
                        <option value="train_station"> train station</option> \
                        <option value="transit_station"> transit station</option> \
                        <option value="travel_agency"> travel agency</option> \
                        <option value="university"> university</option> \
                        <option value="veterinary_care"> veterinary care</option> \
                        <option value="zoo"> zoo</option> \
                    </select> \
                </div> \
            </div> \
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2"> \
                <label class="az-content-label tx-11 tx-medium tx-gray-600" data-toggle="tooltip" data-placement="top" title="Script uses automatic marker icons for places, but you can override it with your own.">Marker</label> \
                <select name="filter-icon-type" class="form-control select2"> \
                    <option value="automatic" selected>automatic</option> \
                    <option value="image">image</option> \
                    <option value="font">font-icon</option> \
                </select> \
            </div> \
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2"> \
                <div class="form-group filter-image-icon-div" style="display: none;"> \
                    <label class="az-content-label tx-11 tx-medium tx-gray-600" data-toggle="tooltip" data-placement="top" title="Enter marker image URL or path.">Marker Image</label> \
                    <input type="text" value="" name="filter-image-icon" class="form-control"> \
                </div> \
                <div class="form-group filter-image-font-div" style="display: none;"> \
                    <input type="hidden" name="filter-image-font"> \
                    <label class="az-content-label tx-11 tx-medium tx-gray-600" data-toggle="tooltip" data-placement="top" title="Enter MapIcon font here. You can browse icons here: http://map-icons.com/">Marker Font Icon</label> \
                    <div><button class="btn btn-secondary iconpicker" data-target="filter-image-font"></button></div> \
                </div> \
            </div> \
        </div>';
        
        $("#filter-area").removeClass("d-none").append(html);
        
        $('.select2').select2({
            placeholder: 'Choose',
            minimumResultsForSearch: Infinity,
        });
        
        $('.iconpicker').iconpicker({
            arrowClass: 'btn-primary',
            arrowPrevIconClass: 'fas fa-angle-left',
            arrowNextIconClass: 'fas fa-angle-right',
            cols: 6,
            iconset: 'mapicon',
            rows: 5,
            search: true,
            searchText: 'Search',
            selectedClass: 'btn-success',
            unselectedClass: ''
        }).on("change", function (e) { 
            var target = $(this).data("target");

            $(this).closest(".row").find("input[name=" + target + "]").val(e.icon);
        });	
        $(".filterInfo").hide();
    });

    $(document).on("submit", ".form-builder", function (e) {
        e.preventDefault();
        var $form = $(this);
        var $mapArea = $("#map-preview");
        var $filters = $("#filter-area").find(".row.filter-item");

        $("#card-map").addClass("d-none");
        $("#card-code").addClass("d-none");
        
        var host = $form.data("host");
        var bodyScript = '';
        var mapHtml = '\
        <section class="apoi-section">\
            <!-- MARKUP --> \
            <!-- FILTERS  --> \
            <div class="apoi-filter-container"> \
            </div> \
            \
            <!-- PLACES LIST CONTAINER--> \
            <div id="apoi-list"></div> \
            \
            <!-- PLACE DETAILS CONTAINER --> \
            <div id="apoi-details-container"></div> \
            \
            <!-- AUTOCOMPLETE INPUT --> \
            <div class="apoi-autocomplete-container"> \
                <input id="apoi-autocomplete" type="text" placeholder="Enter a location" /> \
                <a href="#" id="apoi-geolocation"><span class="apoi-map-icon map-icon-crosshairs"></span></a> \
            </div> \
            \
            <!-- TRANSPORT MODE --> \
            <div class="apoi-transport-modes"> \
                <input id="apoi-driving" type="radio" name="apoi-mode" value="DRIVING" checked /> \
                <label for="apoi-driving"><span class="apoi-map-icon map-icon-taxi-stand"></span></label> \
                <input id="apoi-bicycling" type="radio" name="apoi-mode" value="BICYCLING" /> \
                <label for="apoi-bicycling"><span class="apoi-map-icon map-icon-bicycling"></span></label> \
                <input id="apoi-transit" type="radio" name="apoi-mode" value="TRANSIT" /> \
                <label for="apoi-transit"><span class="apoi-map-icon map-icon-bus-station"></span></label> \
                <input id="apoi-walking" type="radio" name="apoi-mode" value="WALKING" /> \
                <label for="apoi-walking"><span class="apoi-map-icon map-icon-walking"></span></label> \
            </div> \
            \
            <!-- MAP/STREET VIEW SWITCH --> \
            <a href="#" id="apoi-switch-street"><span class="apoi-map-icon map-icon-male"></span></a> \
            <a href="#" id="apoi-switch-map"><span class="apoi-map-icon map-icon-map-pin"></span></a> \
            \
            <!-- ROUTE TIME --> \
            <div id="apoi-route-time"></div> \
            \
            <!-- MAP CONTAINER --> \
            <div id="apoi-maps-container"></div> \
            \
        </section>';
        var filterHtml = '';

        $filters.each(function(i) {
            var filterIconType = $(this).find("select[name=filter-icon-type]").val();
            var filterType = $(this).find("select[name=filter-types]").val();
            var filterIcon = $(this).find("input[name=filter-icon]").val();
            var filterName = $(this).find("input[name=filter-name]").val();
            
            if (filterIconType == "image") {
                var iconImageUrl = $(this).find("input[name=filter-image-icon]").val();
                filterHtml += '<div class="apoi-filter" data-types="' + filterType + '" data-icon-img="' + iconImageUrl + '"><span class="apoi-map-icon ' + filterIcon + '"></span>' + filterName + '</div>';
            } else if (filterIconType == "font") {
                var iconFont = $(this).find("input[name=filter-image-font]").val();
                filterHtml += '<div class="apoi-filter" data-types="' + filterType + '" data-icon-font="' + iconFont + '"><span class="apoi-map-icon ' + filterIcon + '"></span>' + filterName + '</div>';
            } else {
                filterHtml += '<div class="apoi-filter" data-types="' + filterType + '"><span class="apoi-map-icon ' + filterIcon + '"></span>' + filterName + '</div>';
            }
        });
        
        var start = $form.find("input[name=start]:checked").val();
        var address = $form.find("input[name=address]").val();
        var lat = $form.find("input[name=lat]").val();
        var lng = $form.find("input[name=lng]").val();
        var radius = $form.find("input[name=radius]").val();
        var streetView = $form.find("input[name=street_view]").is(":checked");
        var autocomplete = $form.find("input[name=autocomplete]").is(":checked");
        var userLocation = $form.find("input[name=user_location]").is(":checked");
        var routeMode = $form.find("input[name=route_mode]").is(":checked");
        var placesList = $form.find("input[name=places_list]").is(":checked");
        var placeDetails = $form.find("input[name=place_details]").is(":checked");
        var routeTime = $form.find("input[name=route_time]").is(":checked");
        var route = $form.find("input[name=route]").is(":checked");
        var multiple = $form.find("input[name=multiple]").is(":checked");
        var iconSize = $form.find("select[name=icon_size]").val();
        var iconType = $form.find("input[name=icon_type]:checked").val();
        var iconColor = $form.find("select[name=icon_color]").val();
        var startTypes = $form.find("select[name=start_types]").val();
        var startPointIcon = $form.find("input[name=start_point_icon]").val();
        var startTypesIcon = $form.find("input[name=start_types_icon]").val();
        var cluster = $form.find("input[name=cluster]").is(":checked");
        var zoom = $form.find("select[name=zoom]").val();
        var wheelScroll = $form.find("input[name=wheel_scroll]").is(":checked");
        var mapApiJs = $("#map-api-js").attr("src");
        
        var mapOpt = {
            start: start,
            address: address,
            pyrmont: lat && lng ? { lat: parseFloat(lat), lng: parseFloat(lng)} : null,
            radius: radius ? parseInt(radius) : '',			
            streetView: streetView,
            autocomplete: autocomplete,
            userLocation: userLocation,
            routeMode: routeMode,
            placesList: placesList,
            placeDetails: placeDetails,
            routeTime: routeTime,
            route: route,
            multiple: multiple,				
            iconSize: iconSize,
            iconType: iconType,
            iconColor: iconColor,				
            startTypes: startTypes.join(", "),
            startPointIcon: startPointIcon ? startPointIcon : host + "/static/images/map-icons/icon-pin1-blue.png",
            startTypesIcon: startTypesIcon ? startTypesIcon : host + "/static/images/map-icons/icon-pin1-red.png",
            cluster: cluster,
            zoom: zoom ? parseInt(zoom) : '',
            wheelScroll: wheelScroll
        };
        
        $mapArea.html(mapHtml).find(".apoi-filter-container").html(filterHtml);
        bodyScript = $mapArea.html();
        bodyScript += '\
        <script src="'+ mapApiJs +'"></script> \
        <script src="'+ host +'/static/lib/jquery-poi/jquery.awesomePOI.js"></script> \
        <script>\
            $( window ).load(function() { \
                $("#apoi-maps-container").awesomePOI('+ JSON.stringify(mapOpt) +'); \
            }); \
        </script>\
        ';

        bodyEditor.session.setValue(html_beautify(bodyScript));
        $mapArea.find('#apoi-maps-container').unbind().awesomePOI(mapOpt);

        $("#card-map").removeClass("d-none");
        $("#card-code").removeClass("d-none");
    });
});