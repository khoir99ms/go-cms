$(function () {
    "use strict";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    var allLang = [];
    var primaryLang = "";
    var primaryLangJson = {};
    var tagsGroupedLang = {};
    var tabSettingItems = [];

    var generateGroupItem = function (
        targetID,
        itemType,
        itemTypeLabel,
        refID,
        itemLang,
        itemTitle,
        itemProperty,
        itemDisplayTitle,
        itemDisplayImage,
        itemTags,
        itemTemplate
    ) {
        itemTypeLabel = itemTypeLabel == "gc" ? "Group Content" : itemTypeLabel;
        itemTags = itemTags ? itemTags : [];
        var displayImage = itemDisplayImage && itemDisplayImage.match(/^http/) ? itemDisplayImage : "/static/uploads/media/" + itemDisplayImage;
        var showDeleteBtnDisplayImage = itemDisplayImage ? "" : " d-none";
        
        // handling itemDisplayTitle is string
        var displayTitleJson = {};
        if (typeof itemDisplayTitle == "object") {
            displayTitleJson = itemDisplayTitle;
            itemDisplayTitle = itemDisplayTitle[primaryLang] ? itemDisplayTitle[primaryLang] : "";
        } else {
            displayTitleJson[primaryLang] = itemDisplayTitle;
        }

        var dropdownLang = "";
        $.each(allLang, function(k, v) {
            dropdownLang += '<a class="dropdown-item" href="#" data-trigger="change-display-title" data-lang="' + v.code + '">' + v.name +'</a>';
        });

        var html = '\
        <li> \
            <div class="card bd mg-b-5 tx-gray-600"> \
                <div class="card-header tx-medium cursor-pointer" data-toggle="collapse" data-target="#' + targetID + '"> \
                    ' + itemTitle.ucwords() + ' \
                    <span class="type-link text-capitalize float-right"> \
                        ' + itemTypeLabel.ucwords() + (itemProperty ? ' @ ' + itemProperty : "") + ' \
                        <a class="badge badge-danger delete-group-item tx-bold mg-l-10" style="color: #fff;">X</a> \
                    </span> \
                </div> \
                <div id="' + targetID + '" class="collapse" data-parent="#group_item"> \
                    <div class="card-body bd-t"> \
                        <input type="hidden" name="type" value="' + itemType + '" required> \
                        <input type="hidden" name="ref_id" value="' + refID + '"> \
                        <input type="hidden" name="lang" value="' + itemLang + '"> \
                        <div class="row row-sm"> \
                            <div class="col-md-9"> \
                                <div class="form-group"> \
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Display Title</label> \
                                    <div class="input-group"> \
                                        <div class="input-group-append"> \
                                            <div class="btn-group"> \
                                                <button type="button" class="btn btn-info active-lang text-uppercase pd-t-8 pd-b-8" data-lang="'+ primaryLang +'">'+ primaryLang +'</button> \
                                                <button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" style="padding: 8px 10px !important;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> \
                                                    <span class="sr-only">Toggle Dropdown</span> \
                                                </button> \
                                                <div class="dropdown-menu bd-1"> \
                                                    ' + dropdownLang + ' \
                                                </div> \
                                            </div> \
                                        </div> \
                                        <input type="text" name="title" class="form-control bd-l-0" value="'+ itemDisplayTitle + '" data-json=\'' + JSON.stringify(displayTitleJson) +'\' placeholder="Display Title"> \
                                    </div> \
                                </div><!-- form-group --> \
                                <div class="row row-sm"> \
                                    <div class="col-md-7"> \
                                        <div class="form-group"> \
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Tag</label> \
                                            <select name="tags" multiple class="form-control tags" data-value=\''+ JSON.stringify(itemTags) +'\' style="width:100% !important"> \
                                                <option></option> \
                                            </select> \
                                        </div><!-- form-group --> \
                                    </div> \
                                    <div class="col-md-5"> \
                                        <div class="form-group"> \
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Template</label> \
                                            <input type="text" name="template" class="form-control" value="'+ itemTemplate + '" placeholder="Template"> \
                                        </div><!-- form-group --> \
                                    </div> \
                                </div> \
                            </div> \
                            <div class="col-md-3"> \
                                <div class="form-group pos-relative"> \
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Display Image</label > \
                                    <div class="pos-relative"> \
                                        <img src="'+ displayImage + '" onerror="this.src=\'/static/images/default-img.png\';" class="img-fluid img-thumbnail wd-100p ht-120 display-image"> \
                                        <div class="image-toolbar pos-absolute" style="top: 3px; right: 8px;"> \
                                            <a href="#" data-toggle="media-library" data-parent-target="#' + targetID + '" data-input-target="input[name=display_image]" class="badge badge-info tx-bold">Change</a> \
                                            <a href="#" data-trigger="delete-display-image" class="badge badge-danger tx-bold'+ showDeleteBtnDisplayImage + '">X</a> \
                                        </div> \
                                    </div> \
                                    <input type="hidden" name="display_image" value="'+ itemDisplayImage + '"> \
                                </div> \
                            </div> \
                        </div> \
                    </div> \
                </div> \
            </div> \
        </li>';

        return html;
    }

    var getTagGroupedLang = function () {
        var $request = $.ajax({
            url: "/tag/grouped-lang", // wherever your data is actually coming from
        });

        $request.then(function (json) {
            tagsGroupedLang = json.data ? json.data : {};
            $(document).trigger("loaded.tag");
        });

        $request.always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    $.fn.serializeItem = function () {
        var $self = $(this);
        var $parent = $self.children();
        var results = [];      
        
        $parent.each(function () {
            var $li = $(this)

            var id = $li.find("input[name=ref_id]").val();
            var displayTitle = $li.find("input[name=title]").data("json");
            var displayImage = $li.find("input[name=display_image]").val();
            var tags = $li.find("select[name=tags]").val();
            var type = $li.find("input[name=type]").val();
            var lang = $li.find("input[name=lang]").val();
            var template = $li.find("input[name=template]").val();

            var item = {
                ref_id: id ? id : null,
                title: displayTitle,
                image: displayImage,
                tags: tags,
                type: type,
                lang: lang,
                template: template
            }
            results.push(item)
        });

        return results;
    }

    $.fn.getPrimaryLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang/primary' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data) {
                toastr["error"]("Primary lang isn't set.", "Failed");
            } else {
                primaryLang = json.data.code;
                primaryLangJson = json.data;
                $self.trigger({
                    type: "loaded.primary-lang",
                    lang: json.data.code
                });
            }
        });
    }

    $.fn.getAllLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data.item) {
                toastr["error"]("All lang isn't set.", "Failed");
            } else {
                allLang = json.data.item;
                $(document).trigger("loaded.all-lang");
            }
        });
    }

    $.fn.getPages = function (params) {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $header = $parent.find(".card-header");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        params = params ? params : {};
        
        params.property_id = params.property_id ? params.property_id : "";
        params.category_id = params.category_id ? params.category_id : "";

        $self.pagination({
            dataSource: "/page/primary",
            ajax: {
                data: {
                    property_id: params.property_id,
                    category_id: params.category_id,
                }
            },
            locator: 'data.item',
            alias: {
                pageNumber: "start",
                pageSize: "length"
            },
            totalNumberLocator: function (response) {
                // you can return totalNumber by analyzing response content
                return response.data.total;
            },
            pageSize: 10,
            showPageNumbers: false,
            showNavigator: true,
            callback: function (data, pagination) {
                var html = "";
                var cardTitle = "Pages";

                $.each(data, function (i, v) {
                    var property = (v.property.name ? v.property.name : v.brand.name + ' Brand');
                    if (i == 0) {
                        cardTitle += " @ " + property;
                    }

                    html += '\
                    <div class="form-check">\
                        <input class="form-check-input" type="checkbox" value="'+ v.id + '" id="page-' + v.id + '" data-lang="' + v.lang.code + '" data-property="' + property + '">\
                        <label class="form-check-label" for="page-' + v.id + '">\
                        <span class="title">' + v.title + '</span>\
                        </label>\
                    </div>';
                });

                if (data.length == 0) {
                    $alert.html('No items.').removeClass("d-none");
                    $self.addClass("d-none");
                } else {
                    $alert.addClass("d-none");
                    $self.removeClass("d-none");
                    $items.html(html);
                }
                $header.html(cardTitle);
            }
        });
    }

    $.fn.resetPages = function () {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $header = $parent.find(".card-header");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        $header.html("Pages");
        $alert.html("No items.").removeClass("d-none");
        $items.html("");
        $self.addClass("d-none");
    }

    $.fn.getCategories = function () {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        $self.pagination({
            dataSource: "/category/primary",
            ajax: {
                data: {}
            },
            locator: 'data.item',
            alias: {
                pageNumber: "start",
                pageSize: "length"
            },
            totalNumberLocator: function (response) {
                // you can return totalNumber by analyzing response content
                return response.data.total;
            },
            pageSize: 10,
            showPageNumbers: false,
            showNavigator: true,
            callback: function (data, pagination) {
                var html = "";
                $.each(data, function (i, v) {
                    html += '\
                    <div class="form-check">\
                        <input class="form-check-input" type="checkbox" value="'+ v.id + '" id="category-' + v.id + '" data-lang="' + v.lang.code + '">\
                        <label class="form-check-label" for="category-' + v.id + '">\
                        <span class="title">' + v.name + '</span>\
                        </label>\
                    </div>';
                });

                if (data.length == 0) {
                    $alert.html('No items.').removeClass("d-none");
                    $self.addClass("d-none");
                } else {
                    $alert.addClass("d-none");
                    $self.removeClass("d-none");
                    $items.html(html);
                }
            }
        });
    }

    $.fn.resetCategories = function () {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $header = $parent.find(".card-header");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        $alert.html("No items.").removeClass("d-none");
        $items.html("");
        $self.addClass("d-none");
    }

    $.fn.getGroupContent = function () {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        $self.pagination({
            dataSource: "/group_content/get",
            ajax: {
                data: {}
            },
            locator: 'data.item',
            alias: {
                pageNumber: "start",
                pageSize: "length"
            },
            totalNumberLocator: function (response) {
                // you can return totalNumber by analyzing response content
                return response.data.total;
            },
            pageSize: 10,
            showPageNumbers: false,
            showNavigator: true,
            callback: function (data, pagination) {
                var html = "";
                $.each(data, function (i, v) {
                    var label = v.name.replace(/-/g, ' ');
                    html += '\
                    <div class="form-check">\
                        <input class="form-check-input" type="checkbox" value="'+ v.id + '" id="gc-' + v.id + '" data-lang="">\
                        <label class="form-check-label" for="gc-' + v.id + '">\
                        <span class="title">' + label.ucwords() + '</span>\
                        </label>\
                    </div>';
                });

                if (data.length == 0) {
                    $alert.html('No items.').removeClass("d-none");
                    $self.addClass("d-none");
                } else {
                    $alert.addClass("d-none");
                    $self.removeClass("d-none");
                    $items.html(html);
                }
            }
        });
    }

    $.fn.resetGroupContent = function () {
        var $self = $(this);
        var $parent = $self.closest(".card");
        var $header = $parent.find(".card-header");
        var $alert = $parent.find(".alert");
        var $items = $parent.find(".items");

        $alert.html("No items.").removeClass("d-none");
        $items.html("");
        $self.addClass("d-none");
    }

    $.fn.generateTranslateToDropdown = function (exceptIDs) {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            exceptIDs = exceptIDs ? exceptIDs : [];

            // Select2 has been initialized
            var options = [];
            $.each(allLang, function (i, v) {
                if ($.inArray(v.code, exceptIDs) === -1) {
                    options.push({ id: v.code, text: v.name })
                }
            });

            if (options.length > 0) {
                $self.select2({
                    placeholder: 'Transalte To',
                    allowClear: true,
                    minimumResultsForSearch: Infinity,
                    data: options
                });
                $self.closest(".form-group").removeClass("d-none");
                $self.closest(".modal-header").addClass("pd-t-10 pd-b-10");
            } else {
                $self.closest(".form-group").addClass("d-none");
                $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
            }
        }
    }

    $.fn.generateSelectTagDropdown = function () {
        var $self = $(this);

        $self.each(function (i) {
            var $select = $(this);

            $select.destroySelectDropdown();

            $select.select2({
                placeholder: "Tag",
                searchInputPlaceholder: "Search",
                allowClear: true,
            });

            $.each(tagsGroupedLang[primaryLang], function (i, v) {
                var option = new Option(v.title, v.id, false, false);
                $select.append(option);
            });

            var value = $select.data("value");
            if (value) {
                $select.val(value);
            }
            $select.trigger('change');
        });

    }

    $.fn.destroySelectDropdown = function () {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("select2-hidden-accessible")) {
                $(this).select2('destroy');
                $(this).find("option").not(':first').remove();
                $(this).removeAttr("data-select2-id tabindex aria-hidden");
            }
        });
    }

    $.fn.destroyTranslateToDropdown = function () {
        var $self = $(this);

        $self.destroySelectDropdown();
        $self.closest(".form-group").addClass("d-none");
        $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
    }

    $.fn.generateTinyMce = function(options) {
        var $self = $(this);

        var defaults = {
            max_height: 800,
            placeholder: 'Type here ...',
            branding: false,
            // menubar: false,
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons autoresize',
            menubar: 'file edit view insert format tools table help',
            toolbar: 'preview code | undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | superscript subscript | outdent indent | table | numlist bullist | forecolor backcolor removeformat | hr pagebreak | charmap emoticons | fullscreen save print | insertfile image media template link anchor codesample | ltr rtl',
            image_caption: true,
            image_advtab: true,
            toolbar_sticky: true,
            toolbar_mode: 'sliding',
            importcss_append: true,
            allow_script_urls: true,
            relative_urls : false,
            remove_script_host : false,
            imagetools_toolbar: "imageoptions",
            content_css: ['/static/lib/tinymce/skins/content/default/content.min.css', '/static/lib/fontawesome-free/css/all.min.css'],
            quickbars_selection_toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | h1 h2 h3 blockquote | quicklink quicktable',
            quickbars_insert_toolbar: false,
            contextmenu: "link image table",
        };

        // Merge defaults and options, without modifying defaults
        var settings = $.extend({}, defaults, options ? options : {});

        $self.each(function () {
            $(this).tinymce(settings);
            $(this).addClass("has-tinymce");
        });
    }

    $.fn.destroyTinyMce = function() {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("has-tinymce")) {
                $(this).tinymce().destroy();
            }
        });
    }

    $.fn.changeGroupImage = function () {
        var $self = $(this);
        var $parent = $self.closest(".form-group");
        var $btnDelete = $parent.find("[data-trigger=delete-group-image]");

        var value = $self.val();
        var image = value.match(/^http/) ? value : "/static/uploads/media/" + value;
        if (value) {
            $btnDelete.removeClass("d-none");
        } else {
            $btnDelete.addClass("d-none");
        }
        $parent.find("img.group-image").attr("src", image);
    }

    $.fn.changeDisplayImage = function () {
        var $self = $(this);
        var $parent = $self.closest(".card-body");
        var $btnDelete = $parent.find("[data-trigger=delete-display-image]");

        var value = $self.val();
        var image = value.match(/^http/) ? value : "/static/uploads/media/" + value;
        if (value) {
            $btnDelete.removeClass("d-none");
        } else {
            $btnDelete.addClass("d-none");
        }
        $parent.find("img.display-image").attr("src", image);
    }

    $("select.flt-group-content").select2({
        placeholder: 'Select Group Content',
        allowClear: true,
        searchInputPlaceholder: 'Search',
        ajax: {
            url: "/group_content/get",
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                var options = [];
                $.each(data.data.item, function (i, v) {
                    options.push({ id: v.id, text: v.name })
                });

                return {
                    results: options
                };
            }
        },
    });

    $("select.flt-property").select2({
        placeholder: pageSession.level == "brand" ? pageSession.name + " Brand" : pageSession.name,
        searchInputPlaceholder: 'Search',
        allowClear: true,
        ajax: {
            url: "/utils/property_page_session",
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                var options = [];
                var item = data.data.item;
                if (!$.isArray(item)) {
                    item = [item];
                }

                $.each(item, function (i, v) {
                    if (v.property_code != "COR") {
                        options.push({ id: v.property_id, text: v.property_name })
                    }
                });

                return {
                    results: options
                };
            }
        },
    });

    $("select.flt-category").select2({
        placeholder: 'Select Category',
        allowClear: true,
        searchInputPlaceholder: 'Search',
        ajax: {
            url: "/category/primary",
            dataType: "json",
            data: function (params) {
                return {
                    search: {value: params.term}, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                var options = [];
                $.each(data.data.item, function (i, v) {
                    options.push({ id: v.id, text: v.name });
                });

                return {
                    results: options
                };
            }
        },
    });

    $(document).getAllLang();
    $(document).getPrimaryLang();

    var oldContainer;
    $("ol.group_item").sortable({
        group: 'nested',
        afterMove: function (placeholder, container) {
            if (oldContainer != container) {
                if (oldContainer)
                    oldContainer.el.removeClass("active");
                container.el.addClass("active");

                oldContainer = container;
            }
        },
        onDrop: function ($item, container, _super) {
            container.el.removeClass("active");
            _super($item, container);
        }
    });

    $(document).on("loaded.primary-lang", function (e) {
        getTagGroupedLang();
    });

    $(document).on("submit", ".form-group-content.filter", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $formUpdate = $(".form-group-content.update");
        var $formFilterItem = $(".form-group-content.filter-item");
        var $formSetting = $(".form-group-content.setting");
        var $modalSetting = $formSetting.closest(".modal");
        var $collapse = $("#accordion").find(".collapse");
        var defaultFormSetting = $formSetting.html();

        $formUpdate.find("input[name=group_id]").val("");
        $formUpdate.find("input[name=name]").val("");
        $formUpdate.find("input[name=image]").val("");
        $formUpdate.find("select[name=tags]").destroySelectDropdown();
        $formUpdate.find("#group_item").html("");

        tabSettingItems = [];
        $formSetting.find("input[name=lang]").val("");
        $formSetting.find("input[name=id]").val("");
        $modalSetting.removeData();

        $formFilterItem.find("select[name=property_id]").removeAttr("disabled");
        $formFilterItem.find("select[name=category_id]").removeAttr("disabled");
        
        var groupId = $form.find("select.flt-group-content").val();
        var propertyId = $formFilterItem.find("select[name=property_id]").val();
        var categoryId = $formFilterItem.find("select[name=category_id]").val();

        $(".pagesContainer").getPages({ property_id: propertyId, category_id: categoryId });
        $(".categoryContainer").getCategories();
        $(".groupContentContainer").getGroupContent();

        var $request = $.ajax({
            url: "/group_content/get",
            data: { groupId: groupId }
        });

        $request.then(function (data) {
            var group = data.data.item;
            
            $formUpdate.find("input[name=group_id]").val(group.id);
            $formUpdate.find("input[name=name]").removeAttr("disabled").val(group.name);
            $formUpdate.find("input[name=image]").val(group.image).changeGroupImage();
            
            tabSettingItems = group.setting ? group.setting : [];
            $formSetting.find("input[name=id]").val(group.id);

            var html = "";
            $.each(group.items, function (i, item) {
                var eID = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5));
                var targetID = item.type + "Item" + eID + item.ref_id,
                    itemType = item.type,
                    itemTypeLabel = item.type,
                    refID = item.id,
                    itemLang = item.lang,
                    itemTitle = item.title ? item.title.replace(/-/g, ' ') : item.title,
                    itemProperty = item.property && item.property.name ? item.property.name : (item.brand && item.brand.name ? item.brand.name + " Brand" : ""),
                    itemDisplayTitle = item.display_title,
                    itemDisplayImage = item.display_image,
                    itemTags = item.tags,
                    itemTemplate = item.template;

                html += generateGroupItem(
                    targetID,
                    itemType,
                    itemTypeLabel,
                    refID,
                    itemLang,
                    itemTitle,
                    itemProperty,
                    itemDisplayTitle,
                    itemDisplayImage,
                    itemTags,
                    itemTemplate
                );
            });

            $formUpdate.find(".group_item").append(html);
            $formUpdate.find("select[name=tags]").generateSelectTagDropdown();
        });

        $request.always(function () {
            $formUpdate.find(".btn-action").removeClass("d-none");
            $formUpdate.closest(".card-body").removeClass("d-none");
            
            $collapse.removeClass("show");
            $collapse.first().addClass("show");

            $modalSetting.data("defaultForm", defaultFormSetting);
        });
    });

    $(document).on("submit", ".form-group-content.add", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $modal = $form.closest(".modal");
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr('action');
        var type = $form.attr('type');
        var data = $form.serialize();
        var btnText = $btnSave.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");

                $modal.modal("hide");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("submit", ".form-group-content.update", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");
        var btnText = $btnSave.html();

        var groupId = $form.find("input[name=group_id]").val();
        var groupName = $form.find("input[name=name]").val();
        var groupImage = $form.find("input[name=image]").val();
        var groupItem = $("ol#group_item").serializeItem();

        var data = {
            name: groupName,
            image: groupImage,
            items: groupItem
        }

        $.ajaxq("aq", {
            url: "/group_content/" + groupId,
            data: JSON.stringify(data),
            type: $form.attr("type"),
            contentType: 'application/json',
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (json) {
            if (json.success) {
                toastr["success"](json.message, "success");
                tabSettingItems = json.data && json.data.group_content && json.data.group_content.setting ? json.data.group_content.setting : [];
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("submit", ".form-group-content.setting", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $modal = $form.closest(".modal");
        var $btnSave = $form.find("button[type=submit]");
        var btnText = $btnSave.html();

        var id = $form.find("input[name=id]").val();
        var url = $form.attr("action").replace(":id", id);
        var type = $form.attr("type");

        var params = [];
        $form.find(".tab-content .tab-pane").each(function (index) {
            // clean editor
            $(this).find("textarea[name]").each(function () {
                var content = cleanString($(this).val());
                $(this).val(content);
            });
            params.push($(this).find("input[name], select[name], textarea[name]").serializeObject());
        });

        $.ajaxq("aq", {
            url: url,
            data: JSON.stringify(params),
            type: type,
            contentType: "application/json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (json) {
            if (json.success) {
                toastr["success"](json.message, "success");
                tabSettingItems = json.data && json.data.group_content ? json.data.group_content.setting : [];

                $modal.modal("hide");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("keyup", "input[name=name]", function (e) {
        e.preventDefault();
        this.value = this.value.toLowerCase();
        $.utils.no_spaces(this, '-');
    });

    $(document).on("click", ".add-to", function (e) {
        var $formUpdate = $("form.form-group-content.update");
        var selectTagCount = $formUpdate.find("select[name=tags]").length;

        var $itemChecked = $(this).closest(".collapse").find(".items > .form-check input:checked");

        var html = "";
        $itemChecked.each(function (i) {
            var eID = new Date().getTime();
            var $parent = $(this).closest(".form-check");
            var $checkbox = $parent.find("input[type=checkbox]");

            var typeId = $checkbox.attr("id").split("-");
            var lang = $checkbox.data("lang");
            var property = $checkbox.data("property");

            var targetID = typeId[0] + "Item" + eID + typeId[1],
                itemType = typeId[0],
                itemTypeLabel = typeId[0],
                refID = typeId[1],
                itemLang = lang,
                itemTitle = $parent.find("label.form-check-label > .title").html().trim(),
                itemProperty = property,
                itemDisplayTitle = '',
                itemDisplayImage = '',
                itemTags = [],
                itemTemplate = '';

            html += generateGroupItem(
                targetID,
                itemType,
                itemTypeLabel,
                refID,
                itemLang,
                itemTitle,
                itemProperty,
                itemDisplayTitle,
                itemDisplayImage,
                itemTags,
                itemTemplate
            );
            this.checked = false;
        });
        $formUpdate.find(".group_item").append(html);

        var selectTagNewCount = $formUpdate.find("select[name=tags]").length;
        for (var i = selectTagCount; i < selectTagNewCount; i++) {
            $formUpdate.find("select[name=tags]").eq(i).generateSelectTagDropdown();
        }
    });

    $(document).on("click", "[data-trigger=change-display-title]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $parent = $self.closest(".form-group");
        var $btnLang = $parent.find(".btn.active-lang");
        var $inputTitle = $parent.find("input[name=title]");
        
        var lang = $self.data("lang");
        var data = $inputTitle.data("json");
        var displayTitle = data[lang] ? data[lang] : "";

        $btnLang.data("lang", lang).html(lang);
        $inputTitle.val(displayTitle);
    });

    $(document).on("click", "[data-trigger=delete-group-image]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $form = $(this).closest("form");
        $form.find("input[name=image]").val("").changeGroupImage();
    });

    $(document).on("click", "[data-trigger=delete-display-image]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $parent = $(this).closest(".card-body");
        $parent.find("input[name=display_image]").val("").changeDisplayImage();
    });

    $(document).on("click", ".delete-group", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $form = $self.closest("form");
        var $formFilter = $(".form-group-content.filter");
        var $formFilterItem = $(".form-group-content.filter-item");
        var $formSetting = $(".form-group-content.setting");
        var $modalSetting = $formSetting.closest(".modal");
        var defaultFormSetting = $modalSetting.data("defaultForm");

        var groupId = $form.find("input[name=group_id]").val();
        if (!groupId) return;
        var groupName = $form.find("input[name=name]").val();
        var accept = confirm("Are you sure to delete group " + groupName + " ?");
        if (!accept) return;

        var url = $(this).attr('href');
        var btnText = $self.html();

        $.ajaxq("aq", {
            url: url + "/" + groupId,
            type: "delete",
            beforeSend: function () {
                $self.html(waitText);
            }
        }).done(function (json) {
            if (json.success) {
                toastr["success"](json.message, "Success");

                $form.closest(".card-body").addClass("d-none");
                $form.find("input[name=group_id]").val("");
                $form.find("input[name=name]").prop("disabled", true).val("");
                $form.find("input[name=image]").val("").changeGroupImage();
                $form.find("#group_item").html("");
                $form.find(".btn-action").addClass("d-none");
                
                $formSetting.html(defaultFormSetting);
                $modalSetting.removeData();

                $formFilter.find("select.flt-group-content").val(null).trigger("change");
                $formFilterItem.find("select[name=property_id]").val(null).trigger("change").attr('disabled', true);
                $formFilterItem.find("select[name=category_id]").val(null).trigger("change").attr('disabled', true);

                $('.pagesContainer').resetPages();
                $('.categoryContainer').resetCategories();
                $('.groupContentContainer').resetGroupContent();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $self.html(btnText);
        });
    });

    $(document).on("click", ".delete-group-item", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        $(this).closest("li").remove();
    });

    $(document).on("click", "[data-trigger=delete-tab]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $self.closest("form");

        var $translateToDropdown = $modal.find('select.translate-to');

        var $tabs = $form.find(".nav.nav-tabs");
        var $navTabLinks = $tabs.find("a.nav-link");
        var $activeTab = $tabs.find("a.nav-link.active");

        var $tabContent = $form.find(".tab-content");
        var $activeTabPane = $tabContent.find(".tab-pane.active");

        var activeLang = $activeTab.text();
        var accept = confirm("Delete this setting in " + activeLang + " language ?");
        if (!accept) return;

        if ($activeTab.hasClass("new")) {
            $activeTab.remove();
            $activeTabPane.remove();
            $tabs.find('a.nav-link:last-child').tab("show");

            var langIDs = $form.find("input[name=lang]").map(function () {
                return this.value;
            }).get();
            $translateToDropdown.destroyTranslateToDropdown();
            $translateToDropdown.getTranslateToDropdown(langIDs);
        } else {
            var id = $form.find("input[name=id]").val();
            var lang = $activeTabPane.find("input[name=lang]").val();
            var url = $self.attr("href");
            var btnText = $self.html();

            $.ajaxq("aq", {
                url: url + '/' + id + '/' + lang,
                type: "delete",
                beforeSend: function () {
                    $self.html(waitText);
                }
            }).done(function (json) {
                if (json.success) {
                    toastr["success"](json.message, "Success");
                    
                    tabSettingItems = json.data && json.data.group_content ? json.data.group_content.setting : [];

                    if ($navTabLinks.length > 1) {
                        $activeTab.remove();
                        $activeTabPane.remove();
                        $tabs.find('a.nav-link:last-child').tab("show");
                    }

                    var langIDs = $form.find("input[name=lang]").map(function () {
                        return this.value;
                    }).get();
                    $translateToDropdown.destroyTranslateToDropdown();
                    $translateToDropdown.generateTranslateToDropdown(langIDs);
                }
            }).always(function (jqXHR) {
                if (!jqXHR.success) {
                    var r = jqXHR.responseJSON;
                    toastr["error"](r.error, "Failed");
                }
                $self.html(btnText);
            });
        }
    });

    $(document).on("keyup", ".group_item input[name=title]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $parent = $self.closest(".form-group");
        var $btnLang = $parent.find(".btn.active-lang");

        var title = $self.val();
        var lang = $btnLang.data("lang");
        var data = $self.data("json");

        data[lang] = title;
        $self.data("json", data);
    });

    $(document).on("select2:select select2:clear", "select.flt-property, select.flt-category", function (e) {
        e.preventDefault();
        var $self = $(this);
        var $form = $self.closest("form");
        var propertyId = $form.find("select[name=property_id]").val();
        var categoryId = $form.find("select[name=category_id]").val();

        $(".pagesContainer").getPages({ property_id: propertyId, category_id: categoryId });
    });

    $(document).on("select2:select", "select.translate-to", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $modal.find("form.form-group-content.setting");
        var $parent = $self.closest(".card");
        var $translateToDropdown = $modal.find('select.translate-to');
        var defaultForm = $modal.data("defaultForm");

        var lang = $self.select2("data")[0];

        var $tabs = $modal.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();
        var $tabContent = $modal.find(".tab-content");
        var $tabPaneSource = $tabContent.find(".tab-pane").first();

        var $tab = $(tab);
        var $tabPane = $(defaultForm).find(".tab-pane");

        var href = $tab.attr("href").replace(/\w+$/, lang.id);
        var tabID = href.replace("#", "");
        var source = $tabPaneSource.find("input[name], select[name], textarea[name]").serializeObject();

        $tab.removeClass("active show");
        $tabPane.removeClass("active show");

        $tab.attr("href", href);
        $tab.html(lang.text);
        $tab.addClass("new");
        $tabs.append($tab);

        $tabPane.attr("id", tabID)
        $tabPane.find("input[name=lang]").val(lang.id);
        $tabPane.find("input[name=title]").val("");
        $tabPane.find("textarea[name=description]").val("");
        $tabPane.find("input[name=label_detail]").val("");
        $tabPane.find("input[name=label_more]").val("");

        $tabContent.append($tabPane);
        $tabs.find('a.nav-link:last-child').tab("show");

        $tabPane.find("textarea[name=description]").generateTinyMce({menubar: false});
        var langIDs = $form.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.generateTranslateToDropdown(langIDs);
    });

    $(document).on("selected.media.library", "input[name=image]", function (e) {
        e.preventDefault();
        var $parent = $(this).closest(".form-group");
        var image = e.value.match(/^http/) ? e.value : "/static/uploads/media/" + e.value;

        $parent.find("[data-trigger=delete-group-image]").removeClass("d-none");
        $parent.find("img.group-image").attr("src", image);
    });

    $(document).on("selected.media.library", "input[name=display_image]", function (e) {
        e.preventDefault();
        var $parent = $(this).closest(".card-body");
        var image = e.value.match(/^http/) ? e.value : "/static/uploads/media/" + e.value;

        $parent.find("[data-trigger=delete-display-image]").removeClass("d-none");
        $parent.find("img.display-image").attr("src", image);
    });

    $(document).on("show.bs.tab", "[data-toggle=tab]", function (e) {
        var $modal = $(this).closest(".modal");
        var $btnDelete = $modal.find("[data-trigger=delete-tab]");

        if ($(this).index() > 0) {
            $btnDelete.removeClass("d-none");
        } else {
            $btnDelete.addClass("d-none");
        }
    });

    $(document).on("show.bs.modal", "#modal-form-setting", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form.form-group-content.setting");
        var $formUpdate = $("form.form-group-content.update");
        var $translateToDropdown = $modal.find('select.translate-to');

        var id = $formUpdate.find("input[name=group_id]").val();
        var name = $formUpdate.find("input[name=name]").val();
        var title = "Group Content Setting > ";
        
        name = name.replace(/-/g, ' ');
        title += name.ucwords();

        $form.find("input[name=id]").val(id);
        $modal.find(".modal-title").html(title);

        var $tabs = $modal.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $modal.find(".tab-content");
        var tabPaneContent = $('<div>').append($tabContent.find(".tab-pane").first().clone(true)).html();

        var items = [];
        if (tabSettingItems.length == 0) {
            items.push({
                title: "",
                description: "",
                label_detail: "",
                label_more: "",
                meta_title: "",
                meta_keyword: "",
                meta_description: "",
                lang: primaryLang
            })
        } else {
            var primaryItems = [];
            var otherItems = [];
            $.each(tabSettingItems, function (i, item) {
                if (item.lang == primaryLang) {
                    primaryItems.push(item);
                } else {
                    otherItems.push(item);
                }
            });
            items = $.merge(primaryItems, otherItems);
        }

        $tabs.html("");
        $tabContent.html("");
        $.each(items, function (i, item) {
            var $thisTab = $(tab);
            var $thisTabPane = $(tabPaneContent);
            
            var sourceLang = $.grep(allLang, function (v) {
                return v.code == item.lang;
            });
            var itemLang = sourceLang.length > 0 ? sourceLang[0] : sourceLang;
            
            var href = $thisTab.attr("href").replace(/\w+$/, itemLang.code);
            var tabID = href.replace("#", "");

            $thisTab.attr("href", href);
            $thisTab.html(itemLang.name);

            $tabs.append($thisTab);

            $thisTabPane.attr("id", tabID);
            $thisTabPane.find("input[name=lang]").val(itemLang.code);
            $thisTabPane.find("input[name=title]").val(item.title);
            $thisTabPane.find("textarea[name=description]").val(item.description);
            $thisTabPane.find("input[name=label_detail]").val(item.label_detail);
            $thisTabPane.find("input[name=label_more]").val(item.label_more);
            $thisTabPane.find("input[name=meta_title]").val(item.meta_title);
            $thisTabPane.find("input[name=meta_keyword]").val(item.meta_keyword);
            $thisTabPane.find("input[name=meta_description]").val(item.meta_description);

            $tabContent.append($thisTabPane);
        });
        $tabs.find('a.nav-link:first-child').tab('show');

        var langIDs = $modal.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.generateTranslateToDropdown(langIDs);
    });

    $(document).on("shown.bs.modal", "#modal-form-setting", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form.form-group-content.setting");

        $form.find("textarea[name=description]").generateTinyMce({menubar: false});
    });

    $(document).on("hidden.bs.modal", "#modal-form-setting", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form.form-group-content.setting");
        var defaultForm = $modal.data("defaultForm");

        $modal.find('select.translate-to').destroyTranslateToDropdown();
        $form.find("textarea[name=description]").destroyTinyMce();

        $form.html(defaultForm);
    });
});