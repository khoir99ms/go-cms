$(function () {
    "use strict";
    
    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    var allLang = [];
    var primaryLang = "";
    var primaryLangJson = {};

    $.fn.getAllLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data.item) {
                toastr["error"]("All lang isn't set.", "Failed");
            } else {
                allLang = json.data.item;
                $self.trigger("loaded.all-lang");
            }
        });
    }

    $.fn.getPrimaryLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang/primary' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data) {
                toastr["error"]("Primary lang isn't set.", "Failed");
            } else {
                primaryLang = json.data.code;
                primaryLangJson = json.data;
                $self.val(json.data.code).trigger({
                    type: "loaded.primary-lang",
                    lang: json.data.code
                });
            }
        });
    }

    $.fn.generateTinyMce = function(options) {
        var $self = $(this);

        var defaults = {
            max_height: 800,
            menubar: false,
            branding: false,
            placeholder: 'Type here ...',
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons autoresize',
            toolbar: 'bold italic underline forecolor | fontsizeselect | alignleft aligncenter alignright | bullist | code',
            image_advtab: true,
            toolbar_sticky: true,
            toolbar_mode: 'sliding',
            importcss_append: true,
            allow_script_urls: true,
            relative_urls : false,
            remove_script_host : false,
            imagetools_toolbar: "imageoptions",
            content_css: ['/static/lib/tinymce/skins/content/default/content.min.css', '/static/lib/fontawesome-free/css/all.min.css'],
            quickbars_selection_toolbar: 'bold italic underline | alignleft aligncenter alignright',
            quickbars_insert_toolbar: false,
            forced_root_block: "",
        };

        // Merge defaults and options, without modifying defaults
        var settings = $.extend({}, defaults, options ? options : {});

        $self.each(function () {
            $(this).tinymce(settings);
            $(this).addClass("has-tinymce");
        });
    }

    $.fn.destroyTinyMce = function() {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("has-tinymce")) {
                $(this).tinymce().destroy();
            }
        });
    }

    $.fn.generatePageDropdown = function () {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            var $request = $.ajax({
                url: '/page/get',
                data: {lang: primaryLang},
            });

            $request.then(function (json) {
                var options = [];
                $.each(json.data.item, function (i, v) {
                    options.push({ id: v.id, text: v.title });
                });

                $self.destroySelectDropdown();

                $self.select2({
                    placeholder: 'Select Page',
                    allowClear: true,
                    data: options
                });
            });
        }
    }

    $.fn.generateCategoryDropdown = function () {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            var $request = $.ajax({
                url: '/category/get',
                data: {lang: primaryLang},
            });

            $request.then(function (json) {
                var options = [];
                $.each(json.data.item, function (i, v) {
                    options.push({ id: v.id, text: v.name });
                });

                $self.destroySelectDropdown();

                $self.select2({
                    placeholder: 'Select Category',
                    allowClear: true,
                    data: options
                });
            });
        }
    }

    $.fn.generateGCDropdown = function () {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            var $request = $.ajax({
                url: '/group_content/get',
                data: {lang: primaryLang},
            });

            $request.then(function (json) {
                var options = [];
                $.each(json.data.item, function (i, v) {
                    options.push({ id: v.id, text: v.name });
                });

                $self.destroySelectDropdown();

                $self.select2({
                    placeholder: 'Select Group Content',
                    allowClear: true,
                    data: options
                });
            });
        }
    }

    $.fn.changeImage = function () {
        var $self = $(this);
        var $parent = $self.closest(".form-group");

        var value = $self.val();
        var image = value.match(/^http/) ? value : "/static/uploads/media/" + value;
        $parent.find("img.image").attr("src", image);
    }

    $.fn.generateTranslateToDropdown = function (exceptIDs) {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            exceptIDs = exceptIDs ? exceptIDs : [];

            // Select2 has been initialized
            var options = [];
            $.each(allLang, function (i, v) {
                if ($.inArray(v.code, exceptIDs) === -1) {
                    options.push({ id: v.code, text: v.name })
                }
            });

            if (options.length > 0) {
                $self.select2({
                    placeholder: 'Transalte To',
                    allowClear: true,
                    minimumResultsForSearch: Infinity,
                    data: options
                });
                $self.closest(".form-group").removeClass("d-none");
                $self.closest(".modal-header").addClass("pd-t-10 pd-b-10");
            } else {
                $self.closest(".form-group").addClass("d-none");
                $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
            }
        }
    }

    $.fn.destroySelectDropdown = function () {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("select2-hidden-accessible")) {
                $(this).select2('destroy');
                $(this).find("option").not(':first').remove();
                $(this).removeAttr("data-select2-id tabindex aria-hidden");
            }
        });
    }

    $.fn.destroyTranslateToDropdown = function () {
        var $self = $(this);

        $self.destroySelectDropdown();
        $self.closest(".form-group").addClass("d-none");
        $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
    }

    $.fn.getAssignedMessage = function () {
        var $self = $(this);
        var $tabPane = $self.closest(".tab-pane");
        var $form = $tabPane.find("form.form-assign");
        var $alert = $self.find("div.alert");
        var $table = $self.find("table");
        var $tbody = $table.find("tbody");

        var id = $form.find("input[name=id]").val();
        var url = $self.data("api");
        var type = $self.data("type");

        var $request = $.ajax({
            url: url,
            data: {id: id},
        });

        $request.then(function (json) {
            if (!json.success) {
                toastr["error"](r.error, "Failed");
            }

            var items = json.data.item ? json.data.item : [];
            
            if (items.length > 0) {
                var tbody = "";
                $.each(items, function (i, item) {
                    tbody += '\
                    <tr>\
                        <td>'+ (i + 1) + '</td>\
                        <td>'+ item.title + '</td>\
                        <td><a href="/banner/assigned/'+ type + '/' + item.banner_id + '/' + item.id + '" data-trigger="delete-assigned" class="badge badge-danger tx-bold">X</a></td>\
                    </tr>\
                    ';
                });
                $tbody.html(tbody);
                $table.closest("div").removeClass("d-none");
                $alert.addClass("d-none");
            } else {
                $table.closest("div").addClass("d-none");
                $alert.removeClass("d-none");
            }
        });
    }

    $(document).getAllLang();
    $("input.primary-lang").getPrimaryLang();

    var $tableMessage = $("#datatable-message").DataTable({
        responsive: true,
        language: {
            searchPlaceholder: "Search ...",
            sSearch: "",
            lengthMenu: "_MENU_ items/page",
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        processing: true,
        serverSide: true, 
        deferRender: true,
        ajax: {
            url: "/message",
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);

                json.recordsTotal = json.data.total;
                json.recordsFiltered = json.data.total;
                // json.recordsFiltered = json.data.item ? json.data.item.length : 0;
                json.data = json.data.item ? json.data.item : [];
                
                return JSON.stringify(json); // return JSON string
            },
        },
        initComplete: function (settings, json) {
            var api = new $.fn.dataTable.Api(settings);
            if (pageSession.level == "brand") {
                api.columns([4]).visible(false);
            } else {
                api.columns([3]).visible(false);
            }
        },
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: "name" },
            {
                data: "type",
                render: function (data, type, row, meta) {
                    return row.type ? row.type.capitalize() : "";
                }
            },
            {
                data: "brand.name",
                render: function (data, type, row, meta) {
                    var brand = "";
                    if (row.brand) {
                        brand = row.brand.name ? row.brand.name : "";
                    }
                    return brand;
                }
            },
            {
                data: "property.name",
                render: function (data, type, row, meta) {
                    var property = "";
                    if (row.property) {
                        property = row.property.name ? row.property.name : "";
                    }
                    return property;
                }
            },
            { data: "created_at" },
            { 
                data: "actions", 
                className: "",
                searchable: false, 
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a href="#" data-id="' + row.id + '" class="badge badge-info" data-trigger="assign">Assign</a> \
                            <a href="/message/' + row.id + '" class="badge badge-secondary" data-trigger="update">Edit</a> \
                            <a href="/message/' + row.id + '" class="badge badge-danger" data-trigger="delete">Delete</a>';
                }
            }
        ],
        order: [[1, "asc"]]
    });

    $("div.dataTables_filter input").unbind().keyup(function (e) {
        if (e.keyCode == 13) $tableMessage.search(this.value).draw();
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    $("select[name=type]").select2({
        placeholder: 'Select Type',
        allowClear: true,
        minimumResultsForSearch: Infinity
    });

    $("select[name=position]").select2({
        placeholder: 'Select Position',
        minimumResultsForSearch: Infinity
    });

    $("select[name=country_code]").select2({
        placeholder: 'Select Country Code',
        allowClear: true,
        minimumResultsForSearch: Infinity
    });

    $(document).on("submit", ".form-message", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSubmit = $form.find("button[type=submit]");

        var url = $form.attr("action");
        var type = $form.attr("type");
        var data = $form.serialize();
        var btnSubmit = $btnSubmit.html();
        var isUpdateMode = $form.hasClass("update");

        var data = $form.serializeObject();
        var headline = {}, description = {}, buttonLabel = {};
        if (isUpdateMode) {
            url = url + "/" + data.id;
            $form.find(".tab-content .tab-pane").each(function (index) {
                var param = $(this).find("input[name], select[name], textarea[name]").serializeObject();
                headline[param.lang] = param.headline;
                description[param.lang] = param.description;
                buttonLabel[param.lang] = param.button_label;
            });

            data.headline = headline;
            data.description = description;
            data.button_label = buttonLabel;
            data = JSON.stringify(data);
        } else {
            headline[primaryLang] = data.headline;
            description[primaryLang] = data.description;
            buttonLabel[primaryLang] = data.button_label;

            data.headline = headline;
            data.description = description;
            data.button_label = buttonLabel;
            data = JSON.stringify(data);
        }
        
        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            contentType: "application/json",
            beforeSend: function () {
                $btnSubmit.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");

                $tableMessage.draw();
                $form.closest(".modal").modal("hide");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSubmit.prop('disabled', false).html(btnSubmit);
        });
    });

    $(document).on("submit", ".form-assign", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $parent = $form.closest(".tab-pane");
        var $card = $parent.find(".card");
        var $btnSave = $form.find("button[type='submit']");

        var url = $form.attr('action');
        var type = $form.attr('type');
        var data = $form.serialize();
        var btnText = $btnSave.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            dataType: "json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
                $form.find("select[name]").val(null).trigger("change");
                $card.getAssignedBanner();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("click", ".btn-refresh-table", function (e) {
        $tableMessage.draw();
    });

    $(document).on("click", "[data-trigger=update]", function(e) {
        e.preventDefault();
        
        var url = $(this).attr('href');
        var $form = $(".form-message.update");
        var $defaultForm = $form.html();
        var $modal = $form.closest(".modal");

        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (json) {
            if (json.success) {
                var data = json.data;

                $form.find('input[name=id]').val(data.id);
                $form.find('input[name=name]').val(data.name);
                $form.find("select[name=type]").val(data.type).trigger('change');
                $form.find("input[name=image]").val(data.image).changeImage();
                $form.find("input[name=image_message]").prop("checked", data.setting.image_message);
                $form.find("select[name=country_code]").val(data.setting.country_code).trigger('change');
                $form.find('input[name=show_date]').val(data.setting.show_date);
                $form.find('input[name=end_date]').val(data.setting.end_date);
                $form.find("select[name=position]").val(data.setting.position).trigger('change');
                $form.find('input[name=backgroud]').val(data.setting.backgroud);
                $form.find("input[name=message_type]").prop("checked", data.setting.message_type);
                
                $modal.modal("show").data({
                    defaultForm: $defaultForm,
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=assign]", function(e) {
        e.preventDefault();
        
        var $self = $(this);
        var $modal = $("#modal-assign-banner");
        var $form = $modal.find("form.form-assign");
        var $tabs = $modal.find(".nav.nav-tabs");
        
        var id = $self.data("id");
        var name = $self.closest("tr").find("td").eq(1).text();
        var modalTitle = "Assign Banner " + name.ucwords();

        $form.find("input[name=id]").val(id);
        $modal.find(".modal-title").html(modalTitle);

        $tabs.find('a.nav-link:first-child').tab('show');
        $modal.modal("show");
    });

    $(document).on("click", "[data-trigger=delete]", function (e) {
        e.preventDefault();
        
        var team = $(this).closest("tr").find("td:eq(1)").text();
        var accept = confirm("Are you sure to delete " + team + " ?");
        if (!accept) return;
        
        var $btn = $(this);
        var url = $(this).attr('href');
        var btnText = $btn.html();
        
        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");
                $tableMessage.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
                
                $btn.html(btnText);
            }
        });
    });

    $(document).on("click", "[data-trigger=delete-image]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");
        var $item = isModalUpdate ? $self.closest(".card") : $self.closest(".row");

        $item.remove();
    });

    $(document).on("click", "[data-trigger=delete-tab]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $card = $self.closest(".card");

        var $translateToDropdown = $card.find('select.translate-to');

        var $tabs = $card.find(".nav.nav-tabs");
        var $navTabLinks = $tabs.find("a.nav-link");
        var $activeTab = $tabs.find("a.nav-link.active");

        var $tabContent = $card.find(".tab-content"); primaryLangJson
        var $activeTabPane = $tabContent.find(".tab-pane.active");

        var activeLang = $activeTab.text();
        var accept = confirm("Delete this page in " + activeLang + " language ?");
        if (!accept) return;

        if ($navTabLinks.length > 1) {
            $activeTab.remove();
            $activeTabPane.remove();
            $tabs.find('a.nav-link:last-child').tab("show");
        }

        var langIDs = $card.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.generateTranslateToDropdown(langIDs);
    });

    $(document).on("click", "[data-trigger=delete-assigned]", function (e) {
        e.preventDefault();
        var $self = $(this);
        var $card = $self.closest(".card");

        var item = $self.closest("tr").find("td:eq(1)").text();
        var accept = confirm("Are you sure to delete " + item + " ?");
        if (!accept) return;

        var url = $self.attr('href');
        var btnText = $self.html();

        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $self.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");
                $card.getAssignedBanner();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");

                $self.html(btnText);
            }
        });
    });

    $(document).on("change", "input[name=button_link]", function (e) {
        var $self = $(this);
        var $parent = $self.closest(".card-body");
        var value = $self.val();

        $parent.find("input[name=button_link]").val(value);
    });

    $(document).on("change", "input[name=button_color]", function (e) {
        var $self = $(this);
        var $parent = $self.closest(".card-body");
        var value = $self.val();

        $parent.find("input[name=button_color]").val(value);
    });

    $(document).on("change", "input.enable-date-restriction", function() {
        var $self = $(this);
        var $dateRestrict = $("div.date-restriction");
        var $showDate = $dateRestrict.find("input[name=show_date]");
        var $endDate = $dateRestrict.find("input[name=end_date]");

        if($self.is(':checked')) {
            $dateRestrict.removeClass("d-none");
            $showDate.prop("required", true);
            $endDate.prop("required", true);
        } else {
            $dateRestrict.addClass("d-none");
            $showDate.val("").prop("required", false);
            $endDate.val("").prop("required", false);
        }
    });

    $(document).on("select2:select", "select.translate-to", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $modal.find("form.form-banner");
        var $parent = $self.closest(".card");
        var defaultForm = $modal.data("defaultForm");
        var $translateToDropdown = $parent.find('select.translate-to');

        var lang = $self.select2("data")[0];

        var $tabs = $parent.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $parent.find(".tab-content");
        var $tabPaneSource = $tabContent.find(".tab-pane").first();

        var $tab = $(tab);
        var $tabPane = $(defaultForm).find(".tab-pane");

        var href = $tab.attr("href").replace(/\w+$/, lang.id);
        var tabID = href.replace("#", "");
        var source = $tabPaneSource.find("input[name], select[name], textarea[name]").serializeObject();

        $tab.removeClass("active show");
        $tabPane.removeClass("active show");

        $tab.attr("href", href);
        $tab.html(lang.text);
        $tab.addClass("new");
        $tabs.append($tab);

        $tabPane.attr("id", tabID);
        $tabPane.find("input[name=id]").val("");
        $tabPane.find("input[name=lang]").val(lang.id);
        $tabPane.find("input[name=title]").val("");
        $tabPane.find("input[name=order]").val(source.order);
        $tabPane.find("textarea[name=description]").val("");
        $tabPane.find("input[name=image]").val(source.image).changeBannerImage();

        $tabContent.append($tabPane);
        $tabs.find('a.nav-link:last-child').tab("show");

        var langIDs = $parent.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.generateTranslateToDropdown(langIDs);
    });

    $(document).on("selected.media.library", "input[name=image]", function (e) {
        e.preventDefault();

        $(this).changeImage();
    });

    $(document).on("selected.media.library", ".description-editor", function (e) {
        e.preventDefault();
        var $self = $(this);
        var origin = window.location.origin;

        var json = e.json;
        var file = json.file ? json.file.name : "";
        var src = json.url ? json.url.trim() : ""

        if (!src) {
            src = file ? origin + "/static/uploads/media/" + file : "";
        }

        var $html = "";
        if (json.type == "video") {
            var isYoutubeUrl = src.match(/(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=|\/sandalsResorts#\w\/\w\/.*\/))([^\/&]{10,12})/);
            var videoUrl = isYoutubeUrl ? "https://www.youtube.com/embed/" + isYoutubeUrl[1] + "?rel=0&showinfo=1&autoplay=0" : src.trim();
            $html += '<iframe style="width: 500px; height: 300px;" src="' + videoUrl + '" frameborder="0" allowfullscreen></iframe>';
        } else if (json.type == "attachment") {
            var caption = json.caption ? json.caption : json.title;
            $html += '<a href="' + src + '" target="_blank">' + caption + '</a>';
        } else {
            $html += '<img src="' + src + '" style="width: 190px;" onerror="this.src=\'https://via.placeholder.com/500x334?text=No Picture\'">';
        }
        $self.find("textarea.has-tinymce").tinymce().insertContent($html);
    });

    $(document).on("show.bs.tab", "[data-toggle=tab]", function (e) {
        var $self = $(this);
        var $card = $self.closest(".card");
        var $btnDelete = $card.find("[data-trigger=delete-tab]");

        if ($self.index() > 0) {
            $btnDelete.removeClass("d-none");
        } else {
            $btnDelete.addClass("d-none");
        }
    });

    $(document).on("shown.bs.modal", "#modal-form-message, #modal-form-message-update", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form.form-message");
        var isModalUpdate = $form.hasClass("update");

        if (isModalUpdate) {
            $modal.find(".banner-items > .card").each(function() {
                var $item = $(this);
                
                var langIDs = $item.find("input[name=lang]").map(function () {
                    return this.value;
                }).get();
                $item.find('select.translate-to').generateTranslateToDropdown(langIDs);
            })

        } else {
            var $defaultForm = $form.html();
            $modal.data("defaultForm", $defaultForm);
        }

        $modal.find("input[name=show_date], input[name=end_date]").each(function (i) {
            $(this).appendDtpicker({
                closeOnSelected: true,
                autodateOnStart: false
            });
        });

        $modal.find('textarea[name=headline], textarea[name=description]').generateTinyMce();
    });

    $(document).on("shown.bs.modal", "#modal-assign-message", function (e) {
        var $modal = $(this);
        
        $modal.find("div.modal-title").html("Assign Banner");
        $modal.find("div.page-data").getAssignedBanner();
        $modal.find("div.category-data").getAssignedBanner();
        $modal.find("div.group-content-data").getAssignedBanner();
        $modal.find("select[name=page_id]").generatePageDropdown();
        $modal.find("select[name=category_id]").generateCategoryDropdown();
        $modal.find("select[name=group_id]").generateGCDropdown();
    });
    
    $(document).on("hidden.bs.modal", "#modal-form-message, #modal-form-message-update", function (e) {
        var $modal = $(this);
        var defaultForm = $modal.data('defaultForm');
        var isModalUpdate = $modal.find("form").hasClass("update");

        if (isModalUpdate) {
            $modal.find('select.translate-to').destroyTranslateToDropdown();
        }
        
        $modal.find("input[name=show_date], input[name=end_date]").each(function (i) {
            $(this).handleDtpicker('destroy');
        });
        
        $modal.find('form.form-message').html(defaultForm);
        $modal.removeData();
    });  
});