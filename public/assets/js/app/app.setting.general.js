$(function () {
    "use strict";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    var allLang = [];
    var primaryLang = "";
    var primaryLangJson = {};
    
    var headerEditor = ace.edit("header-script");
    headerEditor.setOptions({
        autoScrollEditorIntoView: true,
        minLines: 8,
        maxLines: 50
    });
    headerEditor.session.setMode("ace/mode/html");
    var bodyEditor = ace.edit("body-script");
    bodyEditor.setOptions({
        autoScrollEditorIntoView: true,
        minLines: 8,
        maxLines: 50
    });
    bodyEditor.session.setMode("ace/mode/html");
    var bcEditor = ace.edit("booking-calendar-script");
    bcEditor.setOptions({
        autoScrollEditorIntoView: true,
        minLines: 8,
        maxLines: 50
    });
    bcEditor.session.setMode("ace/mode/html");
    var themesEditor = ace.edit("themes-script");
    themesEditor.setOptions({
        autoScrollEditorIntoView: true,
        minLines: 8,
        maxLines: 50
    });
    themesEditor.session.setMode("ace/mode/html");

    var arrangeMetaTab = function () {
        var meta = defaultMetaSetting ? defaultMetaSetting : {};
        var tabData = { title: {}, description: {}, keyword: {}};

        if (typeof meta.title == "string") {
            tabData["title"][primaryLang] = meta.title;
        } else {
            if ($.isEmptyObject(meta.title)) {
                tabData["title"][primaryLang] = "";
            } else {
                tabData["title"] = meta.title;
            }
        }
        if (typeof meta.description == "string") {
            tabData["description"][primaryLang] = meta.description;
        } else {
            if ($.isEmptyObject(meta.description)) {
                tabData["description"][primaryLang] = "";
            } else {
                tabData["description"] = meta.description;
            }
        }
        if (typeof meta.keyword == "string") {
            tabData["keyword"][primaryLang] = meta.keyword;
        } else {
            if ($.isEmptyObject(meta.keyword)) {
                tabData["keyword"][primaryLang] = "";
            } else {
                tabData["keyword"] = meta.keyword;
            }
        }

        var primary = {}, others = {};
        $.each(tabData, function (key, items) {
            $.each(items, function (lang, item) {
                if (lang == primaryLang) {
                    if (primary["lang"] === undefined) {
                        primary["lang"] = lang;
                    }
                    primary[key] = item;
                } else {
                    if (others[lang] === undefined) {
                        others[lang] = {};
                    }
                    if (others[lang]["lang"] === undefined) {
                        others[lang]["lang"] = lang;
                    }
                    others[lang][key] = item;
                }
            });
        });

        var results = [primary];
        $.each(others, function (i, other) {
            results.push(other);
        });

        return results;
    }

    $.fn.getAllLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data.item) {
                toastr["error"]("All lang isn't set.", "Failed");
            } else {
                allLang = json.data.item;
                $self.trigger("loaded.all-lang");
            }
        });
    }

    $.fn.getPrimaryLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang/primary' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data) {
                toastr["error"]("Primary lang isn't set.", "Failed");
            } else {
                primaryLang = json.data.code;
                primaryLangJson = json.data;
                $self.trigger({
                    type: "loaded.primary-lang",
                    lang: json.data.code
                });
            }
        });
    }

    $.fn.generateTranslateToDropdown = function (exceptIDs) {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            exceptIDs = exceptIDs ? exceptIDs : [];

            // Select2 has been initialized
            var options = [];
            $.each(allLang, function (i, v) {
                if ($.inArray(v.code, exceptIDs) === -1) {
                    options.push({ id: v.code, text: v.name })
                }
            });

            if (options.length > 0) {
                $self.select2({
                    placeholder: 'Transalte To',
                    allowClear: true,
                    minimumResultsForSearch: Infinity,
                    data: options
                });
                $self.closest(".form-group").removeClass("d-none");
            } else {
                $self.closest(".form-group").addClass("d-none");
            }
        }
    }

    $.fn.destroySelectDropdown = function () {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("select2-hidden-accessible")) {
                $(this).select2('destroy');
                $(this).find("option").not(':first').remove();
                $(this).removeAttr("data-select2-id tabindex aria-hidden");
            }
        });
    }

    $.fn.destroyTranslateToDropdown = function () {
        var $self = $(this);

        $self.destroySelectDropdown();
        $self.closest(".form-group").addClass("d-none");
    }

    $(document).getAllLang();
    
    $(document).on("loaded.all-lang", function () {
        $(document).getPrimaryLang();
    });

    $(document).on("loaded.primary-lang", function () {
        var $form = $("form.form-setting.meta");
        var $defaultForm = $form.html();
        var metaData = arrangeMetaTab();
        
        var $tabs = $form.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $form.find(".tab-content");
        var tabPaneContent = $('<div>').append($tabContent.find(".tab-pane").first().clone(true)).html();

        $tabs.html("");
        $tabContent.html("");
        $.each(metaData, function (i, item) {
            var $thisTab = $(tab);
            var $thisTabPane = $(tabPaneContent);

            var sourceLang = $.grep(allLang, function (v) {
                return v.code == item.lang;
            });
            var itemLang = sourceLang.length > 0 ? sourceLang[0] : sourceLang;

            var href = $thisTab.attr("href").replace("default", itemLang.code);
            var tabID = href.replace("#", "");

            $thisTab.attr("href", href);
            $thisTab.html(itemLang.name);

            $tabs.append($thisTab);

            $thisTabPane.attr("id", tabID);
            $thisTabPane.find("input[name=lang]").val(itemLang.code);
            $thisTabPane.find("input[name=meta_title]").val(item.title);
            $thisTabPane.find("input[name=meta_description]").val(item.description);
            $thisTabPane.find("input[name=meta_keyword]").val(item.keyword);

            $tabContent.append($thisTabPane);
        });
        $tabs.find('a.nav-link:first-child').tab('show');

        $form.trigger("loaded.meta-data").data({
            defaultForm: $defaultForm,
        });
    });

    $(document).on("loaded.meta-data", "form.form-setting.meta", function () {
        var $form = $(this);
        var $card = $form.find(".card");

        var langIDs = $card.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $card.find('select.translate-to').generateTranslateToDropdown(langIDs);
    });

    $(document).on("submit", ".form-setting:not(.meta)", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr('action');
        var type = $form.attr('type');
        var data;
        if ($form.hasClass("script")) {
            var hScript = headerEditor.getValue();
            var bScript = bodyEditor.getValue();
            var bcScript = bcEditor.getValue();
            var themesScript = themesEditor.getValue();
            data = { header: hScript, body: bScript, booking_calendar: bcScript, themes: themesScript}
        } else {
            data = $form.serialize();
        }
        var btnText = $btnSave.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("submit", ".form-setting.meta", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr('action');
        var type = $form.attr('type');
        var btnText = $btnSave.html();
        
        var data = {meta_title: {}, meta_description: {}, meta_keyword: {}};
        $form.find(".tab-pane").each(function () {
            var item = $(this).find("input[name], select[name], textarea[name]").serializeObject();

            data.meta_title[item.lang] = item.meta_title;
            data.meta_description[item.lang] = item.meta_description;
            data.meta_keyword[item.lang] = item.meta_keyword;
        });

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete-tab]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $card = $self.closest(".card");

        var $translateToDropdown = $card.find('select.translate-to');

        var $tabs = $card.find(".nav.nav-tabs");
        var $navTabLinks = $tabs.find("a.nav-link");
        var $activeTab = $tabs.find("a.nav-link.active");

        var $tabContent = $card.find(".tab-content"); primaryLangJson
        var $activeTabPane = $tabContent.find(".tab-pane.active");

        var activeLang = $activeTab.text();
        var accept = confirm("Delete this translation " + activeLang + " ?");
        if (!accept) return;

        if ($navTabLinks.length > 1) {
            $activeTab.remove();
            $activeTabPane.remove();
            $tabs.find('a.nav-link:last-child').tab("show");
        }

        var langIDs = $card.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.generateTranslateToDropdown(langIDs);
    });

    $(document).on("select2:select", "select.translate-to", function (e) {
        var $self = $(this);
        var $form = $self.closest("form");
        var $parent = $self.closest(".card");

        var defaultForm = $form.data("defaultForm");
        var lang = $self.select2("data")[0];

        var $tabs = $parent.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $parent.find(".tab-content");

        var $tab = $(tab);
        var $tabPane = $(defaultForm).find(".tab-pane");

        var href = $tab.attr("href").replace(/\w+$/, lang.id);
        var tabID = href.replace("#", "");

        $tab.removeClass("active show");
        $tabPane.removeClass("active show");

        $tab.attr("href", href);
        $tab.html(lang.text);
        $tab.addClass("new");
        $tabs.append($tab);

        $tabPane.attr("id", tabID);
        $tabPane.find("input[name=lang]").val(lang.id);
        $tabPane.find("input[name=title]").val("");
        $tabPane.find("input[name=description]").val("");
        $tabPane.find("input[name=keyword]").val("");

        $tabContent.append($tabPane);
        $tabs.find('a.nav-link:last-child').tab("show");

        var langIDs = $parent.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $self.destroyTranslateToDropdown();
        $self.generateTranslateToDropdown(langIDs);
    });

    var $selectLang = $('.select2.language')
    if (!$selectLang.hasClass("select2-hidden-accessible")) {
        // Select2 has been initialized
        var $request = $.ajax({
            url: '/utils/lang' // wherever your data is actually coming from
        });

        $request.then(function (data) {
            var options = [];
            $.each(data, function (i, v) {
                options.push({ id: i, text: i.toUpperCase() + ' - ' + v.name })
            });

            $selectLang.select2({
                placeholder: 'Choose',
                searchInputPlaceholder: 'Search',
                data: options
            });

            var selectedLang = $selectLang.data("lang");
            $selectLang.val(selectedLang).trigger('change');
        });
    }
});