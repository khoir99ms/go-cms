$(function () {
    "use strict";

    $(document).on("submit", ".reset-form", function(e){
        e.preventDefault();
        var $self   = $(this);
        var $msg    = $self.find(".alert");
        var url     = $self.attr("action");
        var data    = $self.serialize();

        var $btnSubmit = $self.find("button[type=submit]");
        var btnText = $btnSubmit.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: "put",
            dataType: "json",
            beforeSend: function() {
                $btnSubmit.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (json) {
            var msgClass = json.success ? "alert-info" : "alert-warning";
            
            $msg.html(json.message);
            $msg.removeClass("d-none");
            $msg.addClass(msgClass);
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                message = message.match(/is expired/i) ? "Reset key is invalid." : message;
                alert(message);
            }
            $btnSubmit.prop('disabled', false).html(btnText);
            $self[0].reset();
        });
        
    });
});
