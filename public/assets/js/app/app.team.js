$(function () {
    "use strict";
    
    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    $.fn.generateTinyMce = function(options) {
        var $self = $(this);

        var defaults = {
            max_height: 800,
            placeholder: 'Type here ...',
            branding: false,
            // menubar: false,
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons autoresize',
            menubar: 'file edit view insert format tools table help',
            toolbar: 'preview code | undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | superscript subscript | outdent indent | table | numlist bullist | forecolor backcolor removeformat | hr pagebreak | charmap emoticons | fullscreen save print | insertfile image media template link anchor codesample | ltr rtl',
            image_caption: true,
            image_advtab: true,
            toolbar_sticky: true,
            toolbar_mode: 'sliding',
            importcss_append: true,
            allow_script_urls: true,
            relative_urls : false,
            remove_script_host : false,
            imagetools_toolbar: "imageoptions",
            content_css: ['/static/lib/tinymce/skins/content/default/content.min.css', '/static/lib/fontawesome-free/css/all.min.css'],
            quickbars_selection_toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | h1 h2 h3 blockquote | quicklink quicktable',
            quickbars_insert_toolbar: false,
            contextmenu: "link image table",
        };

        // Merge defaults and options, without modifying defaults
        var settings = $.extend({}, defaults, options ? options : {});

        $self.each(function () {
            $(this).tinymce(settings);
            $(this).addClass("has-tinymce");
        });
    }

    $.fn.destroyTinyMce = function() {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("has-tinymce")) {
                $(this).tinymce().destroy();
            }
        });
    }

    $.fn.changePicture = function () {
        var $self = $(this);
        var $parent = $self.closest(".form-group");

        var value = $self.val();
        var image = value.match(/^http/) ? value : "/static/uploads/media/" + value;
        if (value) {
            $parent.find("[data-trigger=delete-picture]").removeClass("d-none");
        }
        $parent.find("img.picture").attr("src", image);
    }

    var $tableTeam = $("#datatable-team").DataTable({
        responsive: true,
        language: {
            searchPlaceholder: "Search ...",
            sSearch: "",
            lengthMenu: "_MENU_ items/page",
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        processing: true,
        serverSide: true, 
        deferRender: true,
        ajax: {
            url: "/team/get",
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);

                json.recordsTotal = json.data.total;
                json.recordsFiltered = json.data.total;
                // json.recordsFiltered = json.data.item ? json.data.item.length : 0;
                json.data = json.data.item ? json.data.item : [];
                
                return JSON.stringify(json); // return JSON string
            }
        },
        initComplete: function (settings, json) {
            var api = new $.fn.dataTable.Api(settings);
            if (pageSession.level == "brand") {
                api.columns([6]).visible(false);
            } else {
                api.columns([5]).visible(false);
            }
        },
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: "name" },
            { data: "position" },
            { 
                data: "socmed",
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    var socmed = "";
                    var mailRE = /^\S+@\S+$/;
                    var httpRE = /^http/i;

                    var email = row.email ? row.email : "";
                    var facebook = row.facebook ? row.facebook : "";
                    var twitter = row.twitter ? row.twitter : "";
                    var instagram = row.instagram ? row.instagram : "";
                    var linkedin = row.linkedin ? row.linkedin : "";
                    
                    if (email.match(mailRE)) {
                        socmed += "<i class='fas fa-envelope'></i> <a href='mailto:" + email +"' target='_blank'>" + email + "</a> <br>";
                    } else if (facebook) {
                        socmed += facebook + " <br>";
                    }

                    if (facebook.match(httpRE)) {
                        socmed += "<i class='fab fa-facebook-square'></i> <a href='" + facebook +"' target='_blank'>" + facebook + "</a> <br>";
                    } else if (facebook) {
                        socmed += facebook + " <br>";
                    }

                    if (twitter.match(httpRE)) {
                        socmed += "<i class='fab fa-twitter-square'></i> <a href='" + twitter + "' target='_blank'>" + twitter + "</a> <br>";
                    } else if (twitter) {
                        socmed += twitter + " <br>";
                    }

                    if (instagram.match(httpRE)) {
                        socmed += "<i class='fab fa-instagram'></i> <a href='" + instagram + "' target='_blank'>" + instagram + "</a> <br>";
                    } else if (instagram) {
                        socmed += instagram + " <br>";
                    }

                    if (linkedin.match(httpRE)) {
                        socmed += "<i class='fab fa-linkedin'></i> <a href='" + linkedin + "' target='_blank'>" + linkedin + "</a> <br>";
                    } else if (linkedin) {
                        socmed += linkedin + " <br>";
                    }
                    
                    return socmed;
                }
            },
            { data: "order" },
            {
                data: "brand.name",
                render: function (data, type, row, meta) {
                    var brand = "";
                    if (row.brand) {
                        brand = row.brand.name ? row.brand.name : "";
                    }

                    return brand;
                }
            },
            {
                data: "property.name",
                render: function (data, type, row, meta) {
                    var property = "";
                    if (row.property) {
                        property = row.property.name ? row.property.name : "";
                    }

                    return property;
                }
            },
            { data: "created_at" },
            { 
                data: "actions", 
                className: "",
                searchable: false, 
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a href="/team/get/' + row.id + '" class="badge badge-secondary" data-trigger="update">Edit</a> \
                            <a href="/team/' + row.id + '" class="badge badge-danger" data-trigger="delete">Delete</a>';
                }
            }
        ],
        order: [[1, "asc"]]
    });

    $("div.dataTables_filter input").unbind().keyup(function (e) {
        if (e.keyCode == 13) $tableTeam.search(this.value).draw();
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    $(document).on("click", ".btn-refresh-table", function (e) {
        $tableTeam.draw();
    });

    $(document).on("submit", ".form-team", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr("action");
        var type = $form.attr("type");
        var btnText = $btnSave.html();
        
        // clean editor
        $form.find("textarea[name]").each(function () {
            var content = cleanString($(this).val());
            $(this).val(content);
        });

        var data = $form.serializeObject();

        $.ajaxq("aq", {
            url: $form.hasClass("update") ? url + "/" + data.id : url,
            data: JSON.stringify(data),
            type: type,
            contentType: "application/json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
                $tableTeam.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
            $form.closest(".modal").modal("hide");
        });
    });

    $(document).on("click", "[data-trigger=update]", function(e) {
        e.preventDefault();

        var url = $(this).attr('href');
        var $form = $(".form-team.update");
        var $modal = $form.closest(".modal");

        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function(json) {
            if (json.success) {
                var data = json.data;

                $form.find("input[name=id]").val(data.id);
                $form.find("input[name=name]").val(data.name);
                $form.find("input[name=position]").val(data.position);
                $form.find("input[name=picture]").val(data.picture).changePicture();
                $form.find("input[name=email]").val(data.email);
                $form.find("input[name=facebook]").val(data.facebook);
                $form.find("input[name=twitter]").val(data.twitter);
                $form.find("input[name=instagram]").val(data.instagram);
                $form.find("input[name=linkedin]").val(data.linkedin);
                $form.find("textarea[name=description]").val(data.description);
                $form.find("input[name=order]").val(data.order);

                $modal.modal("show");
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete]", function (e) {
        e.preventDefault();
        
        var team = $(this).closest("tr").find("td:eq(1)").text();
        var accept = confirm("Are you sure to delete " + team + " ?");
        if (!accept) return;
        
        var $btn = $(this);
        var url = $(this).attr("href");
        var btnText = $btn.html();
        
        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");
                $tableTeam.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
                $btn.html(btnText);
            }
        });
    });

    $(document).on("click", "[data-trigger=delete-picture]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $self = $(this);
        var $form = $self.closest("form");

        $form.find("input[name=picture]").val("");
        $form.find("img.picture").attr("src", "#");
        $form.find("[data-trigger=delete-picture]").addClass("d-none");
    });

    $(document).on("selected.media.library", "input[name=picture]", function (e) {
        $(this).changePicture();
    });

    $(document).on("show.bs.modal", "#modal-form-team, #modal-form-team-update", function (e) {
        var $modal = $(this);

        $modal.find("textarea[name=description]").generateTinyMce({menubar: false});
    });
    
    $(document).on("hidden.bs.modal", "#modal-form-team, #modal-form-team-update", function (e) {
        var $modal = $(this);

        $modal.find("textarea").destroyTinyMce();
    });
    
});