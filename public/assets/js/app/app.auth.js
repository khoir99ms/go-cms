$(function () {
    "use strict";

    $(document).on("submit", ".login-form", function(e){
        e.preventDefault();
        var url     = $(this).attr('action');
        var data    = $(this).serialize();
        var $loginBtn = $(".btn-login");
        var btnText = $loginBtn.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: "post",
            dataType: "json",
            beforeSend: function() {
                $loginBtn.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                window.location.href = "/dashboard/";
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                alert(message);
            }
            $loginBtn.prop('disabled', false).html(btnText);
        });
    });

    $(document).on("submit", ".forgot-form", function(e){
        e.preventDefault();
        var $self   = $(this);
        var $msg  = $self.find(".alert");
        var url     = $self.attr('action');
        var data    = $self.serialize();

        var $btnSubmit = $self.find("button[type=submit]");
        var btnText = $btnSubmit.html();

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: "post",
            dataType: "json",
            beforeSend: function() {
                $btnSubmit.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (json) {
            var msgClass = json.success ? "alert-info" : "alert-warning";
            
            $msg.html(json.message);
            $msg.removeClass("d-none");
            $msg.addClass(msgClass);

            if (json.success) {
                $self[0].reset();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                alert(r.error);
            }
            $btnSubmit.prop('disabled', false).html(btnText);
        });        
    });
});