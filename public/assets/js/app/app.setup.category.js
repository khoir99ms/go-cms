$(function () {
    "use strict";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    $.fn.getPrimaryLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang/primary' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data) {
                toastr["error"]("Primary lang isn't set.", "Failed");
            } else {
                $self.val(json.data.code);
            }
        });
    }

    $.fn.getTranslateToDropdown = function (exceptIDs) {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            exceptIDs = exceptIDs ? exceptIDs : [];

            // Select2 has been initialized
            var $request = $.ajax({
                url: '/lang' // wherever your data is actually coming from
            });

            $request.then(function (json) {
                var options = [];
                $.each(json.data.item, function (i, v) {
                    if ($.inArray(v.code, exceptIDs) === -1) {
                        options.push({ id: v.code, text: v.name })
                    }
                });

                if (options.length > 0) {
                    $self.select2({
                        placeholder: 'Transalte To',
                        allowClear: true,
                        minimumResultsForSearch: Infinity,
                        data: options
                    });
                    $self.closest(".form-group").removeClass("d-none");
                    $self.closest(".modal-header").addClass("pd-t-10 pd-b-10");
                } else {
                    $self.closest(".form-group").addClass("d-none");
                    $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
                }
            });
        }
    }

    $.fn.destroyTranslateToDropdown = function () {
        var $self = $(this);

        if ($self.data("select2")) {
            $self.select2('destroy');
            $self.find("option").not(':first').remove();
            $self.removeAttr("data-select2-id tabindex aria-hidden");
            $self.closest(".form-group").addClass("d-none");
            $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
        }
    }

    $.fn.generateTinyMce = function(options) {
        var $self = $(this);

        var defaults = {
            max_height: 800,
            placeholder: 'Type here ...',
            branding: false,
            // menubar: false,
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons autoresize',
            menubar: 'file edit view insert format tools table help',
            toolbar: 'preview code | undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | superscript subscript | outdent indent | table | numlist bullist | forecolor backcolor removeformat | hr pagebreak | charmap emoticons | fullscreen save print | insertfile image media template link anchor codesample | ltr rtl',
            image_caption: true,
            image_advtab: true,
            toolbar_sticky: true,
            toolbar_mode: 'sliding',
            importcss_append: true,
            allow_script_urls: true,
            relative_urls : false,
            remove_script_host : false,
            imagetools_toolbar: "imageoptions",
            content_css: ['/static/lib/tinymce/skins/content/default/content.min.css', '/static/lib/fontawesome-free/css/all.min.css'],
            quickbars_selection_toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | h1 h2 h3 blockquote | quicklink quicktable',
            quickbars_insert_toolbar: false,
            contextmenu: "link image table",
        };

        // Merge defaults and options, without modifying defaults
        var settings = $.extend({}, defaults, options ? options : {});

        $self.each(function () {
            $(this).tinymce(settings);
            $(this).addClass("has-tinymce");
        });
    }

    $.fn.destroyTinyMce = function() {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("has-tinymce")) {
                $(this).tinymce().destroy();
            }
        });
    }

    $.fn.changeDisplayImage = function () {
        var $self = $(this);
        var $parent = $self.closest(".form-group");
        
        var value = $self.val();
        var image = value.match(/^http/) ? value : "/static/uploads/media/" + value;
        if (value) {
            $parent.find("[data-trigger=delete-display-image]").removeClass("d-none");
        }
        $parent.find("img.display-image").attr("src", image);
    }

    $("input.primary-lang").getPrimaryLang();

    var $tableCategory = $("#datatable-category").DataTable({
        responsive: false,
        language: {
            searchPlaceholder: "Search ...",
            sSearch: "",
            lengthMenu: "_MENU_ items/page",
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        processing: true,
        serverSide: true,
        deferRender: true,
        ajax: {
            url: "/category/primary?get=master",
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);

                json.recordsTotal = json.data.total;
                json.recordsFiltered = json.data.total;
                // json.recordsFiltered = json.data.item ? json.data.item.length : 0;
                json.data = json.data.item ? json.data.item : [];

                return JSON.stringify(json); // return JSON string
            }
        },
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                className: "align-middle",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: "name" },
            { data: "title" },
            {
                data: "lang.code",
                render: function (data, type, row, meta) {
                    return row.lang.code.toUpperCase();
                }
            },
            { data: "created_at" },
            {
                data: "actions",
                className: "align-middle",
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a href="/category/multiple-lang/' + row.id + '" class="badge badge-secondary" data-trigger="update">Edit</a> \
                            <a href="/category/multiple-lang/' + row.id + '" class="badge badge-danger" data-trigger="delete">Delete</a>';
                }
            }
        ],
        order: [[1, "asc"]]
    });

    $(document).on("click", ".btn-refresh-table", function (e) {
        $tableCategory.draw();
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    $(document).on("submit", ".form-category", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr("action");
        var type = $form.attr("type");
        var contentType = "application/x-www-form-urlencoded";
        var btnText = $btnSave.html();
        var isUpdateMode = $form.hasClass("update");

        // clean editor
        $form.find("textarea[name]").each(function () {
            var content = cleanString($(this).val());
            $(this).val(content);
        });
        
        var data;
        if (isUpdateMode) {
            var params = [];
            $form.find(".tab-content .tab-pane").each(function (index) {
                params.push($(this).find("input[name], select[name], textarea[name]").serializeObject());
            });
            data = JSON.stringify(params);
            contentType = "application/json";
        } else {
            data = $form.serialize();
        }

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            contentType: contentType,
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
                $tableCategory.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
            $form.closest(".modal").modal("hide");
        });
    });

    $(document).on("click", "[data-trigger=update]", function (e) {
        e.preventDefault();

        var url = $(this).attr('href');
        var $form = $(".form-category.update");
        var $defaultForm = $form.html();
        var $modal = $form.closest(".modal");

        var $tabs = $modal.find(".nav.nav-tabs");
        // var $defaultTab = $tabs.html();
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $modal.find(".tab-content");
        // var $defaultTabContent = $tabContent.html();
        var tabPaneContent = $('<div>').append($tabContent.find(".tab-pane").first().clone(true)).html();

        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (json) {
            if (json.success) {
                var data = json.data;

                var primaryCategoryLang = [data];
                var otherCategoryLang = data.other_lang ? data.other_lang : [];
                var categories = $.merge(primaryCategoryLang, otherCategoryLang);

                $tabs.html("");
                $tabContent.html("");
                $.each(categories, function (i, category) {
                    var $thisTab = $(tab);
                    var $thisTabPane = $(tabPaneContent);

                    var href = $thisTab.attr("href").replace("default", category.lang.code);
                    var tabID = href.replace("#", "");

                    $thisTab.attr("href", href);
                    $thisTab.html(category.lang.name);

                    $tabs.append($thisTab);

                    $thisTabPane.attr("id", tabID);
                    $thisTabPane.find("input[name=id]").val(category.id);
                    $thisTabPane.find("input[name=lang]").val(category.lang.code);
                    $thisTabPane.find("input[name=source_id]").val(category.source_id);
                    $thisTabPane.find("input[name=name]").val(category.name);
                    $thisTabPane.find("input[name=title]").val(category.title);
                    $thisTabPane.find("textarea[name=description]").val(category.description);
                    $thisTabPane.find("input[name=image]").val(category.image).changeDisplayImage();
                    $thisTabPane.find("input[name=use_validity]").prop("checked", category.page.use_validity);
                    $thisTabPane.find("input[name=use_amenities]").prop("checked", category.page.use_amenities);
                    $thisTabPane.find("input[name=use_badge]").prop("checked", category.page.use_badge);
                    $thisTabPane.find("input[name=label_detail]").val(category.label.detail);
                    $thisTabPane.find("input[name=label_more]").val(category.label.more);
                    $thisTabPane.find("input[name=meta_title]").val(category.meta.title);
                    $thisTabPane.find("input[name=meta_description]").val(category.meta.description);
                    $thisTabPane.find("input[name=meta_keyword]").val(category.meta.keyword);

                    $tabContent.append($thisTabPane);
                });
                $tabs.find('a.nav-link:first-child').tab('show');

                $modal.modal("show").data({
                    defaultForm: $defaultForm,
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete]", function (e) {
        e.preventDefault();

        var category = $(this).closest("tr").find("td:eq(1)").text();
        var accept = confirm("Are you sure to delete " + category + " ?");
        if (!accept) return;

        var $btn = $(this);
        var url = $(this).attr('href');
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");

                $tableCategory.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete-tab]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $self.closest("form");

        var $translateToDropdown = $modal.find('.select2.translate-to');

        var $tabs = $form.find(".nav.nav-tabs");
        var $navTabLinks = $tabs.find("a.nav-link");
        var $activeTab = $tabs.find("a.nav-link.active");

        var $tabContent = $form.find(".tab-content");
        var $activeTabPane = $tabContent.find(".tab-pane.active");

        var activeLang = $activeTab.text();
        var accept = confirm("Delete this category in " + activeLang + " language ?");
        if (!accept) return;

        if ($activeTab.hasClass("new")) {
            $activeTab.remove();
            $activeTabPane.remove();
            $tabs.find('a.nav-link:last-child').tab("show");
            
            var langIDs = $modal.find("input[name=lang]").map(function () {
                return this.value;
            }).get();
            $translateToDropdown.destroyTranslateToDropdown();
            $translateToDropdown.getTranslateToDropdown(langIDs);
        } else {
            var id = $activeTabPane.find("input[name=id]").val();
            var url = $self.attr('href');
            var btnText = $self.html();

            $.ajaxq("aq", {
                url: url + '/' + id,
                type: "delete",
                beforeSend: function () {
                    $self.html(waitText);
                }
            }).done(function (jqXHR) {
                if (jqXHR.success) {
                    toastr["success"](jqXHR.message, "Success");

                    if ($navTabLinks.length > 1) {
                        $activeTab.remove();
                        $activeTabPane.remove();
                        $tabs.find('a.nav-link:last-child').tab("show");
                    }

                    var langIDs = $modal.find("input[name=lang]").map(function () {
                        return this.value;
                    }).get();
                    $translateToDropdown.destroyTranslateToDropdown();
                    $translateToDropdown.getTranslateToDropdown(langIDs);

                    $tableCategory.draw();
                }
            }).always(function (jqXHR) {
                if (!jqXHR.success) {
                    var r = jqXHR.responseJSON;
                    var message = r ? r.error : jqXHR.statusText;
                    toastr["error"](message, "Failed");
                }
                $self.html(btnText);
            });
        }
    });

    $(document).on("click", "[data-trigger=delete-display-image]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $form = $(this).closest("form");
        $form.find("input[name=image]").val("").changeDisplayImage();
    });

    $(document).on("change", "input[name=use_validity]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $form = $self.closest("form");
        var checked = $self.is(':checked');

        $form.find("input[name=use_validity]").prop("checked", checked);
    });

    $(document).on("change", "input[name=use_amenities]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $form = $self.closest("form");
        var checked = $self.is(':checked');

        $form.find("input[name=use_amenities]").prop("checked", checked);
    });

    $(document).on("change", "input[name=use_badge]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $form = $self.closest("form");
        var checked = $self.is(':checked');

        $form.find("input[name=use_badge]").prop("checked", checked);
    });

    $(document).on("select2:select", "select.translate-to", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var defaultForm = $modal.data("defaultForm");
        var $translateToDropdown = $modal.find('.select2.translate-to');
        
        var lang = $(this).select2("data")[0];
        
        var $tabs = $modal.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();
        
        var $tabContent = $modal.find(".tab-content");
        var $tabPaneSource = $tabContent.find(".tab-pane").first();

        var checkedValidity = $tabPaneSource.find("input[name=use_validity]").is(":checked");
        var checkedAmenities = $tabPaneSource.find("input[name=use_amenities]").is(":checked");
        var checkedBadge = $tabPaneSource.find("input[name=use_badge]").is(":checked");

        var $tab = $(tab);
        var $tabPane = $(defaultForm).find(".tab-pane");

        var href = $tab.attr("href").replace(/\w+$/, lang.id);
        var tabID = href.replace("#", "");
        var sourceID = $tabPaneSource.find("input[name=id]").val();

        $tab.removeClass("active show");
        $tabPane.removeClass("active show");

        $tab.attr("href", href);
        $tab.html(lang.text);
        $tab.addClass("new");
        $tabs.append($tab);

        $tabPane.attr("id", tabID);
        $tabPane.find("input[name=id]").val("");
        $tabPane.find("input[name=lang]").val(lang.id);
        $tabPane.find("input[name=source_id]").val(sourceID);
        $tabPane.find("input[name=name]").val("");
        $tabPane.find("input[name=title]").val("");
        $tabPane.find("textarea[name=description]").val("").removeClass("has-tinymce");
        $tabPane.find("input[name=label_detail]").val("");
        $tabPane.find("input[name=label_more]").val("");
        $tabPane.find("input[name=use_validity]").prop("checked", checkedValidity);
        $tabPane.find("input[name=use_amenities]").prop("checked", checkedAmenities);
        $tabPane.find("input[name=use_badge]").prop("checked", checkedBadge);
        $tabPane.find("input[name=meta_title]").val("");
        $tabPane.find("input[name=meta_description]").val("");
        $tabPane.find("input[name=meta_keyword]").val("");

        $tabContent.append($tabPane);
        $tabs.find('a.nav-link:last-child').tab("show");
        
        $tabPane.find("textarea[name=description]").removeData().generateTinyMce({menubar: false});
        var langIDs = $modal.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.getTranslateToDropdown(langIDs);
    });

    $(document).on("selected.media.library", "input[name=image]", function (e) {
        $(this).changeDisplayImage();
    });

    $(document).on("show.bs.tab", "[data-toggle=tab]", function (e) {
        var $modal = $(this).closest(".modal");
        var $btnDelete = $modal.find("[data-trigger=delete-tab]");

        if ($(this).index() > 0) {
            $btnDelete.removeClass("d-none");
        } else {
            $btnDelete.addClass("d-none");
        }
    });

    $(document).on("shown.bs.modal", "#modal-form-category, #modal-form-category-update", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form.form-category");
        var isModalUpdate = $form.hasClass("update");

        $modal.find('textarea[name=description]').generateTinyMce({menubar: false});

        if (isModalUpdate) {
            var langIDs = $modal.find("input[name=lang]").map(function () {
                return this.value;
            }).get();

            $('.select2.translate-to').getTranslateToDropdown(langIDs);
        } else {
            var $defaultForm = $form.html();
            $modal.data("defaultForm", $defaultForm);
        }
    });

    $(document).on("hidden.bs.modal", "#modal-form-category, #modal-form-category-update", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form.form-category");
        var defaultForm = $modal.data('defaultForm');
        var isModalUpdate = $modal.find("form").hasClass("update");

        $modal.find("textarea[name=description]").destroyTinyMce();
        if (isModalUpdate) {
            /* var defaultTab = $modal.data('defaultTab');
            var defaultTabContent = $modal.data('defaultTabContent');
            if (defaultTab) {
                $modal.find(".nav.nav-tabs").html(defaultTab);
            }
            if (defaultTabContent) {
                $modal.find(".tab-content").html(defaultTabContent);
            }

            $modal.removeData(); */
            $('.select2.translate-to').destroyTranslateToDropdown();
        }

        $form.html(defaultForm);
        $modal.removeData();
    });
});