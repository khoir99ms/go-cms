$(function () {
    "use strict";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    var scriptEditor = ace.edit("script");
    scriptEditor.session.setMode("ace/mode/html");

    $(document).on("submit", ".form-builder", function (e) {
        e.preventDefault();
        var $form = $(this);
        
        var api = $form.find("input[name=api]").val();
        var source = $form.find("input[name=source]").val();
        var width = $form.find("input[name=width]").val();
        var layout = $form.find("input[name=layout]:checked").val();
        var columns = $form.find("input[name=columns]").val();
        var rows = $form.find("input[name=rows]").val();
        var lang = $form.find("input[name=lang]").val();

        var scriptHtml = '<div data-is \
            data-is-api="'+ api + '" \
            data-is-source="'+ source + '" \
            data-is-width="'+ width + '" \
            data-is-layout="'+ layout + '" \
            data-is-columns="'+ columns + '" \
            data-is-rows="'+ rows + '" \
            data-is-lang="'+ lang + '"></div>';
        
        scriptEditor.session.setValue(html_beautify(scriptHtml));
    });
});