$(function () {
    "use strict";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    var primaryLang = '';
    var pageGroupedLang = {};
    var categoryGroupedLang = {};
    var amenitiesGroupedLang = {};
    var tagsGroupedLang = {};
    var categoryRequiredValidity = [];
    var categoryUsingAmenities = [];
    var categoryUsingBadge = [];

    var serializeAmenitiesTree = function (json) {
        var tree = [];

        $.each(json, function (i, item) {
            var children = item.children;

            var text = '<a href="#" class="tx-gray-800" data-id="' + item.id + '" ' + (item.parent_id ? 'data-parent-id="' + item.parent_id + '"' : '') + '>';

            var $icon = $.parseHTML(item.icon);
            var isImg = $icon.length > 0 && $icon[0].nodeName.toLowerCase() == "img";
            if (isImg) {
                item.icon = $("<div>").append($(item.icon).attr({ "width": 20, "height": 20 })).html();
            }

            if (item.icon) {
                text += '<span class="mg-l-5 mg-r-10">' + item.icon + '</span>';
            }

            text += item.name;
            text += '</a>';

            var node = {
                text: text,
            }
            if (children && children.length > 0) {
                node["nodes"] = serializeAmenitiesTree(children);
            }
            tree.push(node);
        });

        return tree;
    }

    var getCategoryGroupedLang = function (silent) {
        var $request = $.ajax({
            url: '/category/grouped-lang' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            categoryGroupedLang = json.data ? json.data : {};
            $.each(categoryGroupedLang, function (i, category) {
                $.each(category, function (k, item) {
                    if (item.page.use_validity) categoryRequiredValidity.push(item.id);
                    if (item.page.use_amenities) categoryUsingAmenities.push(item.id);
                    if (item.page.use_badge) categoryUsingBadge.push(item.id);
                });
            });
            if (!silent) {
                $(document).trigger("loaded.category");
            }
        });

        $request.always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    var getPageGroupedLang = function (silent) {
        var $request = $.ajax({
            url: '/page/grouped-lang' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            pageGroupedLang = json.data ? json.data : {};
            if (!silent) {
                $(document).trigger("loaded.page");
            }
        });

        $request.always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    var getAmenitiesGroupedLang = function () {
        var $request = $.ajax({
            url: "/amenities/tree/grouped-lang", // wherever your data is actually coming from
        });

        $request.then(function (json) {
            amenitiesGroupedLang = json.data ? json.data : {};
            $(document).trigger("loaded.amenities");
        });

        $request.always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    var getTagGroupedLang = function () {
        var $request = $.ajax({
            url: "/tag/grouped-lang", // wherever your data is actually coming from
        });

        $request.then(function (json) {
            tagsGroupedLang = json.data ? json.data : {};
            $(document).trigger("loaded.tag");
        });

        $request.always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    var getRelatedCategoryID = function (id, sourceLang, destLang) {
        var sourceCategory = categoryGroupedLang[sourceLang] ? categoryGroupedLang[sourceLang] : [];
        var source = $.grep(sourceCategory, function (v) {
            return v.id == id;
        });

        var category = {};
        if (source.length > 0) {
            var sc = source[0];
            $.each(categoryGroupedLang[destLang], function (i, v) {
                if (v.source_id == sc.id || sc.source_id == v.id) {
                    category = v;

                    return false;
                }
            });
        }

        return category;
    }

    var getRelatedPageID = function (id, sourceLang, destLang) {
        var sourcePage = pageGroupedLang[sourceLang] ? pageGroupedLang[sourceLang] : [];
        var source = $.grep(sourcePage, function (v) {
            return v.id == id;
        });

        var page = {};
        if (source.length > 0) {
            var sc = source[0];
            $.each(pageGroupedLang[destLang], function (i, v) {
                if (v.source_id == sc.id || sc.source_id == v.id) {
                    page = v;

                    return false;
                }
            });
        }

        return page;
    }

    $.fn.getPrimaryLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang/primary' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data) {
                toastr["error"]("Primary lang isn't set.", "Failed");
            } else {
                primaryLang = json.data.code;
                $self.val(json.data.code).trigger({
                    type: "loaded.primary-lang",
                    lang: json.data.code
                });
            }
        });
    }

    $.fn.getTranslateToDropdown = function (exceptIDs) {
        var $self = $(this);

        if (!$self.hasClass("select2-hidden-accessible")) {
            exceptIDs = exceptIDs ? exceptIDs : [];

            // Select2 has been initialized
            var $request = $.ajax({
                url: '/lang' // wherever your data is actually coming from
            });

            $request.then(function (json) {
                var options = [];
                $.each(json.data.item, function (i, v) {
                    if ($.inArray(v.code, exceptIDs) === -1) {
                        options.push({ id: v.code, text: v.name })
                    }
                });

                if (options.length > 0) {
                    $self.select2({
                        placeholder: 'Transalte To',
                        allowClear: true,
                        minimumResultsForSearch: Infinity,
                        data: options
                    });
                    $self.closest(".form-group").removeClass("d-none");
                    $self.closest(".modal-header").addClass("pd-t-10 pd-b-10");
                } else {
                    $self.closest(".form-group").addClass("d-none");
                    $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
                }
            });
        }
    }

    $.fn.destroySelectDropdown = function () {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("select2-hidden-accessible")) {
                $(this).select2('destroy');
                $(this).find("option").not(':first').remove();
                $(this).removeAttr("data-select2-id tabindex aria-hidden");
            }
        });
    }

    $.fn.destroyTranslateToDropdown = function () {
        var $self = $(this);

        $self.destroySelectDropdown();
        $self.closest(".form-group").addClass("d-none");
        $self.closest(".modal-header").removeClass("pd-t-10 pd-b-10");
    }

    $.fn.generateTinyMce = function(options) {
        var $self = $(this);

        var defaults = {
            max_height: 800,
            placeholder: 'Type here ...',
            branding: false,
            // menubar: false,
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons autoresize',
            menubar: 'file edit view insert format tools table help',
            toolbar: 'preview code | undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | superscript subscript | outdent indent | table | numlist bullist | forecolor backcolor removeformat | hr pagebreak | charmap emoticons | fullscreen save print | insertfile image media template link anchor codesample | ltr rtl',
            image_caption: true,
            image_advtab: true,
            toolbar_sticky: true,
            toolbar_mode: 'sliding',
            importcss_append: true,
            allow_script_urls: true,
            relative_urls : false,
            remove_script_host : false,
            imagetools_toolbar: "imageoptions",
            content_css: ['/static/lib/tinymce/skins/content/default/content.min.css', '/static/lib/fontawesome-free/css/all.min.css'],
            quickbars_selection_toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | h1 h2 h3 blockquote | quicklink quicktable',
            quickbars_insert_toolbar: false,
            contextmenu: "link image table",
        };

        // Merge defaults and options, without modifying defaults
        var settings = $.extend({}, defaults, options ? options : {});

        $self.each(function () {
            $(this).tinymce(settings);
            $(this).addClass("has-tinymce");
        });
    }

    $.fn.destroyTinyMce = function() {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("has-tinymce")) {
                $(this).tinymce().destroy();
            }
        });
    }

    $.fn.generateAceEditor = function () {
        var $self = $(this);

        $self.each(function (i) {
            var $textarea = $(this);
            var mode = $textarea.data('editor');

            var $container = $('<div>', {
                'class': 'ace-editor ace-editor-' + i
            }).insertBefore($textarea);
            $textarea.addClass("d-none");

            var editor = ace.edit($container[0]);
            editor.setOptions({
                maxLines: 50
            });

            editor.getSession().setValue($textarea.val());
            editor.getSession().setMode("ace/mode/" + mode);

            editor.getSession().on("change", function (e) {
                var value = editor.getSession().getValue();
                $textarea.val(value);
            });
        });
    }

    $.fn.destroyAceEditor = function () {
        var $self = $(this);

        $self.each(function () {
            var $editor = $(this);
            if ($editor.length > 0) {
                $editor[0].env.editor.destroy();
                $editor.remove();
            }
        });
    }

    $.fn.generateSelectCategoryDropdown = function (lang) {
        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");

        $self.each(function (i) {
            var $select = $(this);
            var indexLang = lang;
            if (indexLang === undefined) {
                var $parent = isModalUpdate ? $(this).closest(".tab-pane") : $form;
                indexLang = $parent.find("input[name=lang]").val();
            }

            $select.destroySelectDropdown();
            $select.select2({
                placeholder: 'Category',
                searchInputPlaceholder: 'Search',
                allowClear: true,
            });

            $.each(categoryGroupedLang[indexLang], function (i, v) {
                var option = new Option(v.name, v.id, false, false);
                $select.append(option);
            });

            var value = $select.data("value");
            if (value) {
                $select.val(value);
            }
            $select.trigger('change');

            if (lang !== undefined) return;
        });
    }

    $.fn.generateSelectParentDropdown = function (lang) {
        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");

        $self.each(function (i) {
            var $select = $(this);
            var indexLang = lang;
            if (indexLang === undefined) {
                var $parent = isModalUpdate ? $(this).closest(".tab-pane") : $form;
                indexLang = $parent.find("input[name=lang]").val();
            }
            var exceptId = $parent.find("input[name=id]").val();

            $select.destroySelectDropdown();

            $select.select2({
                placeholder: 'Parent',
                searchInputPlaceholder: 'Search',
                allowClear: true,
            });

            $.each(pageGroupedLang[indexLang], function (i, v) {
                if (exceptId != v.id) {
                    var option = new Option(v.title, v.id, false, false);
                    $select.append(option);
                }
            });

            var value = $select.data("value");
            if (value) {
                $select.val(value);
            }
            $select.trigger('change');

            if (lang !== undefined) return;
        });

    }

    $.fn.generateSelectTagDropdown = function (lang) {
        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");

        $self.each(function (i) {
            var $select = $(this);
            var indexLang = lang;
            if (indexLang === undefined) {
                var $parent = isModalUpdate ? $(this).closest(".tab-pane") : $form;
                indexLang = $parent.find("input[name=lang]").val();
            }

            $select.destroySelectDropdown();

            $select.select2({
                placeholder: 'Tag',
                searchInputPlaceholder: 'Search',
                allowClear: true,
            });

            $.each(tagsGroupedLang[indexLang], function (i, v) {
                var option = new Option(v.title, v.id, false, false);
                $select.append(option);
            });

            var value = $select.data("value");
            if (value) {
                $select.val(value);
            }
            $select.trigger('change');

            if (lang !== undefined) return;
        });

    }

    $.fn.generateAmenitiesTreeView = function (lang) {
        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");

        $self.each(function (i) {
            var $tree = $(this);
            var $container = $tree.closest(".form-group");
            var $alert = $tree.closest(".card-body").find(".alert");
            
            var indexLang = lang;
            if (indexLang === undefined) {
                var $parent = isModalUpdate ? $(this).closest(".tab-pane") : $form;
                indexLang = $parent.find("input[name=lang]").val();
            }
            
            var tree = serializeAmenitiesTree(amenitiesGroupedLang[indexLang]);
            if (tree.length > 0) {
                $tree.treeview({
                    expandIcon: 'fas fa-caret-right',
                    collapseIcon: 'fas fa-caret-down',
                    checkedIcon: 'far fa-check-square',
                    uncheckedIcon: 'far fa-square',
                    showCheckbox: true,
                    highlightSelected: false,
                    data: tree,
                });

                $alert.addClass("d-none");
                $container.removeClass("d-none");
            } else {
                $alert.removeClass("d-none");
                $container.addClass("d-none");
            }

            $tree.trigger("loaded.treeview");

            if (lang !== undefined) return;
        });
    }

    $.fn.checkAmenitiesTreeView = function () {
        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");
        var $parent = isModalUpdate ? $self.closest(".tab-pane") : $form;

        $self.each(function (i) {
            var $treeview = $(this);
            var $amenities = $parent.find('input[name=amenities]');

            $amenities.each(function (i) {
                var amenities = $(this).val();

                var $item = $treeview.find(".list-group-item > a[data-id=" + amenities + "]");
                if ($item.length > 0) {
                    var $node = $item.closest(".list-group-item");
                    var nID = $node.data("nodeid");

                    $treeview.treeview('checkNode', [nID, { silent: true }]);
                }
            });
        });
    }

    $.fn.changeFeaturedImage = function () {
        var $self = $(this);
        var $parent = $self.closest(".form-group");

        var value = $self.val();
        var image = value.match(/^http/) ? value : "/static/uploads/media/" + value;
        if (value) {
            $parent.find("[data-trigger=delete-fi]").removeClass("d-none");
        }
        $parent.find("img.featured-image").attr("src", image);
    }

    $.fn.changeAttachment = function () {
        var $self = $(this);
        var $attachmentImage = $self.find(".attachment-image");
        var $images = $self.find("input[name=attachment_image]");

        var attachmentImage = "";
        $images.each(function (i) {
            var value = $(this).val();
            var image = value.match(/^http/) ? value : "/static/uploads/media/" + value;

            attachmentImage += ' \
                <div class="col-lg-2 col-md-3 col-4 ai-item"> \
                <div class="d-block mb-4 h-100 pos-relative"> \
                <img data-image="'+ value + '" class="img-fluid img-thumbnail" src="' + image + '" onerror="this.src=\'/static/images/default-img.png\';" alt="" style="width: 200px; height: 100px"> \
                <div class="ai-toolbar pos-absolute" style="top: 3px; right: 8px;"> \
                    <a href="#" data-trigger="delete-ai" class="badge badge-danger tx-bold">X</a> \
                </div> \
                </div> \
            </div >';
        });
        $attachmentImage.html(attachmentImage);

        if ($images.length > 0) {
            $self.find(".attachment-info").addClass("d-none");
        }
    }

    $.fn.collapsibleElement = function (options) {
        var $self = $(this);

        var defaults = { parent: "" };
        // Merge defaults and options, without modifying defaults
        var settings = $.extend({}, defaults, options ? options : {});

        $self.each(function (i) {
            var $collapse = $(this);
            var $toggle = $collapse.find(".collapse-toggle");
            var $target = $collapse.find(".collapse-target");

            var random = Math.random().toString(36).substring(7);
            var targetID = "collapse" + random + "-" + i;

            $toggle.attr("data-toggle", "collapse");
            $toggle.attr("data-target", "#" + targetID);
            $toggle.css({ "cursor": "pointer" });

            $target.attr("id", targetID);
            if (settings.parent) {
                $target.attr("parent", settings.parent);
            }
            $target.addClass("collapse");
        });
    }

    $.fn.resizeIframe = function () {
        var $self = $(this);

        if ($self.contents().find('html').height() > 40) {
            var height = $self.contents().find('html').height();
            $self.height(height + 'px');
        } else {
            setTimeout(function (e) {
                $self.resizeIframe();
            }, 50);
        }
    }

    var $tablePage = $("#datatable-page").DataTable({
        responsive: false,
        dom: '<<t>ip>',
        language: {
            searchPlaceholder: "Search ...",
            sSearch: "",
            lengthMenu: "_MENU_ items/page",
        },
        // lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        processing: true,
        serverSide: true,
        deferRender: true,
        ajax: {
            url: "/page/primary",
            data: function (data) {
                // Read values
                var category = $('select.dt[name=category]').val();

                // Append to data
                data.category_id = category;
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);

                json.recordsTotal = json.data ? json.data.total : 0;
                json.recordsFiltered = json.data ? json.data.total : 0;
                // json.recordsFiltered = json.data.item ? json.data.item.length : 0;
                json.data = json.data && json.data.item ? json.data.item : [];

                return JSON.stringify(json); // return JSON string
            }
        },
        initComplete: function (settings, json) {
            var api = new $.fn.dataTable.Api(settings);
            if (pageSession.level == "brand") {
                api.columns([6]).visible(false);
            } else {
                api.columns([7]).visible(false);
            }
        },
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                className: "align-middle",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { 
                data: "title",
                className: "align-middle",
                render: function (data, type, row, meta) {
                    return row.subtitle ? row.title + "<br><span class=\"tx-12 tx-gray-600\">" + row.subtitle + "</span>" : row.title;
                }
            },
            { data: "order", className: "align-middle" },
            { 
                data: "status",
                className: "align-middle",
                render: function (data, type, row, meta) {
                    var badge = row.status == "publish" ? "badge-success" : "badge-danger";
                    return '<span class="badge ' + badge + '">' + row.status.ucwords()+'</span>';
                }
            },
            {
                data: "lang.code",
                className: "align-middle",
                render: function (data, type, row, meta) {
                    return row.lang.code.toUpperCase();
                }
            },
            {
                data: "category.name",
                className: "align-middle",
                render: function (data, type, row, meta) {
                    return row.category.name ? row.category.name : "";
                }
            },
            {
                data: "property.name",
                className: "align-middle",
                render: function (data, type, row, meta) {
                    return row.property.name ? row.property.name : "";
                }
            },
            {
                data: "brand.name",
                className: "align-middle",
                render: function (data, type, row, meta) {
                    return row.brand.name ? row.brand.name : "";
                }
            },
            { data: "created_at", className: "align-middle" },
            {
                data: "actions",
                className: "align-middle",
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a href="/page/multiple-lang/' + row.id + '" class="badge badge-secondary" data-trigger="update">Edit</a> \
                            <a href="/page/multiple-lang/' + row.id + '" class="badge badge-danger" data-trigger="delete">Delete</a>';
                }
            }
        ],
        order: [[1, "asc"]]
    });

    $("input.primary-lang").getPrimaryLang();

    $("input.dt[name=search]").unbind().keyup(function (e) {
        if (e.keyCode == 13) $tablePage.search(this.value).draw();
    });

    $('select.dt[name=length]').select2({
        minimumResultsForSearch: Infinity
    }).on('change', function (e) {
        $tablePage.page.len(this.value).draw();
    });

    $(document).on("loaded.primary-lang", function (e) {
        getCategoryGroupedLang();
        getPageGroupedLang();
        getAmenitiesGroupedLang();
        getTagGroupedLang();

        $(document).on("loaded.category", function (e) {
            $("select.dt[name=category]").generateSelectCategoryDropdown(primaryLang);
            $("select.dt[name=category]").on("change", function (e) {
                $tablePage.draw();
            });
        });
    });

    $(document).on("click", ".btn-refresh-table", function (e) {
        $tablePage.draw();

        getCategoryGroupedLang(true);
        getPageGroupedLang(true);
        getAmenitiesGroupedLang();
        getTagGroupedLang();
    });

    $(document).on("click", ".toggle-page-status", function (e) {
        var $form = $(this).closest("form");
        var $toggle = $form.find(".toggle-page-status");
        var $status = $form.find("input[name=status]");

        $toggle.toggleClass("on");
        if ($toggle.hasClass('on')) {
            $status.val("publish");
        } else {
            $status.val("draft");
        }
    });

    $(document).on("click", "[data-trigger=update]", function (e) {
        e.preventDefault();

        var url = $(this).attr('href');
        var $form = $(".form-page.update");
        var $defaultForm = $form.html();
        var $modal = $form.closest(".modal");

        var $tabs = $modal.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $modal.find(".tab-content");
        var tabPaneContent = $('<div>').append($tabContent.find(".tab-pane").first().clone(true)).html();

        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (json) {
            if (json.success) {
                var data = json.data;

                var primaryPageLang = [data];
                var otherPageLang = data.other_lang ? data.other_lang : [];
                var pages = $.merge(primaryPageLang, otherPageLang);

                $tabs.html("");
                $tabContent.html("");
                $.each(pages, function (i, page) {
                    var $thisTab = $(tab);
                    var $thisTabPane = $(tabPaneContent);

                    var href = $thisTab.attr("href").replace("default", page.lang.code);
                    var tabID = href.replace("#", "");

                    $thisTab.attr("href", href);
                    $thisTab.html(page.lang.name);

                    $tabs.append($thisTab);

                    $thisTabPane.attr("id", tabID);
                    $thisTabPane.find("input[name=id]").val(page.id);
                    $thisTabPane.find("input[name=lang]").val(page.lang.code);
                    $thisTabPane.find("input[name=source_id]").val(page.source_id);
                    $thisTabPane.find("input[name=title]").val(page.title);
                    $thisTabPane.find("input[name=subtitle]").val(page.subtitle);
                    $thisTabPane.find("textarea[name=content]").val(page.content);
                    $thisTabPane.find("textarea[name=excerpt]").val(page.excerpt);
                    $thisTabPane.find("input[name=featured_image]").val(page.featured_image).changeFeaturedImage();

                    $.each(page.attachment_image, function (i, attachment) {
                        $thisTabPane.find(".attachment").append('<input type="hidden" name="attachment_image" value="' + attachment + '">');
                    });
                    $thisTabPane.find(".attachment").changeAttachment();

                    $thisTabPane.find("input[name=custom_field]").val(page.custom.field);
                    $thisTabPane.find("input[name=custom_link]").val(page.custom.link);
                    $thisTabPane.find("input[name=custom_label_detail]").val(page.custom.label_detail);
                    $thisTabPane.find("input[name=meta_title]").val(page.meta.title);
                    $thisTabPane.find("input[name=meta_description]").val(page.meta.description);
                    $thisTabPane.find("input[name=meta_keyword]").val(page.meta.keyword);
                    $thisTabPane.find("textarea[name=script_header]").val(page.script.header);
                    $thisTabPane.find("textarea[name=script_body]").val(page.script.body);

                    $thisTabPane.find("select[name=category_id]").data("value", page.category.id);
                    $thisTabPane.find("select[name=parent_id]").data("value", page.parent_id);
                    $thisTabPane.find("select[name=tags]").data("value", page.tags);
                    $thisTabPane.find("input[name=template]").val(page.template);
                    $thisTabPane.find("input[name=order]").val(page.order);

                    $thisTabPane.find("input[name=status]").val(page.status);
                    if (page.status == "publish") {
                        $thisTabPane.find('.az-toggle.toggle-page-status').addClass("on");
                    } else {
                        $thisTabPane.find('.az-toggle.toggle-page-status').removeClass("on");
                    }

                    var categoryId = page.category && page.category.id ? page.category.id : "";
                    if (categoryRequiredValidity.includes(categoryId)) {
                        var startDate = page.show_date ? page.show_date.slice(0, 10) : "";
                        var endDate = page.end_date ? page.end_date.slice(0, 10) : "";

                        $thisTabPane.find('input[name=show_date]').val(startDate).prop("required", true);
                        $thisTabPane.find('input[name=end_date]').val(endDate).prop("required", true);
                        $thisTabPane.find(".page-validity").removeClass("d-none");
                    } else {
                        $thisTabPane.find('input[name=show_date]').val("").prop("required", false);
                        $thisTabPane.find('input[name=end_date]').val("").prop("required", false);
                        $thisTabPane.find(".page-validity").addClass("d-none");
                    }

                    if (categoryUsingAmenities.includes(categoryId)) {
                        $thisTabPane.find(".card-facility").removeClass("d-none");

                        var $formGroup = $thisTabPane.find(".card-facility").find(".form-group");
                        $.each(page.amenities, function (i, amenities) {
                            var notExistFacility = $formGroup.find("input[name=amenities][value='" + amenities + "']").length == 0;

                            if (notExistFacility) {
                                $formGroup.append('<input type="hidden" name="amenities" value="' + amenities + '">');
                            }
                        });
                    } else {
                        $thisTabPane.find(".card-facility").addClass("d-none");
                        $thisTabPane.find('input[name=amenities]').remove();
                    }

                    if (categoryUsingBadge.includes(categoryId)) {
                        page.badge = page.badge ? page.badge : {}
                        $thisTabPane.find("input[name=badge_text]").val(page.badge.text);
                        $thisTabPane.find("input[name=badge_text_color]").val(page.badge.text_color);
                        $thisTabPane.find("input[name=badge_bg_color]").val(page.badge.background_color);
                        $thisTabPane.find("input[name=badge_num_bought]").val(page.badge.num_bought);
                        $thisTabPane.find("input[name=badge_price]").val(page.badge.price);
                        $thisTabPane.find("input[name=badge_btn_label]").val(page.badge.button_label);
                        $thisTabPane.find("input[name=badge_btn_link]").val(page.badge.button_link);
                        $thisTabPane.find(".card-badge").removeClass("d-none");
                    } else {
                        $thisTabPane.find(".card-badge").addClass("d-none");
                        $thisTabPane.find("input[name=badge_text]").val("");
                        $thisTabPane.find("input[name=badge_text_color]").val("");
                        $thisTabPane.find("input[name=badge_bg_color]").val("");
                        $thisTabPane.find("input[name=badge_num_bought]").val("");
                        $thisTabPane.find("input[name=badge_price]").val("");
                        $thisTabPane.find("input[name=badge_btn_label]").val("");
                        $thisTabPane.find("input[name=badge_btn_link]").val("");
                    }

                    $tabContent.append($thisTabPane);
                });
                $tabs.find('a.nav-link:first-child').tab('show');

                $modal.modal("show").data({
                    defaultForm: $defaultForm,
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete]", function (e) {
        e.preventDefault();

        var page = $(this).closest("tr").find("td:eq(1)").text();
        var accept = confirm("Are you sure to delete " + page + " ?");
        if (!accept) return;

        var $btn = $(this);
        var url = $(this).attr('href');
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            type: "delete",
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "Success");

                $tablePage.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete-tab]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $self.closest("form");

        var $translateToDropdown = $modal.find('.select2.translate-to');

        var $tabs = $form.find(".nav.nav-tabs");
        var $navTabLinks = $tabs.find("a.nav-link");
        var $activeTab = $tabs.find("a.nav-link.active");

        var $tabContent = $form.find(".tab-content");
        var $activeTabPane = $tabContent.find(".tab-pane.active");

        var activeLang = $activeTab.text();
        var accept = confirm("Delete this page in " + activeLang + " language ?");
        if (!accept) return;

        if ($activeTab.hasClass("new")) {
            $activeTab.remove();
            $activeTabPane.remove();
            $tabs.find('a.nav-link:last-child').tab("show");

            var langIDs = $modal.find("input[name=lang]").map(function () {
                return this.value;
            }).get();
            $translateToDropdown.destroyTranslateToDropdown();
            $translateToDropdown.getTranslateToDropdown(langIDs);
        } else {
            var id = $activeTabPane.find("input[name=id]").val();
            var url = $self.attr("href");
            var btnText = $self.html();

            $.ajaxq("aq", {
                url: url + '/' + id,
                type: "delete",
                beforeSend: function () {
                    $self.html(waitText);
                }
            }).done(function (jqXHR) {
                if (jqXHR.success) {
                    toastr["success"](jqXHR.message, "Success");

                    if ($navTabLinks.length > 1) {
                        $activeTab.remove();
                        $activeTabPane.remove();
                        $tabs.find('a.nav-link:last-child').tab("show");
                    }

                    var langIDs = $modal.find("input[name=lang]").map(function () {
                        return this.value;
                    }).get();
                    $translateToDropdown.destroyTranslateToDropdown();
                    $translateToDropdown.getTranslateToDropdown(langIDs);

                    $tablePage.draw();
                }
            }).always(function (jqXHR) {
                if (!jqXHR.success) {
                    var r = jqXHR.responseJSON;
                    toastr["error"](r.error, "Failed");
                }
                $self.html(btnText);
            });
        }
    });

    $(document).on("click", "[data-trigger=delete-fi]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");
        var $parent = isModalUpdate ? $self.closest(".tab-pane") : $form;

        $parent.find("input[name=featured_image]").val("");
        $parent.find(".featured-image").attr("src", "#");
        $parent.find("[data-trigger=delete-fi]").addClass("d-none");
    });

    $(document).on("click", "[data-trigger=delete-ai]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $self = $(this);
        var $form = $self.closest("form");
        var isModalUpdate = $form.hasClass("update");
        var $parent = isModalUpdate ? $self.closest(".tab-pane") : $form;
        var image = $self.closest(".ai-item").find("img").data("image");

        $self.closest(".ai-item").remove();
        $parent.find("input[name=attachment_image][value='" + image + "']").remove();
    });

    $(document).on("click", "[data-trigger=static-widget]", function (e) {
        e.preventDefault();

        $(this).widget();
    });

    $(document).on("click", "[data-trigger=dynamic-widget]", function (e) {
        e.preventDefault();

        $(this).widget();
    });

    $(document).on("change", "input[name=badge_text_color]", function (e) {
        var $self = $(this);
        var $form = $self.closest("form.form-page");
        var value = $self.val();

        $form.find("input[name=badge_text_color]").val(value);
    });

    $(document).on("change", "input[name=badge_bg_color]", function (e) {
        var $self = $(this);
        var $form = $self.closest("form.form-page");
        var value = $self.val();

        $form.find("input[name=badge_bg_color]").val(value);
    });

    $(document).on("change", "input[name=show_date]", function (e) {
        var $self = $(this);
        var $form = $self.closest("form.form-page");
        var value = $self.val();

        $form.find("input[name=show_date]").val(value);
    });

    $(document).on("change", "input[name=end_date]", function (e) {
        var $self = $(this);
        var $form = $self.closest("form.form-page");
        var value = $self.val();

        $form.find("input[name=end_date]").val(value);
    });

    $(document).on("change", "input[name=order]", function (e) {
        var $self = $(this);
        var $parent = $self.closest("form");
        var value = $self.val();

        $parent.find("input[name=order]").val(value);
    });

    $(document).on("keyup", "input[name=badge_num_bought]", function (e) {
        var $self = $(this);
        var $form = $self.closest("form.form-page");
        var value = $self.val();

        $form.find("input[name=badge_num_bought]").val(value);
    });

    $(document).on("keyup", "input[name=badge_price]", function (e) {
        var $self = $(this);
        var $form = $self.closest("form.form-page");
        var value = $self.val();

        $form.find("input[name=badge_price]").val(value);
    });

    $(document).on("submit", ".form-page", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $modal = $form.closest(".modal");
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr("action");
        var type = $form.attr("type");
        var btnText = $btnSave.html();
        var isUpdateMode = $form.hasClass("update");

        // clean editor
        $form.find("textarea[name]").each(function () {
            var content = cleanString($(this).val());
            $(this).val(content);
        });

        var data;
        if (isUpdateMode) {
            var params = [];
            $form.find(".tab-content .tab-pane").each(function (index) {
                var param = $(this).find("input[name], select[name], textarea[name]").serializeObject();
                if (param.attachment_image && typeof param.attachment_image === 'string') {
                    param.attachment_image = [param.attachment_image]
                }
                if (param.amenities && typeof param.amenities === 'string') {
                    param.amenities = [param.amenities]
                }
                if (param.tags && typeof param.tags === 'string') {
                    param.tags = [param.tags]
                }
                params.push(param);
            });
            data = JSON.stringify(params);
        } else {
            var param = $form.serializeObject();
            if (param.attachment_image && typeof param.attachment_image === 'string') {
                param.attachment_image = [param.attachment_image]
            }
            if (param.amenities && typeof param.amenities === 'string') {
                param.amenities = [param.amenities]
            }
            if (param.tags && typeof param.tags === 'string') {
                param.tags = [param.tags]
            }
            data = JSON.stringify(param);
        }

        $.ajaxq("aq", {
            url: url,
            data: data,
            type: type,
            contentType: "application/json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
                
                $tablePage.draw();
                
                getCategoryGroupedLang(true);
                getPageGroupedLang(true);
                getAmenitiesGroupedLang();
                getTagGroupedLang();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }

            $btnSave.prop('disabled', false).html(btnText);
            $modal.modal("hide");
        });
    });

    $(document).on("selected.media.library", "input[name=featured_image]", function (e) {
        $(this).changeFeaturedImage();
    });

    $(document).on("selected.media.library", ".content-editor, .excerpt-editor", function (e) {
        e.preventDefault();
        var $self = $(this);
        var origin = window.location.origin;

        var json = e.json;
        var file = json.file ? json.file.name : "";
        var src = json.url ? json.url.trim() : ""

        if (!src) {
            src = file ? origin + "/static/uploads/media/" + file : "";
        }

        var $html = "";
        if (json.type == "video") {
            var isYoutubeUrl = src.match(/(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=|\/sandalsResorts#\w\/\w\/.*\/))([^\/&]{10,12})/);
            var videoUrl = isYoutubeUrl ? "https://www.youtube.com/embed/" + isYoutubeUrl[1] + "?rel=0&showinfo=1&autoplay=0" : src.trim();
            $html += '<iframe style="width: 500px; height: 300px;" src="' + videoUrl + '" frameborder="0" allowfullscreen></iframe>';
        } else if (json.type == "attachment") {
            var caption = json.caption ? json.caption : json.title;
            $html += '<a href="' + src + '" target="_blank">' + caption + '</a>';
        } else {
            $html += '<img src="' + src + '" style="width: 300px;" onerror="this.src=\'https://via.placeholder.com/500x334?text=No Picture\'">';
        }
        $self.find("textarea.has-tinymce").tinymce().insertContent($html);
    });

    $(document).on("selected.multiple-media.library", ".attachment", function (e) {
        $(this).changeAttachment();
    });

    $(document).on("widget.selected", "[data-trigger=static-widget]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $parent = $self.closest(".form-group");
        var $editor = $parent.find("textarea.has-tinymce");

        $editor.tinymce().insertContent(e.widget);
    });

    $(document).on("widget.selected", "[data-trigger=dynamic-widget]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $parent = $self.closest(".form-group");
        var $editor = $parent.find(".ace_editor");

        if ($editor.length > 0) {
            var $ace = $editor[0].env.editor;
            var cursorPosition = $ace.getCursorPosition();

            $ace.session.insert(cursorPosition, e.widget);
        }
    });

    $(document).on("select2:select", "select.translate-to", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $modal.find("form.form-page");
        var defaultForm = $modal.data("defaultForm");
        var $translateToDropdown = $modal.find('.select2.translate-to');

        var lang = $(this).select2("data")[0];

        var $tabs = $modal.find(".nav.nav-tabs");
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $modal.find(".tab-content");
        var $tabPaneSource = $tabContent.find(".tab-pane").first();

        var $tab = $(tab);
        var $tabPane = $(defaultForm).find(".tab-pane");

        var href = $tab.attr("href").replace(/\w+$/, lang.id);
        var tabID = href.replace("#", "");
        var source = $tabPaneSource.find("input[name], select[name], textarea[name]").serializeObject();

        $tab.removeClass("active show");
        $tabPane.removeClass("active show");
        $tabPane.find(".fr-box").remove();

        $tab.attr("href", href);
        $tab.html(lang.text);
        $tab.addClass("new");
        $tabs.append($tab);

        $tabPane.attr("id", tabID);
        $tabPane.find("input[name=id]").val("");
        $tabPane.find("input[name=lang]").val(lang.id);
        $tabPane.find("input[name=source_id]").val(source.id);
        $tabPane.find("input[name=title]").val("");
        $tabPane.find("input[name=subtitle]").val("");
        $tabPane.find("input[name=featured_image]").val(source.featured_image).changeFeaturedImage();

        var attachments = (typeof source.attachment_image === "string") ? [source.attachment_image] : source.attachment_image;
        $.each(attachments, function (i, attachment) {
            $tabPane.find(".attachment").append('<input type="hidden" name="attachment_image" value="' + attachment + '">');
        });
        $tabPane.find(".attachment").changeAttachment();

        $tabPane.find("input[name=custom_field]").val(source.custom_field);
        $tabPane.find("input[name=custom_link]").val(source.custom_link);
        $tabPane.find("input[name=custom_label_detail]").val(source.custom_label_detail);
        $tabPane.find("textarea[name=script_header]").val(source.script_header);
        $tabPane.find("textarea[name=script_body]").val(source.script_body);

        var category = getRelatedCategoryID(source.category_id, source.lang, lang.id);
        if (category.id) {
            $tabPane.find("select[name=category_id]").data("value", category.id);
        }

        var page = getRelatedPageID(source.parent_id, source.lang, lang.id);
        if (page.id) {
            $tabPane.find("select[name=parent_id]").data("value", page.id);
        }

        if (source.tags) {
            $tabPane.find("select[name=tags]").data("value", source.tags);
        }

        $tabPane.find("input[name=template]").val(source.template);
        $tabPane.find("input[name=order]").val(source.order);

        $tabPane.find("input[name=status]").val(source.status);
        if (source.status == "publish") {
            $tabPane.find('.az-toggle.toggle-page-status').addClass("on");
        } else {
            $tabPane.find('.az-toggle.toggle-page-status').removeClass("on");
        }

        var categoryId = category.id ? category.id : "";
        if (categoryRequiredValidity.includes(categoryId)) {
            $tabPane.find(".page-validity").removeClass("d-none");
            $tabPane.find('input[name=show_date]').val(source.show_date).prop("required", true);
            $tabPane.find('input[name=end_date]').val(source.end_date).prop("required", true);
        } else {
            $tabPane.find(".page-validity").addClass("d-none");
            $tabPane.find('input[name=show_date]').val("").prop("required", false);
            $tabPane.find('input[name=end_date]').val("").prop("required", false);
        }

        if (categoryUsingAmenities.includes(categoryId)) {
            $tabPane.find(".card-facility").removeClass("d-none");

            var $formGroup = $tabPane.find(".card-facility").find(".form-group");
            source.amenities = typeof source.amenities === 'string' ? [source.amenities] : source.amenities;
            $.each(source.amenities, function (i, amenities) {
                var notExistFacility = $formGroup.find("input[name=amenities][value='" + amenities + "']").length == 0;

                if (notExistFacility) {
                    $formGroup.append('<input type="hidden" name="amenities" value="' + amenities + '">');
                }
            });
        } else {
            $tabPane.find(".card-facility").addClass("d-none");
            $tabPane.find('input[name=amenities]').remove();
        }

        if (categoryUsingBadge.includes(categoryId)) {
            $tabPane.find("input[name=badge_text]").val("");
            $tabPane.find("input[name=badge_text_color]").val(source.badge_text_color);
            $tabPane.find("input[name=badge_bg_color]").val(source.badge_bg_color);
            $tabPane.find("input[name=badge_num_bought]").val(source.badge_num_bought);
            $tabPane.find("input[name=badge_price]").val(source.badge_price);
            $tabPane.find("input[name=badge_btn_label]").val("");
            $tabPane.find("input[name=badge_btn_link]").val(source.badge_btn_link);
            $tabPane.find(".card-badge").removeClass("d-none");
        } else {
            $tabPane.find("input[name=badge_text]").val("");
            $tabPane.find("input[name=badge_text_color]").val("");
            $tabPane.find("input[name=badge_bg_color]").val("");
            $tabPane.find("input[name=badge_num_bought]").val("");
            $tabPane.find("input[name=badge_price]").val("");
            $tabPane.find("input[name=badge_btn_label]").val("");
            $tabPane.find("input[name=badge_btn_link]").val("");
            $tabPane.find(".card-badge").addClass("d-none");
        }

        $tabContent.append($tabPane);
        $tabs.find('a.nav-link:last-child').tab("show");

        $tabPane.find("input[name=show_date]").each(function (i) {
            $(this).datepicker({
                dateFormat: "yy-mm-dd",
            });
        });

        $tabPane.find("input[name=end_date]").each(function (i) {
            $(this).datepicker({
                dateFormat: "yy-mm-dd",
            });
        });

        $tabPane.find('select[name=category_id]').generateSelectCategoryDropdown();
        $tabPane.find('select[name=parent_id]').generateSelectParentDropdown();
        $tabPane.find('select[name=tags]').generateSelectTagDropdown();
        $tabPane.find('textarea[name=content], textarea[name=excerpt]').generateTinyMce();
        $tabPane.find('textarea[data-editor]').generateAceEditor();
        $tabPane.find('.roomAmenitiesTree').generateAmenitiesTreeView();
        $tabPane.find('.collapsible').collapsibleElement();

        var langIDs = $form.find("input[name=lang]").map(function () {
            return this.value;
        }).get();
        $translateToDropdown.destroyTranslateToDropdown();
        $translateToDropdown.getTranslateToDropdown(langIDs);
    });

    $(document).on("select2:select select2:clear", "select[name=category_id]", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $modal.find("form.form-page");
        var isModalUpdate = $form.hasClass("update");
        var $parent = isModalUpdate ? $self.closest(".tab-pane") : $form;

        var data = $self.select2('data')[0];

        if (categoryRequiredValidity.includes(data.id)) {
            $form.find(".page-validity").removeClass("d-none");
            $form.find("input[name=show_date]").prop('required', true);
            $form.find("input[name=end_date]").prop('required', true);
        } else {
            $form.find(".page-validity").addClass("d-none");
            $form.find("input[name=show_date]").val("").prop('required', false);
            $form.find("input[name=end_date]").val("").prop('required', false);
        }

        if (categoryUsingAmenities.includes(data.id)) {
            $form.find(".card-facility").removeClass("d-none");
        } else {
            $form.find(".card-facility").addClass("d-none");
            $form.find('input[name=facilities]').remove();
            $form.find(".treeview").treeview('uncheckAll', { silent: true });
        }

        if (categoryUsingBadge.includes(data.id)) {
            $form.find(".card-badge").removeClass("d-none");
        } else {
            $form.find(".card-badge").addClass("d-none");
        }

        if (isModalUpdate) {
            var selectedLang = $parent.find("input[name=lang]").val();
            $modal.find(".tab-pane").each(function (i) {
                var $tabPane = $(this);
                var $targetCategory = $tabPane.find("select[name=category_id]");

                var lang = $tabPane.find("input[name=lang]").val();
                if (lang != selectedLang) {
                    var targetCategory = getRelatedCategoryID(data.id, selectedLang, lang);

                    if (targetCategory.id) {
                        $targetCategory.val(targetCategory.id);
                    } else {
                        $targetCategory.val(null);
                    }
                    $targetCategory.trigger("change");
                }
            });
        }
    });

    $(document).on("select2:select select2:clear", "select[name=parent_id]", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $modal.find("form.form-page");
        var isModalUpdate = $form.hasClass("update");
        var $parent = isModalUpdate ? $self.closest(".tab-pane") : $form;

        var data = $self.select2('data')[0];
        if (isModalUpdate) {
            var selectedLang = $parent.find("input[name=lang]").val();
            $modal.find(".tab-pane").each(function (i) {
                var $tabPane = $(this);
                var $targetParent = $tabPane.find("select[name=parent_id]");

                var lang = $tabPane.find("input[name=lang]").val();
                if (lang != selectedLang) {
                    var targetPage = getRelatedPageID(data.id, selectedLang, lang);

                    if (targetPage.id) {
                        $targetParent.val(targetPage.id);
                    } else {
                        $targetParent.val(null);
                    }
                    $targetParent.trigger("change");
                }
            });
        }
    });

    $(document).on("select2:select select2:clear", "select[name=tags]", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $form = $modal.find("form.form-page");
        var isModalUpdate = $form.hasClass("update");

        var value = $self.val();
        value = ($.isArray(value) ? value : []).filter(function (v) {
            v = $.trim(v);
            return v != "" || v
        });

        if (isModalUpdate) {
            $modal.find(".tab-pane").each(function (i) {
                var $tabPane = $(this);
                var $targetParent = $tabPane.find("select[name=tags]");

                $targetParent.val(value).trigger("change");
            });
        }
    });

    $(document).on("show.bs.tab", "[data-toggle=tab]", function (e) {
        var $self = $(this);
        var $modal = $self.closest(".modal");
        var $btnDelete = $modal.find("[data-trigger=delete-tab]");

        if ($self.index() > 0) {
            $btnDelete.removeClass("d-none");
        } else {
            $btnDelete.addClass("d-none");
        }
    });

    $(document).on("nodeChecked", ".treeview", function (e, data) {
        var $self = $(this);
        var $form = $self.closest("form");
        var $treeview = $form.find(".treeview");

        var $data = $(data.text);
        var ID = $data.data("id");
        var parentID = $data.data("parentId");

        $treeview.each(function (i) {
            var $thisTree = $(this);
            var $formGroup = $thisTree.closest(".form-group");

            $thisTree.find(".list-group-item").each(function () {
                var nId = $(this).data("nodeid");
                var fID = $(this).find("a").data("id");
                var pID = $(this).find("a").data("parentId");

                if (ID == fID) {
                    $thisTree.treeview('checkNode', [nId, { silent: true }]);
                    if ($formGroup.find('input[name=amenities][value=' + fID + ']').length == 0) {
                        $formGroup.append('<input type="hidden" name="amenities" value="' + fID + '">');
                    }
                }

                // check parent
                if (parentID == fID) {
                    $thisTree.treeview('checkNode', [nId, { silent: true }]);
                    // add input field facilities parent
                    if ($formGroup.find('input[name=amenities][value=' + fID + ']').length == 0) {
                        $formGroup.append('<input type="hidden" name="amenities" value="' + fID + '">');
                    }
                }

                // check child
                if (ID == pID) {
                    $thisTree.treeview('checkNode', [nId, { silent: true }]);
                    // add input field facilities child
                    if ($formGroup.find('input[name=amenities][value=' + fID + ']').length == 0) {
                        $formGroup.append('<input type="hidden" name="amenities" value="' + fID + '">');
                    }
                }
            });
        });
    });

    $(document).on("nodeUnchecked ", ".treeview", function (e, data) {
        var $self = $(this);
        var $form = $self.closest("form");
        var $treeview = $form.find(".treeview");

        var $data = $(data.text);
        var ID = $data.data("id");
        var parentID = $data.data("parentId");

        $treeview.each(function (i) {
            var $thisTree = $(this);
            var $formGroup = $thisTree.closest(".form-group");

            $thisTree.find(".list-group-item").each(function () {
                var nID = $(this).data("nodeid");
                var fID = $(this).find("a").data("id");
                var pID = $(this).find("a").data("parentId");

                if (ID == fID) {
                    $thisTree.treeview('uncheckNode', [nID, { silent: true }]);
                    $formGroup.find('input[name=amenities][value=' + fID + ']').remove();
                }

                // uncheck parent
                if (parentID == fID) {
                    var childs = $thisTree.find(".list-group-item.node-checked").find("a[data-parent-id=" + parentID + "]").length;
                    if (childs == 1) {
                        $thisTree.treeview('uncheckNode', [nID, { silent: true }]);
                        $formGroup.find('input[name=amenities][value=' + fID + ']').remove();
                    }
                }

                // uncheck child
                if (ID == pID) {
                    $thisTree.treeview('uncheckNode', [nID, { silent: true }]);
                    $formGroup.find('input[name=amenities][value=' + fID + ']').remove();
                }
            });
        });
    });

    $(document).on("loaded.treeview", ".roomAmenitiesTree", function (e) {
        e.preventDefault();

        $(this).checkAmenitiesTreeView();
    });

    $(document).on("shown.bs.modal", "#modal-form-page, #modal-form-page-update", function (e) {
        var $modal = $(this);
        var $form = $modal.find("form.form-page");
        var isModalUpdate = $form.hasClass("update");

        if (isModalUpdate) {
            var langIDs = $modal.find("input[name=lang]").map(function () {
                return this.value;
            }).get();

            $('.select2.translate-to').getTranslateToDropdown(langIDs);
        } else {
            var $defaultForm = $form.html();
            $modal.data("defaultForm", $defaultForm);
        }

        $modal.find("input[name=show_date]").each(function (i) {
            $(this).datepicker({
                dateFormat: "yy-mm-dd",
            });
        });

        $modal.find("input[name=end_date]").each(function (i) {
            $(this).datepicker({
                dateFormat: "yy-mm-dd",
            });
        });

        $modal.find('select[name=category_id]').generateSelectCategoryDropdown();
        $modal.find('select[name=parent_id]').generateSelectParentDropdown();
        $modal.find('select[name=tags]').generateSelectTagDropdown();
        $modal.find('textarea[name=content], textarea[name=excerpt]').generateTinyMce();
        $modal.find('textarea[data-editor]').generateAceEditor();
        $modal.find('.roomAmenitiesTree').generateAmenitiesTreeView();
        $modal.find('.collapsible').collapsibleElement();

        $modal.find('[data-toggle="popover-icon"]').popover({
            html: true,
            placement: "bottom",
            content: '<div class="iconpicker"></div>',
        });
    });

    $(document).on("hidden.bs.modal", "#modal-form-page, #modal-form-page-update", function (e) {
        var $modal = $(this);
        var $form = $modal.find('form.form-page');
        var defaultForm = $modal.data('defaultForm');
        var isModalUpdate = $modal.find("form").hasClass("update");

        $modal.find('textarea[name=content], textarea[name=excerpt]').destroyTinyMce();
        $modal.find(".ace_editor").destroyAceEditor();
        $modal.find('select[name=category_id]').destroySelectDropdown();
        $modal.find('select[name=parent_id]').destroySelectDropdown();
        $modal.find('select[name=tags]').destroySelectDropdown();
        if (isModalUpdate) {
            $modal.find('.select2.translate-to').destroyTranslateToDropdown();
        }

        $form.html(defaultForm);
        $modal.removeData();
    });

    $(document).on("inserted.bs.popover", "[data-toggle='popover-icon']", function (e) {
        var $self = $(this);
        var $parent = $self.closest(".form-group");

        $(".iconpicker").iconpicker({
            arrowClass: 'btn-primary',
            arrowPrevIconClass: 'fas fa-angle-left',
            arrowNextIconClass: 'fas fa-angle-right',
            cols: 6,
            iconset: 'fontawesome5',
            rows: 5,
            search: true,
            searchText: 'Search',
            selectedClass: 'btn-success',
            unselectedClass: ''
        }).on("change", function (e) {
            $parent.find("textarea.has-tinymce").tinymce().insertContent('<i class="' + e.icon + '">&nbsp;</i> ');
            $self.popover("hide");
        });
    });
});