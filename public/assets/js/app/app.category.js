$(function () {
    "use strict";

    // https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal
    $.fn.modal.Constructor.prototype._enforceFocus = function () { };

    $.fn.getPrimaryLang = function () {
        var $self = $(this);

        var $request = $.ajax({
            url: '/lang/primary' // wherever your data is actually coming from
        });

        $request.then(function (json) {
            if (!json.data) {
                toastr["error"]("Primary lang isn't set.", "Failed");
            } else {
                $self.val(json.data.code);
            }
        });
    }

    $.fn.generateTinyMce = function(options) {
        var $self = $(this);

        var defaults = {
            max_height: 800,
            placeholder: 'Type here ...',
            branding: false,
            // menubar: false,
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons autoresize',
            menubar: 'file edit view insert format tools table help',
            toolbar: 'preview code | undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | superscript subscript | outdent indent | table | numlist bullist | forecolor backcolor removeformat | hr pagebreak | charmap emoticons | fullscreen save print | insertfile image media template link anchor codesample | ltr rtl',
            image_caption: true,
            image_advtab: true,
            toolbar_sticky: true,
            toolbar_mode: 'sliding',
            importcss_append: true,
            allow_script_urls: true,
            relative_urls : false,
            remove_script_host : false,
            imagetools_toolbar: "imageoptions",
            content_css: ['/static/lib/tinymce/skins/content/default/content.min.css', '/static/lib/fontawesome-free/css/all.min.css'],
            quickbars_selection_toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | h1 h2 h3 blockquote | quicklink quicktable',
            quickbars_insert_toolbar: false,
            contextmenu: "link image table",
        };

        // Merge defaults and options, without modifying defaults
        var settings = $.extend({}, defaults, options ? options : {});

        $self.each(function () {
            $(this).tinymce(settings);
            $(this).addClass("has-tinymce");
        });
    }

    $.fn.destroyTinyMce = function() {
        var $self = $(this);

        $self.each(function () {
            if ($(this).hasClass("has-tinymce")) {
                $(this).tinymce().destroy();
            }
        });
    }

    $.fn.changeDisplayImage = function () {
        var $self = $(this);
        var $parent = $self.closest(".form-group");

        var value = $self.val();
        var image = value.match(/^http/) ? value : "/static/uploads/media/" + value;
        if (value) {
            $parent.find("[data-trigger=delete-display-image]").removeClass("d-none");
        }
        $parent.find("img.display-image").attr("src", image);
    }

    $("input.primary-lang").getPrimaryLang();

    var $tableCategory = $("#datatable-category").DataTable({
        responsive: false,
        language: {
            searchPlaceholder: "Search ...",
            sSearch: "",
            lengthMenu: "_MENU_ items/page",
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        processing: true,
        serverSide: true,
        deferRender: true,
        ajax: {
            url: "/category/primary?get=extends",
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);

                json.recordsTotal = json.data.total;
                json.recordsFiltered = json.data.total;
                // json.recordsFiltered = json.data.item ? json.data.item.length : 0;
                json.data = json.data.item ? json.data.item : [];

                return JSON.stringify(json); // return JSON string
            }
        },
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                className: "align-middle",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: "name" },
            { data: "title" },
            {
                data: "lang.code",
                render: function (data, type, row, meta) {
                    return row.lang.code.toUpperCase();
                }
            },
            { data: "created_at" },
            {
                data: "actions",
                className: "align-middle",
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a href="/category/multiple-lang/' + row.id + '?get=extends" class="badge badge-secondary" data-trigger="update">Edit</a>';
                }
            }
        ],
        order: [[1, "asc"]]
    });

    $(document).on("click", ".btn-refresh-table", function (e) {
        $tableCategory.draw();
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    $(document).on("submit", ".form-category.update", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $btnSave = $form.find("button[type=submit]");

        var url = $form.attr("action");
        var type = $form.attr("type");
        var btnText = $btnSave.html();

        var data = [];
        $form.find(".tab-content .tab-pane").each(function (index) {
            // clean editor
            $(this).find("textarea[name]").each(function() {
                var content = cleanString($(this).val());
                $(this).val(content);
            });
            data.push($(this).find("input[name], select[name], textarea[name]").serializeObject());
        });

        $.ajaxq("aq", {
            url: url,
            data: JSON.stringify(data),
            type: type,
            contentType: "application/json",
            beforeSend: function () {
                $btnSave.prop('disabled', true).html(spinnerLoaderSm + " &nbsp;" + loaderText);
            }
        }).done(function (jqXHR) {
            if (jqXHR.success) {
                toastr["success"](jqXHR.message, "success");
                $tableCategory.draw();
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btnSave.prop('disabled', false).html(btnText);
            $form.closest(".modal").modal("hide");
        });
    });

    $(document).on("click", "[data-trigger=update]", function (e) {
        e.preventDefault();

        var url = $(this).attr('href');
        var $form = $(".form-category.update");
        var $modal = $form.closest(".modal");

        var $tabs = $modal.find(".nav.nav-tabs");
        var $defaultTab = $tabs.html();
        var tab = $('<div>').append($tabs.find("a.nav-link").first().clone(true)).html();

        var $tabContent = $modal.find(".tab-content");
        var $defaultTabContent = $tabContent.html();
        var tabPaneContent = $('<div>').append($tabContent.find(".tab-pane").first().clone(true)).html();

        var $btn = $(this);
        var btnText = $btn.html();

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
                $btn.html(waitText);
            }
        }).done(function (json) {
            if (json.success) {
                var data = json.data;

                var primaryCategoryLang = [data];
                var otherCategoryLang = data.other_lang ? data.other_lang : [];
                var categories = $.merge(primaryCategoryLang, otherCategoryLang);

                $tabs.html("");
                $tabContent.html("");
                $.each(categories, function (i, category) {
                    var $thisTab = $(tab);
                    var $thisTabPane = $(tabPaneContent);

                    var href = $thisTab.attr("href").replace("default", category.lang.code);
                    var tabID = href.replace("#", "");

                    $thisTab.attr("href", href);
                    $thisTab.html(category.lang.name);

                    $tabs.append($thisTab);

                    $thisTabPane.attr("id", tabID);
                    $thisTabPane.find("input[name=id]").val(category.id);
                    $thisTabPane.find("input[name=title]").val(category.title);
                    $thisTabPane.find("textarea[name=description]").val(category.description);
                    $thisTabPane.find("input[name=image]").val(category.image).changeDisplayImage();
                    $thisTabPane.find("input[name=label_detail]").val(category.label.detail);
                    $thisTabPane.find("input[name=label_more]").val(category.label.more);
                    $thisTabPane.find("input[name=meta_title]").val(category.meta.title);
                    $thisTabPane.find("input[name=meta_description]").val(category.meta.description);
                    $thisTabPane.find("input[name=meta_keyword]").val(category.meta.keyword);

                    $tabContent.append($thisTabPane);
                });
                $tabs.find('a.nav-link:first-child').tab('show');

                $modal.modal("show").data({
                    defaultTab: $defaultTab,
                    defaultTabContent: $defaultTabContent,
                });
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
            $btn.html(btnText);
        });
    });

    $(document).on("click", "[data-trigger=delete-display-image]", function (e) {
        e.preventDefault();

        var accept = confirm("Are you sure to delete ?");
        if (!accept) return;

        var $form = $(this).closest("form");
        $form.find("input[name=image]").val("").changeDisplayImage();
    });

    $(document).on("selected.media.library", "input[name=image]", function (e) {
        $(this).changeDisplayImage();
    });

    $(document).on("show.bs.modal", "#modal-form-category-update", function (e) {
        $('textarea[name=description]').generateTinyMce({menubar: false});
    });

    $(document).on("hidden.bs.modal", "#modal-form-category-update", function (e) {
        var $modal = $(this);

        $modal.find("textarea[name=description]").destroyTinyMce();
        

        var defaultTab = $modal.data('defaultTab');
        var defaultTabContent = $modal.data('defaultTabContent');
        if (defaultTab) {
            $modal.find(".nav.nav-tabs").html(defaultTab);
        }
        if (defaultTabContent) {
            $modal.find(".tab-content").html(defaultTabContent);
        }

        $modal.removeData();
    });
});