/* 
usage:
    data-toggle="widget"
*/

$(function () {
    "use strict";

    var modal = "#modal-widget";

    $.fn.widget = function() {
        var $self = $(this);
        var $modal = $(modal);

        $modal.data({target: $self});
        $modal.modal({
            show: true,
            keyboard: false,
            backdrop: "static",
        });
    }

    $.fn.getWidget = function() {
        var $self = $(this);
        var url = $self.data("api");

        $.ajaxq("aq", {
            url: url,
            beforeSend: function () {
            }
        }).done(function (json) {
            if (json.success) {
                var widgets = json.data.item;
                
                var html = '';
                $.each(widgets, function(k, item) {
                    var $item = $('\
                    <div class="row row-sm mg-b-10"> \
                        <div class="col-md-9"> \
                            <input type="hidden" name="code" value="'+ item.code + '"> \
                            <textarea name="content" class="d-none"></textarea> \
                            <input type="text" value="'+ item.name +'" class="form-control bg-white" readonly> \
                        </div> \
                        <div class="col-md-3"> \
                            <button class="btn btn-purple btn-block" data-toggle="select-widget">Select</button> \
                        </div> \
                    </div>');
                    $item.find("textarea[name=content]").html(item.content);
                    html += $("<div>").append($item).html();
                });
                
                if (widgets.length == 0) {
                    html = '<div class="alert alert-outline-warning mg-b-0 tx-center" role="alert">No items.</div>';
                }

                $self.html(html);
            }
        }).always(function (jqXHR) {
            if (!jqXHR.success) {
                var r = jqXHR.responseJSON;
                var message = r ? r.error : jqXHR.statusText;
                toastr["error"](message, "Failed");
            }
        });
    }

    $(document).on("click", "[data-toggle=widget]", function (e) {
        e.preventDefault();

        $(this).widget();
    });

    $(document).on("click", "[data-toggle=select-widget]", function (e) {
        e.preventDefault();

        var $self = $(this);
        var $modal = $self.closest(".modal");
        var modalData = $modal.data();
        var $target = modalData.target;
        var $parent = $self.closest(".row");
        var $content = $("<div>").append($parent.find("textarea[name=content]").val());
        
        var content = "";
        var codes = $parent.find("input[name=code]").val();

        if (codes && codes !== "") {
            codes = codes.split(",");

            $.each(codes, function(i, code){
                code = $.trim(code);
                if (code != "") {
                    code = code.split(":");
                    name = code[0];
                    
                    var required = code.length > 1 && code[1].toLowerCase() == "required" ? "required" : "";
                    var $input = $content.find("input[name='" + name + "'], select[name='" + name + "'], textarea[name='" + name + "']");
                    if ($input.length > 0) {
                        var placeholder = $input.attr("placeholder");
                        placeholder = placeholder ? placeholder : "Input Value";
                        content += '\
                            <div class="form-group"> \
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">' + placeholder + '</label> \
                                <input name="' + name + '" class="form-control" placeholder="' + placeholder + '" ' + required + '> \
                            </div> \
                        ';
                    }
                }
            });

            if (content != "") {
                content = '<form class="form-popover-widget clearfix">' + content;
                content += '<button class="btn btn-secondary float-right" type="submit">Submit</button>';
                content += '</form>';

                $self.popover({
                    html: true,
                    placement: "bottom",
                    content: content,
                }).popover("show");
            }
        } else {
            $target.trigger({ type: "widget.selected", widget: $content.html() });
            $modal.modal("hide");
        }
    });

    $(document).on("submit", "form.form-popover-widget", function (e) {
        e.preventDefault();

        var $form = $(this);
        var $popover = $form.closest(".popover");
        var popoverData = $popover.data("bs.popover");
        var $toggle = $(popoverData.element);
        var $parent = $toggle.closest(".row");
        var $modal = $parent.closest(".modal");
        var modalData = $modal.data();
        var $target = modalData.target;
        var $content = $($parent.find("textarea[name=content]").val());

        var formData = $form.serializeObject();
        $.each(formData, function(name, value) {
            $content.find("input[name='" + name + "'], select[name='" + name + "'], textarea[name='" + name + "']").val(value);
        });

        var content = $("<div>").append($content).html();

        $target.trigger({ type: "widget.selected", widget: content });

        $popover.popover("hide");
        $modal.modal("hide");
    });

    $(document).on("shown.bs.modal", modal, function(){
        $(this).find(".widget-data").getWidget();
    });

    $(document).on("hidden.bs.modal", modal, function(){
        $(this).removeData();
    });

    // Usage
    /* $(document).on("widget.selected", "[data-toggle=widget]", function(e){
        console.log(e);
        console.log(e.widget);
    }); */
});