package config

import (
	"fmt"
	"log"

	"alaric_cms/services"

	"github.com/fsnotify/fsnotify"
	"github.com/go-redis/redis"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/spf13/viper"
)

type Constants struct {
	AppName     string
	Environment string
	Port        string
	BaseURL     string
	Session     struct {
		Name      string
		SecretKey string
		Expire    int
	}
	Jwt struct {
		Expire                 int
		ExpireForgotPassword   int
		SecretKey              string
		SecretResetPasswordKey string
	}
	Role struct {
		Corporate int
		Brand     int
		Hotel     int
	}
	Mongo struct {
		Host     string
		Port     string
		User     string
		Password string
		DBName   string
	}
	Redis struct {
		Host     string
		Port     string
		Password string
		DBName   int
	}
	Google struct {
		ApiKey string
	}
	API struct {
		Host string
		Port string
		URL  string
	}
	SMTP struct {
		Host string
		Port int
	}
	Mail struct {
		Sender string
	}
	Path struct {
		Static           string
		StaticAs         string
		Media            string
		MediaAs          string
		CloudImagePrefix string
	}
}

type DB struct {
	MongoDB *mongo.Database
	RedisDB *redis.Client
}

type Config struct {
	Constants
	DB
}

// NewConfig is used to generate a configuration instance which will be passed around the codebase
func New(configFile string) (*Config, error) {
	config := Config{}
	constants, err := initConfig(configFile)
	config.Constants = constants

	if err != nil {
		return &config, err
	}

	_, config.DB.MongoDB, _ = services.OpenMongoSession(
		config.Constants.Mongo.Host,
		config.Constants.Mongo.Port,
		config.Constants.Mongo.User,
		config.Constants.Mongo.Password,
		config.Constants.Mongo.DBName,
	)

	config.DB.RedisDB, _ = services.OpenRedisSession(
		config.Constants.Redis.Host,
		config.Constants.Redis.Port,
		config.Constants.Redis.Password,
		config.Constants.Redis.DBName,
	)

	return &config, err
}

func initConfig(configFile string) (Constants, error) {
	if configFile == "" {
		configFile = "app"
	}
	/* else {
		// Config file without ext, replace ext file
		configFile = strings.TrimSuffix(configFile, filepath.Ext(configFile))
	} */
	viper.SetConfigName(configFile) // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")        // Search the root directory for the configuration file
	viper.AddConfigPath("./config") // Search the root directory for the configuration file
	err := viper.ReadInConfig()     // Find and read the config file
	if err != nil {                 // Handle errors reading the config file
		return Constants{}, err
	}
	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	viper.SetDefault("Port", "9999")
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)

	return constants, err
}
