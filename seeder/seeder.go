package seeder

import (
	"alaric_cms/app/models"
	conf "alaric_cms/config"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/mongodb/mongo-go-driver/mongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

type City struct {
	CityId   string `json:"city_id" bson:"_id"`
	CityName string `json:"city_name" bson:"city_name"`
}

func Seeder(config *conf.Config) {
	PropertySeed(config)
	// CitySeed(config)
}

func UserSeed(config *conf.Config, propertyId primitive.ObjectID) {
	collection := config.MongoDB.Collection("users")

	var now = time.Now()
	var users = []models.User{
		{
			UserLogin:      "admin",
			UserPassword:   "12345",
			UserFirstname:  "admin",
			UserEmail:      "admin@mail.com",
			UserRole:       config.Role.Corporate,
			UserStatus:     "active",
			PropertyId:     &propertyId,
			UserRegistered: &now,
		},
	}

	for _, data := range users {
		var user models.User
		err := collection.FindOne(context.Background(), bson.D{{"user_login", data.UserLogin}}).Decode(&user)
		if err != nil {
			if err.Error() == "mongo: no documents in result" {
				hash, err := bcrypt.GenerateFromPassword([]byte(data.UserPassword), 5)

				if err != nil {
					fmt.Println("Error while hashing password, Try again ...")
					continue
				}
				data.UserPassword = string(hash)

				_, err = collection.InsertOne(context.Background(), data)
				if err != nil {
					fmt.Println("Error while creating user, Try again ...")
					continue
				}
				fmt.Println("Registration successful")
				continue
			}

			fmt.Println(err.Error())
			continue
		}
		fmt.Println("Username already exists !!!")
	}

}

func PropertySeed(config *conf.Config) {
	collection := config.MongoDB.Collection("properties")

	var now = time.Now()
	var payload = models.Property{
		PropertyCode:       "COR",
		PropertyName:       "Corporate Hotel",
		PropertyEmail:      "corporate@mail.com",
		PropertyTagline:    "My Tagline",
		PropertyRegistered: &now,
	}

	var property models.Property
	err := collection.FindOne(context.Background(), bson.D{{"property_code", payload.PropertyCode}}).Decode(&property)
	if err != nil {
		if err.Error() == "mongo: no documents in result" {
			doc, err := collection.InsertOne(context.Background(), payload)
			if err != nil {
				fmt.Println("Error while creating property.")
			}
			fmt.Println("Inserted property.")
			UserSeed(config, doc.InsertedID.(primitive.ObjectID))
		}
	}
}

func CitySeed(config *conf.Config) {
	collection := config.MongoDB.Collection("cities")

	// Open our jsonFile
	jsonCity, err := os.Open("./seeder/cities.id.json")
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonCity.Close()

	// read our opened json as a byte array.
	jsonByte, _ := ioutil.ReadAll(jsonCity)

	var docs []interface{}
	var cities []City

	json.Unmarshal(jsonByte, &cities)
	for _, data := range cities {
		docs = append(docs, data)
	}

	var result *mongo.InsertManyResult
	var ctx = context.Background()

	result, _ = collection.InsertMany(ctx, docs)
	for _, doc := range docs {
		isFound := false
		for _, id := range result.InsertedIDs {
			if id == doc.(City).CityId {
				isFound = true
				continue
			}
		}
		if !isFound {
			log.Fatal(doc.(City).CityId, "not inserted")
		}
	}
}
