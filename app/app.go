package app

import (
	"alaric_cms/app/models"
	apiRv1 "alaric_cms/app/routers/api/v1"
	webRv1 "alaric_cms/app/routers/web"
	conf "alaric_cms/config"
	"alaric_cms/seeder"
	"encoding/gob"
	"fmt"
	"html/template"
	"log"
	"strings"
	"time"

	"github.com/foolin/goview"
	"github.com/foolin/goview/supports/ginview"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/sessions"
)

type App struct {
	Gin        *gin.Engine
	Session    *sessions.CookieStore // store will hold all session data
	Config     *conf.Config
	ConfigFile *string
	Port       *string
	Env        *string
	Seed       *bool
	Verbose    *bool
}

func (app *App) Initialize() {
	configuration, err := conf.New(*app.ConfigFile)
	if err != nil {
		log.Panicln("Configuration error", err)
	}
	// Force log's color
	gin.ForceConsoleColor()

	app.Gin = gin.New()
	app.Config = configuration

	sessionSecret := []byte(app.Config.Session.SecretKey)
	app.Session = sessions.NewCookieStore(
		sessionSecret,
	)

	// app.Config.Session.Expire is in minute
	app.Session.Options = &sessions.Options{
		Path:   "/",
		MaxAge: int((time.Minute * time.Duration(app.Config.Session.Expire)).Seconds()),
	}
	gob.Register(models.UserSession{})

	//new template engine
	app.Gin.HTMLRender = ginview.New(goview.Config{
		Root:      "public/views",
		Extension: ".html",
		Master:    "layouts/master",
		Partials:  []string{},
		Funcs: template.FuncMap{
			"copy": func() string {
				return time.Now().Format("2006")
			},
			"toUpper": func(v string) string {
				return strings.ToUpper(v)
			},
			/* "marshal": func(v interface{}) template.JS {
				a, _ := json.Marshal(v)
				return template.JS(a)
			}, */
		},
		DisableCache: true,
	})
	app.Gin.Use(gin.Logger())
	// same as
	// config := cors.DefaultConfig()
	// config.AllowAllOrigins = true
	// app.Gin.Use(cors.New(config))
	app.Gin.Use(cors.Default())
	app.Gin.Use(location.Default())

	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	app.Gin.Use(gin.Recovery())
	// app.Gin.Use(app.Menu)

	app.Gin.Static(app.Config.Path.StaticAs, app.Config.Path.Static)
	// app.Gin.LoadHTMLGlob("public/views/**/*")

	if *app.Seed {
		app.DataSeeder()
	} else {
		app.SetRouter()
	}
}

func (app *App) SetRouter() {

	// initialize route api v1
	apiRv1.Init(app.Gin, app.Config)

	// initialize route web app
	webRv1.Init(app.Gin, app.Config, app.Session)
}

func (app *App) DataSeeder() {
	seeder.Seeder(app.Config)
}

// Run the app on it's router
func (app *App) Run() {
	if *app.Port == "" {
		app.Port = &app.Config.Port
	}

	if app.Config.API.Port == "" {
		app.Config.API.Port = *app.Port
	}
	app.Config.API.URL = fmt.Sprintf("%s:%s", app.Config.API.Host, app.Config.API.Port)

	app.Gin.Run(fmt.Sprintf(":%s", *app.Port))
}
