package web

import (
	WebCv1 "alaric_cms/app/controllers/web"
	md "alaric_cms/app/middlewares"
	conf "alaric_cms/config"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/sessions"
)

var WebController *WebCv1.Web

func Init(gin *gin.Engine, config *conf.Config, session *sessions.CookieStore) {
	WebController = &WebCv1.Web{
		Gin:     gin,
		Config:  config,
		Session: session,
	}

	webGv1 := WebController.Gin.Group("/")
	// set all auth route
	Auth(webGv1)
	// set all dashboard route
	Dashboard(webGv1)
	// set all users route
	Users(webGv1)
	// set all users route
	Setup(webGv1)
	// set all brands route
	Brands(webGv1)
	// set all cities route
	Cities(webGv1)
	// set all hotels route
	Hotels(webGv1)
	// set all hotel facilities route
	Facilities(webGv1)
	// set all room amenities route
	Amenities(webGv1)
	// set all categories route
	Categories(webGv1)
	// set all tag route
	Tag(webGv1)
	// set all widget route
	Widget(webGv1)
	// set all our team route
	Team(webGv1)
	// set all banner route
	Banner(webGv1)
	// set all media route
	Media(webGv1)
	// set all pages route
	Pages(webGv1)
	// set all group content route
	GroupContent(webGv1)
	// set all menu route
	Menu(webGv1)
	// set all message route
	Message(webGv1)
	// set all builder route
	Builder(webGv1)
	// set all setting route
	Setting(webGv1)
	// set all utils route
	Utils(webGv1)
	// set all lang route
	Lang(webGv1)
}

func Auth(rg *gin.RouterGroup) {
	auth := rg.Group("/auth")
	{
		auth.GET("/", WebController.Login)
		auth.GET("/reset/:key", WebController.Reset)
		auth.GET("/logout/", WebController.Logout)
		auth.POST("/forgot/", WebController.Forgot)
		auth.POST("/process/", WebController.Auth)
		auth.PUT("/reset/", WebController.ResetProcess)

	}
}

func Dashboard(rg *gin.RouterGroup) {
	rg.GET("", func(c *gin.Context) {
		c.Redirect(http.StatusTemporaryRedirect, "/dashboard")
	})

	dashboard := rg.Group("/dashboard")
	{
		dashboard.Use(md.RequiredSession(WebController.Session, WebController.Config))
		dashboard.GET("/", WebController.Dashboard)
		dashboard.GET("/:Id/:Level", WebController.Dashboard)
	}
}

func Users(rg *gin.RouterGroup) {
	users := rg.Group("/users")
	{
		users.Use(md.RequiredSession(WebController.Session, WebController.Config))
		users.GET("", WebController.Users)
		users.POST("/add", WebController.UserAdd)
		users.GET("/update/:id", WebController.UserUpdate)
		users.POST("/update", WebController.UserUpdateProcess)
		users.POST("/update/password", WebController.UserUpdatePassword)
		users.DELETE("/delete/:id", WebController.UserDelete)
		users.GET("/load", WebController.GetUsers)
	}
}

func Setup(rg *gin.RouterGroup) {
	users := rg.Group("/setup")
	{
		users.Use(md.RequiredSession(WebController.Session, WebController.Config))
		users.GET("/corporate", WebController.SetupCorporate)
		users.GET("/corporate/data", WebController.GetCorporateData)
		users.POST("/corporate", WebController.SetupCorporateProcess)
		users.GET("/brands", WebController.SetupBrands)
		users.GET("/hotels", WebController.SetupHotels)
		users.GET("/facilities", WebController.SetupFacilities)
		users.GET("/category", WebController.SetupCategory)
		users.GET("/page-duplicator", WebController.SetupPageDuplicator)
		users.GET("/tag", WebController.SetupTag)
		users.GET("/widget", WebController.SetupWidget)
		users.GET("/cities", WebController.SetupCity)
		users.GET("/lang", WebController.SetupLang)
	}
}

func Brands(rg *gin.RouterGroup) {
	brands := rg.Group("/brands")
	{
		brands.Use(md.RequiredSession(WebController.Session, WebController.Config))
		brands.GET("", WebController.GetBrands)
		brands.GET("/update/:id", WebController.BrandUpdate)
		// add
		brands.POST("", WebController.BrandAdd)
		// update
		brands.PUT("", WebController.BrandUpdateProcess)
		brands.DELETE("/:id", WebController.BrandDelete)
	}
}

func Cities(rg *gin.RouterGroup) {
	city := rg.Group("/cities")
	{
		city.Use(md.RequiredSession(WebController.Session, WebController.Config))
		city.GET("", WebController.GetCities)
		city.GET("/update/:id", WebController.CityUpdate)
		// add
		city.POST("", WebController.CityAdd)
		// update
		city.PUT("/:id", WebController.CityUpdateProcess)
		city.DELETE("/:id", WebController.CityDelete)
	}
}

func Hotels(rg *gin.RouterGroup) {
	hotels := rg.Group("/hotels")
	{
		hotels.Use(md.RequiredSession(WebController.Session, WebController.Config))
		hotels.GET("", WebController.GetHotels)
		hotels.GET("/update/:id", WebController.HotelUpdate)
		hotels.POST("", WebController.HotelAdd)
		hotels.PUT("", WebController.HotelUpdateProcess)
		hotels.DELETE("/:id", WebController.HotelDelete)
	}
}

func Facilities(rg *gin.RouterGroup) {
	facility := rg.Group("/facility")
	{
		facility.Use(md.RequiredSession(WebController.Session, WebController.Config))
		facility.GET("", WebController.GetFacility)
		facility.GET("/primary", WebController.GetFacilityUsedPrimaryLang)
		facility.GET("/grouped-lang", WebController.GetFacilityGroupedLang)
		facility.GET("/tree/grouped-lang", WebController.GetFacilityTreeGroupedLang)
		facility.GET("/update/:id", WebController.FacilityUpdate)

		facility.POST("", WebController.FacilityAdd)
		facility.PUT("", WebController.FacilityUpdateProcess)
		facility.DELETE("/:id", WebController.FacilityDelete)
		facility.DELETE("/:id/:lang", WebController.FacilityDeleteByLang)
	}
}

func Amenities(rg *gin.RouterGroup) {
	amenities := rg.Group("/amenities")
	{
		amenities.Use(md.RequiredSession(WebController.Session, WebController.Config))

		amenities.GET("", WebController.GetAmenities)
		amenities.GET("/primary", WebController.GetAmenitiesUsedPrimaryLang)
		amenities.GET("/grouped-lang", WebController.GetAmenitiesGroupedLang)
		amenities.GET("/tree/grouped-lang", WebController.GetAmenitiesTreeGroupedLang)
		amenities.GET("/update/:id", WebController.AmenitiesUpdate)

		amenities.POST("", WebController.AmenitiesAdd)
		amenities.PUT("", WebController.AmenitiesUpdateProcess)
		amenities.DELETE("/:id", WebController.AmenitiesDelete)
		amenities.DELETE("/:id/:lang", WebController.AmenitiesDeleteByLang)
	}
}

func Categories(rg *gin.RouterGroup) {
	category := rg.Group("/category")
	{
		category.Use(md.RequiredSession(WebController.Session, WebController.Config))
		category.GET("", WebController.Category)
		category.GET("/get", WebController.GetCategory)
		category.GET("/primary", WebController.GetCategoryUsedPrimaryLang)
		category.GET("/update/:id", WebController.CategoryUpdate)
		category.GET("/multiple-lang/:id", WebController.GetMultipleLangCategoryByID)
		category.GET("/grouped-lang", WebController.GetCategoryGroupedLang)
		category.GET("/menu", WebController.GetCategoryForMenu)
		category.POST("", WebController.CategoryAdd)
		category.PUT("", WebController.CategoryUpdateProcess)
		category.PUT("/extends", WebController.CategoryExtendsUpdateProcess)
		category.DELETE("/single/:id", WebController.CategoryDelete)
		category.DELETE("/multiple-lang/:id", WebController.CategoryDeleteMultipleLangByID)
	}
}

func Tag(rg *gin.RouterGroup) {
	category := rg.Group("/tag")
	{
		category.Use(md.RequiredSession(WebController.Session, WebController.Config))
		category.GET("", WebController.GetTag)
		category.GET("/update/:id", WebController.TagUpdate)
		category.GET("/grouped-lang", WebController.GetTagGroupedLang)
		category.POST("", WebController.TagAdd)
		category.PUT("/:id", WebController.TagUpdateProcess)
		category.DELETE("/:id", WebController.TagDelete)
		category.DELETE("/:id/:lang", WebController.TagDeleteByLang)
	}
}

func Widget(rg *gin.RouterGroup) {
	widget := rg.Group("/widget")
	{
		widget.Use(md.RequiredSession(WebController.Session, WebController.Config))
		widget.GET("", WebController.GetWidget)
		widget.GET("/:id", WebController.GetWidgetByID)
		widget.POST("", WebController.WidgetAdd)
		widget.PUT("", WebController.WidgetUpdate)
		widget.DELETE("/:id", WebController.WidgetDelete)
	}
}

func Team(rg *gin.RouterGroup) {
	team := rg.Group("/team")
	{
		team.Use(md.RequiredSession(WebController.Session, WebController.Config))
		team.GET("", WebController.Team)
		team.GET("/get", WebController.GetTeam)
		team.GET("/get/:id", WebController.GetTeamByID)
		team.POST("", WebController.TeamAdd)
		team.PUT("/:id", WebController.TeamUpdate)
		team.DELETE("/:id", WebController.TeamDelete)
	}
}

func Banner(rg *gin.RouterGroup) {
	banner := rg.Group("/banner")
	{
		banner.Use(md.RequiredSession(WebController.Session, WebController.Config))
		banner.GET("", WebController.Banner)
		banner.GET("/update/:id", WebController.BannerUpdate)
		banner.GET("/assigned/:type", WebController.GetAssignedBanner)
		banner.GET("/load", WebController.GetBanner)
		banner.POST("", WebController.BannerAdd)
		banner.PUT("", WebController.BannerUpdateProcess)
		banner.PUT("/assign/:type", WebController.AssignBanner)
		banner.DELETE("/delete/:id", WebController.BannerDelete)
		banner.DELETE("/assigned/:type/:banner_id/:id", WebController.BannerDeleteAssignedBanner)
	}
}

func Media(rg *gin.RouterGroup) {
	media := rg.Group("/media")
	{
		media.Use(md.RequiredSession(WebController.Session, WebController.Config))
		media.GET("", WebController.Media)
		media.GET("/update/:id", WebController.MediaUpdate)
		media.GET("/load", WebController.GetMedia)
		media.POST("", WebController.MediaAdd)
		media.PUT("", WebController.MediaUpdateProcess)
		media.DELETE("/:id", WebController.MediaDelete)
	}
}

func Pages(rg *gin.RouterGroup) {
	page := rg.Group("/page")
	{
		page.Use(md.RequiredSession(WebController.Session, WebController.Config))
		page.GET("", WebController.Pages)
		page.GET("/get", WebController.GetPages)
		page.GET("/primary", WebController.GetPageUsedPrimaryLang)
		page.GET("/multiple-lang/:id", WebController.GetMultipleLangPageByID)
		page.GET("/grouped-lang", WebController.GetPageGroupedLang)
		page.GET("/menu", WebController.GetPagesForMenu)
		page.GET("/parent", WebController.GetPagesForParent)
		page.GET("/update/:id", WebController.PageUpdate)
		page.POST("", WebController.PageAdd)
		page.POST("/duplicator", WebController.PageDuplicator)
		page.PUT("", WebController.PageUpdateProcess)
		page.DELETE("/single/:id", WebController.PageDelete)
		page.DELETE("/multiple-lang/:id", WebController.PageDeleteMultipleLangByID)
	}
}

func GroupContent(rg *gin.RouterGroup) {
	gc := rg.Group("/group_content")
	{
		gc.Use(md.RequiredSession(WebController.Session, WebController.Config))
		gc.GET("", WebController.GroupContent)
		gc.GET("/get", WebController.GroupContentGet)
		gc.GET("/menu", WebController.GetGroupContentForMenu)
		gc.POST("", WebController.GroupContentAdd)
		gc.PUT("/:id", WebController.GroupContentUpdate)
		gc.PUT("/:id/setting", WebController.GroupContentSettingUpdate)
		gc.DELETE("/:id", WebController.GroupContentDelete)
		gc.DELETE("/:id/:lang", WebController.GroupContentDeleteByLang)
	}
}

func Builder(rg *gin.RouterGroup) {
	pages := rg.Group("/builder")
	{
		pages.Use(md.RequiredSession(WebController.Session, WebController.Config))
		pages.GET("/map-attraction", WebController.MapAttraction)
		pages.GET("/instagram", WebController.InstagramFeed)
	}
}

func Setting(rg *gin.RouterGroup) {
	setting := rg.Group("/setting")
	{
		setting.Use(md.RequiredSession(WebController.Session, WebController.Config))
		setting.GET("", WebController.Setting)
		setting.GET("/menu", WebController.SettingMenu)
		setting.GET("/message", WebController.SettingMessage)
		setting.PUT("", WebController.SettingGeneralProcess)
		setting.PUT("/media", WebController.SettingMediaProcess)
		setting.PUT("/socmed", WebController.SettingSocmedProcess)
		setting.PUT("/meta", WebController.SettingMetaProcess)
		setting.PUT("/script", WebController.SettingScriptProcess)
	}
}

func Menu(rg *gin.RouterGroup) {
	menu := rg.Group("/menu")
	{
		menu.Use(md.RequiredSession(WebController.Session, WebController.Config))
		menu.GET("", WebController.GetMenu)
		menu.GET("/:id", WebController.GetMenuByID)
		menu.POST("", WebController.MenuAdd)
		menu.PUT("/:id", WebController.MenuUpdate)
		menu.DELETE("/:id", WebController.MenuDelete)
	}
}

func Message(rg *gin.RouterGroup) {
	message := rg.Group("/message")
	{
		message.Use(md.RequiredSession(WebController.Session, WebController.Config))
		message.GET("", WebController.GetMessage)
		message.GET("/:id", WebController.GetMessageByID)
		message.POST("", WebController.MessageAdd)
		message.PUT("/:id", WebController.MessageUpdate)
		message.DELETE("/:id", WebController.MessageDelete)
	}
}

func Lang(rg *gin.RouterGroup) {
	lang := rg.Group("/lang")
	{
		lang.Use(md.RequiredSession(WebController.Session, WebController.Config))
		lang.GET("", WebController.GetLang)
		lang.GET("/primary", WebController.GetPrimaryLang)
		lang.GET("/list", WebController.GetLangList)
		lang.POST("", WebController.LangAdd)
		lang.PUT("/change_status/:id/:status", WebController.LangChangeStatus)
		lang.DELETE("/:id", WebController.LangDelete)
	}
}

func Utils(rg *gin.RouterGroup) {
	utils := rg.Group("/utils")
	{
		utils.Use(md.RequiredSession(WebController.Session, WebController.Config))
		utils.GET("/cities", WebController.GetCityList)
		utils.GET("/brands", WebController.GetBrandList)
		utils.GET("/brands/property", WebController.GetBrandHasPropertyList)
		utils.GET("/property", WebController.GetPropertyList)
		utils.GET("/property_page_session", WebController.GetPropertyByPageSession)
		utils.GET("/lang", WebController.GetLanguageList)
		utils.GET("/categories", WebController.GetCategoryList)
		utils.POST("/plugin_file_uploader", WebController.PluginFileUploader)
		utils.POST("/plugin_file_delete", WebController.PluginFileDelete)
	}
}
