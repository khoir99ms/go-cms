package v1

import (
	ApiCv1 "alaric_cms/app/controllers/api/v1"
	md "alaric_cms/app/middlewares"
	conf "alaric_cms/config"

	"github.com/gin-gonic/gin"
)

var ApiController *ApiCv1.Api

func Init(gin *gin.Engine, config *conf.Config) {
	ApiController = &ApiCv1.Api{
		Gin:    gin,
		Config: config,
	}

	apiGv1 := ApiController.Gin.Group("/api/v1")
	// set all util api route
	Utils(apiGv1)
	// set all lang api route
	Lang(apiGv1)
	// set all authentication api route
	Auth(apiGv1)
	// set all city api route
	Cities(apiGv1)
	// set all corporate api route
	Corporate(apiGv1)
	// set all property api route
	Property(apiGv1)
	// set all brand api route
	Brands(apiGv1)
	// set all category api route
	Categories(apiGv1)
	// set all tag api route
	Tag(apiGv1)
	// set all widget api route
	Widget(apiGv1)
	// set all hotel facility api route
	Facility(apiGv1)
	// set all team api route
	Team(apiGv1)
	// set all banner api route
	Banner(apiGv1)
	// set all media api route
	Media(apiGv1)
	// set all pages api route
	Pages(apiGv1)
	// set all group content api route
	GroupContent(apiGv1)
	// set all setting api route
	Setting(apiGv1)
	// set all menu api route
	Menu(apiGv1)
	// set all message api route
	Message(apiGv1)
	// set all public api route
	PublicApi(apiGv1)
}

func Utils(rg *gin.RouterGroup) {
	utils := rg.Group("/utils")
	{
		utils.Use(md.RequiredAuth(ApiController.Config))
		utils.GET("/city", ApiController.GetCity)
		utils.GET("/network/menu", ApiController.GetMyNetworkMenu)
	}
}

func Lang(rg *gin.RouterGroup) {
	lang := rg.Group("/lang")
	{
		lang.Use(md.RequiredAuth(ApiController.Config))
		lang.GET("", ApiController.GetLang)
		lang.GET("/list", ApiController.GetLangList)
		lang.POST("", ApiController.AddLang)
		lang.PUT("/change_status/:id", ApiController.ChangeStatusLang)
		lang.DELETE("/:id", ApiController.DeleteLang)
	}
}

func Auth(rg *gin.RouterGroup) {
	auth := rg.Group("/auth")
	{
		auth.POST("/", ApiController.Login)

		auth.Use(md.RequiredAuth(ApiController.Config))

		auth.GET("/users", ApiController.GetUsers)

		register := auth.Group("/register")
		register.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
			ApiController.Config.Role.Brand,
		)).POST("", ApiController.RegisterUser)

		change := auth.Group("/change")
		change.Use(md.OnlySameUserCanDoThis(
			ApiController.Config.Role.Corporate,
		))
		change.PUT("/:userId", ApiController.UpdateUser)
		change.PUT("/:userId/password", ApiController.ChangeUserPassword)

		delete := auth.Group("/delete")
		delete.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
		))
		delete.DELETE("/:userId", ApiController.DeleteUser)
	}

	forgot := rg.Group("/auth/forgot")
	forgot.POST("", ApiController.ForgotPassword)

	reset := rg.Group("/auth/reset")
	reset.Use(md.RequiredResetPasswordKey(ApiController.Config))
	reset.PUT("", ApiController.ResetPassword)
}

func Cities(rg *gin.RouterGroup) {
	cities := rg.Group("/cities")
	{
		cities.Use(md.RequiredAuth(ApiController.Config))

		cities.GET("", ApiController.GetCities)

		permission := cities.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
		))
		{
			permission.POST("", ApiController.AddCity)
			permission.PUT("/:id", ApiController.UpdateCity)
			permission.DELETE("/:id", ApiController.DeleteCity)
		}
	}
}

func Corporate(rg *gin.RouterGroup) {
	corporate := rg.Group("/corporate")
	{
		corporate.Use(md.RequiredAuth(ApiController.Config))
		corporate.GET("/", ApiController.GetCorporate)

		permission := corporate.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
		))
		{
			permission.POST("/", ApiController.AddCorporate)
			permission.PUT("/:corporateId", ApiController.UpdateCorporate)
			permission.DELETE("/:corporateId", ApiController.DeleteCorporate)
		}
	}
}

func Property(rg *gin.RouterGroup) {
	property := rg.Group("/property")
	{
		property.Use(md.RequiredAuth(ApiController.Config))

		property.GET("/", ApiController.GetProperty)
		property.POST("/", ApiController.AddProperty)
		property.PUT("/:propertyId", ApiController.UpdateProperty)
		property.DELETE("/:propertyId", ApiController.DeleteProperty)
	}
}

func Brands(rg *gin.RouterGroup) {
	brands := rg.Group("/brands")
	{
		brands.Use(md.RequiredAuth(ApiController.Config))

		brands.GET("", ApiController.GetBrand)
		brands.GET("/property", ApiController.GetBrandWithProperty)

		permission := brands.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
			ApiController.Config.Role.Brand,
		))
		{
			permission.POST("", ApiController.AddBrand)
			permission.PUT("/:brandId", ApiController.UpdateBrand)
			permission.DELETE("/:brandId", ApiController.DeleteBrand)
		}
	}
}

func Categories(rg *gin.RouterGroup) {
	category := rg.Group("/category")
	{
		category.Use(md.RequiredAuth(ApiController.Config))

		category.GET("", ApiController.GetCategory)
		category.GET("/primary", ApiController.GetCategoryUsedPrimaryLang)
		category.GET("/multiple-lang/:id", ApiController.GetMultipleLangCategoryByID)
		category.GET("/grouped-lang", ApiController.GetCategoryGroupedLang)

		permission := category.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
		))
		{
			permission.POST("", ApiController.AddCategory)
			permission.PUT("", ApiController.UpdateCategoryBatch)
			permission.PUT("/extends", ApiController.UpdateCategoryExtendsBatch)
			permission.PUT("/single/:id", ApiController.UpdateCategory)
			permission.DELETE("/single/:id", ApiController.DeleteCategory)
			permission.DELETE("/multiple-lang/:id", ApiController.DeleteMultipleLangCategoryByID)
		}
	}
}

func Tag(rg *gin.RouterGroup) {
	tag := rg.Group("/tag")
	{
		tag.Use(md.RequiredAuth(ApiController.Config))
		permission := tag.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
		))
		{
			permission.GET("", ApiController.GetTag)
			permission.GET("/detail/:id", ApiController.GetTagByID)
			permission.GET("/grouped-lang", ApiController.GetTagGroupedLang)
			permission.POST("", ApiController.AddTag)
			permission.PUT("/:id", ApiController.UpdateTag)
			permission.DELETE("/:id", ApiController.DeleteTag)
			permission.DELETE("/:id/:lang", ApiController.DeleteTagByLang)
		}
	}
}

func Widget(rg *gin.RouterGroup) {
	widget := rg.Group("/widget")
	{
		widget.Use(md.RequiredAuth(ApiController.Config))
		permission := widget.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
		))
		{
			permission.GET("", ApiController.GetWidget)
			permission.GET("/:id", ApiController.GetWidgetByID)
			permission.POST("", ApiController.AddWidget)
			permission.PUT("/:id", ApiController.UpdateWidget)
			permission.DELETE("/:id", ApiController.DeleteWidget)
		}
	}
}

func Facility(rg *gin.RouterGroup) {
	facility := rg.Group("/facility")
	{
		facility.Use(md.RequiredAuth(ApiController.Config))

		facility.GET("", ApiController.GetFacility)
		facility.GET("/tree/grouped-lang", ApiController.GetFacilityTreeGroupedLang)
		facility.GET("/primary", ApiController.GetPrimaryFacility)
		facility.GET("/detail/:id", ApiController.GetFacilityByID)
		facility.GET("/grouped-lang", ApiController.GetFacilityGroupedLang)

		permission := facility.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
		))
		{
			permission.POST("", ApiController.AddFacility)
			permission.PUT("/:id", ApiController.UpdateFacility)
			permission.DELETE("/:id", ApiController.DeleteFacility)
			permission.DELETE("/:id/:lang", ApiController.DeleteFacilityByLang)
		}
	}
}

func Team(rg *gin.RouterGroup) {
	team := rg.Group("/team")
	{
		team.Use(md.RequiredAuth(ApiController.Config))

		permission := team.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
			ApiController.Config.Role.Brand,
			ApiController.Config.Role.Hotel,
		))
		{
			permission.GET("", ApiController.GetTeam)
			permission.GET("/:id", ApiController.GetTeamByID)
			permission.POST("", ApiController.AddTeam)
			permission.PUT("/:id", ApiController.UpdateTeam)
			permission.DELETE("/:id", ApiController.DeleteTeam)
		}
	}
}

func Banner(rg *gin.RouterGroup) {
	banner := rg.Group("/banner")
	{
		banner.GET("/assigned/:type", ApiController.GetAssignedBanner)
		banner.Use(md.RequiredAuth(ApiController.Config))

		permission := banner.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
			ApiController.Config.Role.Brand,
			ApiController.Config.Role.Hotel,
		))
		{
			permission.GET("", ApiController.GetBanner)
			permission.POST("", ApiController.AddBanner)
			permission.PUT("/update/:id", ApiController.UpdateBanner)
			permission.PUT("/assign/:type/:banner_id/:id", ApiController.AssignBanner)
			permission.DELETE("/delete/:id", ApiController.DeleteBanner)
			permission.DELETE("/assigned/:type/:banner_id/:id", ApiController.DeleteAssignedBanner)
		}
	}
}

func Media(rg *gin.RouterGroup) {
	media := rg.Group("/media")
	{
		media.Use(md.RequiredAuth(ApiController.Config))

		permission := media.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
			ApiController.Config.Role.Brand,
			ApiController.Config.Role.Hotel,
		))

		permission.GET("", ApiController.GetMedia)
		permission.POST("", ApiController.AddMedia)
		permission.PUT("/:mediaId", ApiController.UpdateMedia)
		permission.DELETE("/:mediaId", ApiController.DeleteMedia)
	}
}

func Pages(rg *gin.RouterGroup) {
	page := rg.Group("/page")
	{
		page.Use(md.RequiredAuth(ApiController.Config))

		permission := page.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
			ApiController.Config.Role.Brand,
			ApiController.Config.Role.Hotel,
		))
		{
			permission.GET("", ApiController.GetPages)
			permission.GET("/primary", ApiController.GetPageUsedPrimaryLang)
			permission.GET("/multiple-lang/:id", ApiController.GetMultipleLangPageByID)
			permission.GET("/grouped-lang", ApiController.GetPageGroupedLang)
			permission.GET("/menu", ApiController.GetPagesForMenu)
			permission.POST("", ApiController.AddPage)
			permission.POST("/duplicator", ApiController.PageDuplicator)
			permission.PUT("", ApiController.UpdatePageBatch)
			permission.PUT("/single/:id", ApiController.UpdatePage)
			permission.DELETE("/single/:id", ApiController.DeletePage)
			permission.DELETE("/multiple-lang/:id", ApiController.DeleteMultipleLangPageByID)
		}
	}
}

func GroupContent(rg *gin.RouterGroup) {
	gc := rg.Group("/group_content")
	{
		gc.Use(md.RequiredAuth(ApiController.Config))

		permission := gc.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
			ApiController.Config.Role.Brand,
			ApiController.Config.Role.Hotel,
		))
		{
			permission.GET("", ApiController.GetGroupContent)
			permission.POST("", ApiController.AddGroupContent)
			permission.PUT("/:id", ApiController.UpdateGroupContent)
			permission.PUT("/:id/setting", ApiController.UpdateGroupContentSetting)
			permission.DELETE("/:id", ApiController.DeleteGroupContent)
			permission.DELETE("/:id/:lang", ApiController.DeleteGroupContentByLang)
		}
	}
}

func Setting(rg *gin.RouterGroup) {
	setting := rg.Group("/setting")
	{
		setting.Use(md.RequiredAuth(ApiController.Config))
		setting.GET("", ApiController.GetSetting)

		permission := setting.Use(md.RequiredPermission(
			ApiController.Config.Role.Corporate,
			ApiController.Config.Role.Brand,
			ApiController.Config.Role.Hotel,
		))
		{
			permission.PUT("/:pageId", ApiController.SettingGeneral)
			permission.PUT("/:pageId/media", ApiController.SettingMedia)
			permission.PUT("/:pageId/socmed", ApiController.SettingSocialMedia)
			permission.PUT("/:pageId/script", ApiController.SettingScript)
			permission.PUT("/:pageId/meta", ApiController.SettingMeta)
		}
	}
}

func Menu(rg *gin.RouterGroup) {
	menu := rg.Group("/menu")
	{
		menu.Use(md.RequiredAuth(ApiController.Config))

		menu.GET("", ApiController.GetMenu)
		menu.GET("/:id", ApiController.GetMenuByID)
		menu.POST("", ApiController.AddMenu)
		menu.PUT("/:id", ApiController.UpdateMenu)
		menu.DELETE("/:id", ApiController.DeleteMenu)
	}
}

func Message(rg *gin.RouterGroup) {
	message := rg.Group("/message")
	{
		message.Use(md.RequiredAuth(ApiController.Config))

		message.GET("", ApiController.GetMessage)
		message.GET("/:id", ApiController.GetMessageByID)
		message.POST("", ApiController.AddMessage)
		message.PUT("/:id", ApiController.UpdateMessage)
		message.DELETE("/:id", ApiController.DeleteMessage)
	}
}

func PublicApi(rg *gin.RouterGroup) {
	public := rg.Group("/x")

	// lang route
	lang := public.Group("/lang")
	lang.GET("", ApiController.GetActiveLang)

	// city route
	city := public.Group("/city")
	city.GET("", ApiController.GetCityList)

	// brand route
	brand := public.Group("/brand")
	brand.GET("", ApiController.GetBrandList)

	// property route
	property := public.Group("/property")
	property.GET("", ApiController.GetPropertyList)
	property.GET("/code/:code", ApiController.GetPropertyByCode)
	property.GET("/slug/:slug", ApiController.GetPropertyBySlug)
	property.GET("/group/city", ApiController.GetPropertyGroupByCity)

	// setting route
	setting := public.Group("/setting")
	setting.GET("", ApiController.GetWebSetting)

	// menu route
	menu := public.Group("/menu")
	menu.GET("", ApiController.GetMenuByPropertyCode)

	// facility route
	facility := public.Group("/facility")
	facility.GET("", ApiController.GetFacilityList)

	// facility route
	amenities := public.Group("/amenities")
	amenities.GET("", ApiController.GetAmenitiesList)

	// banner route
	banner := public.Group("/banner")
	banner.GET("/get/:name", ApiController.GetBannerByName)
	banner.GET("/applied/:id", ApiController.GetBannerByAppliedID)

	// media route
	media := public.Group("/media")
	media.GET("/gallery", ApiController.GetMediaGallery)

	// category route
	category := public.Group("/category")
	category.GET("/id/:id", ApiController.GetCategoryByID)
	category.GET("/slug/:slug", ApiController.GetCategoryBySlug)

	// tag route
	tag := public.Group("/tag")
	tag.GET("", ApiController.GetTagList)

	// team route
	team := public.Group("/team")
	team.GET("", ApiController.GetTeamList)

	// page route
	page := public.Group("/page")
	page.GET("", ApiController.GetPageList)
	page.GET("/id/:id", ApiController.GetPageByID)
	page.GET("/slug/:slug", ApiController.GetPageBySlug)
	page.GET("/parent/id/:id", ApiController.GetPageByParentID)
	page.GET("/parent/slug/:slug", ApiController.GetPageByParentSlug)
	page.GET("/category/id/:id", ApiController.GetPageByCategoryID)
	page.GET("/category/slug/:slug", ApiController.GetPageByCategorySlug)

	// content route
	content := public.Group("/content")
	content.GET("/group/:name", ApiController.GetGroupContentByName)
}
