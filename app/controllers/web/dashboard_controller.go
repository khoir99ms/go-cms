package web

import (
	"alaric_cms/app/models"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

func (app *Web) Dashboard(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	var id = c.Param("Id")
	var level = c.Param("Level")
	levels := map[string]bool{
		"brand": true,
		"hotel": true,
	}
	isBrand := level == "brand"
	var pageId, pageCode, pageName, pageLevel string

	if id != "" && levels[level] {
		pageId = id
		api := fmt.Sprintf("%s/%s%s", app.Config.API.URL, "api/v1/property?id=", id)
		if isBrand {
			api = fmt.Sprintf("%s/%s%s", app.Config.API.URL, "api/v1/brands?id=", id)
		}

		req, err := http.NewRequest("GET", api, nil)
		if err != nil {
			log.Fatalln(err)
			return
		}
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

		client := &http.Client{Timeout: time.Second * 10}
		response, err := client.Do(req)
		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}
		defer response.Body.Close()

		body, _ := ioutil.ReadAll(response.Body)
		property := gjson.ParseBytes(body)

		if property.Get("success").Bool() {
			if isBrand {
				pageName = property.Get("data.item.brand_name").String()
			} else {
				pageCode = property.Get("data.item.property_code").String()
				pageName = property.Get("data.item.property_name").String()
			}
		}
		pageLevel = level
	} else {
		if userSession.UserRole == app.Config.Role.Brand {
			pageId = userSession.BrandId
			pageLevel = "brand"
		} else {
			pageId = userSession.PropertyId
			pageCode = userSession.PropertyCode
			pageLevel = "corporate"
			if userSession.UserRole == app.Config.Role.Hotel {
				pageLevel = "hotel"
			}
		}
		pageName = userSession.PageNameUserLogin
	}

	userSession.PageId = pageId
	userSession.PageCode = pageCode
	userSession.PageName = pageName
	userSession.PageLevel = pageLevel

	session.Values["user"] = userSession
	session.Save(c.Request, c.Writer)

	app.RenderHTML(c, http.StatusOK, "dashboard/index", gin.H{
		"title": "Main website",
	})
}
