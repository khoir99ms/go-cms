package web

import (
	"alaric_cms/app/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

func (app *Web) GetHotels(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	draw := c.Query("draw")
	search := c.Query("search[value]")
	sortDir := c.Query("order[0][dir]")
	sortColId := c.Query("order[0][column]")
	sortCol := c.Query(fmt.Sprintf("columns[%s][data]", sortColId))
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	limit, _ := strconv.ParseInt(c.DefaultQuery("length", "10"), 10, 64)
	if draw != "" {
		// request paging from datatable
		page = (page / limit) + 1
	} else {
		page = page + 1
	}

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/property/?onlyHotel=true")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	q.Add("search", search)
	q.Add("sortDir", sortDir)
	q.Add("sortCol", sortCol)
	q.Add("page", strconv.FormatInt(page, 10))
	q.Add("limit", strconv.FormatInt(limit, 10))
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) HotelAdd(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	hotelCode := c.PostForm("hotel_code")
	bookingUrl := c.PostForm("booking_url")
	corporateBookingCode := c.PostForm("corporate_booking_code")
	brandBookingCode := c.PostForm("brand_booking_code")
	hotelBookingCode := c.PostForm("hotel_booking_code")
	hotelName := c.PostForm("hotel_name")
	hotelDisplayName := c.PostForm("hotel_display_name")
	brandId := c.PostForm("brand_id")
	phone := c.PostForm("phone")
	fax := c.PostForm("fax")
	email := c.PostForm("email")
	address := c.PostForm("address")
	city := c.PostForm("city")
	tagline := c.PostFormMap("tagline")
	description := c.PostFormMap("description")
	customField := c.PostFormMap("custom_field")
	customLink := c.PostFormMap("custom_link")
	latitude := c.PostForm("latitude")
	longitude := c.PostForm("longitude")
	order := c.DefaultPostForm("order", "0")
	propertyOrder, _ := strconv.Atoi(order)
	status := c.PostForm("status")
	facilities := c.PostFormArray("facilities[]")
	logo := c.PostForm("logo")
	secondaryLogo := c.PostForm("secondary_logo")
	favicon := c.PostForm("favicon")
	featuredImage := c.PostForm("featured_image")
	attachmentImage := c.PostFormArray("attachment_image[]")

	var payload = map[string]interface{}{
		"property_code":                   hotelCode,
		"property_booking_url":            bookingUrl,
		"property_corporate_booking_code": corporateBookingCode,
		"property_brand_booking_code":     brandBookingCode,
		"property_hotel_booking_code":     hotelBookingCode,
		"property_name":                   hotelName,
		"property_display_name":           hotelDisplayName,
		"property_phone":                  phone,
		"property_fax":                    fax,
		"property_email":                  email,
		"property_address":                address,
		"property_logo":                   logo,
		"property_secondary_logo":         secondaryLogo,
		"property_favicon":                favicon,
		"property_featured_image":         featuredImage,
		"property_image_attachment":       attachmentImage,
		"property_tagline":                tagline,
		"property_description":            description,
		"property_custom_field":           customField,
		"property_custom_link":            customLink,
		"property_latitude":               latitude,
		"property_longitude":              longitude,
		"property_order":                  propertyOrder,
		"property_status":                 status,
		"facilities":                      facilities,
		"city_id":                         city,
	}
	if brandId != "" {
		payload["brand_id"] = brandId
	}
	var jsonPayload, _ = json.Marshal(payload)

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/property")
	var req, err = http.NewRequest("POST", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) HotelUpdate(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	hotelId := c.Param("id")
	var api = fmt.Sprintf("%s/%s%s", app.Config.API.URL, "api/v1/property?id=", hotelId)
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)
	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) HotelUpdateProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	hotelId := c.PostForm("hotel_id")
	hotelCode := c.PostForm("hotel_code")
	bookingUrl := c.PostForm("booking_url")
	corporateBookingCode := c.PostForm("corporate_booking_code")
	brandBookingCode := c.PostForm("brand_booking_code")
	hotelBookingCode := c.PostForm("hotel_booking_code")
	hotelName := c.PostForm("hotel_name")
	hotelDisplayName := c.PostForm("hotel_display_name")
	brandId := c.PostForm("brand_id")
	phone := c.PostForm("phone")
	fax := c.PostForm("fax")
	email := c.PostForm("email")
	address := c.PostForm("address")
	city := c.PostForm("city")
	tagline := c.PostFormMap("tagline")
	description := c.PostFormMap("description")
	customField := c.PostFormMap("custom_field")
	customLink := c.PostFormMap("custom_link")
	latitude := c.PostForm("latitude")
	longitude := c.PostForm("longitude")
	order := c.DefaultPostForm("order", "0")
	propertyOrder, _ := strconv.Atoi(order)
	status := c.PostForm("status")
	facilities := c.PostFormArray("facilities[]")
	logo := c.PostForm("logo")
	secondaryLogo := c.PostForm("secondary_logo")
	favicon := c.PostForm("favicon")
	featuredImage := c.PostForm("featured_image")
	attachmentImage := c.PostFormArray("attachment_image[]")

	var payload = map[string]interface{}{
		"property_code":                   hotelCode,
		"property_booking_url":            bookingUrl,
		"property_corporate_booking_code": corporateBookingCode,
		"property_brand_booking_code":     brandBookingCode,
		"property_hotel_booking_code":     hotelBookingCode,
		"property_name":                   hotelName,
		"property_display_name":           hotelDisplayName,
		"property_phone":                  phone,
		"property_fax":                    fax,
		"property_email":                  email,
		"property_address":                address,
		"property_tagline":                tagline,
		"property_logo":                   logo,
		"property_secondary_logo":         secondaryLogo,
		"property_favicon":                favicon,
		"property_featured_image":         featuredImage,
		"property_image_attachment":       attachmentImage,
		"property_description":            description,
		"property_custom_field":           customField,
		"property_custom_link":            customLink,
		"property_latitude":               latitude,
		"property_longitude":              longitude,
		"property_order":                  propertyOrder,
		"property_status":                 status,
		"city_id":                         city,
	}
	if len(facilities) > 0 {
		payload["facilities"] = facilities
	}
	if brandId != "" {
		payload["brand_id"] = brandId
	}
	var jsonPayload, _ = json.Marshal(payload)

	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/property", hotelId)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) HotelDelete(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	hotelId := c.Param("id")
	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/property", hotelId)
	var req, err = http.NewRequest("DELETE", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}
