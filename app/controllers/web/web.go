package web

import (
	"alaric_cms/app/models"
	conf "alaric_cms/config"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/foolin/goview/supports/ginview"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/sessions"
	"github.com/tidwall/gjson"
)

type Web struct {
	Gin     *gin.Engine
	Config  *conf.Config
	Session *sessions.CookieStore
}

// only use after login
func (web *Web) RenderHTML(c *gin.Context, status int, template string, data gin.H) {
	session, _ := web.Session.Get(c.Request, web.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	var propertyId string
	if userSession.PropertyCode != "COR" {
		propertyId = userSession.PropertyId
	}

	data["User"] = userSession
	data["Menu"] = web.GetMyNetworkMenu(userSession.AccessToken, userSession.BrandId, propertyId)
	data["Config"] = gin.H{
		"AppName": web.Config.AppName,
	}

	ginview.HTML(c, status, template, data)
}

func (web *Web) GetMyNetworkMenu(token string, brandID string, propertyID string) interface{} {
	var api = fmt.Sprintf("%s/%s", web.Config.API.URL, "api/v1/utils/network/menu")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	q := req.URL.Query()
	if brandID != "" {
		q.Add("brandId", brandID)
	}
	if propertyID != "" {
		q.Add("propertyId", propertyID)
	}
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	return data.Get("data").Value()
}
