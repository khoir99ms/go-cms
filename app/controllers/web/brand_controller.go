package web

import (
	"alaric_cms/app/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

func (app *Web) GetBrands(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	draw := c.Query("draw")
	search := c.Query("search[value]")
	sortDir := c.Query("order[0][dir]")
	sortColId := c.Query("order[0][column]")
	sortCol := c.Query(fmt.Sprintf("columns[%s][data]", sortColId))
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	limit, _ := strconv.ParseInt(c.DefaultQuery("length", "10"), 10, 64)
	if draw != "" {
		// request paging from datatable
		page = (page / limit) + 1
	} else {
		page = page + 1
	}

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/brands")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	q.Add("search", search)
	q.Add("sortDir", sortDir)
	q.Add("sortCol", sortCol)
	q.Add("page", strconv.FormatInt(page, 10))
	q.Add("limit", strconv.FormatInt(limit, 10))
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) BrandAdd(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	brandCode := c.PostForm("brand_code")
	brandName := c.PostForm("brand_name")
	brandDisplayName := c.PostForm("brand_display_name")
	tagline := c.PostFormMap("tagline")
	description := c.PostFormMap("description")
	content := c.PostFormMap("content")
	logo := c.PostForm("logo")
	secondaryLogo := c.PostForm("secondary_logo")
	favicon := c.PostForm("favicon")
	featuredImage := c.PostForm("featured_image")
	attachmentImage := c.PostFormArray("attachment_image[]")
	order, _ := strconv.Atoi(c.DefaultPostForm("order", "0"))

	var payload = map[string]interface{}{
		"brand_code":             brandCode,
		"brand_name":             brandName,
		"brand_display_name":     brandDisplayName,
		"brand_tagline":          tagline,
		"brand_description":      description,
		"brand_content":          content,
		"brand_logo":             logo,
		"brand_secondary_logo":   secondaryLogo,
		"brand_favicon":          favicon,
		"brand_featured_image":   featuredImage,
		"brand_image_attachment": attachmentImage,
		"brand_order":            order,
	}
	var jsonPayload, _ = json.Marshal(payload)

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/brands")
	var req, err = http.NewRequest("POST", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) BrandUpdate(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	brandId := c.Param("id")
	var api = fmt.Sprintf("%s/%s%s", app.Config.API.URL, "api/v1/brands?id=", brandId)
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) BrandUpdateProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	brandId := c.PostForm("brand_id")
	brandCode := c.PostForm("brand_code")
	brandName := c.PostForm("brand_name")
	brandDisplayName := c.PostForm("brand_display_name")
	tagline := c.PostFormMap("tagline")
	description := c.PostFormMap("description")
	content := c.PostFormMap("content")
	logo := c.PostForm("logo")
	secondaryLogo := c.PostForm("secondary_logo")
	favicon := c.PostForm("favicon")
	featuredImage := c.PostForm("featured_image")
	attachmentImage := c.PostFormArray("attachment_image[]")
	order, _ := strconv.Atoi(c.DefaultPostForm("order", "0"))

	var payload = map[string]interface{}{
		"brand_code":             brandCode,
		"brand_name":             brandName,
		"brand_display_name":     brandDisplayName,
		"brand_tagline":          tagline,
		"brand_description":      description,
		"brand_content":          content,
		"brand_logo":             logo,
		"brand_secondary_logo":   secondaryLogo,
		"brand_favicon":          favicon,
		"brand_featured_image":   featuredImage,
		"brand_image_attachment": attachmentImage,
		"brand_order":            order,
	}
	var jsonPayload, _ = json.Marshal(payload)

	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/brands", brandId)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) BrandDelete(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	brandId := c.Param("id")
	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/brands", brandId)
	var req, err = http.NewRequest("DELETE", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}
