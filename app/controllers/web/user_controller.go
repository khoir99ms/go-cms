package web

import (
	"alaric_cms/app/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/tidwall/gjson"

	"github.com/gin-gonic/gin"
)

func (app *Web) Users(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "user/index", gin.H{
		"title": "All User",
		"role":  app.Config.Role,
	})
}

func (app *Web) GetUsers(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	draw := c.Query("draw")
	search := c.Query("search[value]")
	sortDir := c.Query("order[0][dir]")
	sortColId := c.Query("order[0][column]")
	sortCol := c.Query(fmt.Sprintf("columns[%s][data]", sortColId))
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	limit, _ := strconv.ParseInt(c.DefaultQuery("length", "10"), 10, 64)
	if draw != "" {
		// request paging from datatable
		page = (page / limit) + 1
	} else {
		page = page + 1
	}

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/auth/users")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	q.Add("search", search)
	q.Add("sortDir", sortDir)
	q.Add("sortCol", sortCol)
	q.Add("page", strconv.FormatInt(page, 10))
	q.Add("limit", strconv.FormatInt(limit, 10))
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) UserAdd(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	firstname := c.PostForm("firstname")
	lastname := c.PostForm("lastname")
	email := c.PostForm("email")
	role, _ := strconv.Atoi(c.PostForm("role"))
	brandId := c.PostForm("brand_id")
	propertyId := c.PostForm("property_id")
	username := c.PostForm("username")
	password := c.PostForm("password")

	var payload = map[string]interface{}{
		"user_firstname": firstname,
		"user_lastname":  lastname,
		"user_email":     email,
		"user_role":      role,
		"user_status":    "active",
		"user_login":     username,
		"user_password":  password,
	}
	if brandId != "" {
		payload["brand_id"] = brandId
	}

	if propertyId != "" {
		payload["property_id"] = propertyId
	}
	var jsonPayload, _ = json.Marshal(payload)

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/auth/register")
	var req, err = http.NewRequest("POST", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) UserUpdate(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	userId := c.Param("id")
	var api = fmt.Sprintf("%s/%s%s", app.Config.API.URL, "api/v1/auth/users?id=", userId)
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) UserUpdateProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	userId := c.PostForm("user_id")
	firstname := c.PostForm("firstname")
	lastname := c.PostForm("lastname")
	username := c.PostForm("username")
	email := c.PostForm("email")
	role, _ := strconv.Atoi(c.PostForm("role"))
	status := c.PostForm("status")
	brandId := c.PostForm("brand_id")
	propertyId := c.PostForm("property_id")

	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/auth/change", userId)

	var payload = map[string]interface{}{
		"user_firstname": firstname,
		"user_lastname":  lastname,
		"user_login":     username,
		"user_email":     email,
		"user_role":      role,
		"user_status":    status,
	}

	if brandId != "" {
		payload["brand_id"] = brandId
	}

	if propertyId != "" {
		payload["property_id"] = propertyId
	}

	var jsonPayload, _ = json.Marshal(payload)

	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) UserUpdatePassword(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	userId := c.PostForm("user_id")
	current_password := c.PostForm("current_password")
	password := c.PostForm("password")
	retype_password := c.PostForm("retype_password")

	var api = fmt.Sprintf("%s/%s/%s/password", app.Config.API.URL, "api/v1/auth/change", userId)
	var payload = map[string]interface{}{
		"current_password": current_password,
		"password":         password,
		"retype_password":  retype_password,
	}
	var jsonPayload, _ = json.Marshal(payload)

	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) UserDelete(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	userId := c.Param("id")
	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/auth/delete", userId)

	var req, err = http.NewRequest("DELETE", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}
