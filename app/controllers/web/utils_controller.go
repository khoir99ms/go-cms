package web

import (
	"alaric_cms/app/helpers"
	"alaric_cms/app/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"

	"github.com/gin-contrib/location"
	uuid "github.com/satori/go.uuid"
	"github.com/tidwall/gjson"

	"github.com/gin-gonic/gin"
)

func (app *Web) GetCityList(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	search := c.Query("q")
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	page = page + 1
	limit := c.DefaultQuery("length", "10")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/utils/city")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	q.Add("search", search)
	q.Add("page", string(page))
	q.Add("limit", limit)
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetBrandList(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	search := c.Query("q")
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	page = page + 1
	limit := c.DefaultQuery("length", "10")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/brands")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	q.Add("search", search)
	q.Add("page", string(page))
	q.Add("limit", limit)
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetBrandHasPropertyList(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	search := c.Query("q")
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	page = page + 1
	limit := c.DefaultQuery("length", "10")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/brands/property")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	q.Add("search", search)
	q.Add("page", string(page))
	q.Add("limit", limit)
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetPropertyList(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	search := c.Query("q")
	brandId := c.Query("brandId")
	onlyHotel := c.Query("onlyHotel")
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	page = page + 1
	limit := c.DefaultQuery("length", "10")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/property/")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if brandId != "" {
		q.Add("brandId", brandId)
	}
	if onlyHotel != "" {
		q.Add("onlyHotel", onlyHotel)
	}
	q.Add("search", search)
	q.Add("page", string(page))
	q.Add("limit", limit)
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetPropertyByPageSession(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	search := c.Query("q")
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	page = page + 1
	limit := c.DefaultQuery("length", "10")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/property/")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if userSession.PageLevel == "brand" {
		q.Add("brandId", userSession.PageId)
	} else {
		if userSession.PageCode != "COR" {
			q.Add("code", userSession.PageCode)
		}
	}
	q.Add("search", search)
	q.Add("page", string(page))
	q.Add("limit", limit)
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetLanguageList(c *gin.Context) {
	// Open our jsonFile
	jsonLang, err := os.Open("./config/languages.json")
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonLang.Close()

	// read our opened json as a byte array.
	jsonByte, _ := ioutil.ReadAll(jsonLang)

	var lang bson.M
	json.Unmarshal(jsonByte, &lang)

	c.JSON(http.StatusOK, lang)
}

func (app *Web) GetCategoryList(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	search := c.Query("q")
	lang := c.Query("lang")
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	page = page + 1
	limit := c.DefaultQuery("length", "20")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/category")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if search != "" {
		q.Add("search", search)
	}
	if lang != "" {
		q.Add("lang", lang)
	}
	q.Add("page", string(page))
	q.Add("limit", limit)
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) PluginFileUploader(c *gin.Context) {
	image, _ := c.FormFile("image")
	file, _ := c.FormFile("file")
	video, _ := c.FormFile("video")

	var dst string
	var source *multipart.FileHeader
	if image != nil {
		source = image
		dst = app.Config.Path.Media
	}

	if file != nil {
		source = file
		dst = fmt.Sprintf("%s/%s", app.Config.Path.Static, "/uploads/files")
	}

	if video != nil {
		source = video
		dst = fmt.Sprintf("%s/%s", app.Config.Path.Static, "/uploads/files")
	}

	ext := filepath.Ext(source.Filename)
	uid := uuid.Must(uuid.NewV4())
	filename := fmt.Sprintf("%s%s", uid, ext)
	dst = fmt.Sprintf("%s/%s", dst, filename)

	if err := c.SaveUploadedFile(source, dst); err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	var response = make(map[string]interface{})
	if image != nil {
		if app.Config.Environment == "production" {
			response["link"] = fmt.Sprintf("%s/%s", app.Config.Path.CloudImagePrefix, filename)
		} else {
			response["link"] = fmt.Sprintf("%s%s%s/%s", host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, filename)
		}
	}

	if file != nil || video != nil {
		response["link"] = fmt.Sprintf("%s%s%s/%s", host, app.Config.Path.StaticAs, "/uploads/files", filename)
	}

	c.JSON(200, response)
}

func (app *Web) PluginFileDelete(c *gin.Context) {
	image := c.PostForm("image")
	file := c.PostForm("file")
	video := c.PostForm("video")

	var dst string
	var uriParts []string
	if image != "" {
		uriParts = strings.Split(image, "/")
		dst = app.Config.Path.Media
	}

	if file != "" {
		uriParts = strings.Split(file, "/")
		dst = fmt.Sprintf("%s/%s", app.Config.Path.Static, "/uploads/files")
	}

	if video != "" {
		uriParts = strings.Split(video, "/")
		dst = fmt.Sprintf("%s/%s", app.Config.Path.Static, "/uploads/files")
	}

	response := models.ReS{}
	if len(uriParts) > 0 {
		filename := uriParts[len(uriParts)-1]
		path := fmt.Sprintf("%s/%s", dst, filename)
		if exist := helpers.FileExists(path); exist {
			os.Remove(path)
			response.Success = true
			response.Message = "File was deleted."
		} else {
			response.Message = "Video delete problem"
		}
	}

	c.JSON(200, response)
}
