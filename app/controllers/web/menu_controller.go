package web

import (
	"alaric_cms/app/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

func (app *Web) GetMenu(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/menu")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if userSession.PageLevel == "brand" {
		q.Add("brandId", userSession.PageId)
	} else {
		q.Add("propertyId", userSession.PageId)
	}
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetMenuByID(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	var menuID = c.Param("id")

	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/menu", menuID)
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) MenuAdd(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	name := c.PostForm("name")
	lang := c.PostForm("lang")
	location := c.PostForm("location")

	var payload = map[string]interface{}{
		"name":     name,
		"lang":     lang,
		"location": location,
	}

	if userSession.PageLevel == "brand" {
		payload["brand_id"] = userSession.PageId
	} else {
		payload["property_id"] = userSession.PageId
	}

	var jsonPayload, _ = json.Marshal(payload)

	var api = fmt.Sprintf("%s/api/v1/menu", app.Config.API.URL)
	var req, err = http.NewRequest("POST", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) MenuUpdate(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	menuId := c.Param("id")
	var payload models.MenuForm
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var api = fmt.Sprintf("%s/api/v1/menu/%s", app.Config.API.URL, menuId)
	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) MenuDelete(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	menuId := c.Param("id")
	var api = fmt.Sprintf("%s/api/v1/menu/%s", app.Config.API.URL, menuId)
	var req, err = http.NewRequest("DELETE", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}
