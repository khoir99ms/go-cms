package web

import (
	"alaric_cms/app/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

func (app *Web) Setting(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	var api = fmt.Sprintf("%s/%s%s", app.Config.API.URL, "api/v1/setting?id=", userSession.PageId)
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)
	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	tbMedia := data.Get("data.media.thumbnail_size").String()
	mdMedia := data.Get("data.media.medium_size").String()
	lgMedia := data.Get("data.media.large_size").String()

	var tbWidth, tbHeight, mdWidth, mdHeight, lgWidth, lgHeight string
	tbSize := strings.Split(tbMedia, "x")
	if len(tbSize) > 1 {
		tbWidth = tbSize[0]
		tbHeight = tbSize[1]
	}
	mdSize := strings.Split(mdMedia, "x")
	if len(mdSize) > 1 {
		mdWidth = mdSize[0]
		mdHeight = mdSize[1]
	}
	lgSize := strings.Split(lgMedia, "x")
	if len(lgSize) > 1 {
		lgWidth = lgSize[0]
		lgHeight = lgSize[1]
	}

	var mediaSetting = map[string]interface{}{
		"thumbnail_size": map[string]string{
			"width":  tbWidth,
			"height": tbHeight,
		},
		"medium_size": map[string]string{
			"width":  mdWidth,
			"height": mdHeight,
		},
		"large_size": map[string]string{
			"width":  lgWidth,
			"height": lgHeight,
		},
	}

	var meta, _ = data.Get("data.meta").Value().(map[string]interface{})
	var setting = map[string]interface{}{
		"general": map[string]string{
			"site_title":     data.Get("data.general.site_title").String(),
			"site_copyright": data.Get("data.general.site_copyright").String(),
			"site_lang":      data.Get("data.general.site_lang").String(),
			"posts_per_page": data.Get("data.general.posts_per_page").String(),
		},
		"media": mediaSetting,
		"socmed": map[string]string{
			"email":       data.Get("data.social_media.email").String(),
			"facebook":    data.Get("data.social_media.facebook").String(),
			"twitter":     data.Get("data.social_media.twitter").String(),
			"instagram":   data.Get("data.social_media.instagram").String(),
			"youtube":     data.Get("data.social_media.youtube").String(),
			"tripadvisor": data.Get("data.social_media.tripadvisor").String(),
		},
		"meta": meta,
		"script": map[string]string{
			"header":           data.Get("data.script.header").String(),
			"body":             data.Get("data.script.body").String(),
			"booking_calendar": data.Get("data.script.booking_calendar").String(),
			"themes":           data.Get("data.script.themes").String(),
		},
	}

	app.RenderHTML(c, http.StatusOK, "setting/index", gin.H{
		"title":   "Setting",
		"setting": setting,
	})
}

func (app *Web) SettingMenu(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setting/menu", gin.H{
		"title": "Menu Setting",
	})
}

func (app *Web) SettingMessage(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setting/message", gin.H{
		"title": "Message Manager",
	})
}

func (app *Web) SettingGeneralProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	siteTitle := c.PostForm("site_title")
	siteCopyright := c.PostForm("site_copyright")
	siteLang := c.PostForm("language")
	postsPerPage := c.PostForm("posts_per_page")

	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/setting", userSession.PageId)
	var payload = map[string]interface{}{
		"site_title":     siteTitle,
		"site_copyright": siteCopyright,
		"site_lang":      siteLang,
		"posts_per_page": postsPerPage,
	}

	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) SettingMediaProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	// thumbnail
	tbWidth := c.PostForm("tbwidth")
	tbHeight := c.PostForm("tbheight")

	// thumbnail
	mdWidth := c.PostForm("mdwidth")
	mdHeight := c.PostForm("mdheight")

	// thumbnail
	lgWidth := c.PostForm("lgwidth")
	lgHeight := c.PostForm("lgheight")

	var api = fmt.Sprintf("%s/%s/%s/media", app.Config.API.URL, "api/v1/setting", userSession.PageId)
	var payload = map[string]interface{}{
		"thumbnail_size": fmt.Sprintf("%sx%s", tbWidth, tbHeight),
		"medium_size":    fmt.Sprintf("%sx%s", mdWidth, mdHeight),
		"large_size":     fmt.Sprintf("%sx%s", lgWidth, lgHeight),
	}

	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) SettingSocmedProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	email := c.PostForm("email")
	facebook := c.PostForm("facebook")
	twitter := c.PostForm("twitter")
	instagram := c.PostForm("instagram")
	youtube := c.PostForm("youtube")
	tripadvisor := c.PostForm("tripadvisor")

	var api = fmt.Sprintf("%s/%s/%s/socmed", app.Config.API.URL, "api/v1/setting", userSession.PageId)
	var payload = map[string]interface{}{
		"email":       email,
		"facebook":    facebook,
		"twitter":     twitter,
		"instagram":   instagram,
		"youtube":     youtube,
		"tripadvisor": tripadvisor,
	}

	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) SettingMetaProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	metaTitle := c.PostFormMap("meta_title")
	metaDescription := c.PostFormMap("meta_description")
	metaKeyword := c.PostFormMap("meta_keyword")

	var api = fmt.Sprintf("%s/api/v1/setting/%s/meta", app.Config.API.URL, userSession.PageId)
	var payload = map[string]interface{}{
		"title":       metaTitle,
		"description": metaDescription,
		"keyword":     metaKeyword,
	}

	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) SettingScriptProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	headerScript := c.PostForm("header")
	bodyScript := c.PostForm("body")
	bookingCalendarScript := c.PostForm("booking_calendar")
	themesScript := c.PostForm("themes")

	var api = fmt.Sprintf("%s/api/v1/setting/%s/script", app.Config.API.URL, userSession.PageId)
	var payload = map[string]interface{}{
		"header":           headerScript,
		"body":             bodyScript,
		"booking_calendar": bookingCalendarScript,
		"themes":           themesScript,
	}

	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}
