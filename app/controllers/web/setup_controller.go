package web

import (
	"alaric_cms/app/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

func (app *Web) SetupCorporate(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setup/corporate", gin.H{
		"title": "Setup Corporate",
	})
}

func (app *Web) GetCorporateData(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/property?code=COR")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) SetupCorporateProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	corporateId := c.PostForm("corporate_id")
	corporateCode := "COR"
	corporateName := c.PostForm("corporate_name")
	corporateDisplayName := c.PostForm("corporate_display_name")
	phone := c.PostForm("phone")
	fax := c.PostForm("fax")
	email := c.PostForm("email")
	address := c.PostForm("address")
	city := c.PostForm("city")
	tagline := c.PostFormMap("tagline")
	description := c.PostFormMap("description")
	latitude := c.PostForm("latitude")
	longitude := c.PostForm("longitude")
	logo := c.PostForm("logo")
	secondaryLogo := c.PostForm("secondary_logo")
	favicon := c.PostForm("favicon")
	featuredImage := c.PostForm("featured_image")
	bookingUrl := c.PostForm("booking_url")
	attachmentImage := c.PostFormArray("attachment_image[]")

	var payload = map[string]interface{}{
		"property_code":             corporateCode,
		"property_name":             corporateName,
		"property_display_name":     corporateDisplayName,
		"property_phone":            phone,
		"property_fax":              fax,
		"property_email":            email,
		"property_address":          address,
		"property_tagline":          tagline,
		"property_logo":             logo,
		"property_secondary_logo":   secondaryLogo,
		"property_favicon":          favicon,
		"property_featured_image":   featuredImage,
		"property_image_attachment": attachmentImage,
		"property_description":      description,
		"property_latitude":         latitude,
		"property_longitude":        longitude,
		"property_booking_url":      bookingUrl,
		"city_id":                   city,
	}
	var jsonPayload, _ = json.Marshal(payload)

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/property")
	var httpMethod = "POST"
	if corporateId != "" {
		httpMethod = "PUT"
		api = fmt.Sprintf("%s/%s", api, corporateId)
	}
	var req, err = http.NewRequest(httpMethod, api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) SetupBrands(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setup/brands", gin.H{
		"title": "Setup Brand",
	})
}

func (app *Web) SetupHotels(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setup/hotels", gin.H{
		"title": "Setup Hotel",
	})
}

func (app *Web) SetupCategory(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setup/category", gin.H{
		"title": "Setup Categories",
	})
}

func (app *Web) SetupPageDuplicator(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setup/page-duplicator", gin.H{
		"title": "Page Duplicator",
	})
}

func (app *Web) SetupTag(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setup/tag", gin.H{
		"title": "Setup Tag",
	})
}

func (app *Web) SetupWidget(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setup/widget", gin.H{
		"title": "Setup Widget",
	})
}

func (app *Web) SetupFacilities(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setup/facilities", gin.H{
		"title": "Setup Master Facilities",
	})
}

func (app *Web) SetupCity(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setup/cities", gin.H{
		"title": "Setup Cities",
	})
}

func (app *Web) SetupLang(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "setup/language", gin.H{
		"title": "Setup Language",
	})
}
