package web

import (
	"alaric_cms/app/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

func (app *Web) GroupContent(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "group_content/index", gin.H{
		"title": "Group Content",
	})
}

func (app *Web) GroupContentGet(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	search := c.Query("q")
	groupId := c.Query("groupId")
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "1"), 10, 64)
	limit, _ := strconv.ParseInt(c.DefaultQuery("length", "100"), 10, 64)

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/group_content")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if userSession.PageLevel == "brand" {
		q.Add("brandId", userSession.PageId)
	} else {
		q.Add("propertyId", userSession.PageId)
	}
	if search != "" {
		q.Add("search", search)
	}
	if groupId != "" {
		q.Add("id", groupId)
	}
	q.Add("page", strconv.FormatInt(page, 10))
	q.Add("limit", strconv.FormatInt(limit, 10))
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetGroupContentForMenu(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	search := c.Query("q")
	sortDir := c.Query("sortDir")
	sortCol := c.Query("sortCol")
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "1"), 10, 64)
	limit := c.DefaultQuery("length", "10")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/group_content")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if search != "" {
		q.Add("search", search)
	}
	if sortDir != "" {
		q.Add("sortDir", sortDir)
	}
	if sortCol != "" {
		q.Add("sortCol", sortCol)
	}
	if userSession.PageLevel == "brand" {
		q.Add("brandId", userSession.PageId)
	} else {
		q.Add("propertyId", userSession.PageId)
	}

	q.Add("page", strconv.FormatInt(page, 10))
	q.Add("limit", limit)
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GroupContentAdd(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	name := c.PostForm("name")
	// description := c.PostForm("description")

	var brandId, propertyId string
	if userSession.PageLevel == "brand" {
		brandId = userSession.PageId
	} else {
		propertyId = userSession.PageId
	}

	var api = fmt.Sprintf("%s/api/v1/group_content", app.Config.API.URL)
	var payload = map[string]interface{}{
		"name": name,
		// "description": description,
		"brand_id":    brandId,
		"property_id": propertyId,
	}

	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("POST", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) GroupContentUpdate(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	groupID := c.Param("id")
	var payload map[string]interface{}
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	if userSession.PageLevel == "brand" {
		payload["brand_id"] = userSession.PageId
	} else {
		payload["property_id"] = userSession.PageId
	}

	var api = fmt.Sprintf("%s/api/v1/group_content/%s", app.Config.API.URL, groupID)
	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) GroupContentSettingUpdate(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	groupID := c.Param("id")
	var payload []map[string]interface{}
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var api = fmt.Sprintf("%s/api/v1/group_content/%s/setting", app.Config.API.URL, groupID)
	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) GroupContentDelete(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	groupId := c.Param("id")
	var api = fmt.Sprintf("%s/api/v1/group_content/%s", app.Config.API.URL, groupId)
	var req, err = http.NewRequest("DELETE", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) GroupContentDeleteByLang(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	groupID := c.Param("id")
	lang := c.Param("lang")

	var api = fmt.Sprintf("%s/api/v1/group_content/%s/%s", app.Config.API.URL, groupID, lang)
	var req, err = http.NewRequest("DELETE", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}
