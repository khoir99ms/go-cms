package web

import (
	"alaric_cms/app/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

func (app *Web) Category(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "category/index", gin.H{
		"title": "Categories",
	})
}

func (app *Web) GetCategory(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	draw := c.Query("draw")
	lang := c.Query("lang")
	search := c.Query("search[value]")
	sortDir := c.Query("order[0][dir]")
	sortColId := c.Query("order[0][column]")
	sortCol := c.Query(fmt.Sprintf("columns[%s][data]", sortColId))
	start := c.Query("start")
	limit := c.Query("limit")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/category")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if userSession.PageLevel == "brand" {
		q.Add("brandId", userSession.PageId)
	} else {
		q.Add("propertyId", userSession.PageId)
	}
	if lang != "" {
		q.Add("lang", lang)
	}
	if search != "" {
		q.Add("search", search)
	}
	if sortDir != "" {
		q.Add("sortDir", sortDir)
	}
	if sortCol != "" {
		q.Add("sortCol", sortCol)
	}

	if start != "" || limit != "" {
		if start == "" {
			start = "1"
		}
		if limit == "" {
			limit = "10"
		}
		page, _ := strconv.ParseInt(start, 10, 64)
		pageLimit, _ := strconv.ParseInt(limit, 10, 64)

		if draw != "" {
			// request paging from datatable
			page = (page / pageLimit) + 1
		}

		q.Add("page", strconv.FormatInt(page, 10))
		q.Add("limit", strconv.FormatInt(pageLimit, 10))
	}
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetCategoryUsedPrimaryLang(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	categoryGet := c.Query("get")
	draw := c.Query("draw")
	search := c.Query("search[value]")
	sortDir := c.Query("order[0][dir]")
	sortColId := c.Query("order[0][column]")
	sortCol := c.Query(fmt.Sprintf("columns[%s][data]", sortColId))
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	limit, _ := strconv.ParseInt(c.DefaultQuery("length", "10"), 10, 64)
	if draw != "" {
		// request paging from datatable
		page = (page / limit) + 1
	}

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/category/primary")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if categoryGet == "extends" {
		if userSession.PageLevel == "brand" {
			q.Add("brandId", userSession.PageId)
		} else {
			q.Add("propertyId", userSession.PageId)
		}
	}
	q.Add("search", search)

	if sortDir != "" {
		q.Add("sortDir", sortDir)
	}
	if sortCol != "" {
		q.Add("sortCol", sortCol)
	}

	q.Add("page", strconv.FormatInt(page, 10))
	q.Add("limit", strconv.FormatInt(limit, 10))
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetMultipleLangCategoryByID(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	categoryID := c.Param("id")
	categoryGet := c.Query("get")

	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/category/multiple-lang", categoryID)
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if categoryGet == "extends" {
		if userSession.PageLevel == "brand" {
			q.Add("brandId", userSession.PageId)
		} else {
			q.Add("propertyId", userSession.PageId)
		}
	}
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetCategoryGroupedLang(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/category/grouped-lang")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetCategoryForMenu(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	search := c.Query("q")
	lang := c.Query("lang")
	sortDir := c.Query("sortDir")
	sortCol := c.Query("sortCol")
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "1"), 10, 64)
	limit := c.DefaultQuery("length", "10")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/category")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if search != "" {
		q.Add("search", search)
	}
	if lang != "" {
		q.Add("lang", lang)
	}
	if sortDir != "" {
		q.Add("sortDir", sortDir)
	}
	if sortCol != "" {
		q.Add("sortCol", sortCol)
	}
	q.Add("page", strconv.FormatInt(page, 10))
	q.Add("limit", limit)
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) CategoryAdd(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	name := c.PostForm("name")
	title := c.PostForm("title")
	description := c.PostForm("description")
	image := c.PostForm("image")
	lang := c.PostForm("lang")
	parentID := c.PostForm("parent_id")
	useValidity := c.PostForm("use_validity")
	useAmenities := c.PostForm("use_amenities")
	useBadge := c.PostForm("use_badge")
	labelDetail := c.PostForm("label_detail")
	labelMore := c.PostForm("label_more")
	metaTitle := c.PostForm("meta_title")
	metaKeyword := c.PostForm("meta_keyword")
	metaDescription := c.PostForm("meta_description")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/category")
	var payload = map[string]interface{}{
		"name":             name,
		"title":            title,
		"description":      description,
		"image":            image,
		"lang":             lang,
		"parent_id":        parentID,
		"use_validity":     useValidity,
		"use_amenities":    useAmenities,
		"use_badge":        useBadge,
		"label_detail":     labelDetail,
		"label_more":       labelMore,
		"meta_title":       metaTitle,
		"meta_keyword":     metaKeyword,
		"meta_description": metaDescription,
	}

	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("POST", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) CategoryUpdate(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	categoryId := c.Param("id")
	var api = fmt.Sprintf("%s/%s%s", app.Config.API.URL, "api/v1/category?id=", categoryId)
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) CategoryUpdateProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	var payload []map[string]interface{}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/category")
	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) CategoryExtendsUpdateProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	var payload []map[string]interface{}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	for i := range payload {
		if userSession.PageLevel == "brand" {
			payload[i]["brand_id"] = userSession.PageId
		} else {
			payload[i]["property_id"] = userSession.PageId
		}
	}

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/category/extends")
	var jsonPayload, _ = json.Marshal(payload)
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) CategoryDelete(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	categoryId := c.Param("id")
	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/category/single", categoryId)
	var req, err = http.NewRequest("DELETE", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) CategoryDeleteMultipleLangByID(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	categoryId := c.Param("id")
	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/category/multiple-lang", categoryId)
	var req, err = http.NewRequest("DELETE", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}
