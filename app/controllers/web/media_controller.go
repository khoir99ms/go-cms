package web

import (
	"alaric_cms/app/models"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"path/filepath"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

func (app *Web) Media(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "media/index", gin.H{
		"title": "Media",
	})
}

func (app *Web) GetMedia(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	mediaType := c.Query("type")
	asGallery := c.Query("as_gallery")
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	limit, _ := strconv.ParseInt(c.DefaultQuery("length", "10"), 10, 64)

	draw := c.Query("draw")
	var search, sortDir, sortCol string
	// request from datatable
	if draw != "" {
		search = c.Query("search[value]")
		sortDir = c.Query("order[0][dir]")
		sortColId := c.Query("order[0][column]")
		sortCol = c.Query(fmt.Sprintf("columns[%s][data]", sortColId))

		page = (page / limit) + 1
	} else {
		search = c.Query("search")
		sortCol = c.Query("sortCol")
		sortDir = c.Query("sortDir")

		page = page + 1
	}

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/media")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if userSession.PageLevel == "brand" {
		q.Add("brandId", userSession.PageId)
	} else {
		q.Add("propertyId", userSession.PageId)
	}

	q.Add("search", search)
	q.Add("type", mediaType)
	q.Add("as_gallery", asGallery)
	if sortDir != "" {
		q.Add("sortDir", sortDir)
	}
	if sortCol != "" {
		q.Add("sortCol", sortCol)
	}
	q.Add("page", strconv.FormatInt(page, 10))
	q.Add("limit", strconv.FormatInt(limit, 10))
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) MediaAdd(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	rows, _ := strconv.Atoi(c.PostForm("rows"))
	titles := c.PostFormArray("title")
	captions := c.PostFormArray("caption")
	descriptions := c.PostFormArray("description")
	mTypes := c.PostFormArray("type")
	urls := c.PostFormArray("url")
	asGalleries := c.PostFormArray("as_gallery")
	form, _ := c.MultipartForm()
	files := form.File["file"]

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/media")
	var payload = &bytes.Buffer{}
	var writer = multipart.NewWriter(payload)

	writer.WriteField("rows", c.PostForm("rows"))
	for i := 0; i < rows; i++ {
		var title, caption, description, mType, url, asGallery string
		if len(titles) > i {
			title = titles[i]
		}
		if len(captions) > i {
			caption = captions[i]
		}
		if len(descriptions) > i {
			description = descriptions[i]
		}
		if len(mTypes) > i {
			mType = mTypes[i]
		}
		if len(urls) > i {
			url = urls[i]
		}
		if len(asGalleries) > i {
			asGallery = asGalleries[i]
		}

		writer.WriteField("title", title)
		writer.WriteField("caption", caption)
		writer.WriteField("description", description)
		writer.WriteField("type", mType)
		writer.WriteField("url", url)
		writer.WriteField("as_gallery", asGallery)

		if userSession.PageLevel == "brand" {
			writer.WriteField("brand_id", userSession.PageId)
		} else {
			writer.WriteField("property_id", userSession.PageId)
		}

		if len(files) > i {
			file := files[i]
			if file != nil {
				src, _ := file.Open()
				part, _ := writer.CreateFormFile("file", filepath.Base(file.Filename))
				io.Copy(part, src)

				defer src.Close()
			}
		}
	}
	writer.Close()

	var req, err = http.NewRequest("POST", api, payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) MediaUpdate(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	mediaId := c.Param("id")
	var api = fmt.Sprintf("%s/%s%s", app.Config.API.URL, "api/v1/media?id=", mediaId)
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) MediaUpdateProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	mediaId := c.PostForm("media_id")
	title := c.PostForm("title")
	caption := c.PostForm("caption")
	description := c.PostForm("description")
	mType := c.PostForm("type")
	url := c.PostForm("url")
	asGallery := c.PostForm("as_gallery")

	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/media", mediaId)
	var payload = &bytes.Buffer{}
	var writer = multipart.NewWriter(payload)

	writer.WriteField("title", title)
	writer.WriteField("caption", caption)
	writer.WriteField("description", description)
	writer.WriteField("type", mType)
	writer.WriteField("url", url)
	writer.WriteField("as_gallery", asGallery)

	if userSession.PageLevel == "brand" {
		writer.WriteField("brandId", userSession.PageId)
	} else {
		writer.WriteField("propertyId", userSession.PageId)
	}

	file, _ := c.FormFile("file")
	if file != nil {
		src, _ := file.Open()
		part, _ := writer.CreateFormFile("file", filepath.Base(file.Filename))
		io.Copy(part, src)

		defer src.Close()
	}
	writer.Close()

	var req, err = http.NewRequest("PUT", api, payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) MediaDelete(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	mediaId := c.Param("id")
	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/media", mediaId)
	var req, err = http.NewRequest("DELETE", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}
