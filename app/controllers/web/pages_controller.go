package web

import (
	"alaric_cms/app/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
)

func (app *Web) Pages(c *gin.Context) {
	app.RenderHTML(c, http.StatusOK, "pages/index", gin.H{
		"title": "Pages",
	})
}

func (app *Web) GetPages(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	draw := c.Query("draw")
	lang := c.Query("lang")
	brandID := c.Query("brand_id")
	propertyID := c.Query("property_id")
	categoryID := c.Query("category_id")
	search := c.Query("search[value]")
	sortDir := c.Query("order[0][dir]")
	sortColId := c.Query("order[0][column]")
	sortCol := c.Query(fmt.Sprintf("columns[%s][data]", sortColId))
	start := c.Query("start")
	limit := c.Query("limit")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/page")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if brandID != "" || propertyID != "" {
		if brandID != "" {
			q.Add("brandId", brandID)
		} else if propertyID != "" {
			q.Add("propertyId", propertyID)
		}
	} else {
		if userSession.PageLevel == "brand" {
			q.Add("brandId", userSession.PageId)
		} else {
			q.Add("propertyId", userSession.PageId)
		}
	}
	if lang != "" {
		q.Add("lang", lang)
	}
	if categoryID != "" {
		q.Add("categoryId", categoryID)
	}
	if search != "" {
		q.Add("search", search)
	}
	if sortDir != "" {
		q.Add("sortDir", sortDir)
	}
	if sortCol != "" {
		q.Add("sortCol", sortCol)
	}

	if start != "" || limit != "" {
		if start == "" {
			start = "1"
		}
		if limit == "" {
			limit = "10"
		}
		page, _ := strconv.ParseInt(start, 10, 64)
		pageLimit, _ := strconv.ParseInt(limit, 10, 64)

		if draw != "" {
			// request paging from datatable
			page = (page / pageLimit) + 1
		}

		q.Add("page", strconv.FormatInt(page, 10))
		q.Add("limit", strconv.FormatInt(pageLimit, 10))
	}
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetPageUsedPrimaryLang(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	brandID := c.Query("brand_id")
	propertyID := c.Query("property_id")
	categoryID := c.Query("category_id")
	draw := c.Query("draw")
	search := c.Query("search[value]")
	sortDir := c.Query("order[0][dir]")
	sortColId := c.Query("order[0][column]")
	sortCol := c.Query(fmt.Sprintf("columns[%s][data]", sortColId))
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "0"), 10, 64)
	limit, _ := strconv.ParseInt(c.DefaultQuery("length", "10"), 10, 64)
	if draw != "" {
		// request paging from datatable
		page = (page / limit) + 1
	}

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/page/primary")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if brandID != "" || propertyID != "" {
		if brandID != "" {
			q.Add("brandId", brandID)
		} else if propertyID != "" {
			q.Add("propertyId", propertyID)
		}
	} else {
		if userSession.PageLevel == "brand" {
			q.Add("brandId", userSession.PageId)
		} else {
			q.Add("propertyId", userSession.PageId)
		}
	}

	q.Add("categoryId", categoryID)
	q.Add("search", search)

	if sortDir != "" {
		q.Add("sortDir", sortDir)
	}
	if sortCol != "" {
		q.Add("sortCol", sortCol)
	}

	q.Add("page", strconv.FormatInt(page, 10))
	q.Add("limit", strconv.FormatInt(limit, 10))
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetMultipleLangPageByID(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	pageID := c.Param("id")

	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/page/multiple-lang", pageID)
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetPageGroupedLang(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	brandID := c.Query("brand_id")
	propertyID := c.Query("property_id")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/page/grouped-lang")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if brandID != "" || propertyID != "" {
		if brandID != "" {
			q.Add("brandId", brandID)
		} else if propertyID != "" {
			q.Add("propertyId", propertyID)
		}
	} else {
		if userSession.PageLevel == "brand" {
			q.Add("brandId", userSession.PageId)
		} else {
			q.Add("propertyId", userSession.PageId)
		}
	}
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetPagesForMenu(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	code := c.Query("code")
	lang := c.Query("lang")
	sortDir := c.Query("sortDir")
	sortCol := c.Query("sortCol")
	page, _ := strconv.ParseInt(c.DefaultQuery("start", "1"), 10, 64)
	limit := c.DefaultQuery("length", "10")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/pages/menu")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()

	if code != "" {
		q.Add("propertyCode", code)
	} else {
		if userSession.PageLevel == "brand" {
			q.Add("brandId", userSession.PageId)
		} else {
			q.Add("propertyCode", userSession.PageCode)
		}
	}
	if lang != "" {
		q.Add("lang", lang)
	}
	if sortDir != "" {
		q.Add("sortDir", sortDir)
	}
	if sortCol != "" {
		q.Add("sortCol", sortCol)
	}
	q.Add("page", strconv.FormatInt(page, 10))
	q.Add("limit", limit)
	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) GetPagesForParent(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	lang := c.Query("lang")
	categoryID := c.Query("category_id")
	exceptID := c.Query("except_id")

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/page")
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	q := req.URL.Query()
	if userSession.PageLevel == "brand" {
		q.Add("brandId", userSession.PageId)
	} else {
		q.Add("propertyId", userSession.PageId)
	}

	if lang != "" {
		q.Add("lang", lang)
	}

	if exceptID != "" {
		q.Add("exceptId", exceptID)
	}

	if categoryID != "" {
		q.Add("categoryId", categoryID)
	}

	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	c.JSON(int(data.Get("status_code").Int()), data.Value())
}

func (app *Web) PageAdd(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	var payload map[string]interface{}
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	payload["order"], _ = strconv.Atoi(payload["order"].(string))
	if userSession.PageLevel == "brand" {
		payload["brand_id"] = userSession.PageId
	} else {
		payload["property_id"] = userSession.PageId
	}

	var jsonPayload, _ = json.Marshal(payload)
	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/page")
	var req, err = http.NewRequest("POST", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) PageUpdate(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	pageId := c.Param("id")
	var api = fmt.Sprintf("%s/%s%s", app.Config.API.URL, "api/v1/pages?id=", pageId)
	var req, err = http.NewRequest("GET", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) PageUpdateProcess(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	var payload []map[string]interface{}
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	for i := range payload {
		payload[i]["order"], _ = strconv.Atoi(payload[i]["order"].(string))

		if userSession.PageLevel == "brand" {
			payload[i]["brand_id"] = userSession.PageId
		} else {
			payload[i]["property_id"] = userSession.PageId
		}
	}

	var jsonPayload, _ = json.Marshal(payload)
	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/page")
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) PageDelete(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	pageID := c.Param("id")
	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/page/single", pageID)
	var req, err = http.NewRequest("DELETE", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) PageDeleteMultipleLangByID(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	pageID := c.Param("id")
	var api = fmt.Sprintf("%s/%s/%s", app.Config.API.URL, "api/v1/page/multiple-lang", pageID)
	var req, err = http.NewRequest("DELETE", api, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}

func (app *Web) PageDuplicator(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)
	userSession := session.Values["user"].(models.UserSession)

	src := c.PostFormMap("source")
	dst := c.PostFormMap("destination")

	source := map[string]string{
		"categoryId": src["category_id"],
	}
	if src["name"] == "brand" {
		source["brandId"] = src["id"]
	} else {
		source["propertyId"] = src["id"]
	}

	destination := map[string]string{}
	if dst["name"] == "brand" {
		destination["brandId"] = dst["id"]
	} else {
		destination["propertyId"] = dst["id"]
	}

	var payload = map[string]interface{}{
		"source":      source,
		"destination": destination,
	}
	var jsonPayload, _ = json.Marshal(payload)
	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/page/duplicator")
	var req, err = http.NewRequest("POST", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", userSession.AccessToken))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
		Data:       data.Get("data").Value(),
	})
}
