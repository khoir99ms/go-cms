package web

import (
	"alaric_cms/app/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/tidwall/gjson"

	"github.com/foolin/goview/supports/ginview"
	"github.com/gin-gonic/gin"
)

func (app *Web) Login(c *gin.Context) {
	ginview.HTML(c, http.StatusOK, "auth/signin", gin.H{
		"title":   "Please Sign In",
		"AppName": app.Config.AppName,
	})
}

func (app *Web) Forgot(c *gin.Context) {
	var userEmail = c.PostForm("email")

	var payload = map[string]string{"user_email": userEmail}
	var jsonPayload, _ = json.Marshal(payload)

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/auth/forgot")
	var response, err = http.Post(api, "application/json", bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
	})
}

func (app *Web) Reset(c *gin.Context) {
	key := c.Param("key")

	ginview.HTML(c, http.StatusOK, "auth/reset", gin.H{
		"title": "Reset Password",
		"key":   key,
	})
}

func (app *Web) Logout(c *gin.Context) {
	session, _ := app.Session.Get(c.Request, app.Config.Session.Name)

	session.Options.MaxAge = -1
	session.Save(c.Request, c.Writer)

	c.Redirect(http.StatusTemporaryRedirect, "/auth")
}

func (app *Web) Auth(c *gin.Context) {
	var userLogin = c.PostForm("user_login")
	var userPassword = c.PostForm("user_password")

	var payload = map[string]string{"user_login": userLogin, "user_password": userPassword}
	var jsonPayload, _ = json.Marshal(payload)

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/auth/")
	var response, err = http.Post(api, "application/json", bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	session, err := app.Session.Get(c.Request, app.Config.Session.Name)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var isBrand = int(data.Get("data.user.user_role").Int()) == app.Config.Role.Brand
	var pageId = data.Get("data.user.property_id").String()
	if isBrand {
		pageId = data.Get("data.user.brand_id").String()
	}

	var pageCode = "COR"
	var pageName = "Corporate Panel"
	if pageId != "" {
		if isBrand {
			api = fmt.Sprintf("%s/%s%s", app.Config.API.URL, "api/v1/brands?id=", pageId)
		} else {
			api = fmt.Sprintf("%s/%s%s", app.Config.API.URL, "api/v1/property?id=", pageId)
		}

		req, err := http.NewRequest("GET", api, nil)
		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", data.Get("data.token").String()))

		client := &http.Client{Timeout: time.Second * 10}
		response, err = client.Do(req)
		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}
		defer response.Body.Close()

		body, _ = ioutil.ReadAll(response.Body)
		property := gjson.ParseBytes(body)

		if property.Get("success").Bool() {
			if isBrand {
				pageCode = ""
				pageName = property.Get("data.item.brand_name").String()
			} else {
				pageCode = property.Get("data.item.property_code").String()
				pageName = property.Get("data.item.property_name").String()
			}
		}
	}

	var pageLevel string
	if int(data.Get("data.user.user_role").Int()) == app.Config.Role.Corporate {
		pageLevel = "corporate"
	} else if int(data.Get("data.user.user_role").Int()) == app.Config.Role.Brand {
		pageLevel = "brand"
	} else if int(data.Get("data.user.user_role").Int()) == app.Config.Role.Brand {
		pageLevel = "hotel"
	}

	user := &models.UserSession{
		UserId:            data.Get("data.user.user_id").String(),
		UserLogin:         data.Get("data.user.user_login").String(),
		UserFirstname:     data.Get("data.user.user_firstname").String(),
		UserLastname:      data.Get("data.user.user_lastname").String(),
		UserEmail:         data.Get("data.user.user_email").String(),
		UserRole:          int(data.Get("data.user.user_role").Int()),
		UserStatus:        data.Get("data.user.user_status").String(),
		BrandId:           data.Get("data.user.brand_id").String(),
		PropertyId:        data.Get("data.user.property_id").String(),
		PropertyCode:      pageCode,
		PageId:            pageId,
		PageCode:          pageCode,
		PageName:          pageName,
		PageLevel:         pageLevel,
		PageNameUserLogin: pageName,
		AccessToken:       data.Get("data.token").String(),
		Authenticated:     true,
	}
	session.Values["user"] = user

	err = session.Save(c.Request, c.Writer)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "login successfully.",
	})
}

func (app *Web) ResetProcess(c *gin.Context) {
	var key = c.PostForm("key")
	var userPassword = c.PostForm("password")
	var retypePassword = c.PostForm("retype_password")

	var payload = map[string]string{"password": userPassword, "retype_password": retypePassword}
	var jsonPayload, _ = json.Marshal(payload)

	var api = fmt.Sprintf("%s/%s", app.Config.API.URL, "api/v1/auth/reset")
	var req, err = http.NewRequest("PUT", api, bytes.NewBuffer(jsonPayload))
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", key))

	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer response.Body.Close()

	var body, _ = ioutil.ReadAll(response.Body)
	data := gjson.ParseBytes(body)

	if !data.Get("success").Bool() {
		var statusCode = int(data.Get("status_code").Int())
		c.JSON(statusCode, &models.ReE{
			StatusCode: statusCode,
			Error:      data.Get("error").String(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    data.Get("message").String(),
	})
}
