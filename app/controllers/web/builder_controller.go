package web

import (
	"fmt"
	"net/http"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
)

func (app *Web) MapAttraction(c *gin.Context) {
	url := location.Get(c)
	app.RenderHTML(c, http.StatusOK, "builder/map", gin.H{
		"title":  "Map Attraction",
		"host":   fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host),
		"apikey": app.Config.Google.ApiKey,
	})
}

func (app *Web) InstagramFeed(c *gin.Context) {
	url := location.Get(c)
	app.RenderHTML(c, http.StatusOK, "builder/instagram", gin.H{
		"title":  "Instagram Feed",
		"host":   fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host),
		"apikey": app.Config.Google.ApiKey,
	})
}
