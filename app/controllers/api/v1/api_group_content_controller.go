package v1

import (
	"alaric_cms/app/models"
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetGroupContent(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("group_content")

	groupID := c.Query("id")
	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")

	search := c.Query("search")
	sortCol := c.DefaultQuery("sortCol", "_id")
	sortDir := c.DefaultQuery("sortDir", "asc")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	matchFilter := bson.M{}
	if groupID != "" {
		gID, _ := primitive.ObjectIDFromHex(groupID)
		matchFilter["_id"] = gID
	}
	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["brand_id"] = bID
	}
	if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		matchFilter["property_id"] = pID
	}

	pipeline := bson.A{}
	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["name"] = searchRe
	}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$sort": sort},
		bson.M{"$lookup": bson.M{
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
			"as":           "brand",
		}},
		bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from":         "properties",
			"localField":   "property_id",
			"foreignField": "_id",
			"as":           "property",
		}},
		bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
		bson.M{"$unwind": bson.M{"path": "$items", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from":         "categories",
			"localField":   "items.ref_id",
			"foreignField": "_id",
			"as":           "item_categories",
		}},
		bson.M{"$unwind": bson.M{"path": "$item_categories", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from":         "pages",
			"localField":   "items.ref_id",
			"foreignField": "_id",
			"as":           "item_pages",
		}},
		bson.M{"$unwind": bson.M{"path": "$item_pages", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from":         "brands",
			"localField":   "item_pages.brand_id",
			"foreignField": "_id",
			"as":           "item_pages.brand",
		}},
		bson.M{"$unwind": bson.M{"path": "$item_pages.brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from":         "properties",
			"localField":   "item_pages.property_id",
			"foreignField": "_id",
			"as":           "item_pages.property",
		}},
		bson.M{"$unwind": bson.M{"path": "$item_pages.property", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from":         "group_content",
			"localField":   "items.ref_id",
			"foreignField": "_id",
			"as":           "item_gc",
		}},
		bson.M{"$unwind": bson.M{"path": "$item_gc", "preserveNullAndEmptyArrays": true}},
		bson.M{"$group": bson.M{
			"_id":         "$_id",
			"name":        bson.M{"$first": "$name"},
			"title":       bson.M{"$first": "$title"},
			"description": bson.M{"$first": "$description"},
			"image":       bson.M{"$first": "$image"},
			"brand":       bson.M{"$first": "$brand"},
			"property":    bson.M{"$first": "$property"},
			"setting":     bson.M{"$first": "$setting"},
			"items": bson.M{"$push": bson.M{"$switch": bson.M{
				"branches": bson.A{
					bson.M{
						"case": bson.M{"$and": bson.A{
							bson.M{"$eq": bson.A{"$items.type", "page"}},
							bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$item_pages", bson.M{}}}, bson.M{}}},
						}},
						"then": bson.M{"$mergeObjects": bson.A{"$item_pages", bson.M{"ref": "$items"}}},
					},
					bson.M{
						"case": bson.M{"$and": bson.A{
							bson.M{"$eq": bson.A{"$items.type", "category"}},
							bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$item_categories", bson.M{}}}, bson.M{}}},
						}},
						"then": bson.M{"$mergeObjects": bson.A{"$item_categories", bson.M{"ref": "$items"}}},
					},
					bson.M{
						"case": bson.M{"$and": bson.A{
							bson.M{"$eq": bson.A{"$items.type", "gc"}},
							bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$item_gc", bson.M{}}}, bson.M{}}},
						}},
						"then": bson.M{"$mergeObjects": bson.A{"$item_gc", bson.M{"ref": "$items"}}},
					},
				},
				"default": "$im_not_exist_field",
			}}},
		}},
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$_id",
			"name":        "$name",
			"title":       "$title",
			"description": "$description",
			"image":       "$image",
			"owner": bson.M{
				"brand": bson.M{
					"id":   "$brand._id",
					"code": "$brand.brand_code",
					"name": "$brand.brand_name",
					"slug": "$brand.brand_slug",
				},
				"property": bson.M{
					"id":   "$property._id",
					"code": "$property.property_code",
					"name": "$property.property_name",
					"slug": "$property.property_slug",
				},
			},
			"setting": "$setting",
			"items": bson.M{"$map": bson.M{
				"input": "$items",
				"as":    "item",
				"in": bson.M{"$switch": bson.M{
					"branches": bson.A{
						bson.M{
							"case": bson.M{"$eq": bson.A{"$$item.ref.type", "page"}},
							"then": bson.M{
								"id":    "$$item._id",
								"title": "$$item.title",
								"lang":  "$$item.lang",
								"slug":  "$$item.slug",
								"type":  "$$item.ref.type",
								"display_title": bson.M{"$cond": bson.M{
									"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$item.ref.title", ""}}, ""}},
									"then": "",
									"else": "$$item.ref.title",
								}},
								"display_image": bson.M{"$cond": bson.M{
									"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$item.ref.image", ""}}, ""}},
									"then": "",
									"else": "$$item.ref.image",
								}},
								"tags": bson.M{"$cond": bson.M{
									"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$item.ref.tags", ""}}, ""}},
									"then": bson.A{},
									"else": "$$item.ref.tags",
								}},
								"template": bson.M{"$cond": bson.M{
									"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$item.ref.template", ""}}, ""}},
									"then": "",
									"else": "$$item.ref.template",
								}},
								"property": bson.M{
									"id":   "$$item.property._id",
									"code": "$$item.property.property_code",
									"name": "$$item.property.property_name",
									"slug": "$$item.property.property_slug",
								},
								"brand": bson.M{
									"id":   "$$item.brand._id",
									"code": "$$item.brand.brand_code",
									"name": "$$item.brand.brand_name",
									"slug": "$$item.brand.brand_slug",
								},
								"created_at": "$$item.created_at",
								"updated_at": "$$item.updated_at",
							},
						},
						bson.M{
							"case": bson.M{"$eq": bson.A{"$$item.ref.type", "category"}},
							"then": bson.M{
								"id":    "$$item._id",
								"title": "$$item.name",
								"lang":  "$$item.lang",
								"slug":  "$$item.slug",
								"type":  "$$item.ref.type",
								"display_title": bson.M{"$cond": bson.M{
									"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$item.ref.title", ""}}, ""}},
									"then": "",
									"else": "$$item.ref.title",
								}},
								"display_image": bson.M{"$cond": bson.M{
									"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$item.ref.image", ""}}, ""}},
									"then": "",
									"else": "$$item.ref.image",
								}},
								"tags": bson.M{"$cond": bson.M{
									"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$item.ref.tags", ""}}, ""}},
									"then": bson.A{},
									"else": "$$item.ref.tags",
								}},
								"template":   bson.M{"$ifNull": bson.A{"$$item.ref.template", ""}},
								"created_at": "$$item.created_at",
								"updated_at": "$$item.updated_at",
							},
						},
						bson.M{
							"case": bson.M{"$eq": bson.A{"$$item.ref.type", "gc"}},
							"then": bson.M{
								"id":    "$$item._id",
								"title": "$$item.name",
								"lang":  "$$item.ref.lang",
								"slug":  "$$item.name",
								"type":  "$$item.ref.type",
								"display_title": bson.M{"$cond": bson.M{
									"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$item.ref.title", ""}}, ""}},
									"then": "",
									"else": "$$item.ref.title",
								}},
								"display_image": bson.M{"$cond": bson.M{
									"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$item.ref.image", ""}}, ""}},
									"then": "",
									"else": "$$item.ref.image",
								}},
								"tags": bson.M{"$cond": bson.M{
									"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$item.ref.tags", ""}}, ""}},
									"then": bson.A{},
									"else": "$$item.ref.tags",
								}},
								"template":   bson.M{"$ifNull": bson.A{"$$item.ref.template", ""}},
								"created_at": "$$item.created_at",
								"updated_at": "$$item.updated_at",
							},
						},
					},
					"default": nil,
				}},
			}},
		}},
		bson.M{"$facet": bson.M{
			"paginatedResults": bson.A{bson.M{"$skip": offset}, bson.M{"$limit": limit}},
			"totalCount":       bson.A{bson.M{"$count": "count"}},
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	var results interface{}
	if groupID != "" && len(facetData) == 1 {
		results = facetData[0]
	} else {
		results = facetData
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": total,
		},
	})
}

func (app *Api) AddGroupContent(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("group_content")

	var payload models.GroupContentForm
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	now := time.Now()
	var gc = models.GroupContent{
		Name:      payload.Name,
		CreatedAt: &now,
	}

	findFilter := bson.M{}
	if payload.BrandID != "" {
		bID, _ := primitive.ObjectIDFromHex(payload.BrandID)
		gc.BrandID = &bID
		findFilter["brand_id"] = bID
	}

	if payload.PropertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.PropertyID)
		gc.PropertyID = &pID
		findFilter["property_id"] = pID
	}
	findFilter["name"] = payload.Name

	ctx := context.Background()
	cursor, err := collection.Find(ctx, findFilter)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Group content already exists.",
		})
		return
	}

	result, err := collection.InsertOne(ctx, gc)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"id": result.InsertedID,
		},
	})
}

func (app *Api) UpdateGroupContent(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("group_content")

	groupID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(groupID)

	var gc models.GroupContent
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&gc)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var brandID, propertyID string
	if gc.BrandID != nil {
		brandID = gc.BrandID.Hex()
	}

	if gc.PropertyID != nil {
		propertyID = gc.PropertyID.Hex()
	}

	var payload = models.GroupContentForm{
		Name:       gc.Name,
		Image:      gc.Image,
		Items:      gc.Items,
		BrandID:    brandID,
		PropertyID: propertyID,
	}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	gc.Name = payload.Name
	gc.Image = payload.Image
	gc.Items = payload.Items

	now := time.Now()
	gc.UpdatedAt = &now

	findFilter := bson.M{"_id": bson.M{"$ne": objectID}}
	if payload.BrandID != "" {
		bID, _ := primitive.ObjectIDFromHex(payload.BrandID)
		gc.BrandID = &bID
		findFilter["brand_id"] = bID
	}

	if payload.PropertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.PropertyID)
		gc.PropertyID = &pID
		findFilter["property_id"] = pID
	}
	findFilter["name"] = gc.Name

	cursor, err := collection.Find(ctx, findFilter)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Group content already exists.",
		})
		return
	}

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": gc})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"group_content":  gc,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) UpdateGroupContentSetting(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("group_content")

	groupID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(groupID)

	var gc models.GroupContent
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&gc)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var payload []models.GroupContentSettingForm
	for _, item := range gc.Setting {
		var setting models.GroupContentSettingForm

		setting.Title = item.Title
		setting.Description = item.Description
		setting.LabelDetail = item.LabelDetail
		setting.LabelMore = item.LabelMore
		setting.MetaTitle = item.MetaTitle
		setting.MetaKeyword = item.MetaKeyword
		setting.MetaDescription = item.MetaDescription
		setting.Lang = item.Lang

		payload = append(payload, setting)
	}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	settings := []models.GroupContentSetting{}
	for _, item := range payload {
		setting := models.GroupContentSetting{}

		setting.Title = item.Title
		setting.Description = item.Description
		setting.LabelDetail = item.LabelDetail
		setting.LabelMore = item.LabelMore
		setting.MetaTitle = item.MetaTitle
		if setting.MetaTitle == "" {
			setting.MetaTitle = setting.Title
		}
		setting.MetaKeyword = item.MetaKeyword
		setting.MetaDescription = item.MetaDescription
		setting.Lang = item.Lang

		settings = append(settings, setting)
	}

	if len(settings) > 0 {
		gc.Setting = settings
	}

	now := time.Now()
	gc.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": gc})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"group_content":  gc,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteGroupContent(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("group_content")

	groupID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(groupID)

	ctx := context.Background()
	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            groupID,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) DeleteGroupContentByLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("group_content")

	groupID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(groupID)
	lang := c.Param("lang")

	var gc models.GroupContent
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&gc)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var settings []models.GroupContentSetting
	for _, setting := range gc.Setting {
		if setting.Lang != lang {
			settings = append(settings, setting)
		}
	}
	gc.Setting = settings

	now := time.Now()
	gc.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": gc})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"group_content":  gc,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) fetchGroupContent(params map[string]string) (bson.M, error) {
	collection := app.Config.MongoDB.Collection("group_content")

	host, ok := params["host"]
	if !ok {
		host = ""
	}

	// this is an optional parameter
	ID, ok := params["id"]
	if !ok {
		ID = ""
	}
	name, ok := params["name"]
	if !ok {
		name = ""
	}
	code, ok := params["code"]
	if !ok {
		code = ""
	} else {
		code = strings.ToUpper(code)
	}
	codeSlug, ok := params["codeSlug"]
	if !ok {
		codeSlug = ""
	} else {
		codeSlug = strings.ToLower(codeSlug)
	}

	if ID == "" && name == "" {
		return nil, errors.New("Required group ID or name.")
	}

	if code == "" {
		return nil, errors.New("Required property code.")
	}

	lang, ok := params["lang"]
	if !ok || lang == "" {
		lang = "en"
	}
	status, ok := params["status"]
	if !ok || status == "" {
		status = "publish"
	}
	today := time.Now().Format("2006-01-02")

	sortCol, ok := params["sortCol"]
	if !ok {
		sortCol = "created_at"
	}
	sortDir, ok := params["sortDir"]
	if !ok {
		sortDir = ""
	}

	sort := bson.M{}
	if sortDir != "" {
		if sortDir == "asc" {
			sortDir = "1"
		} else {
			sortDir = "-1"
		}
		sortOrder, _ := strconv.Atoi(sortDir)
		// Sort by `-1` field descending
		sortArray := strings.Split(sortCol, " ")
		for _, val := range sortArray {
			val = fmt.Sprintf("items.%s", val)
			sort[val] = sortOrder
		}
	}

	matchFilter := bson.M{}
	matchFilterExtendsCategory := bson.M{}
	if ID != "" {
		oID, _ := primitive.ObjectIDFromHex(ID)
		matchFilter["_id"] = oID
	}

	if name != "" {
		matchFilter["name"] = name
	}

	if codeSlug != "" {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_slug": codeSlug},
			bson.M{"property.property_slug": codeSlug},
		}
		matchFilterExtendsCategory["$or"] = bson.A{
			bson.M{"brand.brand_slug": codeSlug},
			bson.M{"property.property_slug": codeSlug},
		}
	} else if code != "" {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_code": code},
			bson.M{"property.property_code": code},
		}
		matchFilterExtendsCategory["$or"] = bson.A{
			bson.M{"brand.brand_code": code},
			bson.M{"property.property_code": code},
		}
	}

	pipelineLookupCategory := bson.A{
		bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
			bson.M{"$eq": bson.A{"$_id", "$$ref_id"}},
		}}}},
		bson.M{"$lookup": bson.M{
			"from": "categories",
			"let":  bson.M{"id": "$_id", "source_id": "$source_id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$or": bson.A{
					bson.M{"$eq": bson.A{"$source_id", "$$id"}},
					bson.M{"$eq": bson.A{"$_id", "$$source_id"}},
				}}}},
			},
			"as": "related_categories",
		}},
		bson.M{"$unwind": bson.M{"path": "$related_categories", "preserveNullAndEmptyArrays": true}},
		bson.M{"$group": bson.M{
			"_id":       "$_id",
			"primary":   bson.M{"$first": "$$ROOT"},
			"secondary": bson.M{"$push": "$related_categories"},
		}},
		bson.M{"$project": bson.M{
			"categories": bson.M{"$concatArrays": bson.A{bson.A{"$primary"}, "$secondary"}},
		}},
		bson.M{"$unwind": bson.M{"path": "$categories", "preserveNullAndEmptyArrays": false}},
		bson.M{"$replaceRoot": bson.M{"newRoot": "$categories"}},
		bson.M{"$match": bson.M{"lang": lang}},
		bson.M{"$lookup": bson.M{
			"from": "categories",
			"let":  bson.M{"id": "$_id"},
			"pipeline": bson.A{
				bson.M{"$unwind": bson.M{"path": "$extends", "preserveNullAndEmptyArrays": true}},
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$_id", "$$id"}},
				}}}},
				bson.M{"$lookup": bson.M{
					"from": "brands", "as": "brand",
					"localField": "extends.brand_id", "foreignField": "_id",
				}},
				bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
				bson.M{"$lookup": bson.M{
					"from": "properties", "as": "property",
					"localField": "extends.property_id", "foreignField": "_id",
				}},
				bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
				bson.M{"$match": matchFilterExtendsCategory},
				bson.M{"$project": bson.M{
					"_id":         0,
					"title":       "$extends.title",
					"description": "$extends.description",
					"image":       "$extends.image",
					"label":       "$extends.label",
					"meta":        "$extends.meta",
				}},
			},
			"as": "extends",
		}},
		bson.M{"$unwind": bson.M{"path": "$extends", "preserveNullAndEmptyArrays": true}},
		bson.M{"$project": bson.M{
			"_id":  0,
			"id":   "$_id",
			"name": "$name",
			"featured_image": bson.M{"$cond": bson.M{
				"if": bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.image", ""}}, ""}},
				"then": bson.M{
					"name": "$image",
					"local": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$image", 0, 4}}, "http"}},
						"$image",
						bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$image"},
						},
					}},
					"cloud": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$image", 0, 4}}, "http"}},
						"$image",
						bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$image"},
						},
					}},
				},
				"else": bson.M{
					"name": "$extends.image",
					"local": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$extends.image", 0, 4}}, "http"}},
						"$extends.image",
						bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$extends.image"},
						},
					}},
					"cloud": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$extends.image", 0, 4}}, "http"}},
						"$extends.image",
						bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$extends.image"},
						},
					}},
				},
			}},
			"title": bson.M{"$cond": bson.A{
				bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.title", ""}}, ""}},
				"$title",
				"$extends.title",
			}},
			"description": bson.M{"$cond": bson.A{
				bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.description", ""}}, ""}},
				"$description",
				"$extends.description",
			}},
			"label": bson.M{"$cond": bson.A{
				bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.label.detail", ""}}, ""}},
					bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.meta.more", ""}}, ""}},
				}},
				"$label",
				"$extends.label",
			}},
			"meta": bson.M{"$cond": bson.A{
				bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.meta.title", ""}}, ""}},
				"$meta",
				"$extends.meta",
			}},
			"slug":       "$slug",
			"lang":       "$lang",
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
	}

	pipelineLookupPage := bson.A{
		bson.M{"$sort": bson.M{"order": 1}},
		bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
			bson.M{"$eq": bson.A{"$_id", "$$ref_id"}},
		}}}},
		bson.M{"$lookup": bson.M{
			"from": "pages",
			"let":  bson.M{"id": "$_id", "source_id": "$source_id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$or": bson.A{
					bson.M{"$eq": bson.A{"$source_id", "$$id"}},
					bson.M{"$eq": bson.A{"$_id", "$$source_id"}},
				}}}},
			},
			"as": "related_pages",
		}},
		bson.M{"$unwind": bson.M{"path": "$related_pages", "preserveNullAndEmptyArrays": true}},
		bson.M{"$group": bson.M{
			"_id":       "$_id",
			"primary":   bson.M{"$first": "$$ROOT"},
			"secondary": bson.M{"$push": "$related_pages"},
		}},
		bson.M{"$project": bson.M{
			"pages": bson.M{"$filter": bson.M{
				"input": bson.M{"$concatArrays": bson.A{bson.A{"$primary"}, "$secondary"}},
				"as":    "page",
				"cond": bson.M{"$or": bson.A{
					bson.M{"$or": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$page.show_date", ""}}, ""}},
						bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$page.end_date", ""}}, ""}},
					}},
					bson.M{"$and": bson.A{
						bson.M{"$gte": bson.A{
							bson.M{"$dateFromString": bson.M{
								"dateString": today,
							}}, "$$page.show_date"},
						},
						bson.M{"$lte": bson.A{
							bson.M{"$dateFromString": bson.M{
								"dateString": today,
							}}, "$$page.end_date"},
						},
					}},
				}},
			}},
		}},
		bson.M{"$unwind": bson.M{"path": "$pages", "preserveNullAndEmptyArrays": false}},
		bson.M{"$replaceRoot": bson.M{"newRoot": "$pages"}},
		bson.M{"$match": bson.M{"status": status, "lang": lang}},
		bson.M{"$lookup": bson.M{
			"from": "categories", "as": "category",
			"localField": "category_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$category", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "brands", "as": "brand",
			"localField": "brand_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "properties", "as": "property",
			"localField": "property_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"tags": bson.M{"$ifNull": bson.A{"$tags", bson.A{}}}},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$in": bson.A{"$_id", "$$tags"}},
					bson.M{"$eq": bson.A{"$type", "tag"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id": 0,
					"id":  "$_id",
					"items": bson.M{"$filter": bson.M{
						"input": "$items",
						"as":    "item",
						"cond":  bson.M{"$eq": bson.A{"$$item.lang", lang}},
					}},
				}},
				bson.M{"$addFields": bson.M{
					"items": bson.M{"$cond": bson.A{
						bson.M{"$gt": bson.A{bson.M{"$size": bson.M{"$ifNull": bson.A{"$items", bson.A{}}}}, 0}},
						bson.M{"$arrayElemAt": bson.A{"$items", 0}},
						bson.M{},
					}},
				}},
				bson.M{"$project": bson.M{
					"id":    "$id",
					"name":  "$items.name",
					"title": "$items.title",
				}},
			},
			"as": "full_tags",
		}},
		bson.M{"$project": bson.M{
			"_id":      0,
			"id":       "$_id",
			"title":    "$title",
			"subtitle": "$subtitle",
			"content":  "$content",
			"excerpt":  "$excerpt",
			"featured_image": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$featured_image", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$featured_image",
					"local": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$featured_image", 0, 4}}, "http"}},
						"$featured_image",
						bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$featured_image"},
						},
					}},
					"cloud": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$featured_image", 0, 4}}, "http"}},
						"$featured_image",
						bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$featured_image"},
						},
					}},
				},
			}},
			"attachment_image": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$attachment_image", ""}}, ""}},
				"then": bson.A{},
				"else": bson.M{
					"$map": bson.M{
						"input": "$attachment_image",
						"as":    "image",
						"in": bson.M{
							"name": "$$image",
							"local": bson.M{"$cond": bson.A{
								bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$$image", 0, 4}}, "http"}},
								"$$image",
								bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$image"},
								},
							}},
							"cloud": bson.M{"$cond": bson.A{
								bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$$image", 0, 4}}, "http"}},
								"$$image",
								bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$image"},
								},
							}},
						},
					},
				},
			}},
			"lang":                "$lang",
			"slug":                "$slug",
			"template":            "$template",
			"order":               "$order",
			"parent_id":           "$parent_id",
			"status":              "$status",
			"show_date":           "$show_date",
			"end_date":            "$end_date",
			"amenities":           "$amenities",
			"tags":                bson.M{"$ifNull": bson.A{"$tags", bson.A{}}},
			"full_tags":           "$full_tags",
			"badge":               bson.M{"$ifNull": bson.A{"$badge", bson.M{}}},
			"custom.field":        "$custom.field",
			"custom.link":         "$custom.link",
			"custom.detail_label": "$custom.label_detail",
			"script.header":       "$script.header",
			"script.body":         "$script.body",
			"meta.title":          "$meta.title",
			"meta.keyword":        "$meta.keyword",
			"meta.description":    "$meta.description",
			"category.id":         "$category._id",
			"category.name":       "$category.name",
			"category.lang":       "$category.lang",
			"category.slug":       "$category.slug",
			"brand.id":            "$brand._id",
			"brand.code":          "$brand.brand_code",
			"brand.name":          "$brand.brand_name",
			"brand.slug":          "$brand.brand_slug",
			"property.id":         "$property._id",
			"property.code":       "$property.property_code",
			"property.name":       "$property.property_name",
			"property.slug":       "$property.property_slug",
			"created_at":          "$created_at",
			"updated_at":          "$updated_at",
		}},
	}

	pipelineLookupGroupContent := bson.A{
		bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
			bson.M{"$eq": bson.A{"$_id", "$$ref_id"}},
		}}}},
		bson.M{"$addFields": bson.M{
			"setting": bson.M{"$filter": bson.M{
				"input": "$setting",
				"as":    "item",
				"cond":  bson.M{"$eq": bson.A{"$$item.lang", lang}},
			}},
		}},
		bson.M{"$project": bson.M{
			"_id":  0,
			"id":   "$_id",
			"name": "$name",
			"featured_image": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$image", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$image",
					"local": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$image", 0, 4}}, "http"}},
						"$image",
						bson.M{"$concat": bson.A{
							host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$image",
						}},
					}},
					"cloud": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$image", 0, 4}}, "http"}},
						"$image",
						bson.M{"$concat": bson.A{
							app.Config.Path.CloudImagePrefix, "/", "$image",
						}},
					}},
				},
			}},
			"setting": bson.M{"$cond": bson.A{
				bson.M{"$gt": bson.A{bson.M{"$size": bson.M{"$ifNull": bson.A{"$setting", bson.A{}}}}, 0}},
				bson.M{"$arrayElemAt": bson.A{"$setting", 0}},
				bson.M{},
			}},
			"slug":       "$name",
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
		bson.M{"$project": bson.M{
			"id":             "$id",
			"name":           "$name",
			"featured_image": "$featured_image",
			"title":          bson.M{"$ifNull": bson.A{"$setting.title", ""}},
			"description":    bson.M{"$ifNull": bson.A{"$setting.description", ""}},
			"label": bson.M{
				"detail": "$setting.label_detail",
				"more":   "$setting.label_more",
			},
			"meta": bson.M{
				"title":       "$setting.meta_title",
				"keyword":     "$setting.meta_keyword",
				"description": "$setting.meta_description",
			},
			"slug":       "$name",
			"lang":       bson.M{"$ifNull": bson.A{"$setting.lang", ""}},
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
	}

	pipeline := bson.A{
		bson.M{"$lookup": bson.M{
			"from": "brands", "as": "brand",
			"localField": "brand_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "properties", "as": "property",
			"localField": "property_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
		bson.M{"$match": matchFilter},
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$unwind": bson.M{"path": "$items", "preserveNullAndEmptyArrays": true}},
		bson.M{"$addFields": bson.M{
			"items.title": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{fmt.Sprintf("$items.title.%s", lang), ""}}, ""}},
				"then": "",
				"else": fmt.Sprintf("$items.title.%s", lang),
			}},
		}},
		bson.M{"$lookup": bson.M{
			"from":     "categories",
			"let":      bson.M{"ref_id": "$items.ref_id"},
			"pipeline": pipelineLookupCategory,
			"as":       "item_categories",
		}},
		bson.M{"$unwind": bson.M{"path": "$item_categories", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from":     "pages",
			"let":      bson.M{"ref_id": "$items.ref_id"},
			"pipeline": pipelineLookupPage,
			"as":       "item_pages",
		}},
		bson.M{"$unwind": bson.M{"path": "$item_pages", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from":     "group_content",
			"let":      bson.M{"ref_id": "$items.ref_id"},
			"pipeline": pipelineLookupGroupContent,
			"as":       "item_gc",
		}},
		bson.M{"$unwind": bson.M{"path": "$item_gc", "preserveNullAndEmptyArrays": true}},
		bson.M{"$group": bson.M{
			"_id":  "$_id",
			"name": bson.M{"$first": "$name"},
			"setting": bson.M{"$first": bson.M{"$filter": bson.M{
				"input": "$setting",
				"as":    "item",
				"cond":  bson.M{"$eq": bson.A{"$$item.lang", lang}},
			}}},
			"image": bson.M{"$first": "$image"},
			"items": bson.M{"$push": bson.M{"$switch": bson.M{
				"branches": bson.A{
					bson.M{
						"case": bson.M{"$and": bson.A{
							bson.M{"$eq": bson.A{"$items.type", "page"}},
							bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$item_pages", bson.M{}}}, bson.M{}}},
						}},
						"then": bson.M{"$mergeObjects": bson.A{"$item_pages", bson.M{"ref": "$items"}}},
					},
					bson.M{
						"case": bson.M{"$and": bson.A{
							bson.M{"$eq": bson.A{"$items.type", "category"}},
							bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$item_categories", bson.M{}}}, bson.M{}}},
						}},
						"then": bson.M{"$mergeObjects": bson.A{"$item_categories", bson.M{"ref": "$items"}}},
					},
					bson.M{
						"case": bson.M{"$and": bson.A{
							bson.M{"$eq": bson.A{"$items.type", "gc"}},
							bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$item_gc", bson.M{}}}, bson.M{}}},
						}},
						"then": bson.M{"$mergeObjects": bson.A{"$item_gc", bson.M{"ref": "$items"}}},
					},
				},
				"default": "$nil",
			}}},
			"brand":      bson.M{"$first": "$brand"},
			"property":   bson.M{"$first": "$property"},
			"created_at": bson.M{"$first": "$created_at"},
			"updated_at": bson.M{"$first": "$updated_at"},
		}},
		bson.M{"$unwind": bson.M{"path": "$items", "preserveNullAndEmptyArrays": true}},
	}...)

	if len(sort) > 0 {
		pipeline = append(pipeline, bson.M{"$sort": sort})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$group": bson.M{
			"_id":  "$_id",
			"name": bson.M{"$first": "$name"},
			"setting": bson.M{"$first": bson.M{"$cond": bson.A{
				bson.M{"$gt": bson.A{bson.M{"$size": bson.M{"$ifNull": bson.A{"$setting", bson.A{}}}}, 0}},
				bson.M{"$arrayElemAt": bson.A{"$setting", 0}},
				bson.M{},
			}}},
			"image": bson.M{"$first": "$image"},
			"items": bson.M{"$push": bson.M{"$cond": bson.A{
				bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$items", bson.M{}}}, bson.M{}}},
				bson.M{"$mergeObjects": bson.A{"$items", bson.M{
					"display_title": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$items.ref.title", ""}}, ""}},
						"then": "",
						"else": "$items.ref.title",
					}},
					"display_image": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$items.ref.image", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$items.ref.image",
							"local": bson.M{"$cond": bson.A{
								bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$items.ref.image", 0, 4}}, "http"}},
								"$items.ref.image",
								bson.M{"$concat": bson.A{
									host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$items.ref.image",
								}},
							}},
							"cloud": bson.M{"$cond": bson.A{
								bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$items.ref.image", 0, 4}}, "http"}},
								"$items.ref.image",
								bson.M{"$concat": bson.A{
									app.Config.Path.CloudImagePrefix, "/", "$items.ref.image",
								}},
							}},
						},
					}},
					"template": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$items.ref.template", ""}}, ""}},
						"then": bson.M{"$ifNull": bson.A{"$items.template", ""}},
						"else": "$items.ref.template",
					}},
				}}},
				"$nil",
			}}},
			"brand":      bson.M{"$first": "$brand"},
			"property":   bson.M{"$first": "$property"},
			"created_at": bson.M{"$first": "$created_at"},
			"updated_at": bson.M{"$first": "$updated_at"},
		}},
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$_id",
			"name":        "$name",
			"title":       "$setting.title",
			"description": "$setting.description",
			"image": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$image", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$image",
					"local": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$image", 0, 4}}, "http"}},
						"$image",
						bson.M{"$concat": bson.A{
							host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$image",
						}},
					}},
					"cloud": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$image", 0, 4}}, "http"}},
						"$image",
						bson.M{"$concat": bson.A{
							app.Config.Path.CloudImagePrefix, "/", "$image",
						}},
					}},
				},
			}},
			"label": bson.M{
				"detail": "$setting.label_detail",
				"more":   "$setting.label_more",
			},
			"meta": bson.M{
				"title":       "$setting.meta_title",
				"keyword":     "$setting.meta_keyword",
				"description": "$setting.meta_description",
			},
			"lang": "$setting.lang",
			"brand": bson.M{
				"id":   "$brand._id",
				"code": "$brand.brand_code",
				"name": "$brand.brand_name",
				"slug": "$brand.brand_slug",
			},
			"property": bson.M{
				"id":   "$property._id",
				"code": "$property.property_code",
				"name": "$property.property_name",
				"slug": "$property.property_slug",
			},
			"items": "$items",
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer cursor.Close(ctx)

	results := bson.M{}
	for cursor.Next(ctx) {
		cursor.Decode(&results)
	}

	return results, nil
}

func (app *Api) GetGroupContentByName(c *gin.Context) {
	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	name := c.Param("name")
	lang := c.DefaultQuery("lang", "en")
	status := c.DefaultQuery("status", "publish")
	code := strings.ToUpper(c.DefaultQuery("code", "cor"))
	codeSlug := strings.ToLower(c.Query("codeSlug"))
	sortCol := c.DefaultQuery("sortCol", "created_at")
	sortDir := c.Query("sortDir")

	params := map[string]string{
		"host":     host,
		"name":     name,
		"lang":     lang,
		"status":   status,
		"code":     code,
		"codeSlug": codeSlug,
		"sortCol":  sortCol,
		"sortDir":  sortDir,
	}

	results, err := app.fetchGroupContent(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}
