package v1

import (
	"alaric_cms/app/helpers"
	"alaric_cms/app/models"
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"github.com/h2non/filetype"
	uuid "github.com/satori/go.uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetMedia(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("media")

	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	mediaId := c.Query("id")
	brandId := c.Query("brandId")
	propertyId := c.Query("propertyId")
	mediaType := c.Query("type")
	asGallery := c.Query("as_gallery")
	search := c.Query("search")
	sortCol := c.DefaultQuery("sortCol", "_id")
	sortDir := c.DefaultQuery("sortDir", "-1")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	pipeline := bson.A{}

	matchFilter := bson.M{}
	if mediaId != "" {
		mID, _ := primitive.ObjectIDFromHex(mediaId)
		matchFilter["_id"] = mID
	}
	if brandId != "" {
		brID, _ := primitive.ObjectIDFromHex(brandId)
		matchFilter["brand_id"] = brID
	}
	if propertyId != "" {
		prID, _ := primitive.ObjectIDFromHex(propertyId)
		matchFilter["property_id"] = prID
	}
	if mediaType != "" {
		matchFilter["type"] = mediaType
	}
	if asGallery != "" {
		matchFilter["as_gallery"], _ = strconv.ParseBool(asGallery)
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"title": searchRe},
			bson.M{"caption": searchRe},
			bson.M{"type": searchRe},
			bson.M{"url": searchRe},
		}
	}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$sort": sort},
		bson.M{
			"$lookup": bson.M{
				"as":           "brand",
				"from":         "brands",
				"localField":   "brand_id",
				"foreignField": "_id",
			},
		},
		bson.M{
			"$unwind": bson.M{
				"path":                       "$brand",
				"preserveNullAndEmptyArrays": true,
			},
		},
		bson.M{
			"$lookup": bson.M{
				"as":           "property",
				"from":         "properties",
				"localField":   "property_id",
				"foreignField": "_id",
			},
		},
		bson.M{
			"$unwind": bson.M{
				"path":                       "$property",
				"preserveNullAndEmptyArrays": true,
			},
		},
		bson.M{
			"$project": bson.M{
				"_id":         0,
				"id":          "$_id",
				"title":       "$title",
				"caption":     "$caption",
				"description": "$description",
				"url":         "$url",
				"file": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{"$file", ""}},
					"then": bson.M{},
					"else": bson.M{
						"name": "$file",
						"local": bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$file"},
						},
						"cloud": bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$file"},
						},
					},
				}},
				"type":    "$type",
				"gallery": "$as_gallery",
				"brand": bson.M{
					"id":   "$brand._id",
					"code": "$brand.brand_code",
					"name": "$brand.brand_name",
					"slug": "$brand.brand_slug",
				},
				"property": bson.M{
					"id":   "$property._id",
					"code": "$property.property_code",
					"name": "$property.property_name",
					"slug": "$property.property_slug",
				},
				"created":  "$registered",
				"modified": "$modified",
			},
		},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": bson.A{bson.M{"$skip": offset}, bson.M{"$limit": limit}},
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	var results interface{}
	if mediaId != "" && len(facetData) == 1 {
		results = facetData[0]
	} else {
		results = facetData
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": total,
		},
	})
}

func (app *Api) AddMedia(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("media")

	var payload models.MediaFormAdd
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	form, _ := c.MultipartForm()
	files := form.File["file"]

	var medias []interface{}
	for i := 0; i < payload.Rows; i++ {
		var media models.Media

		if len(payload.Title) > i {
			media.Title = payload.Title[i]
		}
		if len(payload.Caption) > i {
			media.Caption = payload.Caption[i]
		}
		if len(payload.Description) > i {
			media.Description = payload.Description[i]
		}
		if len(payload.Type) > i {
			media.Type = payload.Type[i]
		}
		if len(payload.Url) > i {
			media.Url = payload.Url[i]
		}
		if len(payload.AsGallery) > i {
			media.AsGallery, _ = strconv.ParseBool(payload.AsGallery[i])
		}

		if len(payload.BrandId) > i {
			bID, _ := primitive.ObjectIDFromHex(payload.BrandId[i])
			media.BrandId = &bID
		}
		if len(payload.PropertyId) > i {
			pID, _ := primitive.ObjectIDFromHex(payload.PropertyId[i])
			media.PropertyId = &pID
		}

		if len(files) > i {
			file := files[i]
			if file != nil {
				if media.Title == "" {
					media.Title = strings.TrimSuffix(file.Filename, filepath.Ext(file.Filename))
				}

				if media.Caption == "" {
					media.Caption = strings.TrimSuffix(file.Filename, filepath.Ext(file.Filename))
				}

				// Only the first 512 bytes are used to sniff the content type.
				buffer := make([]byte, 512)
				src, err := file.Open()
				if err != nil {
					c.JSON(http.StatusInternalServerError, &models.ReE{
						StatusCode: http.StatusInternalServerError,
						Error:      err.Error(),
					})
					return
				}
				defer src.Close()

				_, err = src.Read(buffer)
				if err != nil {
					c.JSON(http.StatusInternalServerError, &models.ReE{
						StatusCode: http.StatusInternalServerError,
						Error:      err.Error(),
					})
					return
				}

				filePrefix := ""
				if filetype.IsImage(buffer) {
					// handling for image icon type
					if media.Type == "" {
						media.Type = "image"
					}
					filePrefix = "img"
				} else if filetype.IsVideo(buffer) {
					media.Type = "video"
					filePrefix = "vdo"
				} else if filetype.IsDocument(buffer) || filetype.IsMIME(buffer, "application/pdf") || filetype.IsMIME(buffer, "application/zip") || filetype.IsMIME(buffer, "application/x-tar") || filetype.IsMIME(buffer, "application/x-rar-compressed") || filetype.IsMIME(buffer, "application/gzip") || filetype.IsMIME(buffer, "application/x-bzip2") || filetype.IsMIME(buffer, "application/x-7z-compressed") {
					media.Type = "attachment"
					filePrefix = "atch"
				} else {
					c.JSON(http.StatusBadRequest, &models.ReE{
						StatusCode: http.StatusBadRequest,
						Error:      "Unsupported file type.",
					})
					return
				}

				ext := filepath.Ext(file.Filename)
				uid := uuid.Must(uuid.NewV4())
				filename := fmt.Sprintf("%s-%s%s", filePrefix, uid, ext)
				dst := fmt.Sprintf("%s/%s", app.Config.Path.Media, filename)
				if err = c.SaveUploadedFile(file, dst); err != nil {
					c.JSON(http.StatusInternalServerError, &models.ReE{
						StatusCode: http.StatusInternalServerError,
						Error:      err.Error(),
					})
					return
				}
				media.File = filename
			}
		}

		if media.Type != "" && (media.Url != "" || media.File != "") {
			if media.Title != "" && media.Caption == "" {
				media.Caption = media.Title
			}

			now := time.Now()
			media.Registered = &now

			medias = append(medias, media)
		}
	}

	ctx := context.Background()
	result, err := collection.InsertMany(ctx, medias)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"media_ids":      result.InsertedIDs,
			"inserted_media": len(result.InsertedIDs),
		},
	})
}

func (app *Api) UpdateMedia(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("media")

	mediaId := c.Param("mediaId")
	objectID, _ := primitive.ObjectIDFromHex(mediaId)

	var media models.Media
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&media)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var brandID, propertyID string
	if media.BrandId != nil {
		brandID = media.BrandId.Hex()
	}
	if media.PropertyId != nil {
		propertyID = media.PropertyId.Hex()
	}
	var payload = models.MediaForm{
		Title:       media.Title,
		Caption:     media.Caption,
		Description: media.Description,
		Type:        media.Type,
		Url:         media.Url,
		AsGallery:   strconv.FormatBool(media.AsGallery),
		BrandId:     brandID,
		PropertyId:  propertyID,
	}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	media.Title = payload.Title
	media.Caption = payload.Caption
	media.Description = payload.Description
	media.Type = payload.Type
	media.Url = payload.Url
	media.AsGallery, _ = strconv.ParseBool(payload.AsGallery)

	if payload.BrandId != "" {
		bID, _ := primitive.ObjectIDFromHex(payload.BrandId)
		media.BrandId = &bID
	}

	if payload.PropertyId != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.PropertyId)
		media.PropertyId = &pID
	}

	var oldFile string
	// Upload Logo
	file, _ := c.FormFile("file")
	if file != nil {
		oldFile = media.File

		// Only the first 512 bytes are used to sniff the content type.
		buffer := make([]byte, 512)
		src, err := file.Open()
		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}
		defer src.Close()

		_, err = src.Read(buffer)
		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}

		filePrefix := ""
		if filetype.IsImage(buffer) {
			// handling for image icon type
			if media.Type == "" {
				media.Type = "image"
			}
			filePrefix = "img"
		} else if filetype.IsVideo(buffer) {
			media.Type = "video"
			filePrefix = "vdo"
		} else if filetype.IsDocument(buffer) || filetype.IsMIME(buffer, "application/pdf") || filetype.IsMIME(buffer, "application/zip") || filetype.IsMIME(buffer, "application/x-tar") || filetype.IsMIME(buffer, "application/x-rar-compressed") || filetype.IsMIME(buffer, "application/gzip") || filetype.IsMIME(buffer, "application/x-bzip2") || filetype.IsMIME(buffer, "application/x-7z-compressed") {
			media.Type = "attachment"
			filePrefix = "atch"
		} else {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      "Unsupported file type.",
			})
			return
		}

		ext := filepath.Ext(file.Filename)
		uid := uuid.Must(uuid.NewV4())
		media.File = fmt.Sprintf("%s-%s%s", filePrefix, uid, ext)
		dst := fmt.Sprintf("%s/%s", app.Config.Path.Media, media.File)
		if err = c.SaveUploadedFile(file, dst); err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}
	} else {
		if payload.Url != "" && oldFile != "" {
			media.File = ""
		}
	}

	now := time.Now()
	media.Modified = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": media})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	if oldFile != "" {
		var path string
		// delete File
		path = fmt.Sprintf("%s/%s", app.Config.Path.Media, oldFile)
		if exist := helpers.FileExists(path); exist {
			os.Remove(path)
		}
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"media":          media,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteMedia(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("media")

	mediaId := c.Param("mediaId")
	objectID, _ := primitive.ObjectIDFromHex(mediaId)

	var media models.Media
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&media)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	if media.File != "" {
		// delete logo
		path := fmt.Sprintf("%s/%s", app.Config.Path.Media, media.File)
		if exist := helpers.FileExists(path); exist {
			os.Remove(path)
		}
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"media_id":      mediaId,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) GetMediaGallery(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("media")

	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	id := c.Query("id")
	code := strings.ToUpper(c.Query("code"))
	codeSlug := strings.ToLower(c.Query("codeSlug"))
	mType := c.DefaultQuery("type", "image")

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "asc")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	matchFilter := bson.M{"as_gallery": true}
	if id != "" {
		oID, _ := primitive.ObjectIDFromHex(id)
		matchFilter["$or"] = bson.A{
			bson.M{"brand_id": oID},
			bson.M{"property_id": oID},
		}
	}
	if mType != "" {
		matchFilter["type"] = mType
	}
	if id == "" && code == "" && codeSlug == "" {
		code = "COR"
	}
	if codeSlug != "" {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_slug": codeSlug},
			bson.M{"property.property_slug": codeSlug},
		}
	} else {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_code": code},
			bson.M{"property.property_code": code},
		}
	}

	pipeline := bson.A{
		bson.M{"$lookup": bson.M{
			"as":           "brand",
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$brand",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "property",
			"from":         "properties",
			"localField":   "property_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$property",
			"preserveNullAndEmptyArrays": true,
		}},
	}
	pipeline = append(pipeline, bson.A{
		bson.M{"$match": matchFilter},
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$_id",
			"title":       "$title",
			"caption":     "$caption",
			"description": "$description",
			"type":        "$type",
			"url":         "$url",
			"file": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{"$file", ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$file",
					"local": bson.M{
						"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$file"},
					},
					"cloud": bson.M{
						"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$file"},
					},
				},
			}},
			"created_at":    "$registered",
			"updated_at":    "$modified",
			"property.id":   "$property._id",
			"property.code": "$property.property_code",
			"property.name": "$property.property_name",
			"property.slug": "$property.property_slug",
			"brand.id":      "$brand._id",
			"brand.code":    "$brand.brand_code",
			"brand.name":    "$brand.brand_name",
			"brand.slug":    "$brand.brand_slug",
		}},
		bson.M{"$facet": bson.M{
			"paginatedResults": facetPipelinePaging,
			"totalCount":       bson.A{bson.M{"$count": "count"}},
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"items": facetData,
			"total": total,
		},
	})
}
