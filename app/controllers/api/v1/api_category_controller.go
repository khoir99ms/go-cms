package v1

import (
	"alaric_cms/app/helpers"
	"alaric_cms/app/models"
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/mongodb/mongo-go-driver/mongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetCategory(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("categories")

	search := c.Query("search")
	lang := c.Query("lang")
	exceptID := c.Query("exceptId")
	/* brandID := c.Query("brandId")
	propertyID := c.Query("propertyId") */
	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "asc")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	matchFilter := bson.M{}
	if lang != "" {
		matchFilter["lang"] = lang
	}

	if exceptID != "" {
		eID, _ := primitive.ObjectIDFromHex(exceptID)
		matchFilter["_id"] = bson.M{"$ne": eID}
	}

	/* if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		matchFilter["property_id"] = pID
	}

	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["brand_id"] = bID
	} */

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
				}}}},
			},
			"as": "lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$lang",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "brand",
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$brand",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "property",
			"from":         "properties",
			"localField":   "property_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$property",
			"preserveNullAndEmptyArrays": true,
		}},
	}...)

	if search != "" {
		searchFilter := bson.M{}
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		searchFilter["$or"] = bson.A{
			bson.M{"name": searchRe},
			bson.M{"title": searchRe},
			bson.M{"slug": searchRe},
			bson.M{"lang": searchRe},
			bson.M{"created_at": searchRe},
			bson.M{"updated_at": searchRe},
		}
		pipeline = append(pipeline, bson.M{"$match": searchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$project": bson.M{
			"_id":       0,
			"id":        "$_id",
			"name":      "$name",
			"title":     "$title",
			"image":     "$image",
			"slug":      "$slug",
			"source_id": "$source_id",
			"lang": bson.M{
				"id":     "$lang._id",
				"code":   "$lang.code",
				"name":   "$lang.name",
				"native": "$lang.native",
				"status": "$lang.status",
			},
			"brand": bson.M{
				"id":   "$brand._id",
				"code": "$brand.brand_code",
				"name": "$brand.brand_name",
				"slug": "$brand.brand_slug",
			},
			"property": bson.M{
				"id":   "$property._id",
				"code": "$property.property_code",
				"name": "$property.property_name",
				"slug": "$property.property_slug",
			},
			"created_at": "$created_at",
		}},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": facetPipelinePaging,
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  facetData,
			"total": total,
		},
	})
}

func (app *Api) GetCategoryUsedPrimaryLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("categories")

	search := c.Query("search")
	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "-1")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	matchFilter := bson.M{}
	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"name": searchRe},
			bson.M{"title": searchRe},
			bson.M{"slug": searchRe},
			bson.M{"lang": searchRe},
			bson.M{"created_at": searchRe},
			bson.M{"updated_at": searchRe},
		}
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
					bson.M{"$eq": bson.A{"$status", "primary"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":    0,
					"id":     "$_id",
					"code":   "$code",
					"name":   "$name",
					"native": "$native",
					"status": "$status",
				}},
			},
			"as": "lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$lang",
			"preserveNullAndEmptyArrays": false,
		}},
	}...)

	if brandID != "" || propertyID != "" {
		extendsFilter := bson.A{
			bson.M{"$eq": bson.A{"$_id", "$$id"}},
		}
		if brandID != "" {
			bID, _ := primitive.ObjectIDFromHex(brandID)
			extendsFilter = append(extendsFilter, bson.M{"$eq": bson.A{"$extends.brand_id", bID}})
		}

		if propertyID != "" {
			pID, _ := primitive.ObjectIDFromHex(propertyID)
			extendsFilter = append(extendsFilter, bson.M{"$eq": bson.A{"$extends.property_id", pID}})
		}

		pipeline = append(pipeline, bson.A{
			bson.M{"$lookup": bson.M{
				"from": "categories",
				"let":  bson.M{"id": "$_id"},
				"pipeline": bson.A{
					bson.M{"$unwind": bson.M{
						"path":                       "$extends",
						"preserveNullAndEmptyArrays": true,
					}},
					bson.M{"$match": bson.M{"$expr": bson.M{"$and": extendsFilter}}},
					bson.M{"$project": bson.M{
						"_id":         0,
						"title":       "$extends.title",
						"image":       "$extends.image",
						"meta":        "$extends.meta",
						"description": "$extends.description",
					}},
				},
				"as": "extends",
			}},
			bson.M{"$unwind": bson.M{
				"path":                       "$extends",
				"preserveNullAndEmptyArrays": true,
			}},
		}...)
	} else {
		pipeline = append(pipeline, bson.M{
			"$project": bson.M{"extends": 0},
		})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$project": bson.M{
			"_id":  0,
			"id":   "$_id",
			"name": "$name",
			"title": bson.M{"$cond": bson.A{
				bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.title", ""}}, ""}},
				"$title",
				"$extends.title",
			}},
			"image": bson.M{"$cond": bson.A{
				bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.image", ""}}, ""}},
				"$image",
				"$extends.image",
			}},
			"description": bson.M{"$cond": bson.A{
				bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.description", ""}}, ""}},
				"$description",
				"$extends.description",
			}},
			"slug":       "$slug",
			"lang":       "$lang",
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
		bson.M{"$sort": sort},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": bson.A{bson.M{"$skip": offset}, bson.M{"$limit": limit}},
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  facetData,
			"total": total,
		},
	})
}

func (app *Api) GetMultipleLangCategoryByID(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("categories")

	categoryID := c.Param("id")
	cID, _ := primitive.ObjectIDFromHex(categoryID)
	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")

	matchFilter := bson.M{
		"_id": cID,
	}

	viewLangField := bson.M{
		"_id":    0,
		"id":     "$_id",
		"code":   "$code",
		"name":   "$name",
		"native": "$native",
		"status": "$status",
	}

	pipeline := bson.A{
		bson.M{"$match": matchFilter},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
				}}}},
				bson.M{"$project": viewLangField},
			},
			"as": "lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$lang",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$lookup": bson.M{
			"from": "categories",
			"let":  bson.M{"id": "$_id", "source_id": "$source_id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$or": bson.A{
					bson.M{"$eq": bson.A{"$_id", "$$source_id"}},
					bson.M{"$eq": bson.A{"$source_id", "$$id"}},
				}}}},
			},
			"as": "other_lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$other_lang",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$other_lang.lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
				}}}},
				bson.M{"$project": viewLangField},
			},
			"as": "other_lang.lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$other_lang.lang",
			"preserveNullAndEmptyArrays": true,
		}},
	}

	if brandID != "" || propertyID != "" {
		extendsFilter := bson.A{
			bson.M{"$eq": bson.A{"$_id", "$$id"}},
		}
		if brandID != "" {
			bID, _ := primitive.ObjectIDFromHex(brandID)
			extendsFilter = append(extendsFilter, bson.M{"$eq": bson.A{"$extends.brand_id", bID}})
		}

		if propertyID != "" {
			pID, _ := primitive.ObjectIDFromHex(propertyID)
			extendsFilter = append(extendsFilter, bson.M{"$eq": bson.A{"$extends.property_id", pID}})
		}

		pipeline = append(pipeline, bson.A{
			bson.M{"$lookup": bson.M{
				"from": "categories",
				"let":  bson.M{"id": "$_id"},
				"pipeline": bson.A{
					bson.M{"$unwind": bson.M{
						"path":                       "$extends",
						"preserveNullAndEmptyArrays": true,
					}},
					bson.M{"$match": bson.M{"$expr": bson.M{"$and": extendsFilter}}},
					bson.M{"$project": bson.M{
						"_id":         0,
						"title":       "$extends.title",
						"description": "$extends.description",
						"image":       "$extends.image",
						"label":       "$extends.label",
						"meta":        "$extends.meta",
					}},
				},
				"as": "extends",
			}},
			bson.M{"$unwind": bson.M{
				"path":                       "$extends",
				"preserveNullAndEmptyArrays": true,
			}},
		}...)

		pipeline = append(pipeline, bson.A{
			bson.M{"$lookup": bson.M{
				"from": "categories",
				"let":  bson.M{"id": "$other_lang._id"},
				"pipeline": bson.A{
					bson.M{"$unwind": bson.M{
						"path":                       "$extends",
						"preserveNullAndEmptyArrays": true,
					}},
					bson.M{"$match": bson.M{"$expr": bson.M{"$and": extendsFilter}}},
					bson.M{"$project": bson.M{
						"_id":         0,
						"title":       "$extends.title",
						"description": "$extends.description",
						"image":       "$extends.image",
						"label":       "$extends.label",
						"meta":        "$extends.meta",
					}},
				},
				"as": "other_lang.extends",
			}},
			bson.M{"$unwind": bson.M{
				"path":                       "$other_lang.extends",
				"preserveNullAndEmptyArrays": true,
			}},
		}...)
	} else {
		pipeline = append(pipeline, bson.M{
			"$project": bson.M{"extends": 0, "other_lang.extends": 0},
		})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$group": bson.M{
			"_id":  "$_id",
			"main": bson.M{"$first": "$$ROOT"},
			"other": bson.M{
				"$push": bson.M{"$cond": bson.A{
					bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$other_lang", bson.M{}}}, bson.M{}}},
					bson.M{
						"id":          "$other_lang._id",
						"name":        "$other_lang.name",
						"title":       bson.M{"$ifNull": bson.A{"$other_lang.extends.title", "$other_lang.title"}},
						"description": bson.M{"$ifNull": bson.A{"$other_lang.extends.description", "$other_lang.description"}},
						"image":       bson.M{"$ifNull": bson.A{"$other_lang.extends.image", "$other_lang.image"}},
						"slug":        "$other_lang.slug",
						"lang":        "$other_lang.lang",
						"page": bson.M{
							"use_validity":  bson.M{"$ifNull": bson.A{"$other_lang.page.use_validity", false}},
							"use_amenities": bson.M{"$ifNull": bson.A{"$other_lang.page.use_amenities", false}},
							"use_badge":     bson.M{"$ifNull": bson.A{"$other_lang.page.use_badge", false}},
						},
						"label": bson.M{
							"detail": bson.M{"$ifNull": bson.A{"$other_lang.extends.label.detail", "$other_lang.label.detail"}},
							"more":   bson.M{"$ifNull": bson.A{"$other_lang.extends.label.more", "$other_lang.label.more"}},
						},
						"meta": bson.M{
							"title":       bson.M{"$ifNull": bson.A{"$other_lang.extends.meta.title", "$other_lang.meta.title"}},
							"description": bson.M{"$ifNull": bson.A{"$other_lang.extends.meta.description", "$other_lang.meta.description"}},
							"keyword":     bson.M{"$ifNull": bson.A{"$other_lang.extends.meta.keyword", "$other_lang.meta.keyword"}},
						},
						"source_id":  "$other_lang.source_id",
						"created_at": "$other_lang.created_at",
						"updated_at": "$other_lang.updated_at",
					},
					"$not_exist_other_lang",
				}},
			},
		}},
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$main._id",
			"name":        "$main.name",
			"title":       bson.M{"$ifNull": bson.A{"$main.extends.title", "$main.title"}},
			"description": bson.M{"$ifNull": bson.A{"$main.extends.description", "$main.description"}},
			"image":       bson.M{"$ifNull": bson.A{"$main.extends.image", "$main.image"}},
			"slug":        "$main.slug",
			"lang":        "$main.lang",
			"page": bson.M{
				"use_validity":  bson.M{"$ifNull": bson.A{"$main.page.use_validity", false}},
				"use_amenities": bson.M{"$ifNull": bson.A{"$main.page.use_amenities", false}},
				"use_badge":     bson.M{"$ifNull": bson.A{"$main.page.use_badge", false}},
			},
			"label": bson.M{
				"detail": bson.M{"$ifNull": bson.A{"$main.extends.label.detail", "$main.label.detail"}},
				"more":   bson.M{"$ifNull": bson.A{"$main.extends.label.more", "$main.label.more"}},
			},
			"meta.title":       bson.M{"$ifNull": bson.A{"$main.extends.meta.title", "$main.meta.title"}},
			"meta.description": bson.M{"$ifNull": bson.A{"$main.extends.meta.description", "$main.meta.description"}},
			"meta.keyword":     bson.M{"$ifNull": bson.A{"$main.extends.meta.keyword", "$main.meta.keyword"}},
			"source_id":        "$main.source_id",
			"created_at":       "$main.created_at",
			"updated_at":       "$main.updated_at",
			"other_lang":       "$other",
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var categories bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&categories)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       categories,
	})
}

func (app *Api) GetCategoryGroupedLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("categories")

	pipeline := bson.A{
		bson.M{"$group": bson.M{
			"_id": "$lang",
			"items": bson.M{"$push": bson.M{
				"id":   "$_id",
				"name": "$name",
				"slug": "$slug",
				"lang": "$lang",
				"page": bson.M{
					"use_validity":  bson.M{"$ifNull": bson.A{"$page.use_validity", false}},
					"use_amenities": bson.M{"$ifNull": bson.A{"$page.use_amenities", false}},
					"use_badge":     bson.M{"$ifNull": bson.A{"$page.use_badge", false}},
				},
				"source_id": "$source_id",
			}},
		}},
		bson.M{"$addFields": bson.M{
			"categories": bson.A{bson.M{
				"k": "$_id",
				"v": "$items",
			}},
		}},
		bson.M{"$replaceRoot": bson.M{
			"newRoot": bson.M{"$arrayToObject": "$categories"},
		}},
	}

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var categories bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&categories)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       categories,
	})
}

func (app *Api) AddCategory(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("categories")

	var payload models.CategoryForm
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	ctx := context.Background()
	category, err := app.validateCategoryAddPayload(c, payload)
	result, err := collection.InsertOne(ctx, category)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"id": result.InsertedID,
		},
	})
}

func (app *Api) validateCategoryAddPayload(c *gin.Context, payload models.CategoryForm) (*models.Category, error) {
	collection := app.Config.MongoDB.Collection("categories")

	category := &models.Category{
		Name:        payload.Name,
		Title:       payload.Title,
		Description: payload.Description,
		Image:       payload.Image,
		Lang:        payload.Lang,
		Label: models.CategoryLabel{
			Detail: payload.LabelDetail,
			More:   payload.LabelMore,
		},
		Meta: models.CategoryMeta{
			Title:       payload.MetaTitle,
			Keyword:     payload.MetaKeyword,
			Description: payload.MetaDescription,
		},
	}

	regex, _ := regexp.Compile(`[ ]+`)
	category.Name = strings.Trim(regex.ReplaceAllString(category.Name, " "), " ")
	category.Title = strings.Trim(regex.ReplaceAllString(category.Title, " "), " ")

	if category.Title == "" {
		category.Title = category.Name
	}

	if payload.UseValidity != "" {
		category.Page.UseValidity, _ = strconv.ParseBool(payload.UseValidity)
	}

	if payload.UseAmenities != "" {
		category.Page.UseAmenities, _ = strconv.ParseBool(payload.UseAmenities)
	}

	if payload.UseBadge != "" {
		category.Page.UseBadge, _ = strconv.ParseBool(payload.UseBadge)
	}

	if category.Meta.Title == "" {
		category.Meta.Title = category.Title
	}

	if payload.SourceID != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.SourceID)
		category.SourceID = &pID
	}

	ctx := context.Background()
	cursor, err := collection.Find(ctx, bson.M{
		"name": category.Name,
		"lang": category.Lang,
	})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		return nil, errors.New("category already exists.")
	}

	searchName := primitive.Regex{Pattern: category.Name, Options: "i"}
	count, _ := collection.CountDocuments(ctx, bson.M{"name": searchName})

	category.Slug = helpers.Slugify(category.Name)
	if count > 0 {
		for i := (count - 1); i < (count + 10); i++ {
			countSlug, _ := collection.CountDocuments(ctx, bson.M{"slug": category.Slug})
			if countSlug == 0 {
				break
			} else {
				category.Slug = fmt.Sprintf("%s-%d", category.Slug, i)
			}
		}
	}

	now := time.Now()
	category.CreatedAt = &now

	return category, nil
}

func (app *Api) UpdateCategory(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("categories")

	categoryID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(categoryID)

	category := &models.Category{}
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&category)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var payload = models.CategoryForm{
		Name:            category.Name,
		Slug:            category.Slug,
		Title:           category.Title,
		Description:     category.Description,
		Image:           category.Image,
		Lang:            category.Lang,
		UseValidity:     strconv.FormatBool(category.Page.UseValidity),
		UseAmenities:    strconv.FormatBool(category.Page.UseAmenities),
		UseBadge:        strconv.FormatBool(category.Page.UseBadge),
		LabelDetail:     category.Label.Detail,
		LabelMore:       category.Label.More,
		MetaTitle:       category.Meta.Title,
		MetaKeyword:     category.Meta.Keyword,
		MetaDescription: category.Meta.Description,
	}
	if category.SourceID != nil {
		payload.SourceID = category.SourceID.Hex()
	}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	category, err = app.validateCategoryUpdatePayload(c, categoryID, payload, category)
	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": category})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"category":       category,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) validateCategoryUpdatePayload(c *gin.Context, categoryID string, payload models.CategoryForm, category *models.Category) (*models.Category, error) {
	collection := app.Config.MongoDB.Collection("categories")

	ctx := context.Background()
	objectID, _ := primitive.ObjectIDFromHex(categoryID)

	var regex, _ = regexp.Compile(`[ ]+`)
	payload.Name = strings.Trim(regex.ReplaceAllString(payload.Name, " "), " ")
	payload.Title = strings.Trim(regex.ReplaceAllString(payload.Title, " "), " ")

	// change category slug
	if category.Name != payload.Name {
		searchName := primitive.Regex{Pattern: payload.Name, Options: "i"}
		count, _ := collection.CountDocuments(ctx, bson.M{
			"_id":  bson.M{"$ne": objectID},
			"name": searchName,
		})

		if count > 0 {
			// generate slug from title
			category.Slug = helpers.Slugify(payload.Name)
			for i := (count - 1); i < (count + 10); i++ {
				countSlug, _ := collection.CountDocuments(ctx, bson.M{"slug": category.Slug})
				if countSlug == 0 {
					break
				} else {
					category.Slug = fmt.Sprintf("%s-%d", category.Slug, i)
				}
			}
		}
	}

	category.Name = payload.Name
	category.Title = payload.Title
	category.Description = payload.Description
	category.Image = payload.Image
	category.Lang = payload.Lang
	category.Page.UseValidity, _ = strconv.ParseBool(payload.UseValidity)
	category.Page.UseAmenities, _ = strconv.ParseBool(payload.UseAmenities)
	category.Page.UseBadge, _ = strconv.ParseBool(payload.UseBadge)
	category.Label.Detail = payload.LabelDetail
	category.Label.More = payload.LabelMore
	category.Meta.Title = payload.MetaTitle
	category.Meta.Keyword = payload.MetaKeyword
	category.Meta.Description = payload.MetaDescription

	cursor, err := collection.Find(ctx, bson.M{
		"_id": bson.M{"$ne": objectID},
		"$and": bson.A{
			bson.M{"name": category.Name},
			bson.M{"lang": category.Lang},
		},
	})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		return nil, errors.New("category already exists.")
	}

	now := time.Now()
	category.UpdatedAt = &now

	return category, nil
}

func (app *Api) UpdateCategoryBatch(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("categories")

	var batchPayload []models.CategoryForm
	if err := c.ShouldBind(&batchPayload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	ctx := context.Background()
	var categories []interface{}
	var operations []mongo.WriteModel
	for _, payload := range batchPayload {
		if payload.ID == "" && payload.SourceID != "" {
			category, err := app.validateCategoryAddPayload(c, payload)
			if err != nil {
				c.JSON(http.StatusInternalServerError, &models.ReE{
					StatusCode: http.StatusInternalServerError,
					Error:      err.Error(),
				})
				return
			}

			operation := mongo.NewInsertOneModel()
			operation.SetDocument(category)
			operations = append(operations, operation)
			categories = append(categories, category)
		} else {
			categoryID, _ := primitive.ObjectIDFromHex(payload.ID)

			var category = &models.Category{}
			err := collection.FindOne(ctx, bson.M{"_id": categoryID}).Decode(&category)
			if err != nil {
				c.JSON(http.StatusInternalServerError, &models.ReE{
					StatusCode: http.StatusInternalServerError,
					Error:      err.Error(),
				})
				return
			}
			category, err = app.validateCategoryUpdatePayload(c, payload.ID, payload, category)
			if err != nil {
				c.JSON(http.StatusInternalServerError, &models.ReE{
					StatusCode: http.StatusInternalServerError,
					Error:      err.Error(),
				})
				return
			}

			operation := mongo.NewUpdateOneModel()
			operation.SetFilter(bson.M{"_id": categoryID})
			operation.SetUpdate(category)
			operations = append(operations, operation)
			categories = append(categories, category)
		}
	}

	results, err := collection.BulkWrite(ctx, operations)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"categories":     categories,
			"inserted_count": results.InsertedCount,
			"matched_count":  results.MatchedCount,
			"modified_count": results.ModifiedCount,
		},
	})
}

func (app *Api) UpdateCategoryExtendsBatch(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("categories")

	var batchPayload []models.CategoryExtendForm
	if err := c.ShouldBind(&batchPayload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	ctx := context.Background()
	var categories []interface{}
	var operations []mongo.WriteModel
	for _, payload := range batchPayload {
		categoryID, _ := primitive.ObjectIDFromHex(payload.ID)

		var category = &models.Category{}
		err := collection.FindOne(ctx, bson.M{"_id": categoryID}).Decode(&category)
		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}

		categoryExt := models.CategoryExtend{
			Title:       payload.Title,
			Description: payload.Description,
			Image:       payload.Image,
			Label: models.CategoryLabel{
				Detail: payload.LabelDetail,
				More:   payload.LabelMore,
			},
			Meta: models.CategoryMeta{
				Title:       payload.MetaTitle,
				Keyword:     payload.MetaKeyword,
				Description: payload.MetaDescription,
			},
		}
		regex, _ := regexp.Compile(`[ ]+`)
		categoryExt.Title = strings.Trim(regex.ReplaceAllString(categoryExt.Title, " "), " ")

		if payload.BrandID != "" {
			bID, _ := primitive.ObjectIDFromHex(payload.BrandID)
			categoryExt.BrandID = &bID
		}

		if payload.PropertyID != "" {
			pID, _ := primitive.ObjectIDFromHex(payload.PropertyID)
			categoryExt.PropertyID = &pID
		}

		var exists = false
		for i, ext := range category.Extends {
			if ext.BrandID != nil && *ext.BrandID == *categoryExt.BrandID {
				category.Extends[i] = categoryExt
				exists = true
				break
			} else if ext.PropertyID != nil && *ext.PropertyID == *categoryExt.PropertyID {
				category.Extends[i] = categoryExt
				exists = true
				break
			}
		}

		if !exists {
			category.Extends = append(category.Extends, categoryExt)
		}

		operation := mongo.NewUpdateOneModel()
		operation.SetFilter(bson.M{"_id": categoryID})
		operation.SetUpdate(category)
		operations = append(operations, operation)
		categories = append(categories, category)
	}

	results, err := collection.BulkWrite(ctx, operations)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"categories":     categories,
			"inserted_count": results.InsertedCount,
			"matched_count":  results.MatchedCount,
			"modified_count": results.ModifiedCount,
		},
	})
}

func (app *Api) DeleteCategory(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("categories")

	categoryId := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(categoryId)

	ctx := context.Background()
	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            categoryId,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) DeleteMultipleLangCategoryByID(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("categories")

	categoryId := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(categoryId)

	ctx := context.Background()
	doc, err := collection.DeleteMany(ctx, bson.M{"$or": bson.A{
		bson.M{"_id": objectID},
		bson.M{"source_id": objectID},
	}})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            categoryId,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) fetchCategory(params map[string]string) (bson.M, error) {
	collection := app.Config.MongoDB.Collection("categories")

	host, ok := params["host"]
	if !ok {
		host = ""
	}

	// this is an optional parameter
	ID, ok := params["id"]
	if !ok {
		ID = ""
	}
	slug, ok := params["slug"]
	if !ok {
		slug = ""
	}
	lang, ok := params["lang"]
	if !ok {
		lang = ""
	}
	code, ok := params["code"]
	if !ok {
		code = ""
	} else {
		code = strings.ToUpper(code)
	}
	codeSlug, ok := params["codeSlug"]
	if !ok {
		codeSlug = ""
	} else {
		codeSlug = strings.ToLower(codeSlug)
	}

	page, ok := params["page"]
	if !ok {
		page = ""
	}
	pageLimit, ok := params["limit"]
	if !ok {
		pageLimit = ""
	}
	sortCol, ok := params["sortCol"]
	if !ok {
		sortCol = "id"
	}
	sortDir, ok := params["sortDir"]
	if !ok {
		sortDir = "asc"
	}

	matchFilter := bson.M{}
	if ID != "" {
		cID, _ := primitive.ObjectIDFromHex(ID)
		matchFilter["_id"] = cID
	}
	if slug != "" {
		matchFilter["slug"] = slug
	}
	if lang != "" {
		matchFilter["lang"] = lang
	}

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	pipeline := bson.A{
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
					bson.M{"$eq": bson.A{"$status", "primary"}},
				}}}},
			},
			"as": "is_primary_lang",
		}},
		bson.M{"$unwind": bson.M{"path": "$is_primary_lang"}},
		bson.M{"$lookup": bson.M{
			"from": "categories",
			"let":  bson.M{"id": "$_id", "source_id": "$source_id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$or": bson.A{
					bson.M{"$eq": bson.A{"$source_id", "$$id"}},
					bson.M{"$eq": bson.A{"$_id", "$$source_id"}},
				}}}},
			},
			"as": "related_categories",
		}},
		bson.M{"$unwind": bson.M{"path": "$related_categories", "preserveNullAndEmptyArrays": true}},
		bson.M{"$group": bson.M{
			"_id":       "$_id",
			"primary":   bson.M{"$first": "$$ROOT"},
			"secondary": bson.M{"$push": "$related_categories"},
		}},
		bson.M{"$project": bson.M{
			"categories": bson.M{"$concatArrays": bson.A{bson.A{"$primary"}, "$secondary"}},
		}},
		bson.M{"$unwind": bson.M{"path": "$categories", "preserveNullAndEmptyArrays": false}},
		bson.M{"$replaceRoot": bson.M{"newRoot": "$categories"}},
	}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	if code != "" || codeSlug != "" {
		matchFilterExtends := bson.M{}
		if codeSlug != "" {
			matchFilterExtends["$or"] = bson.A{
				bson.M{"brand.brand_slug": codeSlug},
				bson.M{"property.property_slug": codeSlug},
			}
		} else if code != "" {
			matchFilterExtends["$or"] = bson.A{
				bson.M{"brand.brand_code": code},
				bson.M{"property.property_code": code},
			}
		}

		pipeline = append(pipeline, bson.A{
			bson.M{"$lookup": bson.M{
				"from": "categories",
				"let":  bson.M{"id": "$_id"},
				"pipeline": bson.A{
					bson.M{"$unwind": bson.M{"path": "$extends", "preserveNullAndEmptyArrays": true}},
					bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
						bson.M{"$eq": bson.A{"$_id", "$$id"}},
					}}}},
					bson.M{"$lookup": bson.M{
						"from": "brands", "as": "brand",
						"localField": "extends.brand_id", "foreignField": "_id",
					}},
					bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
					bson.M{"$lookup": bson.M{
						"from": "properties", "as": "property",
						"localField": "extends.property_id", "foreignField": "_id",
					}},
					bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
					bson.M{"$match": matchFilterExtends},
					bson.M{"$project": bson.M{
						"_id":         0,
						"title":       "$extends.title",
						"description": "$extends.description",
						"image":       "$extends.image",
						"label":       "$extends.label",
						"meta":        "$extends.meta",
					}},
				},
				"as": "extends",
			}},
			bson.M{"$unwind": bson.M{"path": "$extends", "preserveNullAndEmptyArrays": true}},
		}...)
	} else {
		pipeline = append(pipeline, bson.M{
			"$project": bson.M{"extends": 0},
		})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$addFields": bson.M{
			"image": bson.M{"$ifNull": bson.A{"$extends.image", "$image"}},
		}},
		bson.M{"$project": bson.M{
			"_id":  0,
			"id":   "$_id",
			"name": "$name",
			"title": bson.M{"$cond": bson.A{
				bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.title", ""}}, ""}},
				"$title",
				"$extends.title",
			}},
			"description": bson.M{"$cond": bson.A{
				bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.description", ""}}, ""}},
				"$description",
				"$extends.description",
			}},
			"image": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$image", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$image",
					"local": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$image", 0, 4}}, "http"}},
						"$image",
						bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$image"},
						},
					}},
					"cloud": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$image", 0, 4}}, "http"}},
						"$image",
						bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$image"},
						},
					}},
				},
			}},
			"label": bson.M{"$cond": bson.A{
				bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.label.detail", ""}}, ""}},
					bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.meta.more", ""}}, ""}},
				}},
				"$label",
				"$extends.label",
			}},
			"meta": bson.M{"$cond": bson.A{
				bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$extends.meta.title", ""}}, ""}},
				"$meta",
				"$extends.meta",
			}},
			"slug":       "$slug",
			"lang":       "$lang",
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": facetPipelinePaging,
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer cursor.Close(ctx)

	var results bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&results)
	}

	return results, nil
}

func (app *Api) GetCategoryByID(c *gin.Context) {
	// this is an optional parameter
	ID := c.Param("id")
	code := c.DefaultQuery("code", "cor")
	codeSlug := c.Query("codeSlug")

	params := map[string]string{
		"id":       ID,
		"code":     code,
		"codeSlug": codeSlug,
	}

	categories, err := app.fetchCategory(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	facetData := categories["paginatedResults"].(primitive.A)

	results := make(bson.M)
	if len(facetData) > 0 {
		results = facetData[0].(bson.M)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}

func (app *Api) GetCategoryBySlug(c *gin.Context) {
	// this is an optional parameter
	slug := c.Param("slug")
	code := c.DefaultQuery("code", "cor")
	codeSlug := c.Query("codeSlug")

	params := map[string]string{
		"slug":     slug,
		"code":     code,
		"codeSlug": codeSlug,
	}

	categories, err := app.fetchCategory(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	facetData := categories["paginatedResults"].(primitive.A)

	results := make(bson.M)
	if len(facetData) > 0 {
		results = facetData[0].(bson.M)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}
