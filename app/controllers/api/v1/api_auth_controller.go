package v1

import (
	"alaric_cms/app/models"
	"alaric_cms/services"
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
)

func (app *Api) Login(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("users")

	var payload models.UserAuth
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var doc models.User
	err := collection.FindOne(context.Background(), bson.M{
		"user_login":  payload.UserLogin,
		"user_status": "active",
	}).Decode(&doc)
	if err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      "Invalid user login.",
		})
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(doc.UserPassword), []byte(payload.UserPassword))
	if err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      "Invalid user password.",
		})
		return
	}

	// Create the token
	// app.Config.Jwt.Expire is in minute
	token, err := services.CreateJWT(doc.UserId.Hex(), app.Config.AppName, app.Config.Jwt.SecretKey, app.Config.Jwt.Expire)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var brandId string
	var propertyId string

	if doc.BrandId != nil {
		brandId = doc.BrandId.Hex()
	}

	if doc.PropertyId != nil {
		propertyId = doc.PropertyId.Hex()
	}

	c.JSON(200, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "Login successfully",
		Data: gin.H{
			"token": token,
			"user": gin.H{
				"user_id":         doc.UserId.Hex(),
				"user_login":      doc.UserLogin,
				"user_firstname":  doc.UserFirstname,
				"user_lastname":   doc.UserLastname,
				"user_email":      doc.UserEmail,
				"user_role":       doc.UserRole,
				"user_status":     doc.UserStatus,
				"brand_id":        brandId,
				"property_id":     propertyId,
				"user_registered": doc.UserRegistered,
			},
		},
	})
}

func (app *Api) ForgotPassword(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("users")

	var payload models.UserForgotPassword
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var doc models.User
	err := collection.FindOne(context.Background(), bson.M{
		"user_email":  payload.UserEmail,
		"user_status": "active",
	}).Decode(&doc)

	if err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      fmt.Sprintf("Sorry, no email %s exists on system.", payload.UserEmail),
		})
		return
	}

	// Create the token
	// app.Config.Jwt.ExpireForgotPassword is in 15 minute
	token, err := services.CreateJWT(doc.UserId.Hex(), app.Config.AppName, app.Config.Jwt.SecretResetPasswordKey, app.Config.Jwt.ExpireForgotPassword)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	baseURL := app.Config.BaseURL
	if app.Config.Environment != "production" {
		url := location.Get(c)
		baseURL = fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)
	}

	to := []string{}
	to = append(to, doc.UserEmail)
	subject := fmt.Sprintf("%s - Forgot Password", app.Config.AppName)
	action := fmt.Sprintf("%s/auth/reset/%s", baseURL, token)
	message := fmt.Sprintf(`
		<h4>Reset Your Password</h4>
		<p>Hi %s, <br>You have made a request to reset password. Please click this link to reset your password. <a href="%s" target="_blank">click here</a></p>
		<p>Thank you.</p>
	`, doc.UserEmail, action)

	params := map[string]interface{}{
		"smtp":    app.Config.SMTP.Host,
		"port":    app.Config.SMTP.Port,
		"from":    app.Config.Mail.Sender,
		"to":      to,
		"subject": subject,
		"message": message,
		"format":  "text/html",
	}

	_, err = services.SendMail(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(200, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    fmt.Sprintf("Account recovery email sent to <strong>%s</strong>.", doc.UserEmail),
	})
}

func (app *Api) ResetPassword(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("users")

	var payload models.UserResetPassword
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	if payload.UserPassword != payload.UserRetypePassword {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      "Retype password not match.",
		})
		return
	}

	resetID, _ := c.Get("ResetID")
	userID, _ := primitive.ObjectIDFromHex(resetID.(string))

	var user models.User
	err := collection.FindOne(context.Background(), bson.M{"_id": userID, "user_status": "active"}).Decode(&user)

	if err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      fmt.Sprintf("Sorry, no user exists on system."),
		})
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(payload.UserPassword), 5)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	now := time.Now()
	user.UserPassword = string(hash)
	user.UserModified = &now

	doc, err := collection.UpdateOne(context.Background(), bson.M{"_id": userID}, bson.M{"$set": user})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "Reset password successfully. <a href=\"/auth\">Sign In</a>",
		Data: gin.H{
			"user_id":        resetID,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) GetUsers(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("users")
	userRole, _ := c.Get("UserRole")

	userId := c.Query("id")
	search := c.Query("search")
	sortCol := c.DefaultQuery("sortCol", "_id")
	sortDir := c.DefaultQuery("sortDir", "-1")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	pipeline := bson.A{}

	matchFilter := bson.M{}
	if userId != "" {
		objectID, _ := primitive.ObjectIDFromHex(userId)
		matchFilter["_id"] = objectID
	}

	if userRole.(int) == app.Config.Role.Brand {
		matchFilter["user_role"] = bson.M{
			"$nin": bson.A{app.Config.Role.Corporate},
		}
	} else if userRole.(int) == app.Config.Role.Hotel {
		/* matchFilter["user_role"] = bson.M{
			"$nin": bson.A{app.Config.Role.Corporate, app.Config.Role.Brand},
		} */
		userID, _ := c.Get("UserId")
		uID, _ := primitive.ObjectIDFromHex(userID.(string))
		matchFilter["_id"] = uID
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"user_firstname": searchRe},
			bson.M{"user_lastname": searchRe},
			bson.M{"user_login": searchRe},
			bson.M{"user_email": searchRe},
			bson.M{"user_role": searchRe},
			bson.M{"user_status": searchRe},
			bson.M{"user_registered": searchRe},
			bson.M{"user_modified": searchRe},
		}
	}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{
			"$lookup": bson.M{
				"as":           "brand",
				"from":         "brands",
				"localField":   "brand_id",
				"foreignField": "_id",
			},
		},
		bson.M{
			"$unwind": bson.M{
				"path":                       "$brand",
				"preserveNullAndEmptyArrays": true,
			},
		},
		bson.M{
			"$lookup": bson.M{
				"as":           "property",
				"from":         "properties",
				"localField":   "property_id",
				"foreignField": "_id",
			},
		},
		bson.M{
			"$unwind": bson.M{
				"path":                       "$property",
				"preserveNullAndEmptyArrays": true,
			},
		},
		bson.M{
			"$addFields": bson.M{
				"user_id":              "$_id",
				"brand.brand_id":       "$brand._id",
				"property.property_id": "$property._id",
			},
		},
		bson.M{
			"$project": bson.M{
				"_id":                           0,
				"brand_id":                      0,
				"property_id":                   0,
				"brand._id":                     0,
				"brand.brand_tagline":           0,
				"brand.brand_content":           0,
				"brand.brand_description":       0,
				"brand.brand_logo":              0,
				"brand.brand_featured_image":    0,
				"brand.brand_image_attachment":  0,
				"property._id":                  0,
				"property.brand_id":             0,
				"property.property_phone":       0,
				"property.property_fax":         0,
				"property.property_address":     0,
				"property.property_tagline":     0,
				"property.property_description": 0,
				"property.property_logo":        0,
			},
		},
		bson.M{"$sort": sort},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": bson.A{bson.M{"$skip": offset}, bson.M{"$limit": limit}},
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	var results interface{}
	if userId != "" && len(facetData) == 1 {
		results = facetData[0]
	} else {
		results = facetData
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": total,
		},
	})
}

func (app *Api) RegisterUser(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("users")

	var payload models.User
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	ctx := context.Background()
	cursor, err := collection.Find(ctx, bson.M{
		"$or": bson.A{bson.M{"user_login": payload.UserLogin}, bson.M{"user_email": payload.UserEmail}},
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "User login or email already exists.",
		})
		return
	}

	now := time.Now()
	payload.UserRegistered = &now
	payload.UserStatus = "active"
	hash, err := bcrypt.GenerateFromPassword([]byte(payload.UserPassword), 5)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	payload.UserPassword = string(hash)

	result, err := collection.InsertOne(ctx, payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"user_id": result.InsertedID,
		},
	})
}

func (app *Api) UpdateUser(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("users")

	userId := c.Param("userId")
	objectID, _ := primitive.ObjectIDFromHex(userId)

	var user models.UserUpdate
	ctx := context.Background()

	if err := c.ShouldBind(&user); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	cursor, err := collection.Find(ctx, bson.M{
		"_id": bson.M{"$ne": objectID},
		"$or": bson.A{bson.M{"user_login": user.UserLogin}, bson.M{"user_email": user.UserEmail}},
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "User login or email already exists.",
		})
		return
	}

	now := time.Now()
	user.UserModified = &now

	unset := bson.M{}
	if user.BrandId == nil {
		unset["brand_id"] = 1
	}
	if user.PropertyId == nil {
		unset["property_id"] = 1
	}

	update := bson.M{"$set": user}
	if len(unset) > 0 {
		update["$unset"] = unset
	}

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, update)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"user_id":        userId,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) ChangeUserPassword(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("users")

	userId := c.Param("userId")
	objectID, _ := primitive.ObjectIDFromHex(userId)

	var payload models.UserChangePassword
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	if payload.UserPassword != payload.UserRetypePassword {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      "Retype password not match.",
		})
		return
	}

	var user models.User
	err := collection.FindOne(context.Background(), bson.M{"_id": objectID}).Decode(&user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.UserPassword), []byte(payload.UserCurrentPassword))
	if err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      "Current password not match.",
		})
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(payload.UserPassword), 5)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	now := time.Now()
	user.UserPassword = string(hash)
	user.UserModified = &now

	doc, err := collection.UpdateOne(context.Background(), bson.M{"_id": objectID}, bson.M{"$set": user})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"user_id":        userId,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteUser(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("users")

	userId := c.Param("userId")
	objectID, _ := primitive.ObjectIDFromHex(userId)

	doc, err := collection.DeleteOne(context.Background(), bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"user_id":       userId,
			"deleted_count": doc.DeletedCount,
		},
	})
}
