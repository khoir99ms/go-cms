package v1

import (
	"alaric_cms/app/models"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	search := c.Query("search")
	langID := c.Query("id")
	langCode := c.Query("code")
	status := c.Query("status")
	sortCol := c.DefaultQuery("sortCol", "code")
	sortDir := c.DefaultQuery("sortDir", "asc")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	// Pass these options to the Find method
	findOptions := options.Find()
	findOptions.SetLimit(limit)
	findOptions.SetSkip(offset)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}
	findOptions.Sort = sort

	findFilter := bson.M{
		"type": "lang",
	}
	if langID != "" {
		objectID, _ := primitive.ObjectIDFromHex(langID)
		findFilter["_id"] = objectID
	}
	if langCode != "" {
		findFilter["code"] = langCode
	}
	if status != "" {
		findFilter["status"] = status
	}
	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		findFilter["$or"] = bson.A{
			bson.M{"code": searchRe},
			bson.M{"name": searchRe},
			bson.M{"native": searchRe},
		}
	}

	ctx := context.Background()
	cursor, err := collection.Find(
		ctx,
		findFilter,
		findOptions,
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var languages []models.Language
	cursor.All(ctx, &languages)
	count, _ := collection.CountDocuments(ctx, findFilter)

	var results interface{}
	if (langID != "" || langCode != "") && len(languages) == 1 {
		results = languages[0]
	} else {
		results = languages
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": count,
		},
	})
}

func (app *Api) GetLangList(c *gin.Context) {
	// Open our jsonFile
	jsonLang, err := os.Open("./config/languages.json")
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonLang.Close()

	// read our opened json as a byte array.
	jsonByte, _ := ioutil.ReadAll(jsonLang)

	var lang bson.M
	json.Unmarshal(jsonByte, &lang)

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       lang,
	})
}

func (app *Api) AddLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	var payload models.Language
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}
	payload.Type = "lang"

	ctx := context.Background()
	cursor, err := collection.Find(ctx, bson.M{"code": payload.Code, "type": payload.Type})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Language already activated.",
		})
		return
	}

	count, _ := collection.CountDocuments(ctx, bson.M{"type": payload.Type})
	if count == 0 {
		payload.Status = "primary"
	} else {
		payload.Status = "secondary"
	}

	now := time.Now()
	payload.CreatedAt = &now

	result, err := collection.InsertOne(ctx, payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"id": result.InsertedID,
		},
	})
}

func (app *Api) ChangeStatusLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	langID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(langID)

	var language models.Language
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID, "type": "lang"}).Decode(&language)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	if err := c.ShouldBind(&language); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	if language.Status == "primary" {
		_, err = collection.UpdateMany(ctx, bson.M{
			"_id":  bson.M{"$ne": objectID},
			"type": language.Type,
		}, bson.M{"$set": bson.M{"status": "secondary"}})
		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}
	}

	now := time.Now()
	language.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": language})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"language":       language,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	langID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(langID)

	ctx := context.Background()
	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID, "type": "lang"})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            langID,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) GetActiveLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	langID := c.Query("id")
	langCode := c.Query("code")
	sortCol := c.DefaultQuery("sortCol", "status name")
	sortDir := c.DefaultQuery("sortDir", "asc")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Pass these options to the Find method
	findOptions := options.Find()
	findOptions.SetProjection(bson.M{
		"type":       0,
		"created_at": 0,
		"updated_at": 0,
	})
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}
	findOptions.Sort = sort

	findFilter := bson.M{
		"type": "lang",
	}
	if langID != "" {
		objectID, _ := primitive.ObjectIDFromHex(langID)
		findFilter["_id"] = objectID
	}
	if langCode != "" {
		findFilter["code"] = strings.ToLower(langCode)
	}

	ctx := context.Background()
	cursor, err := collection.Find(
		ctx,
		findFilter,
		findOptions,
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var languages []models.Language
	cursor.All(ctx, &languages)

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       languages,
	})
}
