package v1

import (
	conf "alaric_cms/config"

	"github.com/gin-gonic/gin"
)

type Api struct {
	Gin    *gin.Engine
	Config *conf.Config
}
