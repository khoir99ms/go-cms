package v1

import (
	"alaric_cms/app/helpers"
	"alaric_cms/app/models"
	"context"
	"fmt"
	"log"
	"net/http"
	neturl "net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetProperty(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("properties")

	// this is an optional parameter
	propertyId := c.Query("id")
	propertyCode := c.Query("code")
	brandId := c.Query("brandId")
	onlyHotel := c.DefaultQuery("onlyHotel", "false")

	search := c.Query("search")
	sortCol := c.DefaultQuery("sortCol", "_id")
	sortDir := c.DefaultQuery("sortDir", "-1")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	pipeline := bson.A{}

	matchFilter := bson.M{}
	if propertyId != "" {
		objectID, _ := primitive.ObjectIDFromHex(propertyId)
		matchFilter["_id"] = bson.M{"$eq": objectID}
	}
	if propertyCode != "" {
		matchFilter["property_code"] = bson.M{"$eq": propertyCode}
	}
	if brandId != "" {
		oBrandID, _ := primitive.ObjectIDFromHex(brandId)
		matchFilter["brand_id"] = bson.M{"$eq": oBrandID}
	}
	if onlyHotel == "true" {
		matchFilter["property_code"] = bson.M{"$ne": "COR"}
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"property_code": searchRe},
			bson.M{"property_name": searchRe},
			bson.M{"property_phone": searchRe},
			bson.M{"property_fax": searchRe},
			bson.M{"property_email": searchRe},
			bson.M{"property_registered": searchRe},
		}
	}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}
	pipeline = append(pipeline, bson.A{
		bson.M{
			"$lookup": bson.M{
				"as":           "brand",
				"from":         "brands",
				"localField":   "brand_id",
				"foreignField": "_id",
			},
		},
		bson.M{
			"$unwind": bson.M{
				"path":                       "$brand",
				"preserveNullAndEmptyArrays": true,
			},
		},
		bson.M{
			"$lookup": bson.M{
				"as":           "city",
				"from":         "cities",
				"localField":   "city_id",
				"foreignField": "code",
			},
		},
		bson.M{
			"$unwind": bson.M{
				"path":                       "$city",
				"preserveNullAndEmptyArrays": true,
			},
		},
		bson.M{
			"$addFields": bson.M{"property_id": "$_id", "brand.brand_id": "$brand._id", "city.id": "$city._id"},
		},
		bson.M{
			"$project": bson.M{
				"_id":       0,
				"brand._id": 0,
				"brand_id":  0,
				"city._id":  0,
				"city_id":   0,
			},
		},
		bson.M{"$sort": sort},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": bson.A{bson.M{"$skip": offset}, bson.M{"$limit": limit}},
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	var results interface{}
	if (propertyId != "" || propertyCode != "") && len(facetData) == 1 {
		results = facetData[0]
	} else {
		results = facetData
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": total,
		},
	})
}

func (app *Api) AddProperty(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("properties")

	var payload models.Property
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var regex, _ = regexp.Compile(`[ ]+`)
	payload.PropertyName = strings.Trim(regex.ReplaceAllString(payload.PropertyName, " "), " ")
	payload.PropertyCode = strings.ToUpper(payload.PropertyCode)

	for _, facility := range payload.Facilities {
		if facility != "" {
			fID, _ := primitive.ObjectIDFromHex(facility)
			payload.PropertyFacilities = append(payload.PropertyFacilities, &fID)
		}
	}
	payload.Facilities = nil

	ctx := context.Background()
	cursor, err := collection.Find(ctx, bson.M{
		"$or": bson.A{
			bson.M{"property_name": payload.PropertyName},
			bson.M{"property_code": payload.PropertyCode},
		},
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Property already exists.",
		})
		return
	}

	now := time.Now()
	payload.PropertyRegistered = &now
	payload.PropertySlug = helpers.Slugify(payload.PropertyName)

	result, err := collection.InsertOne(ctx, payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"property_id": result.InsertedID,
		},
	})
}

func (app *Api) UpdateProperty(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("properties")

	propertyId := c.Param("propertyId")
	objectID, _ := primitive.ObjectIDFromHex(propertyId)

	var property models.Property
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&property)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	if err := c.ShouldBind(&property); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var regex, _ = regexp.Compile(`[ ]+`)
	property.PropertyName = strings.Trim(regex.ReplaceAllString(property.PropertyName, " "), " ")
	property.PropertyCode = strings.ToUpper(property.PropertyCode)

	property.PropertyFacilities = nil
	for _, facility := range property.Facilities {
		if facility != "" {
			fID, _ := primitive.ObjectIDFromHex(facility)
			property.PropertyFacilities = append(property.PropertyFacilities, &fID)
		}
	}
	property.Facilities = nil

	cursor, err := collection.Find(ctx, bson.M{
		"_id": bson.M{"$ne": objectID},
		"$or": bson.A{
			bson.M{"property_name": property.PropertyName},
			bson.M{"property_code": property.PropertyCode},
		},
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Property already exists.",
		})
		return
	}

	now := time.Now()
	property.PropertyModified = &now
	property.PropertySlug = helpers.Slugify(property.PropertyName)

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": property})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"property":       property,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteProperty(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("properties")

	propertyId := c.Param("propertyId")
	objectID, _ := primitive.ObjectIDFromHex(propertyId)

	var property models.Property
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&property)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"property_id":   propertyId,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) GetPropertyList(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("properties")

	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	// this is an optional parameter
	code := strings.ToUpper(c.Query("code"))
	codeSlug := strings.ToLower(c.Query("codeSlug"))
	cityCode := strings.ToUpper(c.Query("cityCode"))
	lang := c.DefaultQuery("lang", "en")

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "order")
	sortDir := c.DefaultQuery("sortDir", "asc")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	matchFilter := bson.M{"property_code": bson.M{"$ne": "COR"}}
	if code != "" {
		matchFilter["brand.brand_code"] = code
	}
	if codeSlug != "" {
		matchFilter["brand.brand_slug"] = codeSlug
	}
	if cityCode != "" {
		matchFilter["city_id"] = cityCode
	}

	pipeline := bson.A{
		bson.M{"$lookup": bson.M{
			"as":           "brand",
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$brand",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "city",
			"from":         "cities",
			"localField":   "city_id",
			"foreignField": "code",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$city",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$match": matchFilter},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"ids": bson.M{"$ifNull": bson.A{"$property_facilities", bson.A{}}}},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$in": bson.A{"$_id", "$$ids"}},
					bson.M{"$eq": bson.A{"$parent_id", nil}},
					bson.M{"$eq": bson.A{"$type", "facility"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":         0,
					"id":          "$_id",
					"name":        "$name",
					"icon":        "$icon",
					"description": "$description",
				}},
			},
			"as": "facilities",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$facilities",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"ids": bson.M{"$ifNull": bson.A{"$property_facilities", bson.A{}}}, "parent": "$facilities.id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$parent_id", "$$parent"}},
					bson.M{"$in": bson.A{"$_id", "$$ids"}},
					bson.M{"$eq": bson.A{"$type", "facility"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":         0,
					"id":          "$_id",
					"name":        "$name",
					"icon":        "$icon",
					"description": "$description",
				}},
			},
			"as": "sub_facilities",
		}},
		bson.M{"$group": bson.M{
			"_id":      "$_id",
			"property": bson.M{"$first": "$$ROOT"},
			"facilities": bson.M{"$push": bson.M{"$cond": bson.A{
				bson.M{"$gt": bson.A{bson.M{"$size": "$sub_facilities"}, 0}},
				bson.M{"$mergeObjects": bson.A{"$facilities", bson.M{"children": "$sub_facilities"}}},
				"$facilities",
			}}},
		}},
		bson.M{
			"$project": bson.M{
				"_id":         0,
				"id":          "$property._id",
				"code":        "$property.property_code",
				"booking_url": bson.M{"$ifNull": bson.A{"$property.property_booking_url", ""}},
				"booking_code": bson.M{
					"corporate": "$property.property_corporate_booking_code",
					"brand":     "$property.property_brand_booking_code",
					"hotel":     "$property.property_hotel_booking_code",
				},
				"name":         "$property.property_name",
				"display_name": "$property.property_display_name",
				"logo": bson.M{
					"primary": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_logo", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$property.property_logo",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_logo"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_logo"},
							},
						},
					}},
					"secondary": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_secondary_logo", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$property.property_secondary_logo",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_secondary_logo"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_secondary_logo"},
							},
						},
					}},
				},
				"favicon": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_favicon", ""}}, ""}},
					"then": bson.M{},
					"else": bson.M{
						"name": "$property.property_favicon",
						"local": bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_favicon"},
						},
						"cloud": bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_favicon"},
						},
					},
				}},
				"featured_image": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_featured_image", ""}}, ""}},
					"then": bson.M{},
					"else": bson.M{
						"name": "$property.property_featured_image",
						"local": bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_featured_image"},
						},
						"cloud": bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_featured_image"},
						},
					},
				}},
				"attachment_image": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_image_attachment", ""}}, ""}},
					"then": bson.A{},
					"else": bson.M{
						"$map": bson.M{
							"input": "$property.property_image_attachment",
							"as":    "image",
							"in": bson.M{
								"name": "$$image",
								"local": bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$image"},
								},
								"cloud": bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$image"},
								},
							},
						},
					},
				}},
				"address":            "$property.property_address",
				"email":              "$property.property_email",
				"fax":                "$property.property_fax",
				"phone":              "$property.property_phone",
				"tagline":            bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_tagline.%s", lang), ""}},
				"description":        bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_description.%s", lang), ""}},
				"custom.field":       bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_custom_field.%s", lang), ""}},
				"custom.link":        bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_custom_link.%s", lang), ""}},
				"location.latitude":  "$property.property_latitude",
				"location.longitude": "$property.property_longitude",
				"slug":               "$property.property_slug",
				"created_at":         "$property.property_registered",
				"updated_at":         "$property.property_modified",
				"status":             "$property.property_status",
				"order":              bson.M{"$ifNull": bson.A{"$property.property_order", 0}},
				"brand": bson.M{
					"id":   "$property.brand._id",
					"code": "$property.brand.brand_code",
					"name": "$property.brand.brand_name",
					"slug": "$property.brand.brand_slug",
				},
				"city": bson.M{
					"id":          "$property.city._id",
					"code":        "$property.city.code",
					"name":        "$property.city.name",
					"description": bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.city.description.%s", lang), ""}},
					"featured_image": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.city.featured_image", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$property.city.featured_image",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.city.featured_image"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.city.featured_image"},
							},
						},
					}},
				},
				"facilities": "$facilities",
			},
		},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": facetPipelinePaging,
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"items": facetData,
			"total": total,
		},
	})
}

func (app *Api) GetPropertyByCode(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("properties")

	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	// this is an optional parameter
	code := strings.ToUpper(c.Param("code"))
	lang := c.DefaultQuery("lang", "en")

	pipeline := bson.A{
		bson.M{"$match": bson.M{"property_code": code}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"ids": bson.M{"$ifNull": bson.A{"$property_facilities", bson.A{}}}},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$in": bson.A{"$_id", "$$ids"}},
					bson.M{"$eq": bson.A{"$parent_id", nil}},
					bson.M{"$eq": bson.A{"$type", "facility"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":         0,
					"id":          "$_id",
					"name":        "$name",
					"icon":        "$icon",
					"description": "$description",
				}},
			},
			"as": "facilities",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$facilities",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"ids": bson.M{"$ifNull": bson.A{"$property_facilities", bson.A{}}}, "parent": "$facilities.id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$parent_id", "$$parent"}},
					bson.M{"$in": bson.A{"$_id", "$$ids"}},
					bson.M{"$eq": bson.A{"$type", "facility"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":         0,
					"id":          "$_id",
					"name":        "$name",
					"icon":        "$icon",
					"description": "$description",
				}},
			},
			"as": "sub_facilities",
		}},
		bson.M{"$lookup": bson.M{
			"as":           "brand",
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$brand",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "city",
			"from":         "cities",
			"localField":   "city_id",
			"foreignField": "code",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$city",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$group": bson.M{
			"_id":      "$_id",
			"property": bson.M{"$first": "$$ROOT"},
			"facilities": bson.M{"$push": bson.M{"$cond": bson.A{
				bson.M{"$gt": bson.A{bson.M{"$size": "$sub_facilities"}, 0}},
				bson.M{"$mergeObjects": bson.A{"$facilities", bson.M{"children": "$sub_facilities"}}},
				"$facilities",
			}}},
		}},
		bson.M{
			"$project": bson.M{
				"_id":         0,
				"id":          "$property._id",
				"code":        "$property.property_code",
				"booking_url": bson.M{"$ifNull": bson.A{"$property.property_booking_url", ""}},
				"booking_code": bson.M{
					"corporate": "$property.property_corporate_booking_code",
					"brand":     "$property.property_brand_booking_code",
					"hotel":     "$property.property_hotel_booking_code",
				},
				"name":         "$property.property_name",
				"display_name": "$property.property_display_name",
				"logo": bson.M{
					"primary": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_logo", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$property.property_logo",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_logo"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_logo"},
							},
						},
					}},
					"secondary": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_secondary_logo", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$property.property_secondary_logo",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_secondary_logo"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_secondary_logo"},
							},
						},
					}},
				},
				"favicon": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_favicon", ""}}, ""}},
					"then": bson.M{},
					"else": bson.M{
						"name": "$property.property_favicon",
						"local": bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_favicon"},
						},
						"cloud": bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_favicon"},
						},
					},
				}},
				"featured_image": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_featured_image", ""}}, ""}},
					"then": bson.M{},
					"else": bson.M{
						"name": "$property.property_featured_image",
						"local": bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_featured_image"},
						},
						"cloud": bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_featured_image"},
						},
					},
				}},
				"attachment_image": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_image_attachment", ""}}, ""}},
					"then": bson.A{},
					"else": bson.M{
						"$map": bson.M{
							"input": "$property.property_image_attachment",
							"as":    "image",
							"in": bson.M{
								"name": "$$image",
								"local": bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$image"},
								},
								"cloud": bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$image"},
								},
							},
						},
					},
				}},
				"address":            "$property.property_address",
				"email":              "$property.property_email",
				"fax":                "$property.property_fax",
				"phone":              "$property.property_phone",
				"tagline":            bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_tagline.%s", lang), ""}},
				"description":        bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_description.%s", lang), ""}},
				"custom.field":       bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_custom_field.%s", lang), ""}},
				"custom.link":        bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_custom_link.%s", lang), ""}},
				"location.latitude":  "$property.property_latitude",
				"location.longitude": "$property.property_longitude",
				"slug":               "$property.property_slug",
				"created_at":         "$property.property_registered",
				"updated_at":         "$property.property_modified",
				"status":             "$property.property_status",
				"order":              "$property.property_order",
				"brand": bson.M{
					"id":   "$property.brand._id",
					"code": "$property.brand.brand_code",
					"name": "$property.brand.brand_name",
					"slug": "$property.brand.brand_slug",
				},
				"city": bson.M{
					"id":          "$property.city._id",
					"code":        "$property.city.code",
					"name":        "$property.city.name",
					"description": bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.city.description.%s", lang), ""}},
					"featured_image": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.city.featured_image", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$property.city.featured_image",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.city.featured_image"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.city.featured_image"},
							},
						},
					}},
				},
				"facilities": "$facilities",
			},
		},
	}

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var result = bson.M{}
	for cursor.Next(ctx) {
		cursor.Decode(&result)
		address := neturl.QueryEscape(fmt.Sprintf("%s %s", result["name"].(string), result["address"].(string)))
		src := fmt.Sprintf("https://www.google.com/maps/embed/v1/place?key=%s&q=%s", app.Config.Google.ApiKey, address)
		result["maps"] = fmt.Sprintf("<iframe width='100%%' height='100%%' frameborder='0' style='border:0' src='%s' allowfullscreen></iframe>", src)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       result,
	})
}

func (app *Api) GetPropertyBySlug(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("properties")

	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	// this is an optional parameter
	slug := strings.ToLower(c.Param("slug"))
	lang := c.DefaultQuery("lang", "en")

	pipeline := bson.A{
		bson.M{"$match": bson.M{"property_slug": slug}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"ids": bson.M{"$ifNull": bson.A{"$property_facilities", bson.A{}}}},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$in": bson.A{"$_id", "$$ids"}},
					bson.M{"$eq": bson.A{"$parent_id", nil}},
					bson.M{"$eq": bson.A{"$type", "facility"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":         0,
					"id":          "$_id",
					"name":        "$name",
					"icon":        "$icon",
					"description": "$description",
				}},
			},
			"as": "facilities",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$facilities",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"ids": bson.M{"$ifNull": bson.A{"$property_facilities", bson.A{}}}, "parent": "$facilities.id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$parent_id", "$$parent"}},
					bson.M{"$in": bson.A{"$_id", "$$ids"}},
					bson.M{"$eq": bson.A{"$type", "facility"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":         0,
					"id":          "$_id",
					"name":        "$name",
					"icon":        "$icon",
					"description": "$description",
				}},
			},
			"as": "sub_facilities",
		}},
		bson.M{"$lookup": bson.M{
			"as":           "brand",
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$brand",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "city",
			"from":         "cities",
			"localField":   "city_id",
			"foreignField": "code",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$city",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$group": bson.M{
			"_id":      "$_id",
			"property": bson.M{"$first": "$$ROOT"},
			"facilities": bson.M{"$push": bson.M{"$cond": bson.A{
				bson.M{"$gt": bson.A{bson.M{"$size": "$sub_facilities"}, 0}},
				bson.M{"$mergeObjects": bson.A{"$facilities", bson.M{"children": "$sub_facilities"}}},
				"$facilities",
			}}},
		}},
		bson.M{
			"$project": bson.M{
				"_id":         0,
				"id":          "$property._id",
				"code":        "$property.property_code",
				"booking_url": bson.M{"$ifNull": bson.A{"$property.property_booking_url", ""}},
				"booking_code": bson.M{
					"corporate": "$property.property_corporate_booking_code",
					"brand":     "$property.property_brand_booking_code",
					"hotel":     "$property.property_hotel_booking_code",
				},
				"name":         "$property.property_name",
				"display_name": "$property.property_display_name",
				"logo": bson.M{
					"primary": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_logo", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$property.property_logo",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_logo"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_logo"},
							},
						},
					}},
					"secondary": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_secondary_logo", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$property.property_secondary_logo",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_secondary_logo"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_secondary_logo"},
							},
						},
					}},
				},
				"favicon": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_favicon", ""}}, ""}},
					"then": bson.M{},
					"else": bson.M{
						"name": "$property.property_favicon",
						"local": bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_favicon"},
						},
						"cloud": bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_favicon"},
						},
					},
				}},
				"featured_image": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_featured_image", ""}}, ""}},
					"then": bson.M{},
					"else": bson.M{
						"name": "$property.property_featured_image",
						"local": bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_featured_image"},
						},
						"cloud": bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_featured_image"},
						},
					},
				}},
				"attachment_image": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_image_attachment", ""}}, ""}},
					"then": bson.A{},
					"else": bson.M{
						"$map": bson.M{
							"input": "$property.property_image_attachment",
							"as":    "image",
							"in": bson.M{
								"name": "$$image",
								"local": bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$image"},
								},
								"cloud": bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$image"},
								},
							},
						},
					},
				}},
				"address":            "$property.property_address",
				"email":              "$property.property_email",
				"fax":                "$property.property_fax",
				"phone":              "$property.property_phone",
				"slug":               "$property.property_slug",
				"tagline":            bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_tagline.%s", lang), ""}},
				"description":        bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_description.%s", lang), ""}},
				"custom.field":       bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_custom_field.%s", lang), ""}},
				"custom.link":        bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.property_custom_link.%s", lang), ""}},
				"location.latitude":  "$property.property_latitude",
				"location.longitude": "$property.property_longitude",
				"created_at":         "$property.property_registered",
				"updated_at":         "$property.property_modified",
				"status":             "$property.property_status",
				"order":              "$property.property_order",
				"brand": bson.M{
					"id":   "$property.brand._id",
					"code": "$property.brand.brand_code",
					"name": "$property.brand.brand_name",
					"slug": "$property.brand.brand_slug",
				},
				"city": bson.M{
					"id":          "$property.city._id",
					"code":        "$property.city.code",
					"name":        "$property.city.name",
					"description": bson.M{"$ifNull": bson.A{fmt.Sprintf("$property.city.description.%s", lang), ""}},
					"featured_image": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.city.featured_image", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$property.city.featured_image",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.city.featured_image"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.city.featured_image"},
							},
						},
					}},
				},
				"facilities": "$facilities",
			},
		},
	}

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var result = bson.M{}
	for cursor.Next(ctx) {
		cursor.Decode(&result)
		address := neturl.QueryEscape(fmt.Sprintf("%s %s", result["name"].(string), result["address"].(string)))
		src := fmt.Sprintf("https://www.google.com/maps/embed/v1/place?key=%s&q=%s", app.Config.Google.ApiKey, address)
		result["maps"] = fmt.Sprintf("<iframe width='100%%' height='100%%' frameborder='0' style='border:0' src='%s' allowfullscreen></iframe>", src)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       result,
	})
}

func (app *Api) GetPropertyGroupByCity(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("properties")

	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	// this is an optional parameter
	code := strings.ToUpper(c.Query("code"))
	codeSlug := strings.ToLower(c.Query("codeSlug"))
	cityCode := strings.ToUpper(c.Query("cityCode"))
	lang := c.DefaultQuery("lang", "en")

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "order")
	sortDir := c.DefaultQuery("sortDir", "asc")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	matchFilter := bson.M{"property_code": bson.M{"$ne": "COR"}}
	if code != "" {
		matchFilter["brand.brand_code"] = code
	}
	if codeSlug != "" {
		matchFilter["brand.brand_slug"] = codeSlug
	}
	if cityCode != "" {
		matchFilter["city_id"] = cityCode
	}

	pipeline := bson.A{
		bson.M{"$lookup": bson.M{
			"as":           "brand",
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$brand",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "city",
			"from":         "cities",
			"localField":   "city_id",
			"foreignField": "code",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$city",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$match": matchFilter},
		bson.M{"$group": bson.M{
			"_id":        "$city._id",
			"city":       bson.M{"$first": "$city"},
			"properties": bson.M{"$push": "$$ROOT"},
		}},
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$_id",
			"code":        "$city.code",
			"name":        "$city.name",
			"description": bson.M{"$ifNull": bson.A{fmt.Sprintf("$city.description.%s", lang), ""}},
			"order":       "$city.order",
			"featured_image": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$city.featured_image", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$city.featured_image",
					"local": bson.M{
						"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$city.featured_image"},
					},
					"cloud": bson.M{
						"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$city.featured_image"},
					},
				},
			}},
			"properties": bson.M{"$map": bson.M{
				"input": "$properties",
				"as":    "property",
				"in": bson.M{
					"id":          "$$property._id",
					"code":        "$$property.property_code",
					"booking_url": bson.M{"$ifNull": bson.A{"$$property.property_booking_url", ""}},
					"booking_code": bson.M{
						"corporate": "$$property.property_corporate_booking_code",
						"brand":     "$$property.property_brand_booking_code",
						"hotel":     "$$property.property_hotel_booking_code",
					},
					"name":         "$$property.property_name",
					"display_name": "$$property.property_display_name",
					"logo": bson.M{
						"primary": bson.M{"$cond": bson.M{
							"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$property.property_logo", ""}}, ""}},
							"then": bson.M{},
							"else": bson.M{
								"name": "$$property.property_logo",
								"local": bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$property.property_logo"},
								},
								"cloud": bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$property.property_logo"},
								},
							},
						}},
						"secondary": bson.M{"$cond": bson.M{
							"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$property.property_secondary_logo", ""}}, ""}},
							"then": bson.M{},
							"else": bson.M{
								"name": "$$property.property_secondary_logo",
								"local": bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$property.property_secondary_logo"},
								},
								"cloud": bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$property.property_secondary_logo"},
								},
							},
						}},
					},
					"favicon": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$property.property_favicon", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$$property.property_favicon",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$property.property_favicon"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$property.property_favicon"},
							},
						},
					}},
					"featured_image": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$property.property_featured_image", ""}}, ""}},
						"then": bson.M{},
						"else": bson.M{
							"name": "$$property.property_featured_image",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$property.property_featured_image"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$property.property_featured_image"},
							},
						},
					}},
					"attachment_image": bson.M{"$cond": bson.M{
						"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$property.property_image_attachment", ""}}, ""}},
						"then": bson.A{},
						"else": bson.M{
							"$map": bson.M{
								"input": "$$property.property_image_attachment",
								"as":    "image",
								"in": bson.M{
									"name": "$$image",
									"local": bson.M{
										"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$image"},
									},
									"cloud": bson.M{
										"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$image"},
									},
								},
							},
						},
					}},
					"address":     "$$property.property_address",
					"email":       "$$property.property_email",
					"fax":         "$$property.property_fax",
					"phone":       "$$property.property_phone",
					"slug":        "$$property.property_slug",
					"tagline":     bson.M{"$ifNull": bson.A{fmt.Sprintf("$$property.property_tagline.%s", lang), ""}},
					"description": bson.M{"$ifNull": bson.A{fmt.Sprintf("$$property.property_description.%s", lang), ""}},
					"custom": bson.M{
						"field": bson.M{"$ifNull": bson.A{fmt.Sprintf("$$property.property_custom_field.%s", lang), ""}},
						"link":  bson.M{"$ifNull": bson.A{fmt.Sprintf("$$property.property_custom_link.%s", lang), ""}},
					},
					"location": bson.M{
						"latitude":  "$$property.property_latitude",
						"longitude": "$$property.property_longitude",
					},
					"created_at": "$$property.property_modified",
					"updated_at": "$$property.property_registered",
					"status":     "$$property.property_status",
					"order":      bson.M{"$ifNull": bson.A{"$$property.property_order", 0}},
					"brand": bson.M{
						"id":   "$$property.brand._id",
						"code": "$$property.brand.brand_code",
						"name": "$$property.brand.brand_name",
						"slug": "$$property.brand.brand_slug",
					},
					"city": bson.M{
						"id":   "$$property.city._id",
						"code": "$$property.city.code",
						"name": "$$property.city.name",
						"featured_image": bson.M{"$cond": bson.M{
							"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$property.city.featured_image", ""}}, ""}},
							"then": bson.M{},
							"else": bson.M{
								"name": "$$property.city.featured_image",
								"local": bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$property.city.featured_image"},
								},
								"cloud": bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$property.city.featured_image"},
								},
							},
						}},
					},
				},
			}},
		}},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": facetPipelinePaging,
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"items": facetData,
			"total": total,
		},
	})
}
