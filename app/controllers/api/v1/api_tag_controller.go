package v1

import (
	"alaric_cms/app/models"
	"context"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetTag(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	search := c.Query("search")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "-1")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	matchFilter := bson.M{"type": "tag"}
	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"items.name": searchRe},
			bson.M{"items.title": searchRe},
			bson.M{"items.lang": searchRe},
			bson.M{"created_at": searchRe},
			bson.M{"updated_at": searchRe},
		}
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$unwind": bson.M{
			"path":                       "$items",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$items.lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
					bson.M{"$eq": bson.A{"$status", "primary"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":    0,
					"id":     "$_id",
					"code":   "$code",
					"name":   "$name",
					"native": "$native",
					"status": "$status",
				}},
			},
			"as": "items.lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$items.lang",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$project": bson.M{
			"_id":        0,
			"id":         "$_id",
			"name":       "$items.name",
			"title":      "$items.title",
			"lang":       "$items.lang",
			"type":       "$type",
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
		bson.M{"$sort": sort},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": bson.A{bson.M{"$skip": offset}, bson.M{"$limit": limit}},
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  facetData,
			"total": total,
		},
	})
}

func (app *Api) GetTagByID(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	tagID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(tagID)

	pipeline := bson.A{
		bson.M{"$match": bson.M{"_id": objectID, "type": "tag"}},
		bson.M{"$unwind": bson.M{
			"path":                       "$items",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$items.lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":    0,
					"id":     "$_id",
					"code":   "$code",
					"name":   "$name",
					"native": "$native",
					"status": "$status",
				}},
			},
			"as": "items.lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$items.lang",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$group": bson.M{
			"_id":        "$_id",
			"type":       bson.M{"$first": "$type"},
			"items":      bson.M{"$push": "$items"},
			"created_at": bson.M{"$first": "$created_at"},
			"updated_at": bson.M{"$first": "$updated_at"},
		}},
		bson.M{"$project": bson.M{
			"_id":        0,
			"id":         "$_id",
			"type":       "$type",
			"items":      "$items",
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
	}

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var result bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&result)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       result,
	})
}

func (app *Api) GetTagGroupedLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	pipeline := bson.A{
		bson.M{"$match": bson.M{"type": "tag"}},
		bson.M{"$unwind": bson.M{
			"path":                       "$items",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$project": bson.M{
			"_id":        0,
			"id":         "$_id",
			"name":       "$items.name",
			"title":      "$items.title",
			"lang":       "$items.lang",
			"type":       "$type",
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
		bson.M{"$group": bson.M{
			"_id":   "$lang",
			"items": bson.M{"$push": "$$ROOT"},
		}},
		bson.M{"$addFields": bson.M{
			"tags": bson.A{bson.M{
				"k": "$_id",
				"v": "$items",
			}},
		}},
		bson.M{"$replaceRoot": bson.M{
			"newRoot": bson.M{"$arrayToObject": "$tags"},
		}},
	}

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var tags bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&tags)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       tags,
	})
}

func (app *Api) AddTag(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	var payload models.TagForm
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	tag := &models.Tag{
		Type: "tag",
	}
	tag.Items = append(tag.Items, models.TagItem{
		Name:  payload.Name,
		Title: payload.Title,
		Lang:  payload.Lang,
	})

	ctx := context.Background()
	cursor, err := collection.Find(ctx, bson.M{
		"items.name": payload.Name,
		"items.lang": payload.Lang,
	})
	defer cursor.Close(ctx)

	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	if cursor.Next(ctx) {
		c.JSON(http.StatusConflict, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Tag already exists.",
		})
		return
	}

	now := time.Now()
	tag.CreatedAt = &now

	result, err := collection.InsertOne(ctx, tag)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"id": result.InsertedID,
		},
	})
}

func (app *Api) UpdateTag(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	tagID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(tagID)

	tag := &models.Tag{}
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID, "type": "tag"}).Decode(&tag)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	payload := []models.TagForm{}
	for _, item := range tag.Items {
		payload = append(payload, models.TagForm{
			Name:  item.Name,
			Title: item.Title,
			Lang:  item.Lang,
		})
	}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	items := []models.TagItem{}
	for _, item := range payload {
		cursor, err := collection.Find(ctx, bson.M{
			"_id": bson.M{"$ne": objectID},
			"$and": bson.A{
				bson.M{"items.name": item.Name},
				bson.M{"items.lang": item.Lang},
			},
		})
		defer cursor.Close(ctx)

		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}

		if cursor.Next(ctx) {
			c.JSON(http.StatusConflict, &models.ReE{
				StatusCode: http.StatusConflict,
				Error:      "Tag already exists.",
			})
			return
		}

		items = append(items, models.TagItem{
			Name:  item.Name,
			Title: item.Title,
			Lang:  item.Lang,
		})
	}

	tag.Items = items
	now := time.Now()
	tag.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": tag})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"tag":            tag,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteTag(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	tagID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(tagID)

	ctx := context.Background()
	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID, "type": "tag"})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            tagID,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) DeleteTagByLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	tagID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(tagID)
	lang := c.Param("lang")

	var tag models.Tag
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID, "type": "tag"}).Decode(&tag)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var items []models.TagItem
	for _, item := range tag.Items {
		if item.Lang != lang {
			items = append(items, item)
		}
	}
	tag.Items = items

	now := time.Now()
	tag.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": tag})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"tag":            tag,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) GetTagList(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	ID := c.Query("id")
	IDs := c.Query("tags")
	lang := c.DefaultQuery("lang", "en")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "-1")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	matchFilter := bson.M{"type": "tag"}
	if ID != "" {
		oID, _ := primitive.ObjectIDFromHex(ID)
		matchFilter["_id"] = oID
	}

	tags := bson.A{}
	tagIDs := strings.Split(IDs, ",")
	for _, tag := range tagIDs {
		tag = strings.TrimSpace(tag)
		tID, err := primitive.ObjectIDFromHex(tag)
		if err == nil {
			tags = append(tags, tID)
		}
	}
	if len(tags) > 0 {
		matchFilter["_id"] = bson.M{"$in": tags}
	}

	if lang != "" {
		matchFilter["items.lang"] = lang
	}

	pipeline := bson.A{
		bson.M{"$unwind": bson.M{"path": "$items", "preserveNullAndEmptyArrays": true}},
		bson.M{"$match": matchFilter},
		bson.M{"$project": bson.M{
			"_id":   0,
			"id":    "$_id",
			"name":  "$items.name",
			"title": "$items.title",
			"lang":  "$items.lang",
		}},
		bson.M{"$facet": bson.M{
			"paginatedResults": bson.A{bson.M{"$sort": sort}},
			"totalCount":       bson.A{bson.M{"$count": "count"}},
		}},
	}

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"items": facetData,
			"total": total,
		},
	})
}
