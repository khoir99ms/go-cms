package v1

import (
	"alaric_cms/app/models"
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetBanner(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("banners")

	bannerID := c.Query("id")
	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")
	search := c.Query("search")
	sortCol := c.DefaultQuery("sortCol", "_id")
	sortDir := c.DefaultQuery("sortDir", "-1")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	pipeline := bson.A{}

	matchFilter := bson.M{}
	if bannerID != "" {
		bID, _ := primitive.ObjectIDFromHex(bannerID)
		matchFilter["_id"] = bID
	}
	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["brand_id"] = bID
	}
	if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		matchFilter["property_id"] = pID
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"name": searchRe},
		}
	}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$unwind": bson.M{"path": "$items", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$items.lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":    0,
					"id":     "$_id",
					"code":   "$code",
					"name":   "$name",
					"native": "$native",
					"status": "$status",
				}},
			},
			"as": "items.lang",
		}},
		bson.M{"$unwind": bson.M{"path": "$items.lang", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "brands", "as": "brand",
			"localField": "brand_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "properties", "as": "property",
			"localField": "property_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
		bson.M{"$group": bson.M{
			"_id":  "$_id",
			"id":   bson.M{"$first": "$_id"},
			"name": bson.M{"$first": "$name"},
			"items": bson.M{"$push": bson.M{"$cond": bson.A{
				bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$items", bson.M{}}}, bson.M{}}},
				"$items",
				"$nil",
			}}},
			"brand": bson.M{"$first": bson.M{
				"id":   "$brand._id",
				"code": "$brand.brand_code",
				"name": "$brand.brand_name",
				"slug": "$brand.brand_slug",
			}},
			"property": bson.M{"$first": bson.M{
				"id":   "$property._id",
				"code": "$property.property_code",
				"name": "$property.property_name",
				"slug": "$property.property_slug",
			}},
			"created_at": bson.M{"$first": "$created_at"},
			"updated_at": bson.M{"$first": "$updated_at"},
		}},
		bson.M{"$project": bson.M{
			"_id": 0,
		}},
		bson.M{"$sort": sort},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": bson.A{bson.M{"$skip": offset}, bson.M{"$limit": limit}},
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	var results interface{}
	if bannerID != "" && len(facetData) == 1 {
		results = facetData[0]
	} else {
		results = facetData
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": total,
		},
	})
}

func (app *Api) GetAssignedBanner(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("banners")

	aType := c.Param("type")
	bannerID := c.Query("id")
	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")

	matchFilter := bson.M{}
	if bannerID != "" {
		bID, _ := primitive.ObjectIDFromHex(bannerID)
		matchFilter["_id"] = bID
	}
	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["brand_id"] = bID
	}
	if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		matchFilter["property_id"] = pID
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	subPipelineLookup := bson.A{
		bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
			bson.M{"$eq": bson.A{"$_id", "$$id"}},
		}}}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
					bson.M{"$eq": bson.A{"$status", "primary"}},
				}}}},
			},
			"as": "lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$lang",
			"preserveNullAndEmptyArrays": false,
		}},
	}

	pipelineLookup := bson.M{}
	if aType == "page" {
		subPipelineLookup = append(subPipelineLookup, bson.M{"$project": bson.M{
			"_id":   0,
			"id":    "$_id",
			"title": "$title",
			"lang":  "$lang.code",
			"type":  "page",
		}})

		pipelineLookup = bson.M{"$lookup": bson.M{
			"from":     "pages",
			"let":      bson.M{"id": "$apply_to.id"},
			"pipeline": subPipelineLookup,
			"as":       "items", // banner at page
		}}
	} else if aType == "category" {
		subPipelineLookup = append(subPipelineLookup, bson.M{"$project": bson.M{
			"_id":   0,
			"id":    "$_id",
			"title": "$name",
			"lang":  "$lang.code",
			"type":  "category",
		}})

		pipelineLookup = bson.M{"$lookup": bson.M{
			"from":     "categories",
			"let":      bson.M{"id": "$apply_to.id"},
			"pipeline": subPipelineLookup,
			"as":       "items", // banner at page
		}}
	} else if aType == "group_content" {
		subPipelineLookup = bson.A{
			bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
				bson.M{"$eq": bson.A{"$_id", "$$id"}},
			}}}},
			/* bson.M{"$unwind": bson.M{
				"path":                       "$setting",
				"preserveNullAndEmptyArrays": true,
			}},
			bson.M{"$lookup": bson.M{
				"from": "complements",
				"let":  bson.M{"lang": "$setting.lang"},
				"pipeline": bson.A{
					bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
						bson.M{"$eq": bson.A{"$code", "$$lang"}},
						bson.M{"$eq": bson.A{"$type", "lang"}},
						bson.M{"$eq": bson.A{"$status", "primary"}},
					}}}},
				},
				"as": "setting.lang",
			}},
			bson.M{"$unwind": bson.M{
				"path":                       "$setting.lang",
				"preserveNullAndEmptyArrays": true,
			}}, */
			bson.M{"$project": bson.M{
				"_id":   0,
				"id":    "$_id",
				"title": "$name",
				"lang":  "",
				"type":  "group_content",
			}},
		}

		pipelineLookup = bson.M{"$lookup": bson.M{
			"from":     "group_content",
			"let":      bson.M{"id": "$apply_to.id"},
			"pipeline": subPipelineLookup,
			"as":       "items", // banner at page
		}}
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$lookup": bson.M{
			"as":           "brand",
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$brand",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "property",
			"from":         "properties",
			"localField":   "property_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$property",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$apply_to",
			"preserveNullAndEmptyArrays": true,
		}},
		pipelineLookup,
		bson.M{"$unwind": bson.M{
			"path":                       "$items",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$project": bson.M{
			"_id":       0,
			"id":        "$items.id",
			"title":     "$items.title",
			"lang":      "$items.lang",
			"type":      "$items.type",
			"banner_id": "$_id",
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var results = []bson.M{}
	cursor.All(ctx, &results)

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": len(results),
		},
	})
}

func (app *Api) AddBanner(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("banners")

	var payload models.FormBanner
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var banner models.Banner
	banner.Name = payload.Name

	existFilter := bson.M{"name": banner.Name}
	if payload.BrandID != "" {
		bID, _ := primitive.ObjectIDFromHex(payload.BrandID)
		banner.BrandID = &bID
		existFilter["brand_id"] = bID
	}

	if payload.PropertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.PropertyID)
		banner.PropertyID = &pID
		existFilter["property_id"] = pID
	}

	ctx := context.Background()
	cursor, err := collection.Find(ctx, existFilter)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Banner name already exists.",
		})
		return
	}

	for i := range payload.Image {
		var item models.BannerItem

		if len(payload.Title) > i {
			item.Title = payload.Title[i]
		}
		if len(payload.Description) > i {
			item.Description = payload.Description[i]
		}
		if len(payload.Image) > i {
			item.Image = payload.Image[i]
		}
		if len(payload.Order) > i {
			item.Order = payload.Order[i]
		}
		if len(payload.Lang) > i {
			item.Lang = payload.Lang[i]
		}
		item.Status = "active"

		if item.Image != "" {
			banner.Items = append(banner.Items, item)
		}
	}

	now := time.Now()
	banner.CreatedAt = &now

	result, err := collection.InsertOne(ctx, banner)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"id": result.InsertedID,
		},
	})
}

func (app *Api) UpdateBanner(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("banners")

	bannerID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(bannerID)

	var banner models.Banner
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&banner)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var brandID, propertyID string
	if banner.BrandID != nil {
		brandID = banner.BrandID.Hex()
	}

	if banner.PropertyID != nil {
		propertyID = banner.PropertyID.Hex()
	}

	var payload = models.FormBanner{
		Name:       banner.Name,
		BrandID:    brandID,
		PropertyID: propertyID,
	}

	for _, item := range banner.Items {
		payload.Title = append(payload.Title, item.Title)
		payload.Description = append(payload.Description, item.Description)
		payload.Image = append(payload.Image, item.Image)
		payload.Order = append(payload.Order, item.Order)
		payload.Status = append(payload.Status, item.Status)
		payload.Lang = append(payload.Lang, item.Lang)
	}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	existFilter := bson.M{
		"_id":  bson.M{"$ne": objectID},
		"name": payload.Name,
	}

	banner.Name = payload.Name
	if payload.BrandID != "" {
		bID, _ := primitive.ObjectIDFromHex(payload.BrandID)
		banner.BrandID = &bID
		existFilter["brand_id"] = bID
	}

	if payload.PropertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.PropertyID)
		banner.PropertyID = &pID
		existFilter["property_id"] = pID
	}

	cursor, err := collection.Find(ctx, existFilter)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Banner name already exists.",
		})
		return
	}

	var items []models.BannerItem
	for i := range payload.Image {
		var item models.BannerItem

		if len(payload.Title) > i {
			item.Title = payload.Title[i]
		}
		if len(payload.Description) > i {
			item.Description = payload.Description[i]
		}
		if len(payload.Image) > i {
			item.Image = payload.Image[i]
		}
		if len(payload.Order) > i {
			item.Order = payload.Order[i]
		}
		if len(payload.Lang) > i {
			item.Lang = payload.Lang[i]
		}
		item.Status = "active"

		if item.Image != "" {
			items = append(items, item)
		}
	}
	banner.Items = items

	now := time.Now()
	banner.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": banner})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"banner":         banner,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) bannerFetchPageID(ctx context.Context, ID string) ([]*primitive.ObjectID, error) {
	collection := app.Config.MongoDB.Collection("pages")

	pageID, _ := primitive.ObjectIDFromHex(ID)

	pipeline := bson.A{
		bson.M{"$match": bson.M{
			"_id": pageID,
		}},
		bson.M{"$lookup": bson.M{
			"from": "pages",
			"let":  bson.M{"id": "$_id", "source_id": "$source_id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$or": bson.A{
					bson.M{"$eq": bson.A{"$source_id", "$$id"}},
					bson.M{"$eq": bson.A{"$_id", "$$source_id"}},
				}}}},
			},
			"as": "related_pages",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$related_pages",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$group": bson.M{
			"_id":     "$_id",
			"related": bson.M{"$push": "$related_pages._id"},
		}},
	}

	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		return nil, err
	}
	defer cursor.Close(ctx)

	var results []*primitive.ObjectID
	for cursor.Next(ctx) {
		var row bson.M

		if err := cursor.Decode(&row); err == nil {
			ID := row["_id"].(primitive.ObjectID)
			results = append(results, &ID)
			for _, v := range row["related"].(bson.A) {
				relID := v.(primitive.ObjectID)
				results = append(results, &relID)
			}
		}
	}

	return results, nil
}

func (app *Api) bannerFetchCategoryID(ctx context.Context, ID string) ([]*primitive.ObjectID, error) {
	collection := app.Config.MongoDB.Collection("categories")

	categoryID, _ := primitive.ObjectIDFromHex(ID)

	pipeline := bson.A{
		bson.M{"$match": bson.M{
			"_id": categoryID,
		}},
		bson.M{"$lookup": bson.M{
			"from": "categories",
			"let":  bson.M{"id": "$_id", "source_id": "$source_id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$or": bson.A{
					bson.M{"$eq": bson.A{"$source_id", "$$id"}},
					bson.M{"$eq": bson.A{"$_id", "$$source_id"}},
				}}}},
			},
			"as": "related_categories",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$related_categories",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$group": bson.M{
			"_id":     "$_id",
			"related": bson.M{"$push": "$related_categories._id"},
		}},
	}

	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		return nil, err
	}
	defer cursor.Close(ctx)

	var results []*primitive.ObjectID
	for cursor.Next(ctx) {
		var row bson.M

		if err := cursor.Decode(&row); err == nil {
			ID := row["_id"].(primitive.ObjectID)
			results = append(results, &ID)
			for _, v := range row["related"].(bson.A) {
				relID := v.(primitive.ObjectID)
				results = append(results, &relID)
			}
		}
	}

	return results, nil
}

func (app *Api) bannerFetchGroupContentID(ctx context.Context, ID string) ([]*primitive.ObjectID, error) {
	collection := app.Config.MongoDB.Collection("group_content")

	groupID, _ := primitive.ObjectIDFromHex(ID)

	pipeline := bson.A{
		bson.M{"$match": bson.M{
			"_id": groupID,
		}},
	}

	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		return nil, err
	}
	defer cursor.Close(ctx)

	var results []*primitive.ObjectID
	for cursor.Next(ctx) {
		var row bson.M

		if err := cursor.Decode(&row); err == nil {
			ID := row["_id"].(primitive.ObjectID)
			results = append(results, &ID)
		}
	}

	return results, nil
}

func (app *Api) AssignBanner(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("banners")

	aType := c.Param("type")
	bannerID := c.Param("banner_id")
	bID, _ := primitive.ObjectIDFromHex(bannerID)
	ID := c.Param("id")
	oID, _ := primitive.ObjectIDFromHex(ID)

	ctx := context.Background()
	var banner models.Banner
	err := collection.FindOne(ctx, bson.M{"_id": bID}).Decode(&banner)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	exists := false
	for _, item := range banner.ApplyTo {
		if oID == *item.ID {
			exists = true
			break
		}
	}

	if !exists {
		applyTo := models.BannerApplyTo{
			ID:   &oID,
			Type: aType,
		}
		banner.ApplyTo = append(banner.ApplyTo, applyTo)
	}

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": bID}, bson.M{"$set": banner})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"banner":         banner,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteBanner(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("banners")

	bannerID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(bannerID)

	var banner models.Banner
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&banner)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            bannerID,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) DeleteAssignedBanner(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("banners")

	aType := c.Param("type")
	bannerID := c.Param("banner_id")
	bID, _ := primitive.ObjectIDFromHex(bannerID)
	ID := c.Param("id")

	ctx := context.Background()
	var banner models.Banner
	err := collection.FindOne(ctx, bson.M{"_id": bID}).Decode(&banner)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var IDs []*primitive.ObjectID
	if aType == "page" {
		IDs, _ = app.bannerFetchPageID(ctx, ID)
	} else if aType == "category" {
		IDs, _ = app.bannerFetchCategoryID(ctx, ID)
	} else if aType == "group_content" {
		IDs, _ = app.bannerFetchGroupContentID(ctx, ID)
	}

	var items = banner.ApplyTo
	for _, refID := range IDs {
		var applyTo []models.BannerApplyTo
		for _, item := range items {
			if *refID != *item.ID {
				applyTo = append(applyTo, item)
			}
		}
		items = applyTo
	}
	banner.ApplyTo = items

	now := time.Now()
	banner.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": bID}, bson.M{"$set": banner})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"banner":         banner,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) fetchAppliedBanner(params map[string]string) ([]bson.M, error) {
	collection := app.Config.MongoDB.Collection("banners")

	host, ok := params["host"]
	if !ok {
		host = ""
	}

	// this is an optional parameter
	ID, ok := params["id"]
	if !ok {
		ID = ""
	}
	name, ok := params["name"]
	if !ok {
		name = ""
	}
	appliedID, ok := params["appliedId"]
	if !ok {
		appliedID = ""
	}
	lang, ok := params["lang"]
	if !ok {
		lang = ""
	}
	status, ok := params["status"]
	if !ok {
		status = ""
	}
	code, ok := params["code"]
	if !ok {
		code = ""
	} else {
		code = strings.ToUpper(code)
	}
	codeSlug, ok := params["codeSlug"]
	if !ok {
		codeSlug = ""
	} else {
		codeSlug = strings.ToLower(codeSlug)
	}

	sortCol, ok := params["sortCol"]
	if !ok {
		sortCol = "id"
	}
	sortDir, ok := params["sortDir"]
	if !ok {
		sortDir = "asc"
	}

	matchFilter := bson.M{}
	if ID != "" {
		bID, _ := primitive.ObjectIDFromHex(ID)
		matchFilter["_id"] = bID
	}
	if name != "" {
		matchFilter["name"] = name
	}
	if appliedID != "" {
		aID, _ := primitive.ObjectIDFromHex(appliedID)
		matchFilter["apply_to.id"] = aID
	}
	if lang != "" {
		matchFilter["items.lang"] = lang
	}
	if status != "" {
		matchFilter["items.status"] = status
	}
	if codeSlug != "" {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_slug": codeSlug},
			bson.M{"property.property_slug": codeSlug},
		}
	} else if code != "" {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_code": code},
			bson.M{"property.property_code": code},
		}
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	pipeline := bson.A{
		bson.M{"$unwind": bson.M{"path": "$items", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "brands", "as": "brand",
			"localField": "brand_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "properties", "as": "property",
			"localField": "property_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
	}
	if appliedID != "" {
		pipelineLookupCategory := bson.A{
			bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
				bson.M{"$eq": bson.A{"$_id", "$$apply_id"}},
			}}}},
			bson.M{"$lookup": bson.M{
				"from": "categories",
				"let":  bson.M{"id": "$_id", "source_id": "$source_id"},
				"pipeline": bson.A{
					bson.M{"$match": bson.M{"$expr": bson.M{"$or": bson.A{
						bson.M{"$eq": bson.A{"$source_id", "$$id"}},
						bson.M{"$eq": bson.A{"$_id", "$$source_id"}},
					}}}},
				},
				"as": "related_categories",
			}},
			bson.M{"$unwind": bson.M{"path": "$related_categories", "preserveNullAndEmptyArrays": true}},
			bson.M{"$group": bson.M{
				"_id":       "$_id",
				"primary":   bson.M{"$first": bson.M{"id": "$_id", "type": "category"}},
				"secondary": bson.M{"$push": bson.M{"id": "$related_categories._id", "type": "category"}},
			}},
			bson.M{"$project": bson.M{
				"categories": bson.M{"$concatArrays": bson.A{bson.A{"$primary"}, "$secondary"}},
			}},
			bson.M{"$unwind": bson.M{"path": "$categories", "preserveNullAndEmptyArrays": false}},
			bson.M{"$replaceRoot": bson.M{"newRoot": "$categories"}},
		}

		pipelineLookupPage := bson.A{
			bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
				bson.M{"$eq": bson.A{"$_id", "$$apply_id"}},
			}}}},
			bson.M{"$lookup": bson.M{
				"from": "pages",
				"let":  bson.M{"id": "$_id", "source_id": "$source_id"},
				"pipeline": bson.A{
					bson.M{"$match": bson.M{"$expr": bson.M{"$or": bson.A{
						bson.M{"$eq": bson.A{"$source_id", "$$id"}},
						bson.M{"$eq": bson.A{"$_id", "$$source_id"}},
					}}}},
				},
				"as": "related_pages",
			}},
			bson.M{"$unwind": bson.M{"path": "$related_pages", "preserveNullAndEmptyArrays": true}},
			bson.M{"$group": bson.M{
				"_id":       "$_id",
				"primary":   bson.M{"$first": bson.M{"id": "$_id", "type": "page"}},
				"secondary": bson.M{"$push": bson.M{"id": "$related_pages._id", "type": "page"}},
			}},
			bson.M{"$project": bson.M{
				"pages": bson.M{"$concatArrays": bson.A{bson.A{"$primary"}, "$secondary"}},
			}},
			bson.M{"$unwind": bson.M{"path": "$pages", "preserveNullAndEmptyArrays": false}},
			bson.M{"$replaceRoot": bson.M{"newRoot": "$pages"}},
		}

		pipelineLookupGroupContent := bson.A{
			bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
				bson.M{"$eq": bson.A{"$_id", "$$apply_id"}},
			}}}},
			bson.M{"$project": bson.M{"id": "$_id", "type": "group_content"}},
		}

		pipeline = append(pipeline, bson.A{
			bson.M{"$unwind": bson.M{"path": "$apply_to", "preserveNullAndEmptyArrays": true}},
			bson.M{"$lookup": bson.M{
				"from":     "categories",
				"let":      bson.M{"apply_id": "$apply_to.id"},
				"pipeline": pipelineLookupCategory,
				"as":       "item_categories",
			}},
			bson.M{"$unwind": bson.M{"path": "$item_categories", "preserveNullAndEmptyArrays": true}},
			bson.M{"$lookup": bson.M{
				"from":     "pages",
				"let":      bson.M{"apply_id": "$apply_to.id"},
				"pipeline": pipelineLookupPage,
				"as":       "item_pages",
			}},
			bson.M{"$unwind": bson.M{"path": "$item_pages", "preserveNullAndEmptyArrays": true}},
			bson.M{"$lookup": bson.M{
				"from":     "group_content",
				"let":      bson.M{"apply_id": "$apply_to.id"},
				"pipeline": pipelineLookupGroupContent,
				"as":       "item_gc",
			}},
			bson.M{"$unwind": bson.M{"path": "$item_gc", "preserveNullAndEmptyArrays": true}},
			bson.M{"$addFields": bson.M{"apply_to.id": bson.M{"$switch": bson.M{
				"branches": bson.A{
					bson.M{
						"case": bson.M{"$and": bson.A{
							bson.M{"$eq": bson.A{"$apply_to.type", "category"}},
						}},
						"then": "$item_categories.id",
					},
					bson.M{
						"case": bson.M{"$and": bson.A{
							bson.M{"$eq": bson.A{"$apply_to.type", "page"}},
						}},
						"then": "$item_pages.id",
					},
				},
				"default": "$item_gc.id",
			}}}},
		}...)
	}
	pipeline = append(pipeline, bson.M{"$match": matchFilter})
	pipeline = append(pipeline, bson.A{
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$_id",
			"name":        "$name",
			"title":       "$items.title",
			"description": "$items.description",
			"image": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{"$items.image", ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$items.image",
					"local": bson.M{
						"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$items.image"},
					},
					"cloud": bson.M{
						"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$items.image"},
					},
				},
			}},
			"order":         "$items.order",
			"status":        "$items.status",
			"lang":          "$items.lang",
			"property.id":   "$property._id",
			"property.code": "$property.property_code",
			"property.name": "$property.property_name",
			"property.slug": "$property.property_slug",
			"brand.id":      "$brand._id",
			"brand.code":    "$brand.brand_code",
			"brand.name":    "$brand.brand_name",
			"brand.slug":    "$brand.brand_slug",
			"created_at":    "$created_at",
			"updated_at":    "$updated_at",
		}},
		bson.M{"$sort": sort},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		return nil, err
	}
	defer cursor.Close(ctx)

	results := []bson.M{}
	cursor.All(ctx, &results)

	return results, nil
}

func (app *Api) GetBannerByName(c *gin.Context) {
	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	name := c.Param("name")
	lang := c.DefaultQuery("lang", "en")
	status := c.DefaultQuery("status", "active")
	code := c.DefaultQuery("code", "cor")
	codeSlug := c.Query("codeSlug")

	sortCol := c.DefaultQuery("sortCol", "order")
	sortDir := c.DefaultQuery("sortDir", "asc")

	params := map[string]string{
		"host":     host,
		"name":     name,
		"lang":     lang,
		"status":   status,
		"code":     code,
		"codeSlug": codeSlug,
		"sortCol":  sortCol,
		"sortDir":  sortDir,
	}

	results, err := app.fetchAppliedBanner(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}

func (app *Api) GetBannerByAppliedID(c *gin.Context) {
	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	ID := c.Param("id")
	lang := c.DefaultQuery("lang", "en")
	status := c.DefaultQuery("status", "active")
	code := c.DefaultQuery("code", "cor")
	codeSlug := c.Query("codeSlug")

	sortCol := c.DefaultQuery("sortCol", "order")
	sortDir := c.DefaultQuery("sortDir", "asc")

	params := map[string]string{
		"host":      host,
		"appliedId": ID,
		"lang":      lang,
		"status":    status,
		"code":      code,
		"codeSlug":  codeSlug,
		"sortCol":   sortCol,
		"sortDir":   sortDir,
	}

	results, err := app.fetchAppliedBanner(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}
