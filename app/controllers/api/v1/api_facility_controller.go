package v1

import (
	"alaric_cms/app/models"
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetFacility(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	search := c.Query("search")
	fType := c.DefaultQuery("type", "facility")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "asc")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	matchFilter := bson.M{"type": fType}
	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"items.name": searchRe},
			bson.M{"items.title": searchRe},
			bson.M{"items.lang": searchRe},
			bson.M{"created_at": searchRe},
			bson.M{"updated_at": searchRe},
		}
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$unwind": bson.M{
			"path":                       "$items",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$items.lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
					bson.M{"$eq": bson.A{"$status", "primary"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":    0,
					"id":     "$_id",
					"code":   "$code",
					"name":   "$name",
					"native": "$native",
					"status": "$status",
				}},
			},
			"as": "items.lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$items.lang",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$_id",
			"name":        "$items.name",
			"title":       "$items.title",
			"description": "$items.description",
			"lang":        "$items.lang",
			"icon":        "$icon",
			"type":        "$type",
			"parent_id":   "$parent_id",
			"created_at":  "$created_at",
			"updated_at":  "$updated_at",
		}},
		bson.M{"$sort": sort},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var results []bson.M
	cursor.All(ctx, &results)

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": len(results),
		},
	})
}

func (app *Api) GetPrimaryFacility(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	search := c.Query("search")
	fType := c.DefaultQuery("type", "facility")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "asc")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	matchFilter := bson.M{"parent_id": nil, "type": fType}
	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"items.name": searchRe},
			bson.M{"items.title": searchRe},
			bson.M{"items.lang": searchRe},
			bson.M{"created_at": searchRe},
			bson.M{"updated_at": searchRe},
		}
	}

	stageUnwindItems := bson.M{
		"path":                       "$items",
		"preserveNullAndEmptyArrays": false,
	}

	stageUnwindLang := bson.M{
		"path":                       "$items.lang",
		"preserveNullAndEmptyArrays": false,
	}

	stageMatch := bson.M{"$expr": bson.M{"$and": bson.A{
		bson.M{"$eq": bson.A{"$type", fType}},
		bson.M{"$eq": bson.A{"$parent_id", "$$id"}},
		bson.M{"$eq": bson.A{"$items.lang", "$$lang"}},
	}}}

	stageLet := bson.M{"id": "$_id", "lang": "$items.lang.code"}

	stageLookupLang := bson.M{
		"from": "complements",
		"let":  bson.M{"lang": "$items.lang"},
		"pipeline": bson.A{
			bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
				bson.M{"$eq": bson.A{"$code", "$$lang"}},
				bson.M{"$eq": bson.A{"$type", "lang"}},
				bson.M{"$eq": bson.A{"$status", "primary"}},
			}}}},
			bson.M{"$project": bson.M{
				"_id":    0,
				"id":     "$_id",
				"code":   "$code",
				"name":   "$name",
				"native": "$native",
				"status": "$status",
			}},
		},
		"as": "items.lang",
	}

	stageProject := bson.M{
		"_id":         0,
		"id":          "$_id",
		"name":        "$items.name",
		"title":       "$items.title",
		"description": "$items.description",
		"lang":        "$items.lang",
		"icon":        "$icon",
		"type":        "$type",
		"parent_id":   "$parent_id",
		"children":    "$children",
		"created_at":  "$created_at",
		"updated_at":  "$updated_at",
	}

	pipeline := bson.A{}
	pipeline = append(pipeline, bson.A{
		bson.M{"$unwind": stageUnwindItems},
		bson.M{"$match": matchFilter},
		bson.M{"$lookup": stageLookupLang},
		bson.M{"$unwind": stageUnwindLang},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  stageLet,
			"pipeline": bson.A{
				bson.M{"$unwind": stageUnwindItems},
				bson.M{"$match": stageMatch},
				bson.M{"$lookup": stageLookupLang},
				bson.M{"$unwind": stageUnwindLang},
				bson.M{"$lookup": bson.M{
					"from": "complements",
					"let":  stageLet,
					"pipeline": bson.A{
						bson.M{"$unwind": stageUnwindItems},
						bson.M{"$match": stageMatch},
						bson.M{"$lookup": stageLookupLang},
						bson.M{"$unwind": stageUnwindLang},
						bson.M{"$lookup": bson.M{
							"from": "complements",
							"let":  stageLet,
							"pipeline": bson.A{
								bson.M{"$unwind": stageUnwindItems},
								bson.M{"$match": stageMatch},
								bson.M{"$lookup": stageLookupLang},
								bson.M{"$unwind": stageUnwindLang},
								bson.M{"$lookup": bson.M{
									"from": "complements",
									"let":  stageLet,
									"pipeline": bson.A{
										bson.M{"$unwind": stageUnwindItems},
										bson.M{"$match": stageMatch},
										bson.M{"$lookup": stageLookupLang},
										bson.M{"$unwind": stageUnwindLang},
										bson.M{"$lookup": bson.M{
											"from": "complements",
											"let":  stageLet,
											"pipeline": bson.A{
												bson.M{"$unwind": stageUnwindItems},
												bson.M{"$match": stageMatch},
												bson.M{"$project": stageProject},
											},
											"as": "children",
										}},
										bson.M{"$project": stageProject},
									},
									"as": "children",
								}},
								bson.M{"$project": stageProject},
							},
							"as": "children",
						}},
						bson.M{"$project": stageProject},
					},
					"as": "children",
				}},
				bson.M{"$project": stageProject},
			},
			"as": "children",
		}},
		bson.M{"$project": stageProject},
		bson.M{"$sort": sort},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var results []bson.M
	cursor.All(ctx, &results)

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": len(results),
		},
	})
}

func (app *Api) GetFacilityByID(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	facilityID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(facilityID)
	fType := c.DefaultQuery("type", "facility")

	pipeline := bson.A{
		bson.M{"$match": bson.M{"_id": objectID, "type": fType}},
		bson.M{"$unwind": bson.M{
			"path":                       "$items",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$items.lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":    0,
					"id":     "$_id",
					"code":   "$code",
					"name":   "$name",
					"native": "$native",
					"status": "$status",
				}},
			},
			"as": "items.lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$items.lang",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$group": bson.M{
			"_id":        "$_id",
			"type":       bson.M{"$first": "$type"},
			"icon":       bson.M{"$first": "$icon"},
			"items":      bson.M{"$push": "$items"},
			"parent_id":  bson.M{"$first": "$parent_id"},
			"created_at": bson.M{"$first": "$created_at"},
			"updated_at": bson.M{"$first": "$updated_at"},
		}},
		bson.M{"$project": bson.M{
			"_id":        0,
			"id":         "$_id",
			"type":       "$type",
			"icon":       "$icon",
			"items":      "$items",
			"parent_id":  "$parent_id",
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
	}

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var result bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&result)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       result,
	})
}

func (app *Api) GetFacilityGroupedLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	fType := c.DefaultQuery("type", "facility")

	pipeline := bson.A{
		bson.M{"$match": bson.M{"type": fType}},
		bson.M{"$unwind": bson.M{
			"path":                       "$items",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$_id",
			"name":        "$items.name",
			"title":       "$items.title",
			"description": "$items.description",
			"lang":        "$items.lang",
			"icon":        "$icon",
			"type":        "$type",
			"parent_id":   "$parent_id",
			"created_at":  "$created_at",
			"updated_at":  "$updated_at",
		}},
		bson.M{"$group": bson.M{
			"_id":   "$lang",
			"items": bson.M{"$push": "$$ROOT"},
		}},
		bson.M{"$addFields": bson.M{
			"facilities": bson.A{bson.M{
				"k": "$_id",
				"v": "$items",
			}},
		}},
		bson.M{"$replaceRoot": bson.M{
			"newRoot": bson.M{"$arrayToObject": "$facilities"},
		}},
	}

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facilities bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facilities)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       facilities,
	})
}

func (app *Api) GetFacilityTreeGroupedLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	search := c.Query("search")
	lang := c.Query("lang")
	fType := c.DefaultQuery("type", "facility")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "asc")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	matchFilter := bson.M{"parent_id": nil, "type": fType}
	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"items.name": searchRe},
			bson.M{"items.title": searchRe},
			bson.M{"items.lang": searchRe},
			bson.M{"created_at": searchRe},
			bson.M{"updated_at": searchRe},
		}
	}

	if lang != "" {
		matchFilter["items.lang"] = lang
	}

	stageUnwindItems := bson.M{
		"path":                       "$items",
		"preserveNullAndEmptyArrays": false,
	}

	stageMatch := bson.M{"$expr": bson.M{"$and": bson.A{
		bson.M{"$eq": bson.A{"$type", fType}},
		bson.M{"$eq": bson.A{"$parent_id", "$$id"}},
		bson.M{"$eq": bson.A{"$items.lang", "$$lang"}},
	}}}

	stageLet := bson.M{"id": "$_id", "lang": "$items.lang"}

	stageProject := bson.M{
		"_id":       0,
		"id":        "$_id",
		"name":      "$items.name",
		"title":     "$items.title",
		"lang":      "$items.lang",
		"icon":      "$icon",
		"parent_id": "$parent_id",
		"children":  "$children",
	}

	stageGroup := bson.A{
		bson.M{"$group": bson.M{
			"_id":   "$lang",
			"items": bson.M{"$push": "$$ROOT"},
		}},
		bson.M{"$addFields": bson.M{
			"items": bson.A{bson.M{
				"k": "$_id",
				"v": "$items",
			}},
		}},
		bson.M{"$replaceRoot": bson.M{
			"newRoot": bson.M{"$arrayToObject": "$items"},
		}},
	}

	pipeline := bson.A{}
	pipeline = append(pipeline, bson.A{
		bson.M{"$unwind": stageUnwindItems},
		bson.M{"$match": matchFilter},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  stageLet,
			"pipeline": bson.A{
				bson.M{"$unwind": stageUnwindItems},
				bson.M{"$match": stageMatch},
				bson.M{"$lookup": bson.M{
					"from": "complements",
					"let":  stageLet,
					"pipeline": bson.A{
						bson.M{"$unwind": stageUnwindItems},
						bson.M{"$match": stageMatch},
						bson.M{"$lookup": bson.M{
							"from": "complements",
							"let":  stageLet,
							"pipeline": bson.A{
								bson.M{"$unwind": stageUnwindItems},
								bson.M{"$match": stageMatch},
								bson.M{"$lookup": bson.M{
									"from": "complements",
									"let":  stageLet,
									"pipeline": bson.A{
										bson.M{"$unwind": stageUnwindItems},
										bson.M{"$match": stageMatch},
										bson.M{"$lookup": bson.M{
											"from": "complements",
											"let":  stageLet,
											"pipeline": bson.A{
												bson.M{"$unwind": stageUnwindItems},
												bson.M{"$match": stageMatch},
												bson.M{"$project": stageProject},
											},
											"as": "children",
										}},
										bson.M{"$project": stageProject},
									},
									"as": "children",
								}},
								bson.M{"$project": stageProject},
							},
							"as": "children",
						}},
						bson.M{"$project": stageProject},
					},
					"as": "children",
				}},
				bson.M{"$project": stageProject},
			},
			"as": "children",
		}},
		bson.M{"$project": stageProject},
		bson.M{"$sort": sort},
	}...)
	pipeline = append(pipeline, stageGroup...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var results bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&results)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}

func (app *Api) AddFacility(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	var payload models.FacilityForm
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	ctx := context.Background()

	var facilities []interface{}
	for i := 0; i < len(payload.Type); i++ {
		var facility models.Facility

		if len(payload.Type) > i {
			facility.Type = payload.Type[i]
		}
		if len(payload.Icon) > i {
			facility.Icon = payload.Icon[i]
		}

		if len(payload.ParentID) > i && payload.ParentID[i] != "" {
			pID, _ := primitive.ObjectIDFromHex(payload.ParentID[i])
			facility.ParentID = &pID
		}

		var item models.FacilityItem
		if len(payload.Name) > i {
			item.Name = payload.Name[i]
		}
		if len(payload.Title) > i {
			item.Title = payload.Title[i]
		}
		if len(payload.Description) > i {
			item.Description = payload.Description[i]
		}
		if len(payload.Lang) > i {
			item.Lang = payload.Lang[i]
		}

		cursor, _ := collection.Find(ctx, bson.M{
			"items.name": item.Name,
			"items.lang": item.Lang,
			"type":       facility.Type,
		})
		defer cursor.Close(ctx)

		// "Facility already exists.",
		if cursor.Next(ctx) {
			continue
		} else {
			if item.Name != "" && item.Lang != "" {
				facility.Items = append(facility.Items, item)

				now := time.Now()
				facility.CreatedAt = &now

				facilities = append(facilities, facility)
			}
		}
	}

	var message string
	var inserted int
	var resultIDs []interface{}
	if len(facilities) > 0 {
		result, err := collection.InsertMany(ctx, facilities)
		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}
		message = "inserted successfully."
		inserted = len(result.InsertedIDs)
		resultIDs = result.InsertedIDs
	} else {
		message = "Empty payload."
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    message,
		Data: gin.H{
			"ids":      resultIDs,
			"inserted": inserted,
		},
	})
}

func (app *Api) UpdateFacility(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	facilityID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(facilityID)

	var facility models.Facility
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&facility)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var fType, name, icon, title, description, lang, parentID []string
	icon = append(name, facility.Icon)
	fType = append(fType, facility.Type)
	for _, item := range facility.Items {
		name = append(name, item.Name)
		title = append(title, item.Title)
		description = append(description, item.Description)
		lang = append(description, item.Lang)
	}

	if facility.ParentID != nil {
		parentID = append(parentID, facility.ParentID.Hex())
	}

	var payload = models.FacilityForm{
		Type:        fType,
		Name:        name,
		Title:       title,
		Icon:        icon,
		Description: description,
		Lang:        lang,
		ParentID:    parentID,
	}
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	items := []models.FacilityItem{}
	for i := range payload.Name {
		item := models.FacilityItem{}

		if len(payload.Name) > i {
			item.Name = payload.Name[i]
		}
		if len(payload.Title) > i {
			item.Title = payload.Title[i]
		}
		if len(payload.Description) > i {
			item.Description = payload.Description[i]
		}
		if len(payload.Lang) > i {
			item.Lang = payload.Lang[i]
		}

		cursor, err := collection.Find(ctx, bson.M{
			"_id":        bson.M{"$ne": objectID},
			"items.name": item.Name,
			"items.lang": item.Lang,
			"type":       facility.Type,
		})

		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}
		defer cursor.Close(ctx)

		if cursor.Next(ctx) {
			c.JSON(http.StatusConflict, &models.ReE{
				StatusCode: http.StatusConflict,
				Error:      fmt.Sprintf("%s %s already exists.", facility.Type, item.Name),
			})
			return
		}

		items = append(items, item)
	}

	if len(items) > 0 {
		facility.Items = items
	}
	if len(payload.Icon) > 0 {
		facility.Icon = payload.Icon[0]
	}

	if len(payload.ParentID) > 0 && payload.ParentID[0] != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.ParentID[0])
		facility.ParentID = &pID
	} else {
		facility.ParentID = nil
	}

	now := time.Now()
	facility.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": facility})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"facility":       facility,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteFacility(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	facilityID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(facilityID)

	ctx := context.Background()
	doc, err := collection.DeleteMany(ctx, bson.M{"$or": bson.A{
		bson.M{"_id": objectID},
		bson.M{"parent_id": objectID},
	}})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            facilityID,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) DeleteFacilityByLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	tagID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(tagID)
	lang := c.Param("lang")

	var facility models.Facility
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&facility)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var items []models.FacilityItem
	for _, item := range facility.Items {
		if item.Lang != lang {
			items = append(items, item)
		}
	}
	facility.Items = items

	now := time.Now()
	facility.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": facility})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"facility":       facility,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) fetchFacility(params map[string]string) ([]bson.M, error) {
	collection := app.Config.MongoDB.Collection("complements")

	facilityType, ok := params["type"]
	if !ok {
		facilityType = "facility"
	}

	inIDs := bson.A{}
	IDs := strings.Split(params["inId"], ",")
	for _, ID := range IDs {
		ID = strings.TrimSpace(ID)
		iID, err := primitive.ObjectIDFromHex(ID)
		if err == nil {
			inIDs = append(inIDs, iID)
		}
	}

	lang, ok := params["lang"]
	if !ok {
		lang = "en"
	}

	sortCol, ok := params["sortCol"]
	if !ok {
		sortCol = "id"
	}
	sortDir, ok := params["sortDir"]
	if !ok {
		sortDir = "asc"
	}

	matchFilter := bson.M{}
	if len(inIDs) > 0 {
		matchFilter["_id"] = bson.M{"$in": inIDs}
	}
	if facilityType != "" {
		matchFilter["type"] = facilityType
	}
	if lang != "" {
		matchFilter["items.lang"] = lang
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	pipeline := bson.A{
		bson.M{"$unwind": bson.M{
			"path":                       "$items",
			"preserveNullAndEmptyArrays": false,
		}},
	}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$_id",
			"name":        "$items.name",
			"title":       "$items.title",
			"description": "$items.description",
			"lang":        "$items.lang",
			"icon":        "$icon",
			"type":        "$type",
			"parent_id":   "$parent_id",
		}},
		bson.M{"$sort": sort},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		return nil, err
	}
	defer cursor.Close(ctx)

	var results []bson.M
	cursor.All(ctx, &results)

	return results, nil
}

func (app *Api) GetFacilityList(c *gin.Context) {
	// this is an optional parameter
	ID := c.Query("id")
	lang := c.Query("lang")

	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "asc")

	params := map[string]string{
		"inId":    ID,
		"lang":    lang,
		"sortCol": sortCol,
		"sortDir": sortDir,
	}

	facilities, err := app.fetchFacility(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"items": facilities,
			"total": len(facilities),
		},
	})
}

func (app *Api) GetAmenitiesList(c *gin.Context) {
	// this is an optional parameter
	ID := c.Query("id")
	lang := c.Query("lang")

	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "asc")

	params := map[string]string{
		"inId":    ID,
		"lang":    lang,
		"type":    "amenities",
		"sortCol": sortCol,
		"sortDir": sortDir,
	}

	amenities, err := app.fetchFacility(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"items": amenities,
			"total": len(amenities),
		},
	})
}
