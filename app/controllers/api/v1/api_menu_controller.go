package v1

import (
	"alaric_cms/app/models"
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetMenu(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("menu")

	lang := c.Query("lang")
	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")

	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	// Pass these options to the Find method
	findOptions := options.Find()
	findOptions.SetLimit(limit)
	findOptions.SetSkip(offset)
	findOptions.SetProjection(bson.M{
		"structure": 0,
	})

	findFilter := bson.M{}
	if lang != "" {
		findFilter["lang"] = lang
	}

	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		findFilter["brand_id"] = bID
	}

	if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		findFilter["property_id"] = pID
	}

	ctx := context.Background()
	cursor, err := collection.Find(
		ctx,
		findFilter,
		findOptions,
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var menus []models.Menu
	cursor.All(ctx, &menus)

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  menus,
			"total": len(menus),
		},
	})
}

func (app *Api) GetMenuByID(c *gin.Context) {
	menuID := c.Param("id")

	params := map[string]string{
		"id": menuID,
	}

	menus, err := app.fetchMenu(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	fmt.Println(menus)
	menu := bson.M{}
	if len(menus) > 0 {
		menu = menus[0]

		brand := menu["brand"].(bson.M)
		property := menu["property"].(bson.M)

		var code string
		if property != nil && len(property) > 0 {
			code = property["code"].(string)
		} else if brand != nil && len(brand) > 0 {
			code = brand["code"].(string)
		}

		structure := []bson.M{}
		if code != "" && menu["structure"] != nil {
			structure = app.serializeTreeMenu(menu["structure"].(bson.A), code, "", "")
		}
		menu["structure"] = structure
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       menu,
	})
}

func (app *Api) AddMenu(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("menu")

	var payload models.MenuForm
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	now := time.Now()
	var menu = models.Menu{
		Name:      payload.Name,
		Lang:      payload.Lang,
		Location:  payload.Location,
		CreatedAt: &now,
	}

	findFilter := bson.M{}
	if payload.BrandID != "" {
		bID, _ := primitive.ObjectIDFromHex(payload.BrandID)
		menu.BrandID = &bID
		findFilter["brand_id"] = bID
	}

	if payload.PropertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.PropertyID)
		menu.PropertyID = &pID
		findFilter["property_id"] = pID
	}

	findFilter["location"] = payload.Location
	findFilter["lang"] = payload.Lang

	ctx := context.Background()
	cursor, err := collection.Find(ctx, findFilter)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusConflict, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Menu already exists.",
		})
		return
	}

	result, err := collection.InsertOne(ctx, menu)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"id": result.InsertedID,
		},
	})
}

func (app *Api) UpdateMenu(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("menu")

	menuID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(menuID)

	var menu models.Menu
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&menu)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var brandID, propertyID string
	if menu.BrandID != nil {
		brandID = menu.BrandID.Hex()
	}

	if menu.PropertyID != nil {
		propertyID = menu.PropertyID.Hex()
	}

	var payload = models.MenuForm{
		Name:       menu.Name,
		Lang:       menu.Lang,
		Location:   menu.Location,
		Structure:  menu.Structure,
		BrandID:    brandID,
		PropertyID: propertyID,
	}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	now := time.Now()

	menu.Name = payload.Name
	menu.Lang = payload.Lang
	menu.Location = payload.Location
	menu.Structure = payload.Structure
	menu.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": menu})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"menu":           menu,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteMenu(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("menu")

	menuID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(menuID)

	ctx := context.Background()
	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            menuID,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) fetchMenu(params map[string]string) ([]bson.M, error) {
	collection := app.Config.MongoDB.Collection("menu")

	host, ok := params["host"]
	if !ok {
		host = ""
	}

	// this is an optional parameter
	ID, ok := params["id"]
	if !ok {
		ID = ""
	}
	location, ok := params["location"]
	if !ok {
		location = ""
	}
	lang, ok := params["lang"]
	if !ok {
		lang = ""
	}
	code, ok := params["code"]
	if !ok {
		code = ""
	} else {
		code = strings.ToUpper(code)
	}
	codeSlug, ok := params["codeSlug"]
	if !ok {
		codeSlug = ""
	} else {
		codeSlug = strings.ToLower(codeSlug)
	}

	matchFilter := bson.M{}
	if ID != "" {
		oID, _ := primitive.ObjectIDFromHex(ID)
		matchFilter["_id"] = oID
	}
	if location != "" {
		matchFilter["location"] = location
	}
	if lang != "" {
		matchFilter["lang"] = lang
	}
	if codeSlug != "" {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_slug": codeSlug},
			bson.M{"property.property_slug": codeSlug},
		}
	} else if code != "" {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_code": code},
			bson.M{"property.property_code": code},
		}
	}

	pipeline := bson.A{
		bson.M{"$lookup": bson.M{
			"from": "brands", "as": "brand",
			"localField": "brand_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "properties", "as": "property",
			"localField": "property_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "brands", "as": "bop", // brand of property
			"localField": "property.brand_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$bop", "preserveNullAndEmptyArrays": true}},
	}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$addFields": bson.M{
			"brand": bson.M{"$ifNull": bson.A{"$brand", "$bop"}},
		}},
		bson.M{"$project": bson.M{
			"_id":       0,
			"id":        "$_id",
			"name":      "$name",
			"lang":      "$lang",
			"location":  "$location",
			"structure": "$structure",
			"brand": bson.M{
				"id":   "$brand._id",
				"code": "$brand.brand_code",
				"name": "$brand.brand_name",
				"slug": "$brand.brand_slug",
				"logo": bson.M{"$cond": bson.A{
					bson.M{"$and": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand.brand_logo", ""}}, ""}},
						bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand.brand_secondary_logo", ""}}, ""}},
					}}, "$no_logo",
					bson.M{
						"primary": bson.M{"$cond": bson.M{
							"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand.brand_logo", ""}}, ""}},
							"then": bson.M{},
							"else": bson.M{
								"name": "$brand.brand_logo",
								"local": bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$brand.brand_logo"},
								},
								"cloud": bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$brand.brand_logo"},
								},
							},
						}},
						"scondary": bson.M{"$cond": bson.M{
							"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand.brand_secondary_logo", ""}}, ""}},
							"then": bson.M{},
							"else": bson.M{
								"name": "$brand.brand_secondary_logo",
								"local": bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$brand.brand_secondary_logo"},
								},
								"cloud": bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$brand.brand_secondary_logo"},
								},
							},
						}},
					},
				}},
			},
			"property": bson.M{
				"id":   "$property._id",
				"code": "$property.property_code",
				"name": "$property.property_name",
				"slug": "$property.property_slug",
				"logo": bson.M{"$cond": bson.A{
					bson.M{"$and": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_logo", ""}}, ""}},
						bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_secondary_logo", ""}}, ""}},
					}}, "$no_logo",
					bson.M{
						"primary": bson.M{"$cond": bson.M{
							"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_logo", ""}}, ""}},
							"then": bson.M{},
							"else": bson.M{
								"name": "$property.property_logo",
								"local": bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_logo"},
								},
								"cloud": bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_logo"},
								},
							},
						}},
						"scondary": bson.M{"$cond": bson.M{
							"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property.property_secondary_logo", ""}}, ""}},
							"then": bson.M{},
							"else": bson.M{
								"name": "$property.property_secondary_logo",
								"local": bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property.property_secondary_logo"},
								},
								"cloud": bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property.property_secondary_logo"},
								},
							},
						}},
					},
				}},
			},
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer cursor.Close(ctx)

	results := []bson.M{}
	cursor.All(ctx, &results)

	return results, nil
}

func (app *Api) serializeTreeMenu(root bson.A, code string, codeSlug string, host string) []bson.M {
	var results []bson.M
	for i, _ := range root {
		item := root[i].(bson.M)
		menu := bson.M{
			"ref_id":   item["ref_id"],
			"type":     item["type"],
			"url":      item["url"],
			"label":    item["label"],
			"original": "",
			"slug":     "",
			"new_tab":  item["new_tab"],
			"children": []bson.M{},
		}

		if host != "" && item["image"] != "" {
			var image = item["image"].(string)
			var localImg, cloudImg string
			if !strings.HasPrefix(image, "http") {
				localImg = fmt.Sprintf("%s%s%s/%s", host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, image)
				cloudImg = fmt.Sprintf("%s/%s", app.Config.Path.CloudImagePrefix, image)
			} else {
				localImg = image
				cloudImg = image
			}

			menu["image"] = bson.M{
				"name":  image,
				"local": localImg,
				"cloud": cloudImg,
			}
		} else {
			menu["image"] = item["image"]
		}

		isErrorFetched := false
		if item["type"] != "custom" {
			params := map[string]string{
				"id":       item["ref_id"].(primitive.ObjectID).Hex(),
				"code":     code,
				"codeSlug": codeSlug,
			}
			if item["type"] == "page" {
				fetch, err := app.fetchPage(params)
				if err != nil {
					isErrorFetched = true
				} else {
					facetData := fetch["paginatedResults"].(primitive.A)
					pages := make(bson.M)
					if len(facetData) > 0 {
						pages = facetData[0].(bson.M)
						menu["slug"] = pages["slug"]
						menu["original"] = pages["title"]
					}
				}
			} else if item["type"] == "category" {
				fetch, err := app.fetchCategory(params)
				if err != nil {
					isErrorFetched = true
				} else {
					facetData := fetch["paginatedResults"].(primitive.A)
					categories := make(bson.M)
					if len(facetData) > 0 {
						categories = facetData[0].(bson.M)
						menu["slug"] = categories["slug"]
						menu["original"] = categories["title"]
					}
				}
			} else if item["type"] == "gc" {
				fetch, err := app.fetchGroupContent(params)
				if err != nil {
					isErrorFetched = true
				} else {
					menu["slug"] = fetch["name"]
					menu["original"] = fetch["name"]
				}
			}
		}

		if item["children"] != nil {
			children := item["children"].(bson.A)
			if len(children) > 0 && !isErrorFetched {
				menu["children"] = app.serializeTreeMenu(children, code, codeSlug, host)
			}
		}

		results = append(results, menu)
	}

	return results
}

func (app *Api) GetMenuByPropertyCode(c *gin.Context) {
	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	code := strings.ToUpper(c.DefaultQuery("code", "cor"))
	codeSlug := strings.ToLower(c.Query("codeSlug"))
	lang := c.Query("lang")
	location := c.DefaultQuery("location", "primary")

	params := map[string]string{
		"host":     host,
		"code":     code,
		"codeSlug": codeSlug,
		"lang":     lang,
		"location": location,
	}

	menus, err := app.fetchMenu(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	for i, _ := range menus {
		item := menus[i]
		structure := []bson.M{}
		if item["structure"] != nil {
			structure = app.serializeTreeMenu(item["structure"].(bson.A), code, codeSlug, host)
		}
		menus[i]["structure"] = structure
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       menus,
	})
}
