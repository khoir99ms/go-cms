package v1

import (
	"alaric_cms/app/helpers"
	"alaric_cms/app/models"
	"context"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetBrand(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("brands")

	brandID := c.Query("id")
	search := c.Query("search")

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "_id")
	sortDir := c.DefaultQuery("sortDir", "-1")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	matchFilter := bson.M{}
	if brandID != "" {
		objectID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["_id"] = objectID
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"brand_code": searchRe},
			bson.M{"brand_name": searchRe},
			bson.M{"brand_registered": searchRe},
			bson.M{"brand_modified": searchRe},
		}
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}
	pipeline = append(pipeline, bson.A{
		bson.M{"$addFields": bson.M{"brand_id": "$_id"}},
		bson.M{"$project": bson.M{"_id": 0}},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": facetPipelinePaging,
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	var results interface{}
	if brandID != "" && len(facetData) == 1 {
		results = facetData[0]
	} else {
		results = facetData
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": total,
		},
	})
}

func (app *Api) GetBrandWithProperty(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("brands")

	brandID := c.Query("id")
	code := strings.ToUpper(c.Query("code"))
	slug := strings.ToLower(c.Query("slug"))
	search := c.Query("search")

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "-1")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	matchFilter := bson.M{}
	if brandID != "" {
		objectID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["_id"] = objectID
	}
	if code != "" {
		matchFilter["brand_code"] = code
	}
	if slug != "" {
		matchFilter["brand_slug"] = slug
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"brand_code": searchRe},
			bson.M{"brand_name": searchRe},
			bson.M{"brand_registered": searchRe},
			bson.M{"brand_modified": searchRe},
		}
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}
	pipeline = append(pipeline, bson.A{
		bson.M{"$lookup": bson.M{
			"from": "properties", "as": "property",
			"localField": "_id", "foreignField": "brand_id",
		}},
		bson.M{"$unwind": bson.M{
			"path": "$property", "preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$group": bson.M{
			"_id":        "$_id",
			"brand":      bson.M{"$first": "$$ROOT"},
			"properties": bson.M{"$push": "$property"},
		}},
		bson.M{"$project": bson.M{
			"_id":          0,
			"id":           "$_id",
			"code":         "$brand.brand_code",
			"name":         "$brand.brand_name",
			"display_name": "$brand.brand_display_name",
			"slug":         "$brand.brand_slug",
			"properties": bson.M{"$map": bson.M{
				"input": "$properties",
				"as":    "property",
				"in": bson.M{
					"id":           "$$property._id",
					"code":         "$$property.property_code",
					"name":         "$$property.property_name",
					"display_name": "$$property.property_display_name",
					"slug":         "$$property.property_slug",
				},
			}},
		}},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": facetPipelinePaging,
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  facetData,
			"total": total,
		},
	})
}

func (app *Api) AddBrand(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("brands")

	var payload models.Brand
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}
	var regex, _ = regexp.Compile(`[ ]+`)
	payload.BrandCode = strings.ToUpper(payload.BrandCode)
	payload.BrandName = strings.Trim(regex.ReplaceAllString(payload.BrandName, " "), " ")

	ctx := context.Background()
	cursor, err := collection.Find(ctx, bson.M{
		"$or": bson.A{
			bson.M{"brand_code": payload.BrandCode},
			bson.M{"brand_name": payload.BrandName},
		},
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Brand already exists.",
		})
		return
	}

	now := time.Now()
	payload.BrandSlug = helpers.Slugify(payload.BrandName)
	payload.BrandRegistered = &now

	result, err := collection.InsertOne(ctx, payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"brand_id": result.InsertedID,
		},
	})
}

func (app *Api) UpdateBrand(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("brands")

	brandId := c.Param("brandId")
	objectID, _ := primitive.ObjectIDFromHex(brandId)

	var brand models.Brand
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&brand)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	if err := c.ShouldBind(&brand); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var regex, _ = regexp.Compile(`[ ]+`)
	brand.BrandName = strings.Trim(regex.ReplaceAllString(brand.BrandName, " "), " ")
	brand.BrandCode = strings.ToUpper(brand.BrandCode)

	cursor, err := collection.Find(ctx, bson.M{
		"_id": bson.M{"$ne": objectID},
		"$or": bson.A{
			bson.M{"brand_code": brand.BrandCode},
			bson.M{"brand_name": brand.BrandName},
		},
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Brand already exists.",
		})
		return
	}

	now := time.Now()
	brand.BrandSlug = helpers.Slugify(brand.BrandName)
	brand.BrandModified = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": brand})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"brand":          brand,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteBrand(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("brands")

	brandId := c.Param("brandId")
	objectID, _ := primitive.ObjectIDFromHex(brandId)

	var brand models.Brand
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&brand)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"brand_id":      brandId,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) GetBrandList(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("brands")

	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	search := c.Query("search")
	code := strings.ToUpper(c.Query("code"))
	codeSlug := strings.ToLower(c.Query("codeSlug"))
	lang := c.DefaultQuery("lang", "en")

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "order")
	sortDir := c.DefaultQuery("sortDir", "asc")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	matchFilter := bson.M{}
	if code != "" {
		matchFilter["brand_code"] = code
	}

	if codeSlug != "" {
		matchFilter["brand_slug"] = codeSlug
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"brand_code": searchRe},
			bson.M{"brand_name": searchRe},
			bson.M{"brand_tagline": searchRe},
			bson.M{"brand_registered": searchRe},
			bson.M{"brand_modified": searchRe},
		}
	}

	pipeline := bson.A{}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$project": bson.M{
			"_id":          0,
			"id":           "$_id",
			"code":         "$brand_code",
			"name":         "$brand_name",
			"display_name": "$brand_display_name",
			"tagline":      bson.M{"$ifNull": bson.A{fmt.Sprintf("$brand_tagline.%s", lang), ""}},
			"description":  bson.M{"$ifNull": bson.A{fmt.Sprintf("$brand_description.%s", lang), ""}},
			"content":      bson.M{"$ifNull": bson.A{fmt.Sprintf("$brand_content.%s", lang), ""}},
			"slug":         "$brand_slug",
			"logo": bson.M{
				"primary": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand_logo", ""}}, ""}},
					"then": bson.M{},
					"else": bson.M{
						"name": "$brand_logo",
						"local": bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$brand_logo"},
						},
						"cloud": bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$brand_logo"},
						},
					},
				}},
				"secondary": bson.M{"$cond": bson.M{
					"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand_secondary_logo", ""}}, ""}},
					"then": bson.M{},
					"else": bson.M{
						"name": "$brand_secondary_logo",
						"local": bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$brand_secondary_logo"},
						},
						"cloud": bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$brand_secondary_logo"},
						},
					},
				}},
			},
			"featured_image": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand_featured_image", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$brand_featured_image",
					"local": bson.M{
						"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$brand_featured_image"},
					},
					"cloud": bson.M{
						"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$brand_featured_image"},
					},
				},
			}},
			"attachment_image": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand_image_attachment", ""}}, ""}},
				"then": bson.A{},
				"else": bson.M{
					"$map": bson.M{
						"input": "$brand_image_attachment",
						"as":    "image",
						"in": bson.M{
							"name": "$$image",
							"local": bson.M{
								"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$image"},
							},
							"cloud": bson.M{
								"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$image"},
							},
						},
					},
				},
			}},
			"created_at": "$brand_registered",
			"updated_at": "$brand_modified",
		}},
		bson.M{"$facet": bson.M{
			"paginatedResults": facetPipelinePaging,
			"totalCount":       bson.A{bson.M{"$count": "count"}},
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	var results interface{}
	if code != "" && len(facetData) == 1 {
		results = facetData[0]
	} else {
		results = facetData
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"items": results,
			"total": total,
		},
	})
}
