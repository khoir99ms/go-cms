package v1

import (
	"alaric_cms/app/models"
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetTeam(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	search := c.Query("search")
	teamID := c.Query("id")
	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")
	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "-1")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	matchFilter := bson.M{}
	if teamID != "" {
		objectID, _ := primitive.ObjectIDFromHex(teamID)
		matchFilter["_id"] = objectID
	}
	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["brand_id"] = bID
	}
	if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		matchFilter["property_id"] = pID
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"name": searchRe},
			bson.M{"position": searchRe},
			bson.M{"created_at": searchRe},
			bson.M{"updated_at": searchRe},
		}
	}

	pipeline := bson.A{
		bson.M{"$match": bson.M{"type": "team"}},
		bson.M{"$lookup": bson.M{
			"from": "brands", "as": "brand",
			"localField": "brand_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "properties", "as": "property",
			"localField": "property_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
	}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$_id",
			"name":        "$name",
			"position":    "$position",
			"description": "$description",
			"order":       "$order",
			"picture":     "$picture",
			"email":       "$email",
			"facebook":    "$facebook",
			"twitter":     "$twitter",
			"instagram":   "$instagram",
			"linkedin":    "$linkedin",
			"brand": bson.M{
				"id":   "$brand._id",
				"code": "$brand.brand_code",
				"name": "$brand.brand_name",
				"slug": "$brand.brand_slug",
			},
			"property": bson.M{
				"id":   "$property._id",
				"code": "$property.property_code",
				"name": "$property.property_name",
				"slug": "$property.property_slug",
			},
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
		bson.M{"$facet": bson.M{
			"paginatedResults": facetPipelinePaging,
			"totalCount":       bson.A{bson.M{"$count": "count"}},
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  facetData,
			"total": total,
		},
	})
}

func (app *Api) GetTeamByID(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	teamID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(teamID)

	pipeline := bson.A{
		bson.M{"$match": bson.M{"_id": objectID, "type": "team"}},
		bson.M{"$lookup": bson.M{
			"from": "brands", "as": "brand",
			"localField": "brand_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "properties", "as": "property",
			"localField": "property_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$_id",
			"name":        "$name",
			"position":    "$position",
			"description": "$description",
			"order":       "$order",
			"picture":     "$picture",
			"email":       "$email",
			"facebook":    "$facebook",
			"twitter":     "$twitter",
			"instagram":   "$instagram",
			"linkedin":    "$linkedin",
			"brand": bson.M{
				"id":   "$brand._id",
				"code": "$brand.brand_code",
				"name": "$brand.brand_name",
				"slug": "$brand.brand_slug",
			},
			"property": bson.M{
				"id":   "$property._id",
				"code": "$property.property_code",
				"name": "$property.property_name",
				"slug": "$property.property_slug",
			},
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
	}

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var results bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&results)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}

func (app *Api) AddTeam(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	var payload models.TeamForm
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var team = models.Team{
		Type:        "team",
		Name:        payload.Name,
		Position:    payload.Position,
		Description: payload.Description,
		Order:       payload.Order,
		Picture:     payload.Picture,
		Email:       payload.Email,
		Facebook:    payload.Facebook,
		Twitter:     payload.Twitter,
		Instagram:   payload.Instagram,
		LinkedIn:    payload.LinkedIn,
	}

	if payload.BrandID != "" {
		bID, _ := primitive.ObjectIDFromHex(payload.BrandID)
		team.BrandID = &bID
	}

	if payload.PropertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.PropertyID)
		team.PropertyID = &pID
	}

	now := time.Now()
	team.CreatedAt = &now

	ctx := context.Background()
	results, err := collection.InsertOne(ctx, team)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"id": results.InsertedID,
		},
	})
}

func (app *Api) UpdateTeam(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	teamID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(teamID)

	var team models.Team
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&team)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var payload = models.TeamForm{
		Name:        team.Name,
		Position:    team.Position,
		Description: team.Description,
		Order:       team.Order,
		Picture:     team.Picture,
		Email:       team.Email,
		Facebook:    team.Facebook,
		Twitter:     team.Twitter,
		Instagram:   team.Instagram,
		LinkedIn:    team.LinkedIn,
	}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	team.Name = payload.Name
	team.Position = payload.Position
	team.Description = payload.Description
	team.Order = payload.Order
	team.Picture = payload.Picture
	team.Email = payload.Email
	team.Facebook = payload.Facebook
	team.Twitter = payload.Twitter
	team.Instagram = payload.Instagram
	team.LinkedIn = payload.LinkedIn

	now := time.Now()
	team.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": team})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"team":           team,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteTeam(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	teamID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(teamID)

	var team models.Team
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID, "type": "team"}).Decode(&team)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID, "type": "team"})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            teamID,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) GetTeamList(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	code := strings.ToUpper(c.DefaultQuery("code", "cor"))
	codeSlug := strings.ToLower(c.Query("codeSlug"))

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "order")
	sortDir := c.DefaultQuery("sortDir", "asc")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	matchFilter := bson.M{}
	if codeSlug != "" {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_slug": codeSlug},
			bson.M{"property.property_slug": codeSlug},
		}
	} else if code != "" {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_code": code},
			bson.M{"property.property_code": code},
		}
	}

	pipeline := bson.A{
		bson.M{"$match": bson.M{"type": "team"}},
		bson.M{"$lookup": bson.M{
			"from": "brands", "as": "brand",
			"localField": "brand_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "properties", "as": "property",
			"localField": "property_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
	}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$project": bson.M{
			"_id":         0,
			"id":          "$_id",
			"name":        "$name",
			"position":    "$position",
			"description": "$description",
			"order":       "$order",
			"picture": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$picture", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$picture",
					"local": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$picture", 0, 4}}, "http"}},
						"$picture",
						bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$picture"},
						},
					}},
					"cloud": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$picture", 0, 4}}, "http"}},
						"$picture",
						bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$picture"},
						},
					}},
				},
			}},
			"email":     "$email",
			"facebook":  "$facebook",
			"twitter":   "$twitter",
			"instagram": "$instagram",
			"linkedin":  "$linkedin",
			"brand": bson.M{
				"id":   "$brand._id",
				"code": "$brand.brand_code",
				"name": "$brand.brand_name",
				"slug": "$brand.brand_slug",
			},
			"property": bson.M{
				"id":   "$property._id",
				"code": "$property.property_code",
				"name": "$property.property_name",
				"slug": "$property.property_slug",
			},
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
		bson.M{"$facet": bson.M{
			"paginatedResults": facetPipelinePaging,
			"totalCount":       bson.A{bson.M{"$count": "count"}},
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"items": facetData,
			"total": total,
		},
	})
}
