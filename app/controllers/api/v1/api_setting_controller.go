package v1

import (
	"alaric_cms/app/models"
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetSetting(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("settings")

	pageId := c.Query("id")
	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	// Pass these options to the Find method
	findOptions := options.Find()
	findOptions.SetLimit(limit)
	findOptions.SetSkip(offset)

	findFilter := bson.M{}
	if pageId != "" {
		objectID, _ := primitive.ObjectIDFromHex(pageId)
		findFilter["_id"] = objectID
	}

	ctx := context.Background()
	cursor, err := collection.Find(
		ctx,
		findFilter,
		findOptions,
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var setting []models.Setting
	cursor.All(ctx, &setting)

	var results interface{}
	if pageId != "" && len(setting) == 1 {
		results = setting[0]
	} else {
		results = setting
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}

func (app *Api) SettingGeneral(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("settings")

	pageId := c.Param("pageId")
	objectID, _ := primitive.ObjectIDFromHex(pageId)

	var setting models.Setting
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&setting)

	now := time.Now()
	if err != nil {
		setting.CreatedAt = &now
	} else {
		setting.UpdatedAt = &now
	}

	var sGeneral models.SettingGeneral
	if err := c.ShouldBind(&sGeneral); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	setting.Id = &objectID
	setting.General = sGeneral

	opt := options.Update()
	opt.SetUpsert(true)

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": setting}, opt)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"setting":        setting,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) SettingMedia(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("settings")

	pageId := c.Param("pageId")
	objectID, _ := primitive.ObjectIDFromHex(pageId)

	var setting models.Setting
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&setting)

	now := time.Now()
	if err != nil {
		setting.CreatedAt = &now
	} else {
		setting.UpdatedAt = &now
	}

	var sMedia models.SettingMedia
	if err := c.ShouldBind(&sMedia); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	setting.Id = &objectID
	setting.Media = sMedia

	opt := options.Update()
	opt.SetUpsert(true)

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": setting}, opt)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"setting":        setting,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) SettingSocialMedia(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("settings")

	pageId := c.Param("pageId")
	objectID, _ := primitive.ObjectIDFromHex(pageId)

	var setting models.Setting
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&setting)

	now := time.Now()
	if err != nil {
		setting.CreatedAt = &now
	} else {
		setting.UpdatedAt = &now
	}

	var sSocMed models.SettingSocialMedia
	if err := c.ShouldBind(&sSocMed); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	setting.Id = &objectID
	setting.SocialMedia = sSocMed

	opt := options.Update()
	opt.SetUpsert(true)

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": setting}, opt)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"setting":        setting,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) SettingMeta(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("settings")

	pageId := c.Param("pageId")
	objectID, _ := primitive.ObjectIDFromHex(pageId)

	var setting models.Setting
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&setting)

	now := time.Now()
	if err != nil {
		setting.CreatedAt = &now
	} else {
		setting.UpdatedAt = &now
	}

	var sMeta models.SettingMeta
	if err := c.ShouldBind(&sMeta); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	setting.Id = &objectID
	setting.Meta = sMeta

	opt := options.Update()
	opt.SetUpsert(true)

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": setting}, opt)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"setting":        setting,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) SettingScript(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("settings")

	pageId := c.Param("pageId")
	objectID, _ := primitive.ObjectIDFromHex(pageId)

	var setting models.Setting
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&setting)

	now := time.Now()
	if err != nil {
		setting.CreatedAt = &now
	} else {
		setting.UpdatedAt = &now
	}

	var sScript models.SettingScript
	if err := c.ShouldBind(&sScript); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	setting.Id = &objectID
	setting.Script = sScript

	opt := options.Update()
	opt.SetUpsert(true)

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": setting}, opt)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"setting":        setting,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) GetWebSetting(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("settings")

	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	ID := c.Query("id")
	code := strings.ToUpper(c.Query("code"))
	codeSlug := strings.ToLower(c.Query("codeSlug"))
	lang := c.DefaultQuery("lang", "en")

	matchFilter := bson.M{}
	if ID != "" {
		objectID, _ := primitive.ObjectIDFromHex(ID)
		matchFilter["_id"] = objectID
	} else {
		if code != "" {
			matchFilter["$or"] = bson.A{
				bson.M{"brand.code": code},
				bson.M{"property.code": code},
			}
		} else if codeSlug != "" {
			matchFilter["$or"] = bson.A{
				bson.M{"brand.slug": codeSlug},
				bson.M{"property.slug": codeSlug},
			}
		} else {
			matchFilter["property.code"] = "COR"
		}
	}

	pipelineProjectBrand := bson.M{"$project": bson.M{
		"_id":          0,
		"id":           "$_id",
		"code":         "$brand_code",
		"name":         "$brand_name",
		"display_name": "$brand_display_name",
		"slug":         "$brand_slug",
		"logo": bson.M{
			"primary": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand_logo", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$brand_logo",
					"local": bson.M{
						"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$brand_logo"},
					},
					"cloud": bson.M{
						"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$brand_logo"},
					},
				},
			}},
			"secondary": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand_secondary_logo", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$brand_secondary_logo",
					"local": bson.M{
						"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$brand_secondary_logo"},
					},
					"cloud": bson.M{
						"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$brand_secondary_logo"},
					},
				},
			}},
		},
		"favicon": bson.M{"$cond": bson.M{
			"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand_favicon", ""}}, ""}},
			"then": bson.M{},
			"else": bson.M{
				"name": "$brand_favicon",
				"local": bson.M{
					"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$brand_favicon"},
				},
				"cloud": bson.M{
					"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$brand_favicon"},
				},
			},
		}},
		"featured_image": bson.M{"$cond": bson.M{
			"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$brand_featured_image", ""}}, ""}},
			"then": bson.M{},
			"else": bson.M{
				"name": "$brand_featured_image",
				"local": bson.M{
					"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$brand_featured_image"},
				},
				"cloud": bson.M{
					"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$brand_featured_image"},
				},
			},
		}},
	}}

	pipelineProjectProperty := bson.M{"$project": bson.M{
		"_id":          0,
		"id":           "$_id",
		"code":         "$property_code",
		"name":         "$property_name",
		"display_name": "$property_display_name",
		"slug":         "$property_slug",
		"booking_url":  bson.M{"$ifNull": bson.A{"$property_booking_url", ""}},
		"booking_code": bson.M{
			"corporate": "$property_corporate_booking_code",
			"brand":     "$property_brand_booking_code",
			"hotel":     "$property_hotel_booking_code",
		},
		"logo": bson.M{
			"primary": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property_logo", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$property_logo",
					"local": bson.M{
						"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property_logo"},
					},
					"cloud": bson.M{
						"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property_logo"},
					},
				},
			}},
			"secondary": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property_secondary_logo", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$property_secondary_logo",
					"local": bson.M{
						"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property_secondary_logo"},
					},
					"cloud": bson.M{
						"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property_secondary_logo"},
					},
				},
			}},
		},
		"favicon": bson.M{"$cond": bson.M{
			"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property_favicon", ""}}, ""}},
			"then": bson.M{},
			"else": bson.M{
				"name": "$property_favicon",
				"local": bson.M{
					"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property_favicon"},
				},
				"cloud": bson.M{
					"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property_favicon"},
				},
			},
		}},
		"featured_image": bson.M{"$cond": bson.M{
			"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property_featured_image", ""}}, ""}},
			"then": bson.M{},
			"else": bson.M{
				"name": "$property_featured_image",
				"local": bson.M{
					"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$property_featured_image"},
				},
				"cloud": bson.M{
					"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$property_featured_image"},
				},
			},
		}},
		"attachment_image": bson.M{"$cond": bson.M{
			"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$property_image_attachment", ""}}, ""}},
			"then": bson.A{},
			"else": bson.M{
				"$map": bson.M{
					"input": "$property_image_attachment",
					"as":    "image",
					"in": bson.M{
						"name": "$$image",
						"local": bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$image"},
						},
						"cloud": bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$image"},
						},
					},
				},
			},
		}},
		"address":            "$property_address",
		"tagline":            bson.M{"$ifNull": bson.A{fmt.Sprintf("$property_tagline.%s", lang), ""}},
		"description":        bson.M{"$ifNull": bson.A{fmt.Sprintf("$property_description.%s", lang), ""}},
		"email":              "$property_email",
		"fax":                "$property_fax",
		"phone":              "$property_phone",
		"location.latitude":  "$property_latitude",
		"location.longitude": "$property_longitude",
		"city": bson.M{
			"id":   "$city._id",
			"code": "$city.code",
			"name": "$city.name",
		},
		"brand_id": "$brand_id",
	}}

	pipeline := bson.A{
		bson.M{"$lookup": bson.M{
			"from": "brands",
			"let":  bson.M{"brand_id": "$_id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$_id", "$$brand_id"}},
				}}}},
				pipelineProjectBrand,
			},
			"as": "brand",
		}},
		bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "properties",
			"let":  bson.M{"property_id": "$_id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$_id", "$$property_id"}},
				}}}},
				bson.M{"$lookup": bson.M{
					"from": "cities",
					"let":  bson.M{"code": "$city_id"},
					"pipeline": bson.A{
						bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
							bson.M{"$eq": bson.A{"$code", "$$code"}},
						}}}},
					},
					"as": "city",
				}},
				bson.M{"$unwind": bson.M{"path": "$city", "preserveNullAndEmptyArrays": true}},
				pipelineProjectProperty,
			},
			"as": "property",
		}},
		bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "properties",
			"let":  bson.M{"property_id": "$property.id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$ne": bson.A{"$_id", "$$property_id"}},
					bson.M{"$eq": bson.A{"$property_code", "COR"}},
				}}}},
				bson.M{"$lookup": bson.M{
					"from": "cities",
					"let":  bson.M{"code": "$city_id"},
					"pipeline": bson.A{
						bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
							bson.M{"$eq": bson.A{"$code", "$$code"}},
						}}}},
					},
					"as": "city",
				}},
				bson.M{"$unwind": bson.M{"path": "$city", "preserveNullAndEmptyArrays": true}},
				pipelineProjectProperty,
			},
			"as": "corporate",
		}},
		bson.M{"$unwind": bson.M{"path": "$corporate", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "brands",
			"let":  bson.M{"brand_id": "$property.brand_id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$_id", "$$brand_id"}},
				}}}},
				pipelineProjectBrand,
			},
			"as": "bop",
		}},
		bson.M{"$unwind": bson.M{"path": "$bop", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "settings", "as": "sob", // setting of brand
			"localField": "property.brand_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$sob", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "settings", "as": "soc", // setting of corporate
			"localField": "corporate.id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$soc", "preserveNullAndEmptyArrays": true}},
	}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$project": bson.M{
			"property.brand_id":  0,
			"corporate.brand_id": 0,
		}},
		bson.M{"$addFields": bson.M{
			"brand": bson.M{"$ifNull": bson.A{"$brand", "$bop"}},
		}},
		bson.M{"$project": bson.M{
			"_id":     0,
			"id":      "$_id",
			"general": bson.M{"$ifNull": bson.A{"$general", bson.M{}}},
			"media":   bson.M{"$ifNull": bson.A{"$media", bson.M{}}},
			"meta": bson.M{
				"title":       bson.M{"$ifNull": bson.A{fmt.Sprintf("$meta.title.%s", lang), ""}},
				"description": bson.M{"$ifNull": bson.A{fmt.Sprintf("$meta.description.%s", lang), ""}},
				"keyword":     bson.M{"$ifNull": bson.A{fmt.Sprintf("$meta.keyword.%s", lang), ""}},
			},
			"script": bson.M{"$cond": bson.M{
				"if":   bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$script.themes", ""}}, ""}},
				"then": "$script",
				"else": bson.M{"$cond": bson.M{
					"if":   bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$sob.script.themes", ""}}, ""}},
					"then": bson.M{"$mergeObjects": bson.A{"$script", bson.M{"themes": "$sob.script.themes"}}},
					"else": "$script",
				}},
			}},
			"corporate": bson.M{"$cond": bson.A{
				bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$corporate", ""}}, ""}},
				bson.M{"$mergeObjects": bson.A{"$corporate", bson.M{"social_media": "$soc.social_media"}}},
				"$corporate",
			}},
			"brand": bson.M{"$cond": bson.A{
				bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$brand", ""}}, ""}},
				bson.M{"$mergeObjects": bson.A{"$brand", bson.M{"social_media": bson.M{"$ifNull": bson.A{"$sob.social_media", bson.M{}}}}}},
				"$brand",
			}},
			"property": bson.M{"$cond": bson.A{
				bson.M{"$ne": bson.A{bson.M{"$ifNull": bson.A{"$property", ""}}, ""}},
				bson.M{"$mergeObjects": bson.A{
					"$property",
					bson.M{"social_media": bson.M{"$ifNull": bson.A{"$social_media", bson.M{}}}},
					bson.M{"booking_url": bson.M{"$cond": bson.A{
						bson.M{"$ne": bson.A{"$property.booking_url", ""}},
						"$property.booking_url",
						"$corporate.booking_url",
					}}},
				}},
				"$property",
			}},
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	setting := bson.M{}
	for cursor.Next(ctx) {
		cursor.Decode(&setting)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       setting,
	})
}
