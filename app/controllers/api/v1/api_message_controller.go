package v1

import (
	"alaric_cms/app/models"
	"context"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetMessage(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("messages")

	search := c.Query("search")
	messageID := c.Query("id")
	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")
	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "_id")
	sortDir := c.DefaultQuery("sortDir", "-1")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	matchFilter := bson.M{}
	if messageID != "" {
		objectID, _ := primitive.ObjectIDFromHex(messageID)
		matchFilter["_id"] = objectID
	}

	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["brand_id"] = bID
	}

	if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		matchFilter["property_id"] = pID
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"name": searchRe},
			bson.M{"type": searchRe},
			bson.M{"created_at": searchRe},
			bson.M{"updated_at": searchRe},
		}
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}
	pipeline = append(pipeline, bson.A{
		bson.M{"$lookup": bson.M{
			"as":           "brand",
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$brand",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "property",
			"from":         "properties",
			"localField":   "property_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$property",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$project": bson.M{
			"_id":  0,
			"id":   "$_id",
			"name": "$name",
			"type": "$type",
			"brand": bson.M{
				"id":   "$brand._id",
				"code": "$brand.brand_code",
				"name": "$brand.brand_name",
				"slug": "$brand.brand_slug",
			},
			"property": bson.M{
				"id":   "$property._id",
				"code": "$property.property_code",
				"name": "$property.property_name",
				"slug": "$property.property_slug",
			},
			"created_at": "$created_at",
			"updated_at": "$updated_at",
		}},
		bson.M{"$facet": bson.M{
			"paginatedResults": facetPipelinePaging,
			"totalCount":       bson.A{bson.M{"$count": "count"}},
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	var results interface{}
	if messageID != "" && len(facetData) == 1 {
		results = facetData[0]
	} else {
		results = facetData
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": total,
		},
	})
}

func (app *Api) GetMessageByID(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("messages")

	messageID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(messageID)

	findFilter := bson.M{"_id": objectID}

	ctx := context.Background()
	cursor, err := collection.Find(
		ctx,
		findFilter,
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var message = models.Message{}
	for cursor.Next(ctx) {
		cursor.Decode(&message)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       message,
	})
}

func (app *Api) AddMessage(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("messages")

	var payload models.FormMessage
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	findFilter := bson.M{"name": payload.Name}

	var brandID, propertyID primitive.ObjectID
	if payload.BrandID != "" {
		brandID, _ = primitive.ObjectIDFromHex(payload.BrandID)
		findFilter["brand_id"] = brandID
	}

	if payload.PropertyID != "" {
		propertyID, _ = primitive.ObjectIDFromHex(payload.PropertyID)
		findFilter["property_id"] = propertyID
	}

	ctx := context.Background()
	cursor, err := collection.Find(ctx, findFilter)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusConflict, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Message name already exists.",
		})
		return
	}

	now := time.Now()
	message := models.Message{
		Name:        payload.Name,
		Type:        payload.Type,
		Headline:    payload.Headline,
		Description: payload.Description,
		Image:       payload.Image,
		ButtonLink:  payload.ButtonLink,
		ButtonLabel: payload.ButtonLabel,
		ButtonColor: payload.ButtonColor,
		Setting: models.MessageSetting{
			Type:        payload.MessageType,
			Position:    payload.Position,
			Background:  payload.Background,
			CountryCode: payload.CountryCode,
		},
		CreatedAt: &now,
	}

	message.ButtonNewTab, _ = strconv.ParseBool(payload.ButtonNewTab)
	message.Setting.ImageMessage, _ = strconv.ParseBool(payload.ImageMessage)

	formatDate := "2006-01-02 15:04"
	if payload.ShowDate != "" {
		sd, _ := time.Parse(formatDate, payload.ShowDate)
		message.Setting.ShowDate = &sd
	}
	if payload.EndDate != "" {
		ed, _ := time.Parse(formatDate, payload.EndDate)
		message.Setting.EndDate = &ed
	}
	if payload.BrandID != "" {
		message.BrandID = &brandID
	}
	if payload.PropertyID != "" {
		message.PropertyID = &propertyID
	}

	result, err := collection.InsertOne(ctx, message)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"id": result.InsertedID,
		},
	})
}

func (app *Api) UpdateMessage(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("messages")

	messageID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(messageID)

	var message models.Message
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&message)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	formatDate := "2006-01-02 15:04"
	var payload = models.FormMessage{
		Name:         message.Name,
		Type:         message.Type,
		Headline:     message.Headline,
		Description:  message.Description,
		Image:        message.Image,
		ButtonLink:   message.ButtonLink,
		ButtonLabel:  message.ButtonLabel,
		ButtonColor:  message.ButtonColor,
		ButtonNewTab: strconv.FormatBool(message.ButtonNewTab),
		MessageType:  message.Setting.Type,
		Position:     message.Setting.Position,
		Background:   message.Setting.Background,
		ImageMessage: strconv.FormatBool(message.Setting.ImageMessage),
		ShowDate:     message.Setting.ShowDate.Format(formatDate),
		EndDate:      message.Setting.EndDate.Format(formatDate),
		CountryCode:  message.Setting.CountryCode,
		BrandID:      message.BrandID.Hex(),
		PropertyID:   message.PropertyID.Hex(),
	}
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	findFilter := bson.M{"_id": bson.M{"$ne": objectID}, "name": payload.Name}
	if message.BrandID != nil {
		findFilter["brand_id"] = message.BrandID
	}

	if message.PropertyID != nil {
		findFilter["property_id"] = message.PropertyID
	}
	cursor, err := collection.Find(ctx, findFilter)

	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusConflict, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Message name already exists.",
		})
		return
	}

	now := time.Now()
	message.Name = payload.Name
	message.Type = payload.Type
	message.Headline = payload.Headline
	message.Description = payload.Description
	message.Image = payload.Image
	message.ButtonLink = payload.ButtonLink
	message.ButtonLabel = payload.ButtonLabel
	message.ButtonColor = payload.ButtonColor
	message.ButtonNewTab, _ = strconv.ParseBool(payload.ButtonNewTab)
	message.Setting.Type = payload.MessageType
	message.Setting.Position = payload.Position
	message.Setting.Background = payload.Background
	message.Setting.ImageMessage, _ = strconv.ParseBool(payload.ImageMessage)
	message.Setting.CountryCode = payload.CountryCode
	message.UpdatedAt = &now

	if payload.ShowDate != "" {
		sd, _ := time.Parse(formatDate, payload.ShowDate)
		message.Setting.ShowDate = &sd
	}
	if payload.EndDate != "" {
		ed, _ := time.Parse(formatDate, payload.EndDate)
		message.Setting.EndDate = &ed
	}

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": message})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"message":        message,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteMessage(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("messages")

	messageID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(messageID)

	var message models.Message
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&message)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            messageID,
			"deleted_count": doc.DeletedCount,
		},
	})
}
