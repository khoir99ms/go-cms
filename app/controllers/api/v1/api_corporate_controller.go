package v1

import (
	"alaric_cms/app/models"
	"context"
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/h2non/filetype"
	uuid "github.com/satori/go.uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetCorporate(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("corporate")

	coporateId := c.Query("id")
	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	// Pass these options to the Find method
	findOptions := options.Find()
	findOptions.SetLimit(limit)
	findOptions.SetSkip(offset)

	findFilter := bson.M{}
	if coporateId != "" {
		objectID, _ := primitive.ObjectIDFromHex(coporateId)
		findFilter["_id"] = objectID
	}

	ctx := context.Background()
	cursor, err := collection.Find(
		ctx,
		findFilter,
		findOptions,
	)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var corporates []models.Corporate
	for cursor.Next(ctx) {
		var corporate models.Corporate
		err := cursor.Decode(&corporate)
		if err != nil {
			log.Fatal(err)
		}
		corporates = append(corporates, corporate)
	}

	var results interface{}
	if coporateId != "" && len(corporates) == 1 {
		results = corporates[0]
	} else {
		results = corporates
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}

func (app *Api) AddCorporate(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("corporate")

	var payload models.Corporate
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var regex, _ = regexp.Compile(`[ ]+`)
	payload.CorporateName = strings.Trim(regex.ReplaceAllString(payload.CorporateName, " "), " ")

	ctx := context.Background()
	cursor, err := collection.Find(ctx, bson.M{"corporate_name": payload.CorporateName})
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Coporate name already exists.",
		})
		return
	}

	now := time.Now()
	payload.CorporateSlug = strings.Replace(strings.ToLower(payload.CorporateName), " ", "-", -1)
	payload.CorporateRegistered = &now
	payload.CorporateStatus = "active"

	result, err := collection.InsertOne(ctx, payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"coporate_id": result.InsertedID,
		},
	})
}

func (app *Api) UpdateCorporate(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("corporate")

	corporateId := c.Param("corporateId")
	objectID, _ := primitive.ObjectIDFromHex(corporateId)

	var corporate models.Corporate

	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&corporate)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	if err := c.ShouldBind(&corporate); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	var regex, _ = regexp.Compile(`[ ]+`)
	corporate.CorporateName = strings.Trim(regex.ReplaceAllString(corporate.CorporateName, " "), " ")

	cursor, err := collection.Find(ctx, bson.M{
		"_id":            bson.M{"$ne": objectID},
		"corporate_name": corporate.CorporateName,
	})
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Coporate name already exists.",
		})
		return
	}

	// Source
	logo, _ := c.FormFile("corporate_logo")
	if logo != nil {
		// Only the first 512 bytes are used to sniff the content type.
		buffer := make([]byte, 512)
		src, err := logo.Open()
		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}
		defer src.Close()

		_, err = src.Read(buffer)
		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}

		if filetype.IsImage(buffer) {
			ext := filepath.Ext(logo.Filename)
			uid := uuid.Must(uuid.NewV4())
			filename := fmt.Sprintf("icon-%s%s", uid, ext)
			dst := fmt.Sprintf("%s/%s", app.Config.Path.Media, filename)
			if err = c.SaveUploadedFile(logo, dst); err != nil {
				c.JSON(http.StatusInternalServerError, &models.ReE{
					StatusCode: http.StatusBadRequest,
					Error:      err.Error(),
				})
				return
			}
			corporate.CorporateLogo = filename
		} else {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusBadRequest,
				Error:      "Corporate logo is not an image.",
			})
			return
		}
	}

	now := time.Now()
	corporate.CorporateSlug = strings.Replace(strings.ToLower(corporate.CorporateName), " ", "-", -1)
	corporate.CorporateModified = &now
	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": corporate})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"corporate":      corporate,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteCorporate(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("corporate")

	corporateId := c.Param("corporateId")
	objectID, _ := primitive.ObjectIDFromHex(corporateId)

	doc, err := collection.DeleteOne(context.Background(), bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"corporate_id":  corporateId,
			"deleted_count": doc.DeletedCount,
		},
	})
}
