package v1

import (
	"alaric_cms/app/models"
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetCities(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("cities")

	search := c.Query("search")
	cityID := c.Query("id")
	cityCode := c.Query("code")
	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "_id")
	sortDir := c.DefaultQuery("sortDir", "-1")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	matchFilter := bson.M{}
	if cityID != "" {
		objectID, _ := primitive.ObjectIDFromHex(cityID)
		matchFilter["_id"] = objectID
	}
	if cityCode != "" {
		matchFilter["code"] = cityCode
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"code": searchRe},
			bson.M{"name": searchRe},
			bson.M{"registered": searchRe},
			bson.M{"modified": searchRe},
		}
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}
	pipeline = append(pipeline, bson.A{
		bson.M{
			"$project": bson.M{
				"_id":            0,
				"id":             "$_id",
				"code":           "$code",
				"name":           "$name",
				"featured_image": "$featured_image",
				"description":    "$description",
				"order":          "$order",
				"registered":     "$registered",
				"modified":       "$modified",
			},
		},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": facetPipelinePaging,
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	var results interface{}
	if (cityID != "" || cityCode != "") && len(facetData) == 1 {
		results = facetData[0]
	} else {
		results = facetData
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  results,
			"total": total,
		},
	})
}

func (app *Api) AddCity(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("cities")

	var payload models.City
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	ctx := context.Background()
	cursor, err := collection.Find(ctx, bson.M{
		"code": payload.Code,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "City code already exists.",
		})
		return
	}

	now := time.Now()
	payload.Registered = &now

	result, err := collection.InsertOne(ctx, payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"id": result.InsertedID,
		},
	})
}

func (app *Api) UpdateCity(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("cities")

	cityId := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(cityId)

	var city models.City
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&city)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	if err := c.ShouldBind(&city); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	cursor, err := collection.Find(ctx, bson.M{
		"_id":  bson.M{"$ne": objectID},
		"code": city.Code,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "City code already exists.",
		})
		return
	}

	now := time.Now()
	city.Modified = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": city})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"city":           city,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteCity(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("cities")

	cityId := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(cityId)

	var city models.City
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&city)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            cityId,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) GetCityList(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("cities")

	search := c.Query("search")
	cityID := c.Query("id")
	cityCode := c.Query("code")
	lang := c.DefaultQuery("lang", "en")
	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "_id")
	sortDir := c.DefaultQuery("sortDir", "-1")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	matchFilter := bson.M{}
	if cityID != "" {
		objectID, _ := primitive.ObjectIDFromHex(cityID)
		matchFilter["_id"] = objectID
	}
	if cityCode != "" {
		matchFilter["code"] = cityCode
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		matchFilter["$or"] = bson.A{
			bson.M{"code": searchRe},
			bson.M{"name": searchRe},
			bson.M{"registered": searchRe},
			bson.M{"modified": searchRe},
		}
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}
	pipeline = append(pipeline, bson.A{
		bson.M{
			"$project": bson.M{
				"_id":            0,
				"id":             "$_id",
				"code":           "$code",
				"name":           "$name",
				"featured_image": "$featured_image",
				"description":    bson.M{"$ifNull": bson.A{fmt.Sprintf("$description.%s", lang), ""}},
				"order":          "$order",
				"registered":     "$registered",
				"modified":       "$modified",
			},
		},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": facetPipelinePaging,
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	var results interface{}
	if (cityID != "" || cityCode != "") && len(facetData) == 1 {
		results = facetData[0]
	} else {
		results = facetData
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"items": results,
			"total": total,
		},
	})
}
