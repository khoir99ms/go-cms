package v1

import (
	"alaric_cms/app/models"
	"context"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetCity(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("cities")

	cityId := c.Query("id")
	search := c.Query("search")
	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	// Pass these options to the Find method
	findOptions := options.Find()
	findOptions.SetLimit(limit)
	findOptions.SetSkip(offset)

	findFilter := bson.M{}
	if cityId != "" {
		objectID, _ := primitive.ObjectIDFromHex(cityId)
		findFilter["_id"] = objectID
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		findFilter["name"] = searchRe
	}

	ctx := context.Background()
	cursor, err := collection.Find(
		ctx,
		findFilter,
		findOptions,
	)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var cities []models.City
	cursor.All(ctx, &cities)

	var results interface{}
	if cityId != "" && len(cities) == 1 {
		results = cities[0]
	} else {
		results = cities
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}

func (app *Api) GetMyNetworkMenu(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("brands")

	// this is an optional parameter
	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")

	sortCol := c.DefaultQuery("sortCol", "order")
	sortDir := c.DefaultQuery("sortDir", "asc")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	matchFilter := bson.M{}
	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["_id"] = bID
	}
	if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		matchFilter["property._id"] = pID
	}

	pipeline := bson.A{
		bson.M{"$lookup": bson.M{
			"as":           "property",
			"from":         "properties",
			"localField":   "_id",
			"foreignField": "brand_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$property",
			"preserveNullAndEmptyArrays": true,
		}},
	}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$group": bson.M{
			"_id": "$_id",
			"brand": bson.M{"$first": bson.M{
				"id":    "$_id",
				"code":  "$brand_code",
				"name":  "$brand_name",
				"order": "$brand_order",
				"slug":  "$brand_slug",
			}},
			"properties": bson.M{"$push": bson.M{
				"id":   "$property._id",
				"code": "$property.property_code",
				"name": "$property.property_name",
				"slug": "$property.property_slug",
			}},
		}},
		bson.M{"$project": bson.M{
			"_id":   0,
			"id":    "$_id",
			"code":  "$brand.code",
			"name":  "$brand.name",
			"slug":  "$brand.slug",
			"order": "$brand.order",
			"properties": bson.M{"$filter": bson.M{
				"input": "$properties",
				"as":    "property",
				"cond":  bson.M{"$ne": bson.A{"$$property", bson.M{}}},
			}},
		}},
		bson.M{"$sort": sort},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var results []bson.M
	cursor.All(ctx, &results)

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}
