package v1

import (
	"alaric_cms/app/helpers"
	"alaric_cms/app/models"
	"context"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"github.com/mongodb/mongo-go-driver/mongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetPages(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("pages")

	search := c.Query("search")
	lang := c.Query("lang")
	exceptID := c.Query("exceptId")
	categoryID := c.Query("categoryId")
	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")
	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "asc")

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	matchFilter := bson.M{}
	if lang != "" {
		matchFilter["lang"] = lang
	}

	if exceptID != "" {
		eID, _ := primitive.ObjectIDFromHex(exceptID)
		matchFilter["_id"] = bson.M{"$ne": eID}
	}

	if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		matchFilter["property_id"] = pID
	}

	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["brand_id"] = bID
	}

	if categoryID != "" {
		cID, _ := primitive.ObjectIDFromHex(categoryID)
		matchFilter["category_id"] = cID
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
				}}}},
			},
			"as": "lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$lang",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "category",
			"from":         "categories",
			"localField":   "category_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$category",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "brand",
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$brand",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "property",
			"from":         "properties",
			"localField":   "property_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$property",
			"preserveNullAndEmptyArrays": true,
		}},
	}...)

	if search != "" {
		searchFilter := bson.M{}
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		searchFilter["$or"] = bson.A{
			bson.M{"title": searchRe},
			bson.M{"order": searchRe},
			bson.M{"status": searchRe},
			bson.M{"category.name": searchRe},
			bson.M{"brand.name": searchRe},
			bson.M{"property.name": searchRe},
			bson.M{"created_at": searchRe},
		}
		pipeline = append(pipeline, bson.M{"$match": searchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$project": bson.M{
			"_id":       0,
			"id":        "$_id",
			"title":     "$title",
			"order":     "$order",
			"status":    "$status",
			"source_id": "$source_id",
			"lang": bson.M{
				"id":     "$lang._id",
				"code":   "$lang.code",
				"name":   "$lang.name",
				"native": "$lang.native",
				"status": "$lang.status",
			},
			"category": bson.M{
				"id":   "$category._id",
				"name": "$category.name",
			},
			"brand": bson.M{
				"id":   "$brand._id",
				"code": "$brand.brand_code",
				"name": "$brand.brand_name",
				"slug": "$brand.brand_slug",
			},
			"property": bson.M{
				"id":   "$property._id",
				"code": "$property.property_code",
				"name": "$property.property_name",
				"slug": "$property.property_slug",
			},
			"created_at": "$created_at",
		}},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": facetPipelinePaging,
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  facetData,
			"total": total,
		},
	})
}

func (app *Api) GetPageUsedPrimaryLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("pages")

	search := c.Query("search")
	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")
	categoryID := c.Query("categoryId")
	sortCol := c.DefaultQuery("sortCol", "id")
	sortDir := c.DefaultQuery("sortDir", "-1")

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	matchFilter := bson.M{}
	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["brand_id"] = bID
	}

	if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		matchFilter["property_id"] = pID
	}

	if categoryID != "" {
		cID, _ := primitive.ObjectIDFromHex(categoryID)
		matchFilter["category_id"] = cID
	}

	pipeline := bson.A{}
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}
	pipeline = append(pipeline, bson.A{
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
					bson.M{"$eq": bson.A{"$status", "primary"}},
				}}}},
			},
			"as": "lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$lang",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "category",
			"from":         "categories",
			"localField":   "category_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$category",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "brand",
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$brand",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "property",
			"from":         "properties",
			"localField":   "property_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$property",
			"preserveNullAndEmptyArrays": true,
		}},
	}...)

	if search != "" {
		searchFilter := bson.M{}
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		searchFilter["$or"] = bson.A{
			bson.M{"title": searchRe},
			bson.M{"order": searchRe},
			bson.M{"status": searchRe},
			bson.M{"category.name": searchRe},
			bson.M{"brand.name": searchRe},
			bson.M{"property.name": searchRe},
			bson.M{"created_at": searchRe},
		}
		pipeline = append(pipeline, bson.M{"$match": searchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$project": bson.M{
			"_id":      0,
			"id":       "$_id",
			"title":    "$title",
			"subtitle": "$subtitle",
			"order":    "$order",
			"status":   "$status",
			"lang": bson.M{
				"id":     "$lang._id",
				"code":   "$lang.code",
				"name":   "$lang.name",
				"native": "$lang.native",
				"status": "$lang.status",
			},
			"category": bson.M{
				"id":   "$category._id",
				"name": "$category.name",
			},
			"brand": bson.M{
				"id":   "$brand._id",
				"code": "$brand.brand_code",
				"name": "$brand.brand_name",
				"slug": "$brand.brand_slug",
			},
			"property": bson.M{
				"id":   "$property._id",
				"code": "$property.property_code",
				"name": "$property.property_name",
				"slug": "$property.property_slug",
			},
			"created_at": "$created_at",
		}},
		bson.M{"$sort": sort},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": bson.A{bson.M{"$skip": offset}, bson.M{"$limit": limit}},
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  facetData,
			"total": total,
		},
	})
}

func (app *Api) GetMultipleLangPageByID(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("pages")

	pageID := c.Param("id")
	pID, _ := primitive.ObjectIDFromHex(pageID)

	lookupPipeline := bson.A{
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id":    0,
					"id":     "$_id",
					"code":   "$code",
					"name":   "$name",
					"native": "$native",
					"status": "$status",
				}},
			},
			"as": "lang",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$lang",
			"preserveNullAndEmptyArrays": false,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "category",
			"from":         "categories",
			"localField":   "category_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$category",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "brand",
			"from":         "brands",
			"localField":   "brand_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$brand",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$lookup": bson.M{
			"as":           "property",
			"from":         "properties",
			"localField":   "property_id",
			"foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{
			"path":                       "$property",
			"preserveNullAndEmptyArrays": true,
		}},
	}

	projectPipeline := bson.M{
		"_id":              0,
		"id":               "$_id",
		"title":            "$title",
		"subtitle":         "$subtitle",
		"content":          "$content",
		"excerpt":          "$excerpt",
		"featured_image":   "$featured_image",
		"attachment_image": "$attachment_image",
		"order":            "$order",
		"status":           "$status",
		"show_date":        "$show_date",
		"end_date":         "$end_date",
		"template":         "$template",
		"slug":             "$slug",
		"lang":             "$lang",
		"meta":             "$meta",
		"script":           "$script",
		"badge":            "$badge",
		"custom":           "$custom",
		"amenities":        "$amenities",
		"tags":             "$tags",
		"created_at":       "$created_at",
		"updated_at":       "$updated_at",
		"source_id":        "$source_id",
		"parent_id":        "$parent_id",
		"category": bson.M{
			"id":    "$category._id",
			"name":  "$category.name",
			"title": "$category.title",
			"slug":  "$category.slug",
		},
		"brand": bson.M{
			"id":   "$brand._id",
			"code": "$brand.brand_code",
			"name": "$brand.brand_name",
			"slug": "$brand.brand_slug",
		},
		"property": bson.M{
			"id":   "$property._id",
			"code": "$property.property_code",
			"name": "$property.property_name",
			"slug": "$property.property_slug",
		},
	}

	pipeline := bson.A{
		bson.M{"$match": bson.M{"_id": pID}},
	}
	pipeline = append(pipeline, lookupPipeline...)

	subPipeline := bson.A{
		bson.M{"$match": bson.M{"$expr": bson.M{"$or": bson.A{
			bson.M{"$eq": bson.A{"$_id", "$$source_id"}},
			bson.M{"$eq": bson.A{"$source_id", "$$id"}},
		}}}},
	}
	subPipeline = append(subPipeline, lookupPipeline...)
	subPipeline = append(subPipeline, bson.M{"$project": projectPipeline})

	pipeline = append(pipeline, bson.A{
		bson.M{"$lookup": bson.M{
			"from":     "pages",
			"let":      bson.M{"id": "$_id", "source_id": "$source_id"},
			"pipeline": subPipeline,
			"as":       "other_lang",
		}},
	}...)

	projectPipeline["other_lang"] = "$other_lang"
	pipeline = append(pipeline, bson.M{"$project": projectPipeline})

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var pages bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&pages)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       pages,
	})
}

func (app *Api) GetPageGroupedLang(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("pages")

	brandID := c.Query("brandId")
	propertyID := c.Query("propertyId")

	matchFilter := bson.M{}
	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["brand_id"] = bID
	}

	if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		matchFilter["property_id"] = pID
	}

	projectPipeline := bson.M{
		"_id":         0,
		"id":          "$_id",
		"title":       "$title",
		"order":       "$order",
		"status":      "$status",
		"slug":        "$slug",
		"lang":        "$lang",
		"source_id":   "$source_id",
		"parent_id":   "$parent_id",
		"category_id": "$category_id",
		"brand_id":    "$brand_id",
		"property_id": "$property_id",
		"created_at":  "$created_at",
		"updated_at":  "$updated_at",
	}

	pipeline := bson.A{}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$match": matchFilter},
		bson.M{"$project": projectPipeline},
		bson.M{"$group": bson.M{
			"_id":   "$lang",
			"items": bson.M{"$push": "$$ROOT"},
		}},
		bson.M{"$addFields": bson.M{
			"pages": bson.A{bson.M{
				"k": "$_id",
				"v": "$items",
			}},
		}},
		bson.M{"$replaceRoot": bson.M{
			"newRoot": bson.M{"$arrayToObject": "$pages"},
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var pages bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&pages)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       pages,
	})
}

func (app *Api) GetPagesForMenu(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("pages")

	// this is an optional parameter
	propertyCode := c.Query("propertyCode")
	brandId := c.Query("brandId")
	categoryId := c.Query("categoryId")
	lang := c.Query("lang")

	sortCol := c.DefaultQuery("sortCol", "_id")
	sortDir := c.DefaultQuery("sortDir", "-1")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)
	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page < 1 {
		page = 1
	}
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 64)
	if err != nil || limit < 1 {
		limit = 10
	}
	offset := (page - 1) * limit

	pipeline := bson.A{}

	matchFilter := bson.M{}
	if brandId != "" {
		oBrandID, _ := primitive.ObjectIDFromHex(brandId)
		matchFilter["brand_id"] = oBrandID
	}

	if propertyCode == "COR" {
		matchFilter["brand_id"] = nil
	}

	if propertyCode != "" {
		matchFilter["property_code"] = propertyCode
	}

	if categoryId != "" {
		oCategoryID, _ := primitive.ObjectIDFromHex(categoryId)
		matchFilter["category_id"] = oCategoryID
	}

	if lang != "" {
		matchFilter["page_lang"] = lang
	}

	matchFilter["page_status"] = "publish"
	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$sort": sort},
		bson.M{
			"$lookup": bson.M{
				"as":           "category",
				"from":         "categories",
				"localField":   "category_id",
				"foreignField": "_id",
			},
		},
		bson.M{
			"$unwind": bson.M{
				"path":                       "$category",
				"preserveNullAndEmptyArrays": true,
			},
		},
		bson.M{
			"$lookup": bson.M{
				"as":           "brand",
				"from":         "brands",
				"localField":   "brand_id",
				"foreignField": "_id",
			},
		},
		bson.M{
			"$unwind": bson.M{
				"path":                       "$brand",
				"preserveNullAndEmptyArrays": true,
			},
		},
		bson.M{
			"$lookup": bson.M{
				"as":           "property",
				"from":         "properties",
				"localField":   "property_code",
				"foreignField": "property_code",
			},
		},
		bson.M{
			"$unwind": bson.M{
				"path":                       "$property",
				"preserveNullAndEmptyArrays": true,
			},
		},
		bson.M{
			"$addFields": bson.M{"page_id": "$_id", "category.category_id": "$category._id", "brand.brand_id": "$brand._id", "property.property_id": "$property._id"},
		},
		bson.M{
			"$project": bson.M{
				"_id":          0,
				"category_id":  0,
				"brand_id":     0,
				"property_id":  0,
				"brand._id":    0,
				"property._id": 0,
				"category._id": 0,
			},
		},
		bson.M{
			"$facet": bson.M{
				"paginatedResults": bson.A{bson.M{"$skip": offset}, bson.M{"$limit": limit}},
				"totalCount":       bson.A{bson.M{"$count": "count"}},
			},
		},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var facetResult bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&facetResult)
	}

	facetData := facetResult["paginatedResults"].(primitive.A)
	facetCount := facetResult["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  facetData,
			"total": total,
		},
	})
}

func (app *Api) AddPage(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("pages")

	var payload models.PageForm
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	ctx := context.Background()
	page, _ := app.validatePageAddPayload(payload)
	result, err := collection.InsertOne(ctx, page)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"page_id": result.InsertedID,
		},
	})
}

func (app *Api) validatePageAddPayload(payload models.PageForm) (*models.Page, error) {
	collection := app.Config.MongoDB.Collection("pages")

	var page = &models.Page{
		Title:           payload.Title,
		SubTitle:        payload.SubTitle,
		Content:         payload.Content,
		Excerpt:         payload.Excerpt,
		FeaturedImage:   payload.FeaturedImage,
		AttachmentImage: payload.AttachmentImage,
		Order:           payload.Order,
		Status:          payload.Status,
		Lang:            payload.Lang,
		Template:        payload.Template,
		Badge: models.PageBadge{
			Text:            payload.BadgeText,
			TextColor:       payload.BadgeTextColor,
			BackgroundColor: payload.BadgeBgColor,
			NumBought:       payload.BadgeNumBought,
			Price:           payload.BadgePrice,
			ButtonLabel:     payload.BadgeBtnLabel,
			ButtonLink:      payload.BadgeBtnLink,
		},
		Custom: models.PageCustom{
			Field:       payload.CustomField,
			Link:        payload.CustomLink,
			LabelDetail: payload.CustomLabelDetail,
		},
		Meta: models.PageMeta{
			Title:       payload.MetaTitle,
			Keyword:     payload.MetaKeyword,
			Description: payload.MetaDescription,
		},
		Script: models.PageScript{
			Header: payload.ScriptHeader,
			Body:   payload.ScriptBody,
		},
	}

	regex, _ := regexp.Compile(`[ ]+`)
	page.Title = strings.Trim(regex.ReplaceAllString(page.Title, " "), " ")

	if payload.MetaTitle == "" {
		page.Meta.Title = payload.Title
	}

	formatDate := "2006-01-02"
	if payload.ShowDate != "" {
		sd, _ := time.Parse(formatDate, payload.ShowDate)
		page.ShowDate = &sd
	}
	if payload.EndDate != "" {
		ed, _ := time.Parse(formatDate, payload.EndDate)
		page.EndDate = &ed
	}

	for _, amenities := range payload.Amenities {
		aID, _ := primitive.ObjectIDFromHex(amenities)
		page.Amenities = append(page.Amenities, &aID)
	}

	for _, tag := range payload.Tags {
		tID, _ := primitive.ObjectIDFromHex(tag)
		page.Tags = append(page.Tags, &tID)
	}

	if payload.ParentID != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.ParentID)
		page.ParentID = &pID
	}

	if payload.SourceID != "" {
		sID, _ := primitive.ObjectIDFromHex(payload.SourceID)
		page.SourceID = &sID
	}

	if payload.CategoryID != "" {
		cID, _ := primitive.ObjectIDFromHex(payload.CategoryID)
		page.CategoryID = &cID
	}

	findSlug := bson.M{}
	if payload.BrandID != "" {
		bID, _ := primitive.ObjectIDFromHex(payload.BrandID)
		page.BrandID = &bID
		findSlug["brand_id"] = bID
	}

	if payload.PropertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.PropertyID)
		page.PropertyID = &pID
		findSlug["property_id"] = pID
	}

	ctx := context.Background()
	// generate slug from title
	page.Slug = helpers.Slugify(page.Title)

	findSlug["title"] = primitive.Regex{Pattern: page.Title, Options: "i"}
	count, _ := collection.CountDocuments(ctx, findSlug)

	if count > 0 {
		delete(findSlug, "title")

		findSlug["slug"] = page.Slug
		for i := (count - 1); i < (count + 10); i++ {
			countSlug, _ := collection.CountDocuments(ctx, findSlug)
			if countSlug == 0 {
				page.Slug = findSlug["slug"].(string)
				break
			} else {
				findSlug["slug"] = fmt.Sprintf("%s-%d", page.Slug, i)
			}
		}
	}

	now := time.Now()
	page.CreatedAt = &now

	return page, nil
}

func (app *Api) UpdatePage(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("pages")

	pageID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(pageID)

	page := &models.Page{}
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&page)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	formatDate := "2006-01-02"
	var amenities, tags []string
	var showDate, endDate, categoryID, parentID, sourceID, brandID, propertyID string
	if page.ShowDate != nil {
		showDate = page.ShowDate.Format(formatDate)
	}
	if page.EndDate != nil {
		endDate = page.EndDate.Format(formatDate)
	}
	for _, aID := range page.Amenities {
		amenities = append(amenities, aID.Hex())
	}
	for _, tID := range page.Tags {
		tags = append(tags, tID.Hex())
	}

	if page.ParentID != nil {
		parentID = page.ParentID.Hex()
	}

	if page.SourceID != nil {
		sourceID = page.SourceID.Hex()
	}

	if page.CategoryID != nil {
		categoryID = page.CategoryID.Hex()
	}

	if page.BrandID != nil {
		brandID = page.BrandID.Hex()
	}

	if page.PropertyID != nil {
		propertyID = page.PropertyID.Hex()
	}

	var payload = models.PageForm{
		Title:             page.Title,
		SubTitle:          page.SubTitle,
		Content:           page.Content,
		Excerpt:           page.Excerpt,
		FeaturedImage:     page.FeaturedImage,
		AttachmentImage:   page.AttachmentImage,
		Order:             page.Order,
		Status:            page.Status,
		ShowDate:          showDate,
		EndDate:           endDate,
		Amenities:         amenities,
		Tags:              tags,
		Lang:              page.Lang,
		Template:          page.Template,
		BadgeText:         page.Badge.Text,
		BadgeTextColor:    page.Badge.TextColor,
		BadgeBgColor:      page.Badge.BackgroundColor,
		BadgeNumBought:    page.Badge.NumBought,
		BadgePrice:        page.Badge.Price,
		BadgeBtnLabel:     page.Badge.ButtonLabel,
		BadgeBtnLink:      page.Badge.ButtonLink,
		CustomField:       page.Custom.Field,
		CustomLink:        page.Custom.Link,
		CustomLabelDetail: page.Custom.LabelDetail,
		MetaTitle:         page.Meta.Title,
		MetaKeyword:       page.Meta.Keyword,
		MetaDescription:   page.Meta.Description,
		ScriptHeader:      page.Script.Header,
		ScriptBody:        page.Script.Body,
		ParentID:          parentID,
		SourceID:          sourceID,
		CategoryID:        categoryID,
		BrandID:           brandID,
		PropertyID:        propertyID,
	}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	page, _ = app.validatePageUpdatePayload(pageID, payload, page)
	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": page})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"page":           page,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) UpdatePageBatch(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("pages")

	var batchPayload []models.PageForm
	if err := c.ShouldBind(&batchPayload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	ctx := context.Background()
	var pages []interface{}
	var operations []mongo.WriteModel
	for _, payload := range batchPayload {
		if payload.ID == "" && payload.SourceID != "" {
			page, err := app.validatePageAddPayload(payload)
			if err != nil {
				c.JSON(http.StatusInternalServerError, &models.ReE{
					StatusCode: http.StatusInternalServerError,
					Error:      err.Error(),
				})
				return
			}

			operation := mongo.NewInsertOneModel()
			operation.SetDocument(page)
			operations = append(operations, operation)
			pages = append(pages, page)
		} else {
			pageID, _ := primitive.ObjectIDFromHex(payload.ID)

			var page = &models.Page{}
			err := collection.FindOne(ctx, bson.M{"_id": pageID}).Decode(&page)
			if err != nil {
				c.JSON(http.StatusInternalServerError, &models.ReE{
					StatusCode: http.StatusInternalServerError,
					Error:      err.Error(),
				})
				return
			}

			page, err = app.validatePageUpdatePayload(payload.ID, payload, page)
			if err != nil {
				c.JSON(http.StatusInternalServerError, &models.ReE{
					StatusCode: http.StatusInternalServerError,
					Error:      err.Error(),
				})
				return
			}

			operation := mongo.NewUpdateOneModel()
			operation.SetFilter(bson.M{"_id": pageID})
			operation.SetUpdate(page)
			operations = append(operations, operation)
			pages = append(pages, page)
		}
	}

	results, err := collection.BulkWrite(ctx, operations)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"pages":          pages,
			"inserted_count": results.InsertedCount,
			"matched_count":  results.MatchedCount,
			"modified_count": results.ModifiedCount,
		},
	})
}

func (app *Api) validatePageUpdatePayload(pageID string, payload models.PageForm, page *models.Page) (*models.Page, error) {
	collection := app.Config.MongoDB.Collection("pages")

	ctx := context.Background()
	objectID, _ := primitive.ObjectIDFromHex(pageID)

	regex, _ := regexp.Compile(`[ ]+`)
	payload.Title = strings.Trim(regex.ReplaceAllString(payload.Title, " "), " ")

	findSlug := bson.M{}
	if payload.BrandID != "" {
		bID, _ := primitive.ObjectIDFromHex(payload.BrandID)
		page.BrandID = &bID
		findSlug["brand_id"] = bID
	} else {
		page.BrandID = nil
	}

	if payload.PropertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.PropertyID)
		page.PropertyID = &pID
		findSlug["property_id"] = pID
	} else {
		page.PropertyID = nil
	}

	// change page slug
	if page.Title != payload.Title {
		// generate slug from title
		page.Slug = helpers.Slugify(payload.Title)

		findSlug["_id"] = bson.M{"$ne": objectID}
		findSlug["title"] = primitive.Regex{Pattern: payload.Title, Options: "i"}
		count, _ := collection.CountDocuments(ctx, findSlug)

		if count > 0 {
			delete(findSlug, "title")

			findSlug["slug"] = page.Slug
			for i := (count - 1); i < (count + 10); i++ {
				countSlug, _ := collection.CountDocuments(ctx, findSlug)
				if countSlug == 0 {
					page.Slug = findSlug["slug"].(string)
					break
				} else {
					findSlug["slug"] = fmt.Sprintf("%s-%d", page.Slug, i)
				}
			}
		}
	}

	page.Title = payload.Title
	page.SubTitle = payload.SubTitle
	page.Content = payload.Content
	page.Excerpt = payload.Excerpt
	page.FeaturedImage = payload.FeaturedImage
	page.AttachmentImage = payload.AttachmentImage
	page.Order = payload.Order
	page.Status = payload.Status
	page.Lang = payload.Lang
	page.Template = payload.Template
	page.Badge.Text = payload.BadgeText
	page.Badge.TextColor = payload.BadgeTextColor
	page.Badge.BackgroundColor = payload.BadgeBgColor
	page.Badge.NumBought = payload.BadgeNumBought
	page.Badge.Price = payload.BadgePrice
	page.Badge.ButtonLabel = payload.BadgeBtnLabel
	page.Badge.ButtonLink = payload.BadgeBtnLink
	page.Custom.Field = payload.CustomField
	page.Custom.Link = payload.CustomLink
	page.Custom.LabelDetail = payload.CustomLabelDetail
	page.Meta.Title = payload.MetaTitle
	page.Meta.Keyword = payload.MetaKeyword
	page.Meta.Description = payload.MetaDescription
	page.Script.Header = payload.ScriptHeader
	page.Script.Body = payload.ScriptBody

	if payload.MetaTitle == "" {
		page.Meta.Title = payload.Title
	}

	formatDate := "2006-01-02"
	if payload.ShowDate != "" {
		sd, _ := time.Parse(formatDate, payload.ShowDate)
		page.ShowDate = &sd
	}

	if payload.EndDate != "" {
		ed, _ := time.Parse(formatDate, payload.EndDate)
		page.EndDate = &ed
	}

	page.Amenities = nil
	for _, amenities := range payload.Amenities {
		aID, _ := primitive.ObjectIDFromHex(amenities)
		page.Amenities = append(page.Amenities, &aID)
	}

	page.Tags = nil
	for _, tag := range payload.Tags {
		tID, _ := primitive.ObjectIDFromHex(tag)
		page.Tags = append(page.Tags, &tID)
	}

	if payload.ParentID != "" {
		pID, _ := primitive.ObjectIDFromHex(payload.ParentID)
		page.ParentID = &pID
	} else {
		page.ParentID = nil
	}

	if payload.SourceID != "" {
		sID, _ := primitive.ObjectIDFromHex(payload.SourceID)
		page.SourceID = &sID
	}

	if payload.CategoryID != "" {
		cID, _ := primitive.ObjectIDFromHex(payload.CategoryID)
		page.CategoryID = &cID
	} else {
		page.CategoryID = nil
	}

	now := time.Now()
	page.UpdatedAt = &now

	return page, nil
}

func (app *Api) DeletePage(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("pages")

	pageID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(pageID)

	var page models.Page
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&page)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            pageID,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) DeleteMultipleLangPageByID(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("pages")

	pageID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(pageID)

	ctx := context.Background()
	doc, err := collection.DeleteMany(ctx, bson.M{"$or": bson.A{
		bson.M{"_id": objectID},
		bson.M{"source_id": objectID},
	}})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            pageID,
			"deleted_count": doc.DeletedCount,
		},
	})
}

func (app *Api) PageDuplicator(c *gin.Context) {
	// this is an optional parameter
	var payload map[string]map[string]string
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	/*
	* source[brandId]
	* source[propertyId]
	* source[categoryId]
	* source[status]
	 */
	source := payload["source"]
	/*
	* destination[brandId]
	* destination[propertyId]
	 */
	destination := payload["destination"]

	source["onlyParent"] = "yes"
	duplicate, err := app.nestedPageDuplicator(source, destination, map[string]string{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "Duplicated successfully.",
		Data:       duplicate,
	})
}

func (app *Api) fetchPage(params map[string]string) (bson.M, error) {
	collection := app.Config.MongoDB.Collection("pages")

	host, ok := params["host"]
	if !ok {
		host = ""
	}

	// this is an optional parameter
	pageID, ok := params["id"]
	if !ok {
		pageID = ""
	}
	pageSlug, ok := params["slug"]
	if !ok {
		pageSlug = ""
	}
	parentID, ok := params["parentId"]
	if !ok {
		parentID = ""
	}
	parentSlug, ok := params["parentSlug"]
	if !ok {
		parentSlug = ""
	}
	onlyParent, ok := params["onlyParent"]
	if !ok {
		onlyParent = ""
	} else {
		onlyParent = strings.ToLower(onlyParent)
	}

	excepts := bson.A{}
	exceptIDs, ok := params["exceptId"]
	if !ok {
		excepts = nil
	} else {
		IDs := strings.Split(exceptIDs, ",")
		for _, ID := range IDs {
			ID = strings.TrimSpace(ID)
			eID, err := primitive.ObjectIDFromHex(ID)
			if err == nil {
				excepts = append(excepts, eID)
			}
		}
	}

	lang, ok := params["lang"]
	if !ok {
		lang = ""
	}
	status, ok := params["status"]
	if !ok {
		status = ""
	}
	code, ok := params["code"]
	if !ok {
		code = ""
	} else {
		code = strings.ToUpper(code)
	}
	codeSlug, ok := params["codeSlug"]
	if !ok {
		codeSlug = ""
	} else {
		codeSlug = strings.ToLower(codeSlug)
	}
	cityCode, ok := params["cityCode"]
	if !ok {
		cityCode = ""
	} else {
		cityCode = strings.ToUpper(cityCode)
	}
	categoryID, ok := params["categoryId"]
	if !ok {
		categoryID = ""
	}
	categorySlug, ok := params["categorySlug"]
	if !ok {
		categorySlug = ""
	}

	tags := bson.A{}
	tagIDs, ok := params["tags"]
	if !ok {
		tags = nil
	} else {
		IDs := strings.Split(tagIDs, ",")
		for _, ID := range IDs {
			ID = strings.TrimSpace(ID)
			tID, err := primitive.ObjectIDFromHex(ID)
			if err == nil {
				tags = append(tags, tID)
			}
		}
	}
	today := time.Now().Format("2006-01-02")

	page, ok := params["page"]
	if !ok {
		page = ""
	}
	pageLimit, ok := params["limit"]
	if !ok {
		pageLimit = ""
	}
	sortCol, ok := params["sortCol"]
	if !ok {
		sortCol = "id"
	}
	sortDir, ok := params["sortDir"]
	if !ok {
		sortDir = "asc"
	}

	matchFilter := bson.M{}
	if pageID != "" {
		pID, _ := primitive.ObjectIDFromHex(pageID)
		matchFilter["_id"] = pID
	}
	if pageSlug != "" {
		matchFilter["slug"] = pageSlug
	}
	if parentID != "" {
		pID, _ := primitive.ObjectIDFromHex(parentID)
		matchFilter["parent_id"] = pID
	}
	if parentSlug != "" {
		matchFilter["parent.slug"] = parentSlug
	}
	if onlyParent == "true" || onlyParent == "yes" || onlyParent == "1" {
		matchFilter["parent_id"] = nil
	}
	if len(excepts) > 0 {
		matchFilter["_id"] = bson.M{"$nin": excepts}
	}
	if len(tags) > 0 {
		matchFilter["tags"] = bson.M{"$in": tags}
	}
	if lang != "" {
		matchFilter["lang"] = lang
	}
	if status != "" {
		matchFilter["status"] = status
	}
	if codeSlug != "" {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_slug": codeSlug},
			bson.M{"property.property_slug": codeSlug},
		}
	} else if code != "" {
		matchFilter["$or"] = bson.A{
			bson.M{"brand.brand_code": code},
			bson.M{"property.property_code": code},
		}
	}
	if cityCode != "" {
		matchFilter["city.code"] = cityCode
	}
	if categoryID != "" {
		cID, _ := primitive.ObjectIDFromHex(categoryID)
		matchFilter["category._id"] = cID
	}
	if categorySlug != "" {
		matchFilter["category.slug"] = categorySlug
	}

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}

	facetPipelinePaging := bson.A{bson.M{"$sort": sort}}
	if setPagination {
		facetPipelinePaging = append(facetPipelinePaging, bson.A{
			bson.M{"$skip": offset},
			bson.M{"$limit": limit},
		}...)
	}

	pipeline := bson.A{
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
					bson.M{"$eq": bson.A{"$status", "primary"}},
				}}}},
			},
			"as": "is_primary_lang",
		}},
		bson.M{"$unwind": bson.M{"path": "$is_primary_lang"}},
		bson.M{"$lookup": bson.M{
			"from": "pages",
			"let":  bson.M{"id": "$_id", "source_id": "$source_id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$or": bson.A{
					bson.M{"$eq": bson.A{"$source_id", "$$id"}},
					bson.M{"$eq": bson.A{"$_id", "$$source_id"}},
				}}}},
			},
			"as": "related_pages",
		}},
		bson.M{"$unwind": bson.M{"path": "$related_pages", "preserveNullAndEmptyArrays": true}},
		bson.M{"$group": bson.M{
			"_id":       "$_id",
			"primary":   bson.M{"$first": "$$ROOT"},
			"secondary": bson.M{"$push": "$related_pages"},
		}},
	}

	if pageID == "" && pageSlug == "" {
		pipeline = append(pipeline, bson.M{"$project": bson.M{
			"pages": bson.M{"$filter": bson.M{
				"input": bson.M{"$concatArrays": bson.A{bson.A{"$primary"}, "$secondary"}},
				"as":    "page",
				"cond": bson.M{"$or": bson.A{
					bson.M{"$or": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$page.show_date", ""}}, ""}},
						bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$$page.end_date", ""}}, ""}},
					}},
					bson.M{"$and": bson.A{
						bson.M{"$gte": bson.A{
							bson.M{"$dateFromString": bson.M{
								"dateString": today,
							}}, "$$page.show_date"},
						},
						bson.M{"$lte": bson.A{
							bson.M{"$dateFromString": bson.M{
								"dateString": today,
							}}, "$$page.end_date"},
						},
					}},
				}},
			}},
		}})
	} else {
		pipeline = append(pipeline, bson.M{"$project": bson.M{
			"pages": bson.M{"$concatArrays": bson.A{bson.A{"$primary"}, "$secondary"}},
		}})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$unwind": bson.M{"path": "$pages", "preserveNullAndEmptyArrays": false}},
		bson.M{"$replaceRoot": bson.M{"newRoot": "$pages"}},
		bson.M{"$lookup": bson.M{
			"from": "categories", "as": "category",
			"localField": "category_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$category", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "brands", "as": "brand",
			"localField": "brand_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$brand", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "properties", "as": "property",
			"localField": "property_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$property", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "cities", "as": "city",
			"localField": "property.city_id", "foreignField": "code",
		}},
		bson.M{"$unwind": bson.M{"path": "$city", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "pages", "as": "parent",
			"localField": "parent_id", "foreignField": "_id",
		}},
		bson.M{"$unwind": bson.M{"path": "$parent", "preserveNullAndEmptyArrays": true}},
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"tags": bson.M{"$ifNull": bson.A{"$tags", bson.A{}}}},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$in": bson.A{"$_id", "$$tags"}},
					bson.M{"$eq": bson.A{"$type", "tag"}},
				}}}},
				bson.M{"$project": bson.M{
					"_id": 0,
					"id":  "$_id",
					"items": bson.M{"$filter": bson.M{
						"input": "$items",
						"as":    "item",
						"cond":  bson.M{"$eq": bson.A{"$$item.lang", lang}},
					}},
				}},
				bson.M{"$addFields": bson.M{
					"items": bson.M{"$cond": bson.A{
						bson.M{"$gt": bson.A{bson.M{"$size": bson.M{"$ifNull": bson.A{"$items", bson.A{}}}}, 0}},
						bson.M{"$arrayElemAt": bson.A{"$items", 0}},
						bson.M{},
					}},
				}},
				bson.M{"$project": bson.M{
					"id":    "$id",
					"name":  "$items.name",
					"title": "$items.title",
				}},
			},
			"as": "full_tags",
		}},
	}...)

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$project": bson.M{
			"_id":      0,
			"id":       "$_id",
			"title":    "$title",
			"subtitle": "$subtitle",
			"content":  "$content",
			"excerpt":  "$excerpt",
			"featured_image": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$featured_image", ""}}, ""}},
				"then": bson.M{},
				"else": bson.M{
					"name": "$featured_image",
					"local": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$featured_image", 0, 4}}, "http"}},
						"$featured_image",
						bson.M{
							"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$featured_image"},
						},
					}},
					"cloud": bson.M{"$cond": bson.A{
						bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$featured_image", 0, 4}}, "http"}},
						"$featured_image",
						bson.M{
							"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$featured_image"},
						},
					}},
				},
			}},
			"attachment_image": bson.M{"$cond": bson.M{
				"if":   bson.M{"$eq": bson.A{bson.M{"$ifNull": bson.A{"$attachment_image", ""}}, ""}},
				"then": bson.A{},
				"else": bson.M{
					"$map": bson.M{
						"input": "$attachment_image",
						"as":    "image",
						"in": bson.M{
							"name": "$$image",
							"local": bson.M{"$cond": bson.A{
								bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$$image", 0, 4}}, "http"}},
								"$$image",
								bson.M{
									"$concat": bson.A{host, app.Config.Path.StaticAs, app.Config.Path.MediaAs, "/", "$$image"},
								},
							}},
							"cloud": bson.M{"$cond": bson.A{
								bson.M{"$eq": bson.A{bson.M{"$substr": bson.A{"$$image", 0, 4}}, "http"}},
								"$$image",
								bson.M{
									"$concat": bson.A{app.Config.Path.CloudImagePrefix, "/", "$$image"},
								},
							}},
						},
					},
				},
			}},
			"lang":                "$lang",
			"slug":                "$slug",
			"template":            "$template",
			"order":               "$order",
			"parent_id":           "$parent_id",
			"status":              "$status",
			"show_date":           "$show_date",
			"end_date":            "$end_date",
			"tags":                bson.M{"$ifNull": bson.A{"$tags", bson.A{}}},
			"full_tags":           "$full_tags",
			"amenities":           "$amenities",
			"badge":               bson.M{"$ifNull": bson.A{"$badge", bson.M{}}},
			"custom.field":        "$custom.field",
			"custom.link":         "$custom.link",
			"custom.detail_label": "$custom.label_detail",
			"script.header":       "$script.header",
			"script.body":         "$script.body",
			"meta.title":          "$meta.title",
			"meta.keyword":        "$meta.keyword",
			"meta.description":    "$meta.description",
			"category.id":         "$category._id",
			"category.name":       "$category.name",
			"category.lang":       "$category.lang",
			"category.slug":       "$category.slug",
			"brand.id":            "$brand._id",
			"brand.code":          "$brand.brand_code",
			"brand.name":          "$brand.brand_name",
			"brand.slug":          "$brand.brand_slug",
			"property.id":         "$property._id",
			"property.code":       "$property.property_code",
			"property.name":       "$property.property_name",
			"property.slug":       "$property.property_slug",
			"created_at":          "$created_at",
			"updated_at":          "$updated_at",
		}},
		bson.M{"$facet": bson.M{
			"paginatedResults": facetPipelinePaging,
			"totalCount":       bson.A{bson.M{"$count": "count"}},
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		log.Fatal(err)

		return nil, err
	}
	defer cursor.Close(ctx)

	var results bson.M
	for cursor.Next(ctx) {
		cursor.Decode(&results)
	}

	return results, nil
}

func (app *Api) doDuplicatePage(page *models.Page, dst map[string]string) (*primitive.ObjectID, error) {
	collection := app.Config.MongoDB.Collection("pages")

	formatDate := "2006-01-02"
	var amenities, tags []string
	var showDate, endDate, categoryID string
	if page.ShowDate != nil {
		showDate = page.ShowDate.Format(formatDate)
	}
	if page.EndDate != nil {
		endDate = page.EndDate.Format(formatDate)
	}
	if page.CategoryID != nil {
		categoryID = page.CategoryID.Hex()
	}

	for _, aID := range page.Amenities {
		amenities = append(amenities, aID.Hex())
	}
	for _, tID := range page.Tags {
		tags = append(tags, tID.Hex())
	}

	var payload = models.PageForm{
		Title:             page.Title,
		SubTitle:          page.SubTitle,
		Content:           page.Content,
		Excerpt:           page.Excerpt,
		FeaturedImage:     page.FeaturedImage,
		AttachmentImage:   page.AttachmentImage,
		Order:             page.Order,
		Status:            page.Status,
		ShowDate:          showDate,
		EndDate:           endDate,
		Amenities:         amenities,
		Tags:              tags,
		Lang:              page.Lang,
		Template:          page.Template,
		BadgeText:         page.Badge.Text,
		BadgeTextColor:    page.Badge.TextColor,
		BadgeBgColor:      page.Badge.BackgroundColor,
		BadgeNumBought:    page.Badge.NumBought,
		BadgePrice:        page.Badge.Price,
		BadgeBtnLabel:     page.Badge.ButtonLabel,
		BadgeBtnLink:      page.Badge.ButtonLink,
		CustomField:       page.Custom.Field,
		CustomLink:        page.Custom.Link,
		CustomLabelDetail: page.Custom.LabelDetail,
		MetaTitle:         page.Meta.Title,
		MetaKeyword:       page.Meta.Keyword,
		MetaDescription:   page.Meta.Description,
		ScriptHeader:      page.Script.Header,
		ScriptBody:        page.Script.Body,
		CategoryID:        categoryID,
		ParentID:          dst["parentId"],
		SourceID:          dst["sourceId"],
		BrandID:           dst["brandId"],
		PropertyID:        dst["propertyId"],
	}

	ctx := context.Background()
	page, _ = app.validatePageAddPayload(payload)
	result, err := collection.InsertOne(ctx, page)
	if err != nil {
		return nil, err
	}

	insertID := result.InsertedID.(primitive.ObjectID)

	return &insertID, nil
}

func (app *Api) fetchNestedPageBundle(params map[string]string) ([]models.PageBundle, error) {
	collection := app.Config.MongoDB.Collection("pages")

	// this is an optional parameter
	status, ok := params["status"]
	if !ok {
		status = ""
	}
	parentID, ok := params["parentId"]
	if !ok {
		parentID = ""
	}
	categoryID, ok := params["categoryId"]
	if !ok {
		categoryID = ""
	}
	brandID, ok := params["brandId"]
	if !ok {
		brandID = ""
	}
	propertyID, ok := params["propertyId"]
	if !ok {
		propertyID = ""
	}
	onlyParent, ok := params["onlyParent"]
	if !ok {
		onlyParent = ""
	} else {
		onlyParent = strings.ToLower(onlyParent)
	}

	matchFilter := bson.M{}
	if status != "" {
		matchFilter["status"] = status
	}
	if parentID != "" {
		pID, _ := primitive.ObjectIDFromHex(parentID)
		matchFilter["parent_id"] = pID
	}
	if categoryID != "" {
		cID, _ := primitive.ObjectIDFromHex(categoryID)
		matchFilter["category_id"] = cID
	}
	if brandID != "" {
		bID, _ := primitive.ObjectIDFromHex(brandID)
		matchFilter["brand_id"] = bID
	}
	if propertyID != "" {
		pID, _ := primitive.ObjectIDFromHex(propertyID)
		matchFilter["property_id"] = pID
	}
	if onlyParent == "true" || onlyParent == "yes" || onlyParent == "1" {
		matchFilter["parent_id"] = nil
	}

	pipeline := bson.A{}

	if len(matchFilter) > 0 {
		pipeline = append(pipeline, bson.M{"$match": matchFilter})
	}

	pipeline = append(pipeline, bson.A{
		bson.M{"$lookup": bson.M{
			"from": "complements",
			"let":  bson.M{"lang": "$lang"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{
					bson.M{"$eq": bson.A{"$code", "$$lang"}},
					bson.M{"$eq": bson.A{"$type", "lang"}},
					bson.M{"$eq": bson.A{"$status", "primary"}},
				}}}},
			},
			"as": "is_primary_lang",
		}},
		bson.M{"$unwind": bson.M{"path": "$is_primary_lang"}},
		bson.M{"$lookup": bson.M{
			"from": "pages",
			"let":  bson.M{"id": "$_id", "source_id": "$source_id"},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$or": bson.A{
					bson.M{"$eq": bson.A{"$source_id", "$$id"}},
					bson.M{"$eq": bson.A{"$_id", "$$source_id"}},
				}}}},
			},
			"as": "related_pages",
		}},
		bson.M{"$unwind": bson.M{"path": "$related_pages", "preserveNullAndEmptyArrays": true}},
		bson.M{"$group": bson.M{
			"_id":       "$_id",
			"primary":   bson.M{"$first": "$$ROOT"},
			"secondary": bson.M{"$push": "$related_pages"},
		}},
	}...)

	ctx := context.Background()
	opts := options.Aggregate()
	cursor, err := collection.Aggregate(ctx, pipeline, opts)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	var results = []models.PageBundle{}
	cursor.All(ctx, &results)

	return results, nil
}

/*
* src[parentId]
* src[categoryId]
* src[brandId]
* src[propertyId]
* src[status]
*
* dst[brandId]
* dst[propertyId]
* dst[sourceId]
* dst[parentId]
 */
func (app *Api) nestedPageDuplicator(src map[string]string, dst map[string]string, mapParent map[string]string) (map[string]int, error) {
	results := map[string]int{"duplicated": 0, "failed": 0, "total": 0}

	pages, err := app.fetchNestedPageBundle(src)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	for _, page := range pages {
		results["total"]++

		// id as string
		IDs := page.Primary.ID.Hex()
		// key is old id, value is new id
		mapParentID := map[string]string{}

		// duplicate primary page
		if page.Primary.ParentID != nil {
			dst["parentId"] = mapParent[page.Primary.ParentID.Hex()]
		} else {
			delete(dst, "parentId")
		}
		newID, err := app.doDuplicatePage(&page.Primary, dst)
		sourceID := newID.Hex()

		if err != nil {
			results["failed"]++
		} else {
			results["duplicated"]++
			mapParentID[IDs] = newID.Hex()

			// duplicate secondary page
			for _, page := range page.Secondary {
				// id as string
				IDs := page.ID.Hex()

				dst["sourceId"] = sourceID
				if page.ParentID != nil {
					dst["parentId"] = mapParent[page.ParentID.Hex()]
				} else {
					delete(dst, "parentId")
				}

				newID, _ := app.doDuplicatePage(&page, dst)
				if err != nil {
					results["failed"]++
				} else {
					results["duplicated"]++
				}

				mapParentID[IDs] = newID.Hex()
				results["total"]++
			}

			src := map[string]string{
				"parentId": IDs,
			}

			delete(dst, "sourceId")
			child, _ := app.nestedPageDuplicator(src, dst, mapParentID)

			results["duplicated"] += child["duplicated"]
			results["failed"] += child["failed"]
			results["total"] += child["total"]
		}
	}

	return results, nil
}

func (app *Api) GetPageList(c *gin.Context) {
	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	// this is an optional parameter
	lang := c.DefaultQuery("lang", "en")
	code := c.Query("code")
	codeSlug := c.Query("codeSlug")
	pageID := c.Query("id")
	pageSlug := c.Query("slug")
	categoryID := c.Query("categoryId")
	categorySlug := c.Query("categorySlug")
	parentID := c.Query("parentId")
	parentSlug := c.Query("parentSlug")
	onlyParent := c.Query("onlyParent")
	exceptID := c.Query("exceptId")
	tags := c.Query("tags")
	cityCode := c.Query("cityCode")
	status := c.DefaultQuery("status", "publish")

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "created_at")
	sortDir := c.DefaultQuery("sortDir", "desc")

	params := map[string]string{
		"host":         host,
		"lang":         lang,
		"code":         code,
		"codeSlug":     codeSlug,
		"id":           pageID,
		"slug":         pageSlug,
		"categoryId":   categoryID,
		"categorySlug": categorySlug,
		"parentId":     parentID,
		"parentSlug":   parentSlug,
		"onlyParent":   onlyParent,
		"exceptId":     exceptID,
		"tags":         tags,
		"cityCode":     cityCode,
		"status":       status,
		"page":         page,
		"limit":        pageLimit,
		"sortCol":      sortCol,
		"sortDir":      sortDir,
	}

	results, err := app.fetchPage(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	facetData := results["paginatedResults"].(primitive.A)
	facetCount := results["totalCount"].(primitive.A)

	var total int32
	if len(facetCount) > 0 {
		total = facetCount[0].(bson.M)["count"].(int32)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"items": facetData,
			"total": total,
		},
	})
}

func (app *Api) GetPageByID(c *gin.Context) {
	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	// this is an optional parameter
	pageID := c.Param("id")

	params := map[string]string{
		"host": host,
		"id":   pageID,
	}

	pages, err := app.fetchPage(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	facetData := pages["paginatedResults"].(primitive.A)

	results := make(bson.M)
	if len(facetData) > 0 {
		results = facetData[0].(bson.M)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}

func (app *Api) GetPageBySlug(c *gin.Context) {
	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	// this is an optional parameter
	slug := c.Param("slug")
	code := c.DefaultQuery("code", "cor")
	codeSlug := c.Query("codeSlug")

	params := map[string]string{
		"host":     host,
		"slug":     slug,
		"code":     code,
		"codeSlug": codeSlug,
	}

	pages, err := app.fetchPage(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	facetData := pages["paginatedResults"].(primitive.A)

	results := make(bson.M)
	if len(facetData) > 0 {
		results = facetData[0].(bson.M)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       results,
	})
}

func (app *Api) GetPageByCategoryID(c *gin.Context) {
	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	// this is an optional parameter
	ID := c.Param("id")
	exceptID := c.Query("exceptId")
	tags := c.Query("tags")
	onlyParent := c.Query("onlyParent")
	code := c.DefaultQuery("code", "cor")
	codeSlug := c.Query("codeSlug")
	status := c.DefaultQuery("status", "publish")

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "created_at")
	sortDir := c.DefaultQuery("sortDir", "desc")

	params := map[string]string{
		"host":       host,
		"categoryId": ID,
		"code":       code,
		"codeSlug":   codeSlug,
		"exceptId":   exceptID,
		"tags":       tags,
		"onlyParent": onlyParent,
		"status":     status,
		"page":       page,
		"limit":      pageLimit,
		"sortCol":    sortCol,
		"sortDir":    sortDir,
	}

	pages, err := app.fetchPage(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	pageData := pages["paginatedResults"].(primitive.A)
	pageCount := pages["totalCount"].(primitive.A)

	var pageTotal int32
	if len(pageCount) > 0 {
		pageTotal = pageCount[0].(bson.M)["count"].(int32)
	}

	paramsCategory := map[string]string{
		"host":     host,
		"id":       ID,
		"code":     code,
		"codeSlug": codeSlug,
	}

	categories, err := app.fetchCategory(paramsCategory)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	categoryData := categories["paginatedResults"].(primitive.A)

	pageTitle := make(bson.M)
	if len(categoryData) > 0 {
		pageTitle = categoryData[0].(bson.M)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"page":  pageTitle,
			"items": pageData,
			"total": pageTotal,
		},
	})
}

func (app *Api) GetPageByCategorySlug(c *gin.Context) {
	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	// this is an optional parameter
	slug := c.Param("slug")
	exceptID := c.Query("exceptId")
	tags := c.Query("tags")
	onlyParent := c.Query("onlyParent")
	code := c.DefaultQuery("code", "cor")
	codeSlug := c.Query("codeSlug")
	status := c.DefaultQuery("status", "publish")

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "created_at")
	sortDir := c.DefaultQuery("sortDir", "desc")

	paramsPage := map[string]string{
		"host":         host,
		"categorySlug": slug,
		"code":         code,
		"codeSlug":     codeSlug,
		"exceptId":     exceptID,
		"tags":         tags,
		"onlyParent":   onlyParent,
		"status":       status,
		"page":         page,
		"limit":        pageLimit,
		"sortCol":      sortCol,
		"sortDir":      sortDir,
	}

	pages, err := app.fetchPage(paramsPage)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	pageData := pages["paginatedResults"].(primitive.A)
	pageCount := pages["totalCount"].(primitive.A)

	var pageTotal int32
	if len(pageCount) > 0 {
		pageTotal = pageCount[0].(bson.M)["count"].(int32)
	}

	paramsCategory := map[string]string{
		"host":     host,
		"slug":     slug,
		"code":     code,
		"codeSlug": codeSlug,
	}

	categories, err := app.fetchCategory(paramsCategory)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	categoryData := categories["paginatedResults"].(primitive.A)

	pageTitle := make(bson.M)
	if len(categoryData) > 0 {
		pageTitle = categoryData[0].(bson.M)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"page":  pageTitle,
			"items": pageData,
			"total": pageTotal,
		},
	})
}

func (app *Api) GetPageByParentID(c *gin.Context) {
	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	// this is an optional parameter
	ID := c.Param("id")
	exceptID := c.Query("exceptId")
	tags := c.Query("tags")
	code := c.DefaultQuery("code", "cor")
	codeSlug := c.Query("codeSlug")
	status := c.DefaultQuery("status", "publish")

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "created_at")
	sortDir := c.DefaultQuery("sortDir", "desc")

	params := map[string]string{
		"host":     host,
		"parentId": ID,
		"code":     code,
		"codeSlug": codeSlug,
		"exceptId": exceptID,
		"tags":     tags,
		"status":   status,
		"page":     page,
		"limit":    pageLimit,
		"sortCol":  sortCol,
		"sortDir":  sortDir,
	}

	pages, err := app.fetchPage(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	pageData := pages["paginatedResults"].(primitive.A)
	pageCount := pages["totalCount"].(primitive.A)

	var pageTotal int32
	if len(pageCount) > 0 {
		pageTotal = pageCount[0].(bson.M)["count"].(int32)
	}

	paramsParent := map[string]string{
		"host": host,
		"id":   ID,
	}

	parent, err := app.fetchPage(paramsParent)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	parentData := parent["paginatedResults"].(primitive.A)

	pageTitle := make(bson.M)
	if len(parentData) > 0 {
		pageTitle = parentData[0].(bson.M)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"page":  pageTitle,
			"items": pageData,
			"total": pageTotal,
		},
	})
}

func (app *Api) GetPageByParentSlug(c *gin.Context) {
	url := location.Get(c)
	host := fmt.Sprintf("%s://%s", url.Scheme, c.Request.Host)

	// this is an optional parameter
	slug := c.Param("slug")
	exceptID := c.Query("exceptId")
	tags := c.Query("tags")
	code := c.DefaultQuery("code", "cor")
	codeSlug := c.Query("codeSlug")
	status := c.DefaultQuery("status", "publish")

	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "created_at")
	sortDir := c.DefaultQuery("sortDir", "desc")

	params := map[string]string{
		"host":       host,
		"parentSlug": slug,
		"code":       code,
		"codeSlug":   codeSlug,
		"exceptId":   exceptID,
		"tags":       tags,
		"status":     status,
		"page":       page,
		"limit":      pageLimit,
		"sortCol":    sortCol,
		"sortDir":    sortDir,
	}

	pages, err := app.fetchPage(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	pageData := pages["paginatedResults"].(primitive.A)
	pageCount := pages["totalCount"].(primitive.A)

	var pageTotal int32
	if len(pageCount) > 0 {
		pageTotal = pageCount[0].(bson.M)["count"].(int32)
	}

	paramsParent := map[string]string{
		"host":     host,
		"slug":     slug,
		"code":     code,
		"codeSlug": codeSlug,
	}

	parent, err := app.fetchPage(paramsParent)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	parentData := parent["paginatedResults"].(primitive.A)

	pageTitle := make(bson.M)
	if len(parentData) > 0 {
		pageTitle = parentData[0].(bson.M)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"page":  pageTitle,
			"items": pageData,
			"total": pageTotal,
		},
	})
}
