package v1

import (
	"alaric_cms/app/models"
	"context"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (app *Api) GetWidget(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	search := c.Query("search")
	widgetID := c.Query("id")
	widgetType := c.DefaultQuery("widget", "form")
	page := c.Query("page")
	pageLimit := c.Query("limit")
	sortCol := c.DefaultQuery("sortCol", "_id")
	sortDir := c.DefaultQuery("sortDir", "-1")
	if sortDir == "asc" {
		sortDir = "1"
	} else {
		sortDir = "-1"
	}
	sortOrder, _ := strconv.Atoi(sortDir)

	var setPagination bool
	var offset, limit int64
	if page != "" || pageLimit != "" {
		if page == "" {
			page = "1"
		}
		if pageLimit == "" {
			pageLimit = "10"
		}

		start, err := strconv.ParseInt(page, 10, 64)
		if err != nil || start < 1 {
			start = 1
		}

		limit, err = strconv.ParseInt(pageLimit, 10, 64)
		if err != nil || limit < 1 {
			limit = 10
		}

		offset = (start - 1) * limit
		setPagination = true
	}

	// Pass these options to the Find method
	findOptions := options.Find()
	if setPagination {
		findOptions.SetLimit(limit)
		findOptions.SetSkip(offset)
	}

	// Sort by `-1` field descending
	sort := bson.M{}
	sortArray := strings.Split(sortCol, " ")
	for _, val := range sortArray {
		sort[val] = sortOrder
	}
	findOptions.Sort = sort

	findFilter := bson.M{"type": "widget"}
	if widgetID != "" {
		objectID, _ := primitive.ObjectIDFromHex(widgetID)
		findFilter["_id"] = objectID
	}
	if widgetType != "" {
		findFilter["widget"] = widgetType
	}

	if search != "" {
		searchRe := primitive.Regex{Pattern: search, Options: "i"}
		findFilter["$or"] = bson.A{
			bson.M{"name": searchRe},
			bson.M{"type": searchRe},
			bson.M{"created_at": searchRe},
			bson.M{"updated_at": searchRe},
		}
	}

	ctx := context.Background()
	cursor, err := collection.Find(
		ctx,
		findFilter,
		findOptions,
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var widgets = []models.Widget{}
	cursor.All(ctx, &widgets)
	count, _ := collection.CountDocuments(ctx, findFilter)

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data: gin.H{
			"item":  widgets,
			"total": count,
		},
	})
}

func (app *Api) GetWidgetByID(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	widgetID := c.Param("id")

	findFilter := bson.M{"type": "widget"}
	if widgetID != "" {
		objectID, _ := primitive.ObjectIDFromHex(widgetID)
		findFilter["_id"] = objectID
	}

	ctx := context.Background()
	cursor, err := collection.Find(
		ctx,
		findFilter,
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	var widget = models.Widget{}
	for cursor.Next(ctx) {
		cursor.Decode(&widget)
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Data:       widget,
	})
}

func (app *Api) AddWidget(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	var payload models.WidgetForm
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	ctx := context.Background()
	cursor, err := collection.Find(ctx, bson.M{
		"name":   payload.Name,
		"type":   "widget",
		"widget": payload.Widget,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Widget name already exists.",
		})
		return
	}

	var widget = models.Widget{
		Name:    payload.Name,
		Type:    "widget",
		Code:    payload.Code,
		Widget:  payload.Widget,
		Content: payload.Content,
	}

	now := time.Now()
	widget.CreatedAt = &now

	result, err := collection.InsertOne(ctx, widget)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "inserted successfully.",
		Data: gin.H{
			"id": result.InsertedID,
		},
	})
}

func (app *Api) UpdateWidget(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	widgetID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(widgetID)

	var widget models.Widget
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&widget)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	var payload = models.WidgetForm{
		Name:    widget.Name,
		Code:    widget.Code,
		Widget:  widget.Widget,
		Content: widget.Content,
	}

	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusBadRequest,
			Error:      err.Error(),
		})
		return
	}

	cursor, err := collection.Find(ctx, bson.M{
		"_id":    bson.M{"$ne": objectID},
		"name":   payload.Name,
		"type":   "widget",
		"widget": payload.Widget,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}
	defer cursor.Close(ctx)

	if cursor.Next(ctx) {
		c.JSON(http.StatusBadRequest, &models.ReE{
			StatusCode: http.StatusConflict,
			Error:      "Widget name already exists.",
		})
		return
	}

	widget.Name = payload.Name
	widget.Code = payload.Code
	widget.Widget = payload.Widget
	widget.Content = payload.Content

	now := time.Now()
	widget.UpdatedAt = &now

	doc, err := collection.UpdateOne(ctx, bson.M{"_id": objectID}, bson.M{"$set": widget})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "updated successfully.",
		Data: gin.H{
			"widget":         widget,
			"matched_count":  doc.MatchedCount,
			"modified_count": doc.ModifiedCount,
		},
	})
}

func (app *Api) DeleteWidget(c *gin.Context) {
	collection := app.Config.MongoDB.Collection("complements")

	widgetID := c.Param("id")
	objectID, _ := primitive.ObjectIDFromHex(widgetID)

	var widget models.Widget
	ctx := context.Background()
	err := collection.FindOne(ctx, bson.M{"_id": objectID, "type": "widget"}).Decode(&widget)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	doc, err := collection.DeleteOne(ctx, bson.M{"_id": objectID, "type": "widget"})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.ReE{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.ReS{
		Success:    true,
		StatusCode: http.StatusOK,
		Message:    "deleted successfully.",
		Data: gin.H{
			"id":            widgetID,
			"deleted_count": doc.DeletedCount,
		},
	})
}
