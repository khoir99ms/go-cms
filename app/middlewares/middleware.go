package middlewares

import (
	"context"
	"net/http"

	"alaric_cms/app/models"
	conf "alaric_cms/config"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/sessions"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func RequiredAuth(config *conf.Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		auth, err := request.ParseFromRequest(c.Request, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
			b := ([]byte(config.Jwt.SecretKey))
			return b, nil
		})

		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, &models.ReE{
				StatusCode: http.StatusUnauthorized,
				Error:      err.Error(),
			})
			return
		}

		userId := auth.Claims.(jwt.MapClaims)["jti"].(string)
		objectID, _ := primitive.ObjectIDFromHex(userId)

		var doc models.User
		err = config.MongoDB.Collection("users").FindOne(context.Background(), bson.M{"_id": objectID, "user_status": "active"}).Decode(&doc)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, &models.ReE{
				StatusCode: http.StatusBadRequest,
				Error:      "User not found.",
			})
			return
		}

		c.Set("UserId", doc.UserId.Hex())
		c.Set("UserLogin", doc.UserLogin)
		c.Set("UserEmail", doc.UserEmail)
		c.Set("UserRole", doc.UserRole)
		c.Set("UserStatus", doc.UserStatus)
		if doc.BrandId != nil {
			c.Set("BrandId", doc.BrandId.Hex())
		}
		if doc.PropertyId != nil {
			c.Set("PropertyId", doc.PropertyId.Hex())
		}

		c.Next()
	}
}

func RequiredPermission(roles ...int) gin.HandlerFunc {
	return func(c *gin.Context) {
		role, ok := c.Get("UserRole")

		if !ok {
			c.AbortWithStatusJSON(http.StatusUnauthorized, &models.ReE{
				StatusCode: http.StatusUnauthorized,
				Error:      "Required permission level.",
			})
			return
		}

		granted := false
		for _, r := range roles {
			if granted = r == role; granted {
				break
			}
		}

		if !granted {
			c.AbortWithStatusJSON(http.StatusUnauthorized, &models.ReE{
				StatusCode: http.StatusUnauthorized,
				Error:      "Unauthorized.",
			})
			return
		}
		c.Next()
	}
}

func OnlySameUserCanDoThis(exceptRoles ...int) gin.HandlerFunc {
	return func(c *gin.Context) {
		userId := c.Param("userId")
		if userId == "" {
			userId = c.Query("userId")
		}

		role, ok := c.Get("UserRole")
		if !ok {
			c.AbortWithStatusJSON(http.StatusUnauthorized, &models.ReE{
				StatusCode: http.StatusUnauthorized,
				Error:      "Required permission level.",
			})
			return
		}

		granted := false
		for _, r := range exceptRoles {
			if granted = r == role; granted {
				break
			}
		}

		activeUserId, _ := c.Get("UserId")
		if activeUserId.(string) != userId && !granted {
			c.AbortWithStatusJSON(http.StatusUnauthorized, &models.ReE{
				StatusCode: http.StatusUnauthorized,
				Error:      "Unauthorized.",
			})
			return
		}

		c.Next()
	}
}

func RequiredResetPasswordKey(config *conf.Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		auth, err := request.ParseFromRequest(c.Request, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
			b := ([]byte(config.Jwt.SecretResetPasswordKey))
			return b, nil
		})

		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, &models.ReE{
				StatusCode: http.StatusUnauthorized,
				Error:      err.Error(),
			})
			return
		}

		userId := auth.Claims.(jwt.MapClaims)["jti"].(string)
		objectID, _ := primitive.ObjectIDFromHex(userId)

		var doc models.User
		err = config.MongoDB.Collection("users").FindOne(context.Background(), bson.M{"_id": objectID, "user_status": "active"}).Decode(&doc)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, &models.ReE{
				StatusCode: http.StatusBadRequest,
				Error:      "User not found.",
			})
			return
		}

		c.Set("ResetID", doc.UserId.Hex())

		c.Next()
	}
}

func RequiredSession(s *sessions.CookieStore, config *conf.Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		session, err := s.Get(c.Request, config.Session.Name)
		if err != nil {
			c.JSON(http.StatusInternalServerError, &models.ReE{
				StatusCode: http.StatusInternalServerError,
				Error:      err.Error(),
			})
			return
		}

		userSession := session.Values["user"]
		userInfo := models.UserSession{}
		userInfo, ok := userSession.(models.UserSession)

		if !ok || !userInfo.Authenticated {
			c.Redirect(http.StatusTemporaryRedirect, "/auth/")
			c.Abort()
			return
		}

		c.Next()
	}
}
