package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Binding from Json or Form
type City struct {
	Id            *primitive.ObjectID `form:"id" json:"id" bson:"_id,omitempty"`
	Code          string              `form:"code" json:"code" bson:"code" binding:"required"`
	Name          string              `form:"name" json:"name" bson:"name" binding:"required"`
	FeaturedImage string              `form:"featured_image" json:"featured_image" bson:"featured_image"`
	Description   interface{}         `form:"description" json:"description" bson:"description"`
	Order         int                 `form:"order" json:"order" bson:"order"`
	Registered    *time.Time          `form:"registered" json:"registered" bson:"registered"`
	Modified      *time.Time          `form:"modified" json:"modified" bson:"modified"`
}
