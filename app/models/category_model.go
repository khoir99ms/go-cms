package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Category binding from Json
type Category struct {
	ID          *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name        string              `json:"name" bson:"name" binding:"required"`
	Slug        string              `json:"slug" bson:"slug"`
	Title       string              `json:"title" bson:"title"`
	Description string              `json:"description" bson:"description"`
	Image       string              `json:"image" bson:"image"`
	Lang        string              `json:"lang" bson:"lang" binding:"required"`
	Label       CategoryLabel       `json:"label" bson:"label"`
	Page        CategoryPage        `json:"page" bson:"page"`
	Meta        CategoryMeta        `json:"meta,omitempty" bson:"meta"`
	Extends     []CategoryExtend    `json:"extends,omitempty" bson:"extends"`
	SourceID    *primitive.ObjectID `json:"source_id" bson:"source_id"`
	CreatedAt   *time.Time          `form:"created_at" json:"created_at" bson:"created_at"`
	UpdatedAt   *time.Time          `form:"updated_at" json:"updated_at" bson:"updated_at"`
}

// CategoryExtend binding from Json or Form
type CategoryExtend struct {
	PropertyID  *primitive.ObjectID `json:"property_id" bson:"property_id,omitempty"`
	BrandID     *primitive.ObjectID `json:"brand_id" bson:"brand_id,omitempty"`
	Title       string              `json:"title" bson:"title"`
	Description string              `json:"description" bson:"description"`
	Image       string              `json:"image" bson:"image"`
	Label       CategoryLabel       `json:"label" bson:"label"`
	Meta        CategoryMeta        `json:"meta" bson:"meta"`
}

// CategoryPage binding from Json or Form
type CategoryPage struct {
	UseValidity  bool `json:"use_validity" bson:"use_validity"`
	UseAmenities bool `json:"use_amenities" bson:"use_amenities"`
	UseBadge     bool `json:"use_badge" bson:"use_badge"`
}

// CategoryLabel binding from Json or Form
type CategoryLabel struct {
	Detail string `json:"detail" bson:"detail"`
	More   string `json:"more" bson:"more"`
}

// CategoryMeta binding from Json or Form
type CategoryMeta struct {
	Title       string `form:"title" json:"title" bson:"title"`
	Keyword     string `form:"keyword" json:"keyword" bson:"keyword"`
	Description string `form:"description" json:"description" bson:"description"`
}

// CategoryForm binding from Json or Form
type CategoryForm struct {
	ID              string `form:"id,omitempty" json:"id,omitempty"`
	Name            string `form:"name" json:"name" binding:"required"`
	Slug            string `form:"slug" json:"slug" `
	Title           string `form:"title" json:"title"`
	Description     string `form:"description" json:"description"`
	Image           string `form:"image" json:"image"`
	Lang            string `form:"lang" json:"lang" binding:"required"`
	UseValidity     string `form:"use_validity" json:"use_validity"`
	UseAmenities    string `form:"use_amenities" json:"use_amenities"`
	UseBadge        string `form:"use_badge" json:"use_badge"`
	LabelDetail     string `form:"label_detail" json:"label_detail"`
	LabelMore       string `form:"label_more" json:"label_more"`
	MetaTitle       string `form:"meta_title" json:"meta_title"`
	MetaKeyword     string `form:"meta_keyword" json:"meta_keyword"`
	MetaDescription string `form:"meta_description" json:"meta_description"`
	SourceID        string `form:"source_id" json:"source_id"`
}

// CategoryForm binding from Json or Form
type CategoryExtendForm struct {
	ID              string `form:"id,omitempty" json:"id,omitempty"`
	Title           string `form:"title" json:"title"`
	Description     string `form:"description" json:"description"`
	Image           string `form:"image" json:"image"`
	LabelDetail     string `form:"label_detail" json:"label_detail"`
	LabelMore       string `form:"label_more" json:"label_more"`
	MetaTitle       string `form:"meta_title" json:"meta_title"`
	MetaKeyword     string `form:"meta_keyword" json:"meta_keyword"`
	MetaDescription string `form:"meta_description" json:"meta_description"`
	PropertyID      string `form:"property_id" json:"property_id"`
	BrandID         string `form:"brand_id" json:"brand_id"`
}
