package models

// response success
type ReS struct {
	Success    bool        `json:"success"`
	StatusCode int         `json:"status_code,omitempty"`
	Message    string      `json:"message,omitempty"`
	Data       interface{} `json:"data,omitempty"`
}

// response error
type ReE struct {
	Success    bool        `json:"success"`
	StatusCode int         `json:"status_code,omitempty"`
	Error      interface{} `json:"error,omitempty"`
}
