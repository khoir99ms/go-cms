package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Binding from Json or Form
type Brand struct {
	BrandId              *primitive.ObjectID `form:"brand_id" json:"brand_id" bson:"_id,omitempty"`
	BrandCode            string              `form:"brand_code" json:"brand_code" bson:"brand_code" binding:"required"`
	BrandName            string              `form:"brand_name" json:"brand_name" bson:"brand_name" binding:"required"`
	BrandDisplayName     string              `form:"brand_display_name" json:"brand_display_name" bson:"brand_display_name"`
	BrandTagline         interface{}         `form:"brand_tagline" json:"brand_tagline" bson:"brand_tagline"`
	BrandDescription     interface{}         `form:"brand_description" json:"brand_description" bson:"brand_description"`
	BrandContent         interface{}         `form:"brand_content" json:"brand_content" bson:"brand_content"`
	BrandLogo            string              `form:"brand_logo" json:"brand_logo" bson:"brand_logo"`
	BrandSecondaryLogo   string              `form:"brand_secondary_logo" json:"brand_secondary_logo" bson:"brand_secondary_logo"`
	BrandFavicon         string              `form:"brand_favicon" json:"brand_favicon" bson:"brand_favicon"`
	BrandSlug            string              `form:"brand_slug" json:"brand_slug" bson:"brand_slug"`
	BrandFeaturedImage   string              `form:"brand_featured_image" json:"brand_featured_image" bson:"brand_featured_image"`
	BrandImageAttachment []string            `form:"brand_image_attachment" json:"brand_image_attachment" bson:"brand_image_attachment"`
	BrandOrder           int                 `form:"brand_order" json:"brand_order" bson:"brand_order"`
	BrandRegistered      *time.Time          `form:"brand_registered" json:"brand_registered" bson:"brand_registered"`
	BrandModified        *time.Time          `form:"brand_modified" json:"brand_modified" bson:"brand_modified"`
}
