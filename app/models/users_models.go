package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Binding from Json or Form
type User struct {
	UserId         *primitive.ObjectID `form:"user_id" json:"user_id" bson:"_id,omitempty"`
	UserLogin      string              `form:"user_login" json:"user_login" bson:"user_login" binding:"required"`
	UserPassword   string              `form:"user_password" json:"user_password" bson:"user_password" binding:"required"`
	UserFirstname  string              `form:"user_firstname" json:"user_firstname" bson:"user_firstname" binding:"required"`
	UserLastname   string              `form:"user_lastname" json:"user_lastname" bson:"user_lastname"`
	UserEmail      string              `form:"user_email" json:"user_email" bson:"user_email" binding:"required"`
	UserRole       int                 `form:"user_role" json:"user_role" bson:"user_role" binding:"required"`
	UserStatus     string              `form:"user_status" json:"user_status" bson:"user_status"`
	BrandId        *primitive.ObjectID `form:"brand_id" json:"brand_id,omitempty" bson:"brand_id,omitempty"`
	PropertyId     *primitive.ObjectID `form:"property_id" json:"property_id,omitempty" bson:"property_id,omitempty"`
	UserRegistered *time.Time          `form:"user_registered" json:"user_registered" bson:"user_registered"`
	UserModified   *time.Time          `form:"user_modified" json:"user_modified" bson:"user_modified"`
}

type ViewUser struct {
	UserId         *primitive.ObjectID `json:"user_id" bson:"_id"`
	UserLogin      string              `json:"user_login" bson:"user_login"`
	UserFirstname  string              `json:"user_firstname" bson:"user_firstname"`
	UserLastname   string              `json:"user_lastname" bson:"user_lastname"`
	UserEmail      string              `json:"user_email" bson:"user_email"`
	UserRole       int                 `json:"user_role" bson:"user_role"`
	UserStatus     string              `json:"user_status" bson:"user_status"`
	BrandId        *primitive.ObjectID `form:"brand_id" json:"brand_id" bson:"brand_id"`
	PropertyId     *primitive.ObjectID `form:"property_id" json:"property_id" bson:"property_id"`
	UserRegistered *time.Time          `json:"user_registered" bson:"user_registered"`
	UserModified   *time.Time          `json:"user_modified" bson:"user_modified"`
}

// Binding from Json or Form
type UserAuth struct {
	UserLogin    string `form:"user_login" json:"user_login"  binding:"required"`
	UserPassword string `form:"user_password" json:"user_password" binding:"required"`
}

// Binding from Json or Form
type UserForgotPassword struct {
	UserEmail string `form:"user_email" json:"user_email"  binding:"required"`
}

// Binding from Json or Form
type UserResetPassword struct {
	UserPassword       string `form:"password" json:"password" binding:"required"`
	UserRetypePassword string `form:"retype_password" json:"retype_password" binding:"required"`
}

// Binding from Json or Form
type UserUpdate struct {
	UserLogin     string              `form:"user_login" json:"user_login" bson:"user_login"`
	UserFirstname string              `form:"user_firstname" json:"user_firstname" bson:"user_firstname"`
	UserLastname  string              `form:"user_lastname" json:"user_lastname" bson:"user_lastname"`
	UserEmail     string              `form:"user_email" json:"user_email" bson:"user_email"`
	UserRole      int                 `form:"user_role" json:"user_role" bson:"user_role"`
	UserStatus    string              `form:"user_status" json:"user_status" bson:"user_status"`
	BrandId       *primitive.ObjectID `form:"brand_id" json:"brand_id,omitempty" bson:"brand_id,omitempty"`
	PropertyId    *primitive.ObjectID `form:"property_id" json:"property_id,omitempty" bson:"property_id,omitempty"`
	UserModified  *time.Time          `form:"user_modified" json:"user_modified" bson:"user_modified"`
}

// Binding from Json or Form
type UserChangePassword struct {
	UserPassword        string `form:"password" json:"password" binding:"required"`
	UserRetypePassword  string `form:"retype_password" json:"retype_password" binding:"required"`
	UserCurrentPassword string `form:"current_password" json:"current_password" binding:"required"`
}

// UserSession holds a users account information
type UserSession struct {
	UserId            string
	UserLogin         string
	UserFirstname     string
	UserLastname      string
	UserEmail         string
	UserRole          int
	UserStatus        string
	BrandId           string
	PropertyId        string
	PropertyCode      string
	PageId            string
	PageCode          string
	PageName          string
	PageLevel         string
	PageNameUserLogin string
	AccessToken       string
	Authenticated     bool
}
