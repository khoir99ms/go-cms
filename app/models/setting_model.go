package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Setting struct {
	Id          *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	General     SettingGeneral      `json:"general" bson:"general"`
	Media       SettingMedia        `json:"media" bson:"media"`
	Meta        SettingMeta         `json:"meta" bson:"meta"`
	Script      SettingScript       `json:"script" bson:"script"`
	SocialMedia SettingSocialMedia  `json:"social_media" bson:"social_media"`
	CreatedAt   *time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt   *time.Time          `json:"updated_at" bson:"updated_at"`
}

type SettingGeneral struct {
	SiteTitle     string `form:"site_title" json:"site_title" bson:"site_title"`
	SiteCopyright string `form:"site_copyright" json:"site_copyright" bson:"site_copyright"`
	SiteLang      string `form:"site_lang" json:"site_lang" bson:"site_lang"`
	PostsPerPage  string `form:"posts_per_page" json:"posts_per_page" bson:"posts_per_page"`
}

type SettingMedia struct {
	ThumbnailSize string `form:"thumbnail_size" json:"thumbnail_size" bson:"thumbnail_size"`
	MediumSize    string `form:"medium_size" json:"medium_size" bson:"medium_size"`
	LargeSize     string `form:"large_size" json:"large_size" bson:"large_size"`
}

type SettingSocialMedia struct {
	Email       string `form:"email" json:"email" bson:"email"`
	Facebook    string `form:"facebook" json:"facebook" bson:"facebook"`
	Twitter     string `form:"twitter" json:"twitter" bson:"twitter"`
	Instagram   string `form:"instagram" json:"instagram" bson:"instagram"`
	Youtube     string `form:"youtube" json:"youtube" bson:"youtube"`
	TripAdvisor string `form:"tripadvisor" json:"tripadvisor" bson:"tripadvisor"`
}

type SettingScript struct {
	Header          string `form:"header" json:"header" bson:"header"`
	Body            string `form:"body" json:"body" bson:"body"`
	BookingCalendar string `form:"booking_calendar" json:"booking_calendar" bson:"booking_calendar"`
	Themes          string `form:"themes" json:"themes" bson:"themes"`
}

type SettingMeta struct {
	Title       map[string]string `form:"title" json:"title" bson:"title"`
	Description map[string]string `form:"description" json:"description" bson:"description"`
	Keyword     map[string]string `form:"keyword" json:"keyword" bson:"keyword"`
}
