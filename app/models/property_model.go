package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Binding from Json or Form
type Property struct {
	PropertyID                   *primitive.ObjectID   `form:"property_id" json:"property_id" bson:"_id,omitempty"`
	BrandID                      *primitive.ObjectID   `form:"brand_id" json:"brand_id" bson:"brand_id,omitempty"`
	PropertyCode                 string                `form:"property_code" json:"property_code" bson:"property_code" binding:"required"`
	PropertyBookingUrl           string                `form:"property_booking_url" json:"property_booking_url" bson:"property_booking_url"`
	PropertyCorporateBookingCode string                `form:"property_corporate_booking_code" json:"property_corporate_booking_code" bson:"property_corporate_booking_code"`
	PropertyBrandBookingCode     string                `form:"property_brand_booking_code" json:"property_brand_booking_code" bson:"property_brand_booking_code"`
	PropertyHotelBookingCode     string                `form:"property_hotel_booking_code" json:"property_hotel_booking_code" bson:"property_hotel_booking_code"`
	PropertyName                 string                `form:"property_name" json:"property_name" bson:"property_name" binding:"required"`
	PropertyDisplayName          string                `form:"property_display_name" json:"property_display_name" bson:"property_display_name"`
	PropertyPhone                string                `form:"property_phone" json:"property_phone" bson:"property_phone"`
	PropertyFax                  string                `form:"property_fax" json:"property_fax" bson:"property_fax"`
	PropertyEmail                string                `form:"property_email" json:"property_email" bson:"property_email"`
	PropertyAddress              string                `form:"property_address" json:"property_address" bson:"property_address"`
	PropertyCity                 string                `form:"city_id" json:"city_id" bson:"city_id"`
	PropertySlug                 string                `form:"property_slug" json:"property_slug" bson:"property_slug"`
	PropertyLogo                 string                `form:"property_logo" json:"property_logo" bson:"property_logo"`
	PropertySecondaryLogo        string                `form:"property_secondary_logo" json:"property_secondary_logo" bson:"property_secondary_logo"`
	PropertyFavicon              string                `form:"property_favicon" json:"property_favicon" bson:"property_favicon"`
	PropertyFeaturedImage        string                `form:"property_featured_image" json:"property_featured_image" bson:"property_featured_image"`
	PropertyImageAttachment      []string              `form:"property_image_attachment" json:"property_image_attachment" bson:"property_image_attachment"`
	PropertyTagline              interface{}           `form:"property_tagline" json:"property_tagline" bson:"property_tagline"`
	PropertyDescription          interface{}           `form:"property_description" json:"property_description" bson:"property_description"`
	PropertyCustomField          interface{}           `form:"property_custom_field" json:"property_custom_field" bson:"property_custom_field"`
	PropertyCustomLink           interface{}           `form:"property_custom_link" json:"property_custom_link" bson:"property_custom_link"`
	PropertyLatitude             string                `form:"property_latitude" json:"property_latitude" bson:"property_latitude"`
	PropertyLongitude            string                `form:"property_longitude" json:"property_longitude" bson:"property_longitude"`
	PropertyOrder                int                   `form:"property_order" json:"property_order" bson:"property_order"`
	PropertyStatus               string                `form:"property_status" json:"property_status" bson:"property_status"`
	Facilities                   []string              `form:"facilities,omitempty" json:"facilities,omitempty" bson:"facilities,omitempty"`
	PropertyFacilities           []*primitive.ObjectID `form:"property_facilities" json:"property_facilities" bson:"property_facilities"`
	PropertyRegistered           *time.Time            `form:"property_registered" json:"property_registered" bson:"property_registered"`
	PropertyModified             *time.Time            `form:"property_modified" json:"property_modified" bson:"property_modified"`
}
