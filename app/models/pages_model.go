package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Page struct {
	ID              *primitive.ObjectID   `json:"id" bson:"_id,omitempty"`
	Title           string                `json:"title" bson:"title" binding:"required"`
	SubTitle        string                `json:"subtitle" bson:"subtitle"`
	Content         string                `json:"content" bson:"content"`
	Excerpt         string                `json:"excerpt" bson:"excerpt"`
	FeaturedImage   string                `json:"featured_image" bson:"featured_image"`
	AttachmentImage []string              `json:"attachment_image" bson:"attachment_image"`
	Order           int                   `json:"order" bson:"order"`
	Status          string                `json:"status" bson:"status"`
	ShowDate        *time.Time            `json:"show_date" bson:"show_date"`
	EndDate         *time.Time            `json:"end_date" bson:"end_date"`
	Amenities       []*primitive.ObjectID `json:"amenities" bson:"amenities"`
	Tags            []*primitive.ObjectID `json:"tags" bson:"tags"`
	Lang            string                `json:"lang" bson:"lang"`
	Slug            string                `json:"slug" bson:"slug"`
	Template        string                `json:"template" bson:"template"`
	ParentID        *primitive.ObjectID   `json:"parent_id" bson:"parent_id"`
	Badge           PageBadge             `json:"badge" bson:"badge"`
	Custom          PageCustom            `json:"custom" bson:"custom"`
	Meta            PageMeta              `json:"meta" bson:"meta"`
	Script          PageScript            `json:"script" bson:"script"`
	SourceID        *primitive.ObjectID   `json:"source_id" bson:"source_id"`
	CategoryID      *primitive.ObjectID   `json:"category_id" bson:"category_id"`
	BrandID         *primitive.ObjectID   `json:"brand_id" bson:"brand_id"`
	PropertyID      *primitive.ObjectID   `json:"property_id" bson:"property_id"`
	CreatedAt       *time.Time            `json:"created_at" bson:"created_at"`
	UpdatedAt       *time.Time            `json:"updated_at" bson:"updated_at"`
}

// PageBundle use for duplicate page
type PageBundle struct {
	ID        *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Primary   Page                `json:"primary" bson:"primary"`
	Secondary []Page              `json:"secondary" bson:"secondary"`
}

type PageBadge struct {
	Text            string `json:"text" bson:"text"`
	TextColor       string `json:"text_color" bson:"text_color"`
	BackgroundColor string `json:"background_color" bson:"background_color"`
	NumBought       string `json:"num_bought" bson:"num_bought"`
	Price           string `json:"price" bson:"price"`
	ButtonLabel     string `json:"button_label" bson:"button_label"`
	ButtonLink      string `json:"button_link" bson:"button_link"`
}

type PageCustom struct {
	Field       string `json:"field" bson:"field"`
	Link        string `json:"link" bson:"link"`
	LabelDetail string `json:"label_detail" bson:"label_detail"`
}

type PageMeta struct {
	Title       string `json:"title" bson:"title"`
	Keyword     string `json:"keyword" bson:"keyword"`
	Description string `json:"description" bson:"description"`
}

type PageScript struct {
	Header string `json:"header" bson:"header"`
	Body   string `json:"body" bson:"body"`
}

type PageForm struct {
	ID                string   `form:"id,omitempty" json:"id,omitempty"`
	Title             string   `form:"title" json:"title"`
	SubTitle          string   `form:"subtitle" json:"subtitle"`
	Content           string   `form:"content" json:"content"`
	Excerpt           string   `form:"excerpt" json:"excerpt"`
	FeaturedImage     string   `form:"featured_image" json:"featured_image"`
	AttachmentImage   []string `form:"attachment_image" json:"attachment_image"`
	Order             int      `form:"order" json:"order"`
	Status            string   `form:"status" json:"status"`
	ShowDate          string   `form:"show_date" json:"show_date"`
	EndDate           string   `form:"end_date" json:"end_date"`
	Amenities         []string `form:"amenities" json:"amenities"`
	Tags              []string `form:"tags" json:"tags"`
	Lang              string   `form:"lang" json:"lang"`
	Template          string   `form:"template" json:"template"`
	ParentID          string   `form:"parent_id" json:"parent_id"`
	BadgeText         string   `json:"badge_text" bson:"text"`
	BadgeTextColor    string   `json:"badge_text_color" bson:"badge_text_color"`
	BadgeBgColor      string   `json:"badge_bg_color" bson:"badge_bg_color"`
	BadgeNumBought    string   `json:"badge_num_bought" bson:"badge_num_bought"`
	BadgePrice        string   `json:"badge_price" bson:"badge_price"`
	BadgeBtnLabel     string   `json:"badge_btn_label" bson:"badge_btn_label"`
	BadgeBtnLink      string   `json:"badge_btn_link" bson:"badge_btn_link"`
	CustomField       string   `form:"custom_field" json:"custom_field"`
	CustomLink        string   `form:"custom_link" json:"custom_link"`
	CustomLabelDetail string   `form:"custom_label_detail" json:"custom_label_detail"`
	MetaTitle         string   `form:"meta_title" json:"meta_title"`
	MetaKeyword       string   `form:"meta_keyword" json:"meta_keyword"`
	MetaDescription   string   `form:"meta_description" json:"meta_description"`
	ScriptHeader      string   `form:"script_header" json:"script_header"`
	ScriptBody        string   `form:"script_body" json:"script_body"`
	SourceID          string   `form:"source_id" json:"source_id"`
	CategoryID        string   `form:"category_id" json:"category_id"`
	BrandID           string   `form:"brand_id" json:"brand_id"`
	PropertyID        string   `form:"property_id" json:"property_id"`
}
