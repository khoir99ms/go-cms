package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Binding from Json or Form
type Corporate struct {
	CorporateId         *primitive.ObjectID `form:"corporate_id" json:"corporate_id" bson:"_id,omitempty"`
	CorporateName       string              `form:"corporate_name" json:"corporate_name" bson:"corporate_name"  binding:"required"`
	CorporateAddress    string              `form:"corporate_address" json:"corporate_address" bson:"corporate_address"`
	CorporateSlug       string              `form:"corporate_slug" json:"corporate_slug" bson:"corporate_slug"  binding:"required"`
	CorporateLogo       string              `form:"corporate_logo" json:"corporate_logo" bson:"corporate_logo"`
	CorporateStatus     string              `form:"corporate_status" json:"corporate_status" bson:"corporate_status"`
	CorporateRegistered *time.Time          `form:"corporate_registered" json:"corporate_registered" bson:"corporate_registered"`
	CorporateModified   *time.Time          `form:"corporate_modified" json:"corporate_modified" bson:"corporate_modified"`
}
