package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Collection scheme
type Media struct {
	Id          *primitive.ObjectID `form:"id" json:"id" bson:"_id,omitempty"`
	Title       string              `form:"title" json:"title" bson:"title"`
	Caption     string              `form:"caption" json:"caption" bson:"caption"`
	Description string              `form:"description" json:"description" bson:"description"`
	Type        string              `form:"type" json:"type" bson:"type"`
	Url         string              `form:"url" json:"url" bson:"url"`
	File        string              `form:"file" json:"file" bson:"file"`
	AsGallery   bool                `form:"as_gallery" json:"as_gallery" bson:"as_gallery"`
	BrandId     *primitive.ObjectID `form:"brand_id" json:"brand_id,omitempty" bson:"brand_id,omitempty"`
	PropertyId  *primitive.ObjectID `form:"property_id" json:"property_id,omitempty" bson:"property_id,omitempty"`
	Registered  *time.Time          `form:"registered" json:"registered" bson:"registered"`
	Modified    *time.Time          `form:"modified" json:"modified" bson:"modified"`
}

// Binding from Json or Form Add
type MediaFormAdd struct {
	Rows        int      `form:"rows" json:"rows"`
	Title       []string `form:"title" json:"title"`
	Caption     []string `form:"caption" json:"caption"`
	Description []string `form:"description" json:"description"`
	Type        []string `form:"type" json:"type" bson:"type"`
	Url         []string `form:"url" json:"url" bson:"url"`
	AsGallery   []string `form:"as_gallery" json:"as_gallery" bson:"as_gallery"`
	BrandId     []string `form:"brand_id" json:"brand_id,omitempty"`
	PropertyId  []string `form:"property_id" json:"property_id,omitempty"`
}

// Binding from Json or Form Update
type MediaForm struct {
	Title       string `form:"title" json:"title"`
	Caption     string `form:"caption" json:"caption"`
	Description string `form:"description" json:"description"`
	Type        string `form:"type" json:"type" bson:"type"`
	Url         string `form:"url" json:"url" bson:"url"`
	AsGallery   string `form:"as_gallery" json:"as_gallery" bson:"as_gallery"`
	BrandId     string `form:"brand_id" json:"brand_id,omitempty"`
	PropertyId  string `form:"property_id" json:"property_id,omitempty"`
}
