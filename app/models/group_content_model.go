package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Binding from Json or Form
type GroupContent struct {
	ID         *primitive.ObjectID   `json:"id" bson:"_id,omitempty"`
	Name       string                `json:"name" bson:"name" binding:"required"`
	Image      string                `json:"image" bson:"image"`
	Setting    []GroupContentSetting `json:"setting" bson:"setting"`
	Items      []GroupContentItem    `json:"items" bson:"items"`
	BrandID    *primitive.ObjectID   `json:"brand_id" bson:"brand_id"`
	PropertyID *primitive.ObjectID   `json:"property_id" bson:"property_id"`
	CreatedAt  *time.Time            `json:"created_at" bson:"created_at"`
	UpdatedAt  *time.Time            `json:"updated_at" bson:"updated_at"`
}

type GroupContentSetting struct {
	Title           string `json:"title" bson:"title"`
	Description     string `json:"description" bson:"description"`
	LabelDetail     string `json:"label_detail" bson:"label_detail"`
	LabelMore       string `json:"label_more" bson:"label_more"`
	MetaTitle       string `json:"meta_title" bson:"meta_title"`
	MetaKeyword     string `json:"meta_keyword" bson:"meta_keyword"`
	MetaDescription string `json:"meta_description" bson:"meta_description"`
	Lang            string `json:"lang" bson:"lang"`
}

type GroupContentItem struct {
	RefID    *primitive.ObjectID   `json:"ref_id" bson:"ref_id"`
	Title    interface{}           `json:"title" bson:"title"`
	Image    string                `json:"image" bson:"image"`
	Tags     []*primitive.ObjectID `json:"tags" bson:"tags"`
	Type     string                `json:"type" bson:"type"`
	Lang     string                `json:"lang" bson:"lang"`
	Template string                `json:"template" bson:"template"`
}

type GroupContentForm struct {
	Name       string             `form:"name" json:"name"`
	Image      string             `form:"image" json:"image"`
	Items      []GroupContentItem `form:"items" json:"items"`
	BrandID    string             `form:"brand_id" json:"brand_id"`
	PropertyID string             `form:"property_id" json:"property_id"`
}

type GroupContentSettingForm struct {
	Title           string `form:"title" json:"title"`
	Description     string `form:"description" json:"description"`
	LabelDetail     string `form:"label_detail" json:"label_detail"`
	LabelMore       string `form:"label_more" json:"label_more"`
	MetaTitle       string `form:"meta_title" json:"meta_title"`
	MetaKeyword     string `form:"meta_keyword" json:"meta_keyword"`
	MetaDescription string `form:"meta_description" json:"meta_description"`
	Lang            string `form:"lang" json:"lang"`
}
