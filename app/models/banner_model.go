package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Banner struct {
	ID         *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name       string              `json:"name" bson:"name" binding:"required"`
	Items      []BannerItem        `json:"items" bson:"items" binding:"required"`
	ApplyTo    []BannerApplyTo     `json:"apply_to" bson:"apply_to"`
	BrandID    *primitive.ObjectID `json:"brand_id" bson:"brand_id"`
	PropertyID *primitive.ObjectID `json:"property_id" bson:"property_id"`
	CreatedAt  *time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt  *time.Time          `json:"updated_at" bson:"updated_at"`
}

type BannerApplyTo struct {
	ID   *primitive.ObjectID `json:"id" bson:"id"`
	Type string              `json:"type" bson:"type"`
}

type BannerItem struct {
	Title       string `json:"title" bson:"title"`
	Description string `json:"description" bson:"description"`
	Image       string `json:"image" bson:"image"`
	Order       int    `json:"order" bson:"order"`
	Status      string `json:"status" bson:"status"`
	Lang        string `json:"lang" bson:"lang"`
}

type FormBanner struct {
	Name        string   `form:"name" json:"name" binding:"required"`
	Title       []string `form:"title" json:"title"`
	Description []string `form:"description" json:"description"`
	Image       []string `form:"image" json:"image"`
	Order       []int    `form:"order" json:"order"`
	Status      []string `form:"status" json:"status"`
	Lang        []string `form:"lang" json:"lang"`
	BrandID     string   `form:"brand_id" json:"brand_id"`
	PropertyID  string   `form:"property_id" json:"property_id"`
}
