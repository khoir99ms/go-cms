package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Menu struct {
	ID         *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name       string              `json:"name" bson:"name"`
	Lang       string              `json:"lang" bson:"lang"`
	Location   string              `json:"location" bson:"location"`
	Structure  []MenuStructure     `json:"structure" bson:"structure"`
	BrandID    *primitive.ObjectID `json:"brand_id" bson:"brand_id"`
	PropertyID *primitive.ObjectID `json:"property_id" bson:"property_id"`
	CreatedAt  *time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt  *time.Time          `json:"updated_at" bson:"updated_at"`
}

type MenuStructure struct {
	RefId    *primitive.ObjectID `json:"ref_id" bson:"ref_id"`
	Type     string              `json:"type" bson:"type"`
	Url      string              `json:"url" bson:"url"`
	Label    string              `json:"label" bson:"label"`
	Image    string              `json:"image" bson:"image"`
	NewTab   bool                `json:"new_tab" bson:"new_tab"`
	Children []MenuStructure     `json:"children" bson:"children"`
}

type MenuForm struct {
	Name       string          `form:"name" json:"name"`
	Lang       string          `form:"lang" json:"lang"`
	Location   string          `form:"location" json:"location"`
	Structure  []MenuStructure `form:"structure" json:"structure"`
	BrandID    string          `form:"brand_id" json:"brand_id"`
	PropertyID string          `form:"property_id" json:"property_id"`
}
