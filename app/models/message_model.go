package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Message struct {
	ID             *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name           string              `json:"name" bson:"name" binding:"required"`
	Type           string              `json:"type" bson:"type" binding:"required"`
	Headline       map[string]string   `json:"headline" bson:"headline"`
	Description    map[string]string   `json:"description" bson:"description"`
	Image          string              `json:"image" bson:"image"`
	ButtonLink     string              `json:"button_link" bson:"button_link"`
	ButtonLabel    map[string]string   `json:"button_label" bson:"button_label"`
	ButtonColor    string              `json:"button_color" bson:"button_color"`
	ButtonNewTab   bool                `json:"button_new_tab" bson:"button_new_tab"`
	ApplyIDs       []MessageApplyIDs   `json:"apply_ids" bson:"apply_ids"`
	ExceptApplyIDs []MessageApplyIDs   `json:"except_apply_ids" bson:"except_apply_ids"`
	Setting        MessageSetting      `json:"setting" bson:"setting"`
	BrandID        *primitive.ObjectID `json:"brand_id" bson:"brand_id"`
	PropertyID     *primitive.ObjectID `json:"property_id" bson:"property_id"`
	CreatedAt      *time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt      *time.Time          `json:"updated_at" bson:"updated_at"`
}

type MessageApplyIDs struct {
	ID   *primitive.ObjectID `json:"id" bson:"id"`
	Type string              `json:"type" bson:"type"`
}

type MessageSetting struct {
	Type         string     `json:"type" bson:"type"`
	Position     string     `json:"position" bson:"position"`
	Background   string     `json:"background" bson:"background"`
	ImageMessage bool       `json:"image_message" bson:"image_message"`
	ShowDate     *time.Time `json:"show_date" bson:"show_date"`
	EndDate      *time.Time `json:"end_date" bson:"end_date"`
	CountryCode  string     `json:"country_code" bson:"country_code"`
}

type FormMessage struct {
	Name         string            `form:"name" json:"name" binding:"required"`
	Type         string            `form:"type" json:"type" binding:"required"`
	Headline     map[string]string `form:"headline" json:"headline"`
	Description  map[string]string `form:"description" json:"description"`
	Image        string            `form:"image" json:"image"`
	ButtonLink   string            `form:"button_link" json:"button_link"`
	ButtonLabel  map[string]string `form:"button_label" json:"button_label"`
	ButtonColor  string            `form:"button_color" json:"button_color"`
	ButtonNewTab string            `form:"button_new_tab" json:"button_new_tab"`
	MessageType  string            `form:"message_type" json:"message_type"`
	Position     string            `form:"position" json:"position"`
	Background   string            `form:"background" json:"background"`
	ImageMessage string            `form:"image_message" json:"image_message"`
	ShowDate     string            `form:"show_date" json:"show_date"`
	EndDate      string            `form:"end_date" json:"end_date"`
	CountryCode  string            `form:"country_code" json:"country_code"`
	BrandID      string            `form:"brand_id" json:"brand_id"`
	PropertyID   string            `form:"property_id" json:"property_id"`
}
