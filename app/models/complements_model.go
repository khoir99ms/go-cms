package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Facility Binding from Json
// Type value "facility" OR "amenities"
type Facility struct {
	ID        *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Type      string              `json:"type" bson:"type" binding"required"`
	Icon      string              `json:"icon" bson:"icon"`
	Items     []FacilityItem      `json:"items" bson:"items" binding:"required"`
	ParentID  *primitive.ObjectID `json:"parent_id" bson:"parent_id"`
	CreatedAt *time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt *time.Time          `json:"updated_at" bson:"updated_at"`
}

// FacilityNested tree facility view
type FacilityNested struct {
	ID          *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Type        string              `json:"type" bson:"type" binding"required"`
	Icon        string              `json:"icon" bson:"icon"`
	Name        string              `json:"items.name" bson:"items.name" binding:"required"`
	Title       string              `json:"items.title" bson:"items.title"`
	Description string              `json:"items.description" bson:"items.description"`
	Lang        string              `json:"items.lang" bson:"items.lang" binding:"required"`
	Children    []FacilityNested    `json:"children" bson:"children" binding:"required"`
	ParentID    *primitive.ObjectID `json:"parent_id" bson:"parent_id"`
	CreatedAt   *time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt   *time.Time          `json:"updated_at" bson:"updated_at"`
}

// FacilityItem binding from Json
type FacilityItem struct {
	Name        string `json:"name" bson:"name" binding:"required"`
	Title       string `json:"title" bson:"title"`
	Description string `json:"description" bson:"description"`
	Lang        string `json:"lang" bson:"lang" binding:"required"`
}

// FacilityForm binding from Json or Form
type FacilityForm struct {
	Icon        []string `form:"icon" json:"icon" binding:"required"`
	Type        []string `form:"type" json:"type" binding:"required"`
	Name        []string `form:"name" json:"name" binding:"required"`
	Title       []string `form:"title" json:"title"`
	Description []string `form:"description" json:"description"`
	Lang        []string `form:"lang" json:"lang" binding:"required"`
	ParentID    []string `form:"parent_id" json:"parent_id" binding:"required"`
}

// HotelFacility Binding from Json
// Type value "facility"
type HotelFacility struct {
	ID          *primitive.ObjectID `form:"id" json:"id" bson:"_id,omitempty"`
	Type        string              `form:"type" json:"type" bson:"type" binding"required"`
	Name        string              `json:"name" bson:"name" binding:"required"`
	Icon        string              `json:"icon" bson:"icon"`
	Description string              `json:"description" bson:"description"`
	ParentID    *primitive.ObjectID `json:"parent_id" bson:"parent_id"`
	CreatedAt   *time.Time          `form:"created_at" json:"created_at" bson:"created_at"`
	UpdatedAt   *time.Time          `form:"updated_at" json:"updated_at" bson:"updated_at"`
}

// HotelFacilityForm binding from Json or Form
type HotelFacilityForm struct {
	Name        []string `form:"name" json:"name" binding:"required"`
	Icon        []string `form:"icon" json:"icon"`
	Description []string `form:"description" json:"description"`
	ParentID    []string `form:"parent_id" json:"parent_id"`
}

// RoomAmenities Binding from Json
// Type value "amenities"
type RoomAmenities struct {
	ID          *primitive.ObjectID `form:"id" json:"id" bson:"_id,omitempty"`
	Type        string              `form:"type" json:"type" bson:"type" binding"required"`
	Name        string              `json:"name" bson:"name" binding:"required"`
	Icon        string              `json:"icon" bson:"icon"`
	Description string              `json:"description" bson:"description"`
	ParentID    *primitive.ObjectID `json:"parent_id" bson:"parent_id"`
	CreatedAt   *time.Time          `form:"created_at" json:"created_at" bson:"created_at"`
	UpdatedAt   *time.Time          `form:"updated_at" json:"updated_at" bson:"updated_at"`
}

// RoomAmenitiesForm binding from Json or Form
type RoomAmenitiesForm struct {
	Name        []string `form:"name" json:"name" binding:"required"`
	Icon        []string `form:"icon" json:"icon"`
	Description []string `form:"description" json:"description"`
	ParentID    []string `form:"parent_id" json:"parent_id"`
}

// Language Binding from Json
// Type value "lang"
type Language struct {
	ID        *primitive.ObjectID `form:"id" json:"id" bson:"_id,omitempty"`
	Type      string              `form:"type" json:"type,omitempty" bson:"type" binding"required"`
	Code      string              `json:"code" bson:"code" binding:"required"`
	Name      string              `json:"name" bson:"name" binding:"required"`
	Native    string              `json:"native" bson:"native"`
	Status    string              `json:"status" bson:"status"`
	CreatedAt *time.Time          `form:"created_at" json:"created_at,omitempty" bson:"created_at"`
	UpdatedAt *time.Time          `form:"updated_at" json:"updated_at,omitempty" bson:"updated_at"`
}

// Tag binding from Json
// Type value "tag"
type Tag struct {
	ID        *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Type      string              `json:"type,omitempty" bson:"type" binding"required"`
	Items     []TagItem           `json:"items" bson:"items" binding:"required"`
	CreatedAt *time.Time          `form:"created_at" json:"created_at" bson:"created_at"`
	UpdatedAt *time.Time          `form:"updated_at" json:"updated_at" bson:"updated_at"`
}

// TagItem binding from Json or Form
type TagItem struct {
	Name  string `json:"name" bson:"name" binding:"required"`
	Title string `json:"title" bson:"title"`
	Lang  string `json:"lang" bson:"lang" binding:"required"`
}

// TagForm binding from Json or Form
type TagForm struct {
	Name  string `form:"name" json:"name" binding:"required"`
	Title string `form:"title" json:"title"`
	Lang  string `form:"lang" json:"lang" binding:"required"`
}

// Widget binding from Json
// Type value "widget"
// Widget value "form,etc...."
type Widget struct {
	ID        *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Type      string              `json:"type,omitempty" bson:"type" binding"required"`
	Name      string              `json:"name" bson:"name" binding"required"`
	Code      string              `json:"code" bson:"code"`
	Widget    string              `json:"widget" bson:"widget" binding"required"`
	Content   string              `json:"content" bson:"content" binding"required"`
	CreatedAt *time.Time          `form:"created_at" json:"created_at" bson:"created_at"`
	UpdatedAt *time.Time          `form:"updated_at" json:"updated_at" bson:"updated_at"`
}

// WidgetForm binding from Json or Form
type WidgetForm struct {
	Name    string `form:"name" json:"name" binding:"required"`
	Code    string `form:"code" json:"code"`
	Widget  string `form:"widget" json:"widget" binding:"required"`
	Content string `form:"content" json:"content" binding:"required"`
}

// Team binding from Json
// Type value "team"
type Team struct {
	ID          *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Type        string              `json:"type,omitempty" bson:"type" binding"required"`
	Name        string              `json:"name" bson:"name" binding:"required"`
	Position    string              `json:"position" bson:"position" binding:"required"`
	Description string              `json:"description" bson:"description"`
	Order       int                 `json:"order" bson:"order"`
	Picture     string              `json:"picture" bson:"picture"`
	Email       string              `json:"email" bson:"email"`
	Facebook    string              `json:"facebook" bson:"facebook"`
	Twitter     string              `json:"twitter" bson:"twitter"`
	Instagram   string              `json:"instagram" bson:"instagram"`
	LinkedIn    string              `json:"linkedin" bson:"linkedin"`
	BrandID     *primitive.ObjectID `json:"brand_id" bson:"brand_id"`
	PropertyID  *primitive.ObjectID `json:"property_id" bson:"property_id"`
	CreatedAt   *time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt   *time.Time          `json:"updated_at" bson:"updated_at"`
}

// TeamForm binding from Json
type TeamForm struct {
	Name        string `form:"name" json:"name" binding:"required"`
	Position    string `form:"position" json:"position" binding:"required"`
	Description string `form:"description" json:"description"`
	Order       int    `form:"order" json:"order"`
	Picture     string `form:"picture" json:"picture"`
	Email       string `form:"email" json:"email"`
	Facebook    string `form:"facebook" json:"facebook"`
	Twitter     string `form:"twitter" json:"twitter"`
	Instagram   string `form:"instagram" json:"instagram"`
	LinkedIn    string `form:"linkedin" json:"linkedin"`
	BrandID     string `form:"brand_id" json:"brand_id"`
	PropertyID  string `form:"property_id" json:"property_id"`
}
